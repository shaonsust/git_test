<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('welcome_model','wm');
    }

    public function index()
    {
        $data = array();


        $data['main_content'] = $this->load->view('driver/dashboard', '', true);
        $this->load->view('driver/master', $data);
    }

    public function my_account()
    {
        $user_id = $this->session->userdata('user_id');


        $data = array();

        $data['main_content'] = $this->load->view('driver/my_account', '', true);
        $this->load->view('driver/master', $data);
    }

    public function change_name()
    {
        $user_id = $this->session->userdata('user_id');
        $data['user_name'] = $this->input->post('user_name', true);
        $data['updated_at'] = date('Y-m-d H:i:s');


        $k = $this->wm->update_user($user_id, $data);


        $sdata = array();
        $sdata['message'] = 'Successfully name Change Thank you !';

        $this->session->set_userdata($sdata);

        redirect('welcome/my_account');
    }

    public function change_email()
    {
        $user_id = $this->session->userdata('user_id');
        $data['user_email'] = $this->input->post('user_email', true);
        $data['updated_at'] = date('Y-m-d H:i:s');
        $this->wm->update_user($user_id, $data);

        /* echo $user_id;

          echo '<pre>';
          print_r($k);
          exit();*/

        $sdata = array();
        $sdata['message'] = 'Successfully email Change Thank you !';

        $this->session->set_userdata($sdata);

        redirect('welcome/my_account');

    }

    public function change_password()
    {
        $user_id = $this->session->userdata('user_id');
        $data['user_password'] = md5($this->input->post('user_password', true));

        $data['updated_at'] = date('Y-m-d H:i:s');
        /*echo '<pre>';
        print_r($data);
        exit();*/

        $this->wm->update_user($user_id, $data);


        $sdata = array();
        $sdata['message'] = 'Successfully password Change Thank you !';

        $this->session->set_userdata($sdata);

        redirect('welcome/my_account');
    }


    public function router_list()
    {
        $data = array();
        $data['all_route'] = $this->wm->get_all_route();

        $data['main_content'] = $this->load->view('driver/routes_list', $data, true);
        $this->load->view('driver/master', $data);
    }

    public function add_route($s, $d)
    {
        /* $k=$this->uri->segment(3);
         echo $k;
         exit();*/

        if ($s == 1) {
            $data = array();


            $data['s'] = $s;
            $data['main_content'] = $this->load->view('driver/add_route', $data, true);
            $this->load->view('driver/master', $data);
        }
        if ($s == 2) {
            $data = array();


            $data['s'] = $s;
            $data['show_route_by_id'] = $this->wm->get_show_route_by_id($d);


            /*  echo '<pre>';
              print_r( $data['show_route_by_id']);
              exit();*/

            $data['main_content'] = $this->load->view('driver/add_route', $data, true);
            $this->load->view('driver/master', $data);
        }


    }

    public function delete_routes($route_id)
    {
        $this->wm->delete_route($route_id);

        $sdata = array();
        $sdata['message'] = 'Successfully Delete Route !';

        $this->session->set_userdata($sdata);

        redirect('welcome/router_list');
    }
    public function delete_fleets($fleet_id)
    {
        $this->wm->delete_fleet($fleet_id);

        $sdata = array();
        $sdata['message'] = 'Successfully Delete Fleet !';

        $this->session->set_userdata($sdata);

        redirect('welcome/router_list');
    }
    public function delete_drivers($driver_id)
    {
        $this->wm->delete_driver($driver_id);

        $sdata = array();
        $sdata['message'] = 'Successfully Delete Driver !';

        $this->session->set_userdata($sdata);

        redirect('welcome/router_list');
    }

    public function save_add_route()
    {
        $data = array();
        $data['route_name'] = $this->input->post('route_name', true);
        $data['route_description'] = $this->input->post('route_description', true);
        $data['created_at'] = date('Y-m-d H:i:s');
        $this->wm->add_route_database($data);

        $sdata = array();
        $sdata['message'] = 'Successfully Add Route !';

        $this->session->set_userdata($sdata);

        redirect('welcome/router_list');
    }

    public function save_edit_update()
    {
        $data = array();
        $route_id = $this->input->post('route_id', true);
        $data['route_name'] = $this->input->post('route_name', true);
        $data['route_description'] = $this->input->post('route_description', true);
        $data['updated_at'] = date('Y-m-d H:i:s');

        $this->wm->save_edit_update_by_id($route_id, $data);
        $sdata = array();
        $sdata['message'] = 'Successfully Update Route !';

        $this->session->set_userdata($sdata);

        redirect('welcome/router_list');


    }

    public function fleet_list()
    {
        $data = array();
        $data['all_fleet'] = $this->wm->get_all_fleet();

        $data['main_content'] = $this->load->view('driver/fleet_list', $data, true);
        $this->load->view('driver/master', $data);
    }


    public function add_fleet($s, $d)
    {
        /* $k=$this->uri->segment(3);
         echo $k;
         exit();*/

        if ($s == 1) {
            $data = array();


            $data['s'] = $s;
            $data['main_content'] = $this->load->view('driver/add_fleet', $data, true);
            $this->load->view('driver/master', $data);
        }
        if ($s == 2) {
            $data = array();


            $data['s'] = $s;
            $data['show_fleet_by_id'] = $this->wm->get_show_fleet_by_id($d);


            /*  echo '<pre>';
              print_r( $data['show_route_by_id']);
              exit();*/

            $data['main_content'] = $this->load->view('driver/add_fleet', $data, true);
            $this->load->view('driver/master', $data);
        }


    }

    public function save_add_fleet()
    {
        $data = array();
        $data['fleet_name'] = $this->input->post('fleet_name', true);
        $data['fleet_no'] = $this->input->post('fleet_no', true);
        $data['fleet_description'] = $this->input->post('fleet_description', true);
        $data['created_at'] = date('Y-m-d H:i:s');
        $this->wm->add_fleet_database($data);

        $sdata = array();
        $sdata['message'] = 'Successfully Add Fleet !';

        $this->session->set_userdata($sdata);

        redirect('welcome/fleet_list');
    }

    public function save_edit_fleet_update()
    {
        $data = array();
        $fleet_id = $this->input->post('fleet_id', true);
        $data['fleet_name'] = $this->input->post('fleet_name', true);
        $data['fleet_no'] = $this->input->post('fleet_no', true);
        $data['fleet_description'] = $this->input->post('fleet_description', true);
        $data['updated_at'] = date('Y-m-d H:i:s');


/*
        echo '<pre>';
        print_r($data);
        exit();*/


        $this->wm->save_edit_fleet_update_by_id($fleet_id, $data);
        $sdata = array();
        $sdata['message'] = 'Successfully Update Fleet !';

        $this->session->set_userdata($sdata);

        redirect('welcome/fleet_list');


    }

    public function driver_list()
    {
        $data = array();
        $data['all_driver'] = $this->wm->get_all_driver();

        $data['main_content'] = $this->load->view('driver/driver_list', $data, true);
        $this->load->view('driver/master', $data);
    }

    public function add_driver($s, $d)
    {
        /* $k=$this->uri->segment(3);
         echo $k;
         exit();*/

        if ($s == 1) {
            $data = array();


            $data['s'] = $s;

            $data['all_route'] = $this->wm->get_all_route();
            $data['all_fleet'] = $this->wm->get_all_fleet();
            $data['main_content'] = $this->load->view('driver/add_driver', $data, true);
            $this->load->view('driver/master', $data);
        }
        if ($s == 2) {
            $data = array();


            $data['s'] = $s;
            $data['all_route'] = $this->wm->get_all_route();
            $data['all_fleet'] = $this->wm->get_all_fleet();
            $data['show_driver_by_id'] = $this->wm->get_show_driver_by_id($d);


              /*echo '<pre>';
              print_r( $data['show_driver_by_id']);
              exit();*/

            $data['main_content'] = $this->load->view('driver/add_driver', $data, true);
            $this->load->view('driver/master', $data);
        }


    }

    public function save_add_driver()
    {
        $kdata['user_name'] = $this->input->post('driver_name', true);
        $kdata['user_email'] = $this->input->post('driver_email', true);
        $kdata['user_role'] = 2;
        $kdata['user_password'] = md5($this->input->post('driver_password', true));
        $kdata['created_at']=date('Y-m-d H:i:s');

        $user=$this->wm->save_driver_info($kdata);

        $data = array();
        $data['driver_no'] = $this->input->post('driver_no', true);
      //  $data['driver_name'] = $this->input->post('driver_name', true);
        $data['user_id']=$user;
        $data['driver_status'] = $this->input->post('driver_status', true);
        $data['route_id'] = $this->input->post('route_id', true);
        $data['fleet_id'] = $this->input->post('fleet_id', true);
        $data['driver_license_no'] = $this->input->post('driver_license_no', true);
        $data['version'] = $this->input->post('version', true);
        $data['driver_ird'] = $this->input->post('driver_ird', true);
        $data['driver_vest_issue'] = $this->input->post('driver_vest_issue', true);
        $data['driver_kiwisaver'] = $this->input->post('driver_kiwisaver', true);
        $data['bank_name'] = $this->input->post('bank_name', true);
        $data['bank_account'] = $this->input->post('bank_account', true);
        $data['driver_payment'] = $this->input->post('driver_payment', true);
        $data['driver_phone'] = $this->input->post('driver_phone', true);
        $data['driver_mobile'] = $this->input->post('driver_mobile', true);
      //  $data['driver_email'] = $this->input->post('driver_email', true);
        $data['driver_next_of_kin'] = $this->input->post('driver_next_of_kin', true);
        $data['driver_next_of_phone'] = $this->input->post('driver_next_of_phone', true);
        $data['driver_hou_wages'] = $this->input->post('driver_hou_wages', true);
        $data['driver_shift'] = $this->input->post('driver_shift', true);
        $data['driver_blood'] = $this->input->post('driver_blood', true);
        $data['driver_birth'] = $this->input->post('driver_birth', true);
        $data['join_date'] = $this->input->post('join_date', true);
        $data['end_date'] = $this->input->post('end_date', true);
        $data['driver_address'] = $this->input->post('driver_address', true);
       // $data['driver_password'] = md5($this->input->post('driver_password', true));
        $data['created_at'] = date('Y-m-d H:i:s');


        /*echo '<pre>';
        print_r($data);
        exit();*/


        $this->wm->add_driver_database($data);

        $sdata = array();
        $sdata['message'] = 'Successfully Add Driver !';

        $this->session->set_userdata($sdata);

        redirect('welcome/driver_list');
    }

    public function save_edit_driver_update()
    {

        $user_id = $this->input->post('user_id', true);
        $kdata['user_name'] = $this->input->post('user_name', true);
        $kdata['user_email'] = $this->input->post('user_email', true);
        $kdata['updated_at']=date('Y-m-d H:i:s');

        $this->wm->update_user($$user_id,$kdata);


        $data = array();
        $driver_id = $this->input->post('driver_id', true);
        $data['driver_no'] = $this->input->post('driver_no', true);
      //  $data['driver_name'] = $this->input->post('driver_name', true);
        $data['driver_status'] = $this->input->post('driver_status', true);
        $data['route_id'] = $this->input->post('route_id', true);
        $data['fleet_id'] = $this->input->post('fleet_id', true);
        $data['driver_license_no'] = $this->input->post('driver_license_no', true);
        $data['version'] = $this->input->post('version', true);
        $data['driver_ird'] = $this->input->post('driver_ird', true);
        $data['driver_vest_issue'] = $this->input->post('driver_vest_issue', true);
        $data['driver_kiwisaver'] = $this->input->post('driver_kiwisaver', true);
        $data['bank_name'] = $this->input->post('bank_name', true);
        $data['bank_account'] = $this->input->post('bank_account', true);
        $data['driver_payment'] = $this->input->post('driver_payment', true);
        $data['driver_phone'] = $this->input->post('driver_phone', true);
        $data['driver_mobile'] = $this->input->post('driver_mobile', true);
       // $data['driver_email'] = $this->input->post('driver_email', true);
        $data['driver_next_of_kin'] = $this->input->post('driver_next_of_kin', true);
        $data['driver_next_of_phone'] = $this->input->post('driver_next_of_phone', true);
        $data['driver_hou_wages'] = $this->input->post('driver_hou_wages', true);
        $data['driver_shift'] = $this->input->post('driver_shift', true);
        $data['driver_blood'] = $this->input->post('driver_blood', true);
        $data['driver_birth'] = $this->input->post('driver_birth', true);
        $data['join_date'] = $this->input->post('join_date', true);
        $data['end_date'] = $this->input->post('end_date', true);
        $data['driver_address'] = $this->input->post('driver_address', true);
        $data['updated_at'] = date('Y-m-d H:i:s');


       /* echo '<pre>';
        print_r($driver_id);
        exit();*/

        $this->wm->save_edit_driver_update_by_id($driver_id, $data);
        $sdata = array();
        $sdata['message'] = 'Successfully Update Driver !';

        $this->session->set_userdata($sdata);

        redirect('welcome/driver_list');


    }

    public function change_driver_password($driver_id)
    {
        /*$k=$this->uri->segment(3);
        echo $k;
        exit();*/

        $data = array();


        $data['main_content'] = $this->load->view('driver/change_driver_password', $data, true);
        $this->load->view('driver/master', $data);

    }

    public function save_driver_password()
    {
        $data = array();
        $user_id = $this->input->post('user_id', true);
        $data['user_password'] = $this->input->post('user_password', true);
        $data['updated_at']=date('Y-m-d H:i:s');
        $this->wm->save_driver_pass($user_id,$data);
        redirect('welcome/driver_list');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */