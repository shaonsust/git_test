<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>hr/employees"> Human Resource - All Employees & Staff</a></li>
                <li class="active">Edit Employee</li>
            </ol>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="content">
                <div class="row">
                    <div class="col-md-4">
                        <h4>Edit Employee</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo base_url() ?>hr/update_employee/<?php echo $employee->id;?>" method="post">
                            <div class="form-group">
                                <label for="name">Employee Name:</label>
                                <input type="text" id="name" required="required" name="name" class="form-control" placeholder="Employee Name eg: Md. Kamal Hossain" value="<?php echo $employee->name;?>"/>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="card_no">Card Number:</label>
                                    <input type="text" id="card_no" name="card_no" class="form-control" placeholder="Card No - eg: 213" value="<?php echo $employee->card_no;?>"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="type">Type:</label>
                                    <select class="form-control" name="type" id="type">
                                        <option selected value="<?php echo $employee->type;?>"><?php echo $employee->type;?></option>
                                        <option value="Staff">Staff</option>
                                        <option value="Labor">Labor</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="designation">Designation:</label>
                                    <input type="text" id="designation" name="designation" class="form-control" placeholder="Designation - eg: General Manager" value="<?php echo $employee->designation;?>"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="basic_salary">Basic Salary:</label>
                                    <input type="text" id="basic_salary" name="basic_salary" class="form-control numOnly" placeholder="Basic Salary - eg: 25000" value="<?php echo $employee->basic_salary;?>"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="daily_salary">Daily Salary:</label>
                                    <input type="text" id="daily_salary" name="daily_salary" class="form-control numOnly" placeholder="Daily Salary - eg: 850" value="<?php echo $employee->daily_salary;?>"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="hourly_salary">Hourly Salary:</label>
                                    <input type="text" id="hourly_salary" name="hourly_salary" class="form-control numOnly" placeholder="Hourly Salary - eg: 110" value="<?php echo $employee->hourly_salary;?>"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="joining_date">Joining Date:</label>
                                    <input type="text" id="joining_date" name="joining_date" class="form-control datepicker" placeholder="Joining Date - YYYY-MM-DD" value="<?php echo $employee->joining_date;?>"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="dob">Date Of Birth:</label>
                                    <input type="text" id="dob" name="dob" class="form-control datepicker" placeholder="Date Of Birth -  Date - YYYY-MM-DD" value="<?php echo $employee->dob;?>"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="phone">Phone/Mobile:</label>
                                    <input type="text" id="phone" name="phone" class="form-control" placeholder="Phone/Mobile - eg: 0171111111" value="<?php echo $employee->phone;?>"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="present_address">Present Address:</label>
                                    <textarea class="form-control" name="present_address" id="present_address" cols="30" rows="3" placeholder="Present Address - eg: Barashia, Fakirhat, Khulna"><?php echo $employee->present_address;?></textarea>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="permanent_address">Permanent Address:</label>
                                    <textarea class="form-control" name="permanent_address" id="permanent_address" cols="30" rows="3" placeholder="Present Address - eg: Barashia, Fakirhat, Khulna"><?php echo $employee->permanent_address;?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="notes">Notes:</label>
                                <textarea class="form-control" name="notes" id="notes" cols="30" rows="5" placeholder="Notes eg: Any addtional Notes that need to be stored just for informative purpose"><?php echo $employee->notes;?></textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit"><span class="fa fa-save"></span>&nbsp; Update Employee</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".numOnly").change(function(){
        var tAmount = $(this).val();
        tAmount = Number(tAmount).toFixed(2);
        $(this).val(tAmount);
    });
    $( ".datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
</script>