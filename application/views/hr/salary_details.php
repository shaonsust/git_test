<style>
    @media screen {
        div.divFooter {
            display: none;
        }
    }
    @media print {
        .noPrint {
            display: none;
        }
        div.divFooter {
            position: fixed;
            bottom: 15px;
            right: 15px;
            page-break-after: always;
        }
        @page { margin: 0; }
        body { margin: 1.6cm; }
        .table-condensed tr td, .table-condensed tr th{
            font-size: 10px;
            line-height: 20px;
            padding: 2px 5px;
        }

    }
</style>
<div class="row noPrint">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>hr/salary">Human Resource - Salaries</a></li>
                <li class="active">Salary Payment Receipt</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible noPrint" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-10">
        <div class="box box-primary">
            <div class="box-header noPrint">
                <div class="row">
                    <div class="col-md-7">
                        <h4 class="box-title">
                            Salary Deatails - <?php echo $salary->receipt_no; ?>
                        </h4>
                    </div>
                    <div class="col-md-5 text-right">
                        <button class="btn btn-default" onclick="printMe()"> <i class="fa fa-print"></i> &nbsp;Print</button>
                        <a href="<?php echo base_url() . 'hr/employees'; ?>" class="btn btn-info"> <i class="fa fa-arrow-left"></i> &nbsp;Back</a>
                        <a href="<?php echo base_url() . 'hr/edit_salary/' . $salary->salary_id; ?>" class="btn btn-primary"> <i class="fa fa-pencil"></i> &nbsp;Edit</a>
                        <a href="<?php echo base_url() . 'hr/delete_salary/' . $salary->salary_id; ?>" class="btn btn-danger"> <i class="fa fa-trash"></i> &nbsp;Delete</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%">
                            <tr>
                                <td width="20%" align="center">
<!--                                    <img src="<?php echo base_url(); ?>logo.jpg" alt=""/>-->
                                </td>
                                <td width="60%" align="center">
                                    <h2>Bismillah Enterprise.</h2>
                                    <h3>Star Particle Board Mills Ltd.</h3>
                                    <p>55, Kazi Nazrul Islam Avenue, Kawran Bazar, Dhaka-1215</p>
                                </td>
                                <td width="20%" align="right">
                                    <p>01926-369889</br>
                                        9127586
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%">
                            <tr>
                                <td width="20%">
                                    Employee Name<br/>
                                    Card Number<br/>
                                    Designation
                                </td>
                                <td width="45%">
                                    : <?php echo $salary->name; ?> <br/>
                                    : <?php echo $salary->card_no; ?> <br/>
                                    : <?php echo $salary->designation; ?>
                                </td>
                                <td width="35%" align="right">
                                    Date: <?php echo $salary->date . ' ' . $salary->time; ?><br/>
                                    Transaction No: <?php echo $salary->receipt_no; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%">
                            <tr>
                                <td align="center">
                                    <h3><u>Salary Payment Receipt</u></h3>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-condensed table-bordered">
                            <tr>
                                <td colspan="5">Basic Salary</td>
                                <td align="right">
                                    <?php echo number_format($salary->basic_salary, 2, '.', ',') . ' BDT'; ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">Daily Salary</td>
                                <td width="15%" align="right">
                                    <?php echo number_format($salary->daily_salary, 2, '.', ',') . ' BDT'; ?>
                                </td>
                                <td width="15%">Presence</td>
                                <td width="15%" align="right">
                                    <?php echo number_format($salary->presence, 2, '.', ','); ?>
                                </td>
                                <td width="20%">Earned Salary</td>
                                <td width="15%" align="right">
                                    <?php echo number_format($salary->earned_salary, 2, '.', ',') . ' BDT'; ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">Hourly Salary (Overtime)</td>
                                <td width="15%" align="right">
                                    <?php echo number_format($salary->hourly_salary, 2, '.', ',') . ' BDT'; ?>
                                </td>
                                <td width="15%">Overtime Worked</td>
                                <td width="15%" align="right">
                                    <?php echo number_format($salary->overtime_hours, 2, '.', ','); ?>
                                </td>
                                <td width="20%">Earned Amount</td>
                                <td width="15%" align="right">
                                    <?php echo number_format($salary->overtime_amount, 2, '.', ',') . ' BDT'; ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">Hourly Salary (Deduction)</td>
                                <td width="15%" align="right">
                                    <?php echo number_format($salary->hourly_salary_deduction, 2, '.', ',') . ' BDT'; ?>
                                </td>
                                <td width="15%">Deduction Hours</td>
                                <td width="15%" align="right">
                                    <?php echo number_format($salary->deduction_hours, 2, '.', ','); ?>
                                </td>
                                <td width="20%">Deduction Amount</td>
                                <td width="15%" align="right">
                                    <?php echo number_format($salary->deduction_amount, 2, '.', ',') . ' BDT'; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                                <td width="20%">Bonus Amount</td>
                                <td width="15%" align="right">
                                    <?php echo number_format($salary->bonus_amount, 2, '.', ',') . ' BDT'; ?>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="5" class="text-right">Net Payment Amount</th>
                                <th width="15%" class="text-right">
                                    <?php echo number_format($salary->net_payable, 2, '.', ',') . ' BDT'; ?>
                                </th>
                            </tr>
                            <tr>
                                <td colspan="6" class="text-right">
                                    <strong>Net Payable In Words: <?php echo $this->numbertowords->convert_currency($salary->net_payable); ?> </strong>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <p>
                            <i>Note: <?php echo $salary->notes; ?></i>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <tr>
                                <td width="10%">&nbsp;</td>
                                <td width="30%" align="center">
                                    <br/>
                                    <br/>
                                    <br/>
                                    ......................................................
                                    <br/>
                                    Employee's Signature
                                </td>
                                <td width="20%">&nbsp;</td>
                                <td width="30%" align="center">
                                    <br/>
                                    <br/>
                                    <br/>
                                    ......................................................
                                    <br/>
                                    For Bismillah Enterprise
                                </td>
                                <td width="20%">&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="divFooter">Software Developed By: SCORSHIA.COM (http://www.scorshia.com)</div>

<div class="row noPrint">
    <div class="col-md-12">
        <button class="btn btn-default" onclick="printMe()"> <i class="fa fa-print"></i> &nbsp;Print</button>
        <a href="<?php echo base_url() . 'hr/employees'; ?>" class="btn btn-info"> <i class="fa fa-arrow-left"></i> &nbsp;Back</a>
        <a href="<?php echo base_url() . 'hr/edit_salary/' . $salary->salary_id; ?>" class="btn btn-primary"> <i class="fa fa-pencil"></i> &nbsp;Edit</a>
        <a href="<?php echo base_url() . 'hr/delete_salary/' . $salary->salary_id; ?>" class="btn btn-danger"> <i class="fa fa-trash"></i> &nbsp;Delete</a>
    </div>
</div>
<script>
    function printMe() {
        window.print();
    }
</script>


