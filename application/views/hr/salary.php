<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Human Resource - All Salaries</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">All Salaries</h4>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Transaction No</th>
                            <th>Date-Time</th>
                            <th>Staff Name</th>
                            <th>Card No</th>
                            <th>Month</th>
                            <th>Year</th>
                            <th>Tran Type</th>
                            <th>Acc_Name</th>
                            <th>Salary Payed</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($salaries as $s) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><a href="<?php echo base_url() . 'hr/salary_details/' . $s->salary_id ?>" title="Details"><?php echo $s->receipt_no; ?></a></td>
                                <td><?php echo $s->date . ' ' . $s->time; ?></td>
                                <td><a href="<?php echo base_url() . 'hr/details/' . $s->staff_id; ?>" title="Details"><?php echo $s->name; ?></a></td>
                                <td><a href="<?php echo base_url() . 'hr/details/' . $s->staff_id; ?>" title="Details"><?php echo $s->card_no; ?></a></td>
                                <td><?php echo $s->month; ?></td>
                                <td><?php echo $s->year; ?></td>
                                <td align="right"><?php echo $s->tr_type; ?></td>
                                <td align="right"><?php echo $s->res_acc; ?></td>
                                <td><?php echo number_format($s->net_payable, 2, '.', ',') . ' BDT'; ?></td>
                                <td align="center">
                                    <a href="<?php echo base_url() . 'hr/salary_details/' . $s->salary_id ?>" class="btn btn-info btn-xs" title="Details"><i class="fa fa-list"></i></a>
                                    <a href="<?php echo base_url() . 'hr/edit_salary/' . $s->salary_id ?>" class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-pencil"></i></a>
                                    <a href="<?php echo base_url() . 'hr/delete_salary/' . $s->salary_id ?>" class="btn btn-danger btn-xs" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>





