<style type="text/css">
    .table-condensed th, .table-condensed td {
        font-size: 12px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>hr/employees"><i class="fa fa-dashboard"></i> All Employees</a></li>
                <li class="active"><?php echo $sub_title;?></li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if(isset($message) && !empty($message))
{
    ?>
    <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message;?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header">
                <h4 class="box-title">
                    <strong>Employee Name : <?php echo $staff['staff']->name;?></strong>
                    <hr/>
                    <p>
                        Designation: <?php echo $staff['staff']->designation;?> | Type: <?php echo $staff['staff']->type;?> | Status: <?php echo $staff['staff']->status;?>
                        <?php
                            if(isset($staff['staff']->card_no) && !empty($staff['staff']->card_no) && $staff['staff']->card_no != 0)
                            {
                        ?>
                                | Status: <?php echo $staff['staff']->card_no;?>
                        <?php
                            }
                        ?>
                    </p>
                </h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-solid">
                            <div class="box-body" style="padding-bottom: 0;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>
                                            Phone: <?php echo $staff['staff']->phone;?><br/>
                                            Present Address: <?php echo $staff['staff']->present_address;?><br/>
                                            Permanent Address: <?php echo $staff['staff']->permanent_address;?>
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                        <p>
                                            Date Of Birth: <?php echo $staff['staff']->dob;?> | Joining Date: <?php echo $staff['staff']->joining_date;?><br/>
                                            Basic Salary: <?php echo number_format($staff['staff']->basic_salary, 2, '.', ',');?> /= | Daily Salary: <?php echo number_format($staff['staff']->daily_salary, 2, '.', ',');?> /= |  Hourly Salary: <?php echo number_format($staff['staff']->hourly_salary, 2, '.', ',');?> /=<br/>
                                            Notes: <?php echo $staff['staff']->notes;?>
                                        </p>                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <p>
                                            <?php
                                                $total_salary = 0;
                                                $total_payed = 0;
                                                $total_overtime = 0;
                                                
                                                foreach($staff['salary'] as $s)
                                                {
                                                    $earned_salary = $s->earned_salary;
                                                    $overtime_amount = $s->overtime_amount;
                                                    $deduction_amount = $s->deduction_amount;
                                                    $bonus_amount = $s->bonus_amount;
                                                    $net_payable = $s->net_payable;

                                                    $total_overtime += $overtime_amount;
                                                    $total_salary = $total_salary + $earned_salary + $overtime_amount + $bonus_amount - $deduction_amount;
                                                    $total_payed = $total_payed + $net_payable;
                                                }                                                       
                                                $total_due = $total_salary - $total_payed;                                            
                                            ?>                                        
                                            <small>Total Salary: <strong><?php echo number_format($total_salary, 2, '.', ',').'/='?></strong> | </small>
                                            <small>Total Payed: <strong><?php echo number_format($total_payed, 2, '.', ',').'/='?></strong> | </small>
                                            <small>Total Due: <strong><?php echo number_format($total_due, 2, '.', ',').'/='?></strong> | </small>
                                            <small>Total Overtime: <strong><?php echo number_format($total_overtime, 2, '.', ',').'/='?></strong> | </small>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h4 class="box-title">Hisoty of Salary Payment</h4>
                            </div>
                            <div class="box-body">
                                <table id="table" class="table table-bordered table-hover table-striped dataTable table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Sl.</th>
                                        <th>Transaction No</th>
                                        <th>Date</th>
                                        <th>Month</th>
                                        <th>Year</th>
                                        <th>Presence</th>
                                        <th title="Earned Salary">Earned Salary</th>
                                        <th title="Overtime Hours">Overtime Hour</th>
                                        <th title="Overtime Amount">Overtime Amount</th>
                                        <th title="Deduction Hours">Deduction Hour</th>
                                        <th title="Deduction Amount">Deduction Amount</th>
                                        <th>Bonus</th>
                                        <th title="Gross Salary">Gross Salary</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i = 1;
                                    foreach($staff['salary'] as $s)
                                    {
                                        ?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><a href="<?php echo base_url() . 'hr/salary_details/' . $s->id ?>" title="Details"><?php echo $s->receipt_no;?></a></td>
                                            <td><?php echo $s->date;?></td>
                                            <td><?php echo $s->month;?></td>
                                            <td><?php echo $s->year;?></td>
                                            <td align="right"><?php echo number_format($s->presence, 2, '.', ',').' /=';?></td>
                                            <td align="right"><?php echo number_format($s->earned_salary, 2, '.', ',').' /=';?></td>
                                            <td align="right"><?php echo number_format($s->overtime_hours, 2, '.', ',');?></td>
                                            <td align="right"><?php echo number_format($s->overtime_amount, 2, '.', ',').' /=';?></td>
                                            <td align="right"><?php echo number_format($s->deduction_hours, 2, '.', ',');?></td>
                                            <td align="right"><?php echo number_format($s->deduction_amount, 2, '.', ',').' /=';?></td>
                                            <td align="right"><?php echo number_format($s->bonus_amount, 2, '.', ',').' /=';?></td>
                                            <td align="right"><?php echo number_format($s->net_payable, 2, '.', ',').' /=';?></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
