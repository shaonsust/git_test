<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>hr/employees"> Human Resource - All Employees & Staff</a></li>
                <li class="active"><?php echo $sub_title?></li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if(isset($message) && !empty($message))
{
    ?>
    <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message;?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
<div class="col-md-10 col-md-offset-1">
<div class="box box-primary">
    <div class="content">
        <div class="row">
            <div class="col-md-4">
                <h4>Edit Salary</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo base_url() ?>hr/update_salary/<?php echo $salary->salary_id;?>" method="post">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name">Employee Name:</label>
                            <input type="text" id="name" name="name" class="form-control" placeholder="Employee Name eg: Md. Kamal Hossain" value="<?php echo $salary->name;?>" readonly/>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="card_no">Card Number:</label>
                            <input type="text" id="card_no" name="card_no" class="form-control" placeholder="Card No - eg: 213" value="<?php echo $salary->card_no;?>" readonly/>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="designation">Designation:</label>
                            <input type="text" id="designation" name="designation" class="form-control" placeholder="Designation - eg: General Manager" value="<?php echo $salary->designation;?>" readonly/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-solid">
                                <div class="box-header">
                                    <h4 class="box-title">
                                        Montly Salary Payment Sheet
                                    </h4>
                                </div>
                                <div class="box-body">
                                    <table class="table table-condensed table-bordered">
                                        <tr>
                                            <th valign="middle">Date</th>
                                            <td><input class="datepicker form-control input-sm" type="text" name="date" value="<?php echo $salary->date;?>"/></td>
                                            <th>Time</th>
                                            <td><input class="form-control input-sm" type="time" name="time" value="<?php echo $salary->time;?>"/></td>
                                            <th>Salary for the month of</th>
                                            <td>
                                                <select name="month" id="month" class="form-control input-sm">
                                                    <?php
                                                    for($i=1;$i<13;$i++)
                                                    {
                                                        $month = date('F', mktime(0,0,0,$i));
                                                        ?>
                                                        <option value="<?php echo $month;?>" <?php
                                                        if($month == $salary->month)
                                                        {
                                                            echo 'selected';
                                                        }
                                                        ?>><?php echo $month;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="year" id="year" class="form-control input-sm">
                                                    <?php
                                                    $this_year = date('Y');
                                                    $starting = $this_year - 1;
                                                    $ending = $this_year + 9;
                                                    for($i = $starting; $i<$ending; $i++)
                                                    {
                                                        ?>
                                                        <option value="<?php echo $i;?>" <?php if($i==$salary->year){echo 'selected';}?>><?php echo $i;?></option>
                                                    <?php
                                                    }
                                                    ?>

                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <table class="table table-condensed table-bordered">
                                        <tr>
                                            <td colspan="5">Basic Salary</td>
                                            <td align="right">
                                                <div class="input-group">
                                                    <input type="text" name="basic_salary" class="form-control input-sm numOnly" value="<?php echo $salary->basic_salary?>"/>
                                                    <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%">Daily Salary</td>
                                            <td width="15%">
                                                <div class="input-group">
                                                    <input type="text" id="daily_salary" name="daily_salary" class="form-control input-sm numOnly dailysSalary" value="<?php echo $salary->daily_salary?>"/>
                                                    <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                </div>
                                            </td>
                                            <td width="15%">Presence</td>
                                            <td width="15%">
                                                <div class="input-group">
                                                    <input type="text" id="presence" name="presence" class="form-control input-sm numOnly dailysSalary" value="<?php echo $salary->presence?>"/>
                                                    <span class="input-group-addon" id="basic-addon2">&nbsp;Days&nbsp;</span>
                                                </div>
                                            </td>
                                            <td width="20%">Earned Salary</td>
                                            <td width="15%">
                                                <div class="input-group">
                                                    <input type="text" name="earned_salary" id="earned_salary" class="form-control input-sm numOnly dailysSalary" value="<?php echo $salary->earned_salary?>"/>
                                                    <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%">Hourly Salary (Overtime)</td>
                                            <td width="15%">
                                                <div class="input-group">
                                                    <input type="text" id="hourly_salary" name="hourly_salary" class="form-control input-sm numOnly overtimeSalary" value="<?php echo $salary->hourly_salary?>"/>
                                                    <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                </div>
                                            </td>
                                            <td width="15%">Overtime Worked</td>
                                            <td width="15%">
                                                <div class="input-group">
                                                    <input type="text" id="overtime" name="overtime" class="form-control input-sm numOnly overtimeSalary" value="<?php echo $salary->overtime_hours?>"/>
                                                    <span class="input-group-addon" id="basic-addon2">Hours</span>
                                                </div>
                                            </td>
                                            <td width="20%">Earned Amount</td>
                                            <td width="15%">
                                                <div class="input-group">
                                                    <input type="text" name="overtime_amount" id="overtime_amount" class="form-control input-sm numOnly" value="<?php echo $salary->overtime_amount?>"/>
                                                    <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%">Hourly Salary (Deduction)</td>
                                            <td width="15%">
                                                <div class="input-group">
                                                    <input type="text" id="hourly_salary_deduction" name="hourly_salary_deduction" class="form-control input-sm numOnly deductionSalary" value="<?php echo $salary->hourly_salary_deduction?>"/>
                                                    <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                </div>
                                            </td>
                                            <td width="15%">Deduction Hours</td>
                                            <td width="15%">
                                                <div class="input-group">
                                                    <input type="text" id="deduction_hours" name="deduction_hours" class="form-control input-sm numOnly deductionSalary" value="<?php echo $salary->deduction_hours?>"/>
                                                    <span class="input-group-addon" id="basic-addon2">Hours</span>
                                                </div>
                                            </td>
                                            <td width="20%">Deduction Amount</td>
                                            <td width="15%">
                                                <div class="input-group">
                                                    <input type="text" name="deduction_amount" id="deduction_amount" class="form-control input-sm numOnly" value="<?php echo $salary->deduction_amount?>"/>
                                                    <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">&nbsp;</td>
                                            <td width="20%">Bonus Amount</td>
                                            <td width="15%">
                                                <div class="input-group">
                                                    <input type="text" name="bonus_amount" id="bonus_amount" class="form-control input-sm numOnly" value="<?php echo $salary->bonus_amount?>"/>
                                                    <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="5" class="text-right">Net Payable Amount</th>
                                            <td width="15%">
                                                <div class="input-group">
                                                    <input type="text" id="net_payable" class="form-control input-sm numOnly" value="<?php echo ($salary->earned_salary + $salary->overtime_amount + $salary->bonus_amount - $salary->deduction_amount)?>"/>
                                                    <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="5" class="text-right">Payment Amount</th>
                                            <td width="15%">
                                                <div class="input-group">
                                                    <input type="text" name="net_payable" class="form-control input-sm numOnly" value="<?php echo $salary->net_payable?>"/>
                                                    <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6">
                                                <div class="form-group">
                                                    <label for="notes">Notes</label>
                                                    <textarea name="notes" id="notes" cols="30" class="form-control input-sm" placeholder="Any note/information that may come handy later"
                                                              rows="4"><?php echo $salary->notes?></textarea>
                                                </div>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary" type="submit"><span class="fa fa-save"></span>&nbsp; Update Salary</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<script>
    $(".numOnly").change(function(){
        var tAmount = $(this).val();
        tAmount = Number(tAmount).toFixed(2);
        $(this).val(tAmount);
    });
    $( ".datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
    $(document).ready(function(){
        $('.dailysSalary').keyup(function(){
            var presence = $("#presence").val();
            var daily_salary = $("#daily_salary").val();
            var earned_salary = Number(presence) * Number(daily_salary);
            earned_salary = earned_salary.toFixed(2);
            $("#earned_salary").val(earned_salary);
            calculate_salary();
        });

        $('.overtimeSalary').keyup(function(){
            var hourly_salary = $("#hourly_salary").val();
            var overtime = $("#overtime").val();
            var overtime_amount = Number(hourly_salary) * Number(overtime);
            overtime_amount = overtime_amount.toFixed(2);
            $("#overtime_amount").val(overtime_amount);
            calculate_salary();
        });

        $('.deductionSalary').keyup(function(){
            var hourly_salary_deduction = $("#hourly_salary_deduction").val();
            var deduction_hours = $("#deduction_hours").val();
            var deduction_amount = Number(hourly_salary_deduction) * Number(deduction_hours);
            deduction_amount = deduction_amount.toFixed(2);
            $("#deduction_amount").val(deduction_amount);
            calculate_salary();
        });
        $("#bonus_amount").keyup(function(){
            calculate_salary();
        });


    });
    function calculate_salary()
    {
        var earned_salary = $("#earned_salary").val();
        var overtime_amount = $("#overtime_amount").val();
        var deduction_amount = $("#deduction_amount").val();
        var bonus_amount = $("#bonus_amount").val();
        var net_payable = Number(earned_salary) + Number(overtime_amount) + Number(bonus_amount) - Number(deduction_amount);
        net_payable = net_payable.toFixed(2);
        $("#net_payable").val(net_payable);
    }
</script>