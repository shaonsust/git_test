<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Human Resource - All Employees & Staffs</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if(isset($message) && !empty($message))
{
    ?>
    <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message;?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Employees & Staffs &nbsp; <a class="btn btn-primary" href="<?php echo base_url();?>hr/add_new_employee"><span class="fa fa-plus"></span> &nbsp; Add New Employee</a></h4>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                    <tr>
                        <th>Sl.</th>
                        <th>Card No</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Designation</th>
                        <th>Basic Salary</th>
                        <th>Daily Wages</th>
                        <th>Hourly Wages</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach($employees as $e)
                    {
                        ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $e->card_no;?></td>
                            <td><a href="<?php echo base_url() . 'hr/details/' . $e->id ?>" title="Details"><?php echo $e->name;?></a></td>
                            <td><?php echo $e->type;?></td>
                            <td><?php echo $e->designation;?></td>
                            <td><?php echo $e->basic_salary;?></td>
                            <td><?php echo $e->daily_salary;?></td>
                            <td><?php echo $e->hourly_salary;?></td>
                            <td><?php echo $e->status;?></td>
                            <td align="center">
                                <a href="<?php echo base_url() . 'hr/details/' . $e->id ?>" class="btn btn-info btn-xs" title="Details"><i class="fa fa-list"></i></a>
                                <a href="<?php echo base_url() . 'hr/edit_staff/' . $e->id ?>" class="btn btn-primary btn-xs" title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo base_url() . 'hr/delete_staff/' . $e->id ?>" class="btn btn-danger btn-xs" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                                <a href="<?php echo base_url() . 'hr/pay_salary/' . $e->id ?>" class="btn bg-purple btn-xs" title="Pay Salary"><i class="fa fa-money"></i></a>
                                <?php
                                    if($e->status == 'Active')
                                    {
                                ?>
                                        <a href="<?php echo base_url() . 'hr/status_update/Inactive/' . $e->id ?>" class="btn bg-orange btn-xs" title="Current Status: Active | Mark as Inactive?"><i class="fa fa-chain-broken"></i></a>
                                <?php
                                    }
                                    else
                                    {
                                ?>
                                        <a href="<?php echo base_url() . 'hr/status_update/Active/' . $e->id ?>" class="btn bg-olive btn-xs" title="Current Status: Inactive | Mark as Active?"><i class="fa fa-chain"></i></a>
                                <?php
                                    }
                                ?>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>





