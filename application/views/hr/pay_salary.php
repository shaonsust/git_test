<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>hr/employees"> Human Resource - All Employees & Staff</a></li>
                <li class="active"><?php echo $sub_title ?></li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="content">
                <div class="row">
                    <div class="col-md-4">
                        <h4>Pay Salary</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo base_url() ?>hr/process_salary/<?php echo $employee->id; ?>" method="post">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="name">Employee Name:</label>
                                    <input type="text" id="name" name="name" class="form-control" placeholder="Employee Name eg: Md. Kamal Hossain" value="<?php echo $employee->name; ?>" readonly/>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="card_no">Card Number:</label>
                                    <input type="text" id="card_no" name="card_no" class="form-control" placeholder="Card No - eg: 213" value="<?php echo $employee->card_no; ?>" readonly/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="designation">Designation:</label>
                                    <input type="text" id="designation" name="designation" class="form-control" placeholder="Designation - eg: General Manager" value="<?php echo $employee->designation; ?>" readonly/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-solid">
                                        <div class="box-header">
                                            <h4 class="box-title">
                                                Montly Salary Payment Sheet
                                            </h4>
                                        </div>
                                        <div class="box-body">
                                            <table class="table table-condensed table-bordered">
                                                <tr>
                                                    <th valign="middle">Date</th>
                                                    <td><input class="datepicker form-control input-sm" type="text" name="date" value="<?php echo date('Y-m-d'); ?>"/></td>
                                                    <th>Time</th>
                                                    <td><input class="form-control input-sm" type="time" name="time" value="<?php echo date('H:i:s') ?>"/></td>
                                                    <th>Salary for the month of</th>
                                                    <td>
                                                        <select name="month" id="month" class="form-control input-sm">
                                                            <?php
                                                            $last_month = date('F', strtotime("first day of last month"));
                                                            for ($i = 1; $i < 13; $i++) {
                                                                $month = date('F', mktime(0, 0, 0, $i));
                                                                ?>
                                                                <option value="<?php echo $month; ?>" <?php
                                                                if ($month == $last_month) {
                                                                    echo 'selected';
                                                                }
                                                                ?>><?php echo $month; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="year" id="year" class="form-control input-sm">
                                                            <?php
                                                            $this_year = date('Y');
                                                            $starting = $this_year - 1;
                                                            $ending = $this_year + 9;
                                                            for ($i = $starting; $i < $ending; $i++) {
                                                                ?>
                                                                <option value="<?php echo $i; ?>" <?php
                                                                if ($i == $this_year) {
                                                                    echo 'selected';
                                                                }
                                                                ?>><?php echo $i; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>

                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br/>
                                            <table class="table table-condensed table-bordered">
                                                <tr>
                                                    <td colspan="5">Basic Salary</td>
                                                    <td align="right">
                                                        <div class="input-group">
                                                            <input type="text" name="basic_salary" class="form-control input-sm numOnly" value="<?php echo $employee->basic_salary ?>"/>
                                                            <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">Daily Salary</td>
                                                    <td width="15%">
                                                        <div class="input-group">
                                                            <input type="text" id="daily_salary" name="daily_salary" class="form-control input-sm numOnly dailysSalary" value="<?php echo $employee->daily_salary ?>"/>
                                                            <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                        </div>
                                                    </td>
                                                    <td width="15%">Presence</td>
                                                    <td width="15%">
                                                        <div class="input-group">
                                                            <input type="text" id="presence" name="presence" class="form-control input-sm numOnly dailysSalary" value="0.00"/>
                                                            <span class="input-group-addon" id="basic-addon2">&nbsp;Days&nbsp;</span>
                                                        </div>
                                                    </td>
                                                    <td width="20%">Earned Salary</td>
                                                    <td width="15%">
                                                        <div class="input-group">
                                                            <input type="text" name="earned_salary" id="earned_salary" class="form-control input-sm numOnly dailysSalary" value="0.00"/>
                                                            <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">Hourly Salary (Overtime)</td>
                                                    <td width="15%">
                                                        <div class="input-group">
                                                            <input type="text" id="hourly_salary" name="hourly_salary" class="form-control input-sm numOnly overtimeSalary" value="<?php echo $employee->hourly_salary ?>"/>
                                                            <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                        </div>
                                                    </td>
                                                    <td width="15%">Overtime Worked</td>
                                                    <td width="15%">
                                                        <div class="input-group">
                                                            <input type="text" id="overtime" name="overtime" class="form-control input-sm numOnly overtimeSalary" value="0.00"/>
                                                            <span class="input-group-addon" id="basic-addon2">Hours</span>
                                                        </div>
                                                    </td>
                                                    <td width="20%">Earned Amount</td>
                                                    <td width="15%">
                                                        <div class="input-group">
                                                            <input type="text" name="overtime_amount" id="overtime_amount" class="form-control input-sm numOnly" value="0.00"/>
                                                            <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">Hourly Salary (Deduction)</td>
                                                    <td width="15%">
                                                        <div class="input-group">
                                                            <input type="text" id="hourly_salary_deduction" name="hourly_salary_deduction" class="form-control input-sm numOnly deductionSalary" value="<?php echo $employee->hourly_salary ?>"/>
                                                            <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                        </div>
                                                    </td>
                                                    <td width="15%">Deduction Hours</td>
                                                    <td width="15%">
                                                        <div class="input-group">
                                                            <input type="text" id="deduction_hours" name="deduction_hours" class="form-control input-sm numOnly deductionSalary" value="0.00"/>
                                                            <span class="input-group-addon" id="basic-addon2">Hours</span>
                                                        </div>
                                                    </td>
                                                    <td width="20%">Deduction Amount</td>
                                                    <td width="15%">
                                                        <div class="input-group">
                                                            <input type="text" name="deduction_amount" id="deduction_amount" class="form-control input-sm numOnly" value="0.00"/>
                                                            <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">&nbsp;</td>
                                                    <td width="20%">Bonus Amount</td>
                                                    <td width="15%">
                                                        <div class="input-group">
                                                            <input type="text" name="bonus_amount" id="bonus_amount" class="form-control input-sm numOnly" value="0.00"/>
                                                            <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="5" class="text-right">Net Payable Amount</th>
                                                    <td width="15%">
                                                        <div class="input-group">
                                                            <input type="text" id="net_payable" class="form-control input-sm numOnly" value="0.00"/>
                                                            <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="5" class="text-right">Payment Amount</th>
                                                    <td width="15%">
                                                        <div class="input-group">
                                                            <input type="text" name="net_payable" class="form-control input-sm numOnly" value="0.00"/>
                                                            <span class="input-group-addon" id="basic-addon2">BDT</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6">
                                                        <div class="form-group">
                                                            <label for="notes">Notes</label>
                                                            <textarea name="notes" id="notes" cols="30" class="form-control input-sm" placeholder="Any note/information that may come handy later"
                                                                      rows="4"></textarea>
                                                        </div>

                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <?php
// ********************Start payment form created by shaon*************************************
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-solid">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Payment</h3>
                                        </div>
                                        <div class="box-body">
                                            <table id="orderTable" class="table table-bordered table-condensed">

                                                <tfoot>
                                                    <tr id="payment_type">
                                                        <td colspan="3" align="right">Transaction Type</td>
                                                        <td>
                                                            <select class="form-control payment_type" id="payment_type" name = "payment_type">
                                                                <option value="">Select Transaction Type</option>
                                                                <option value = "check">Check</option>
                                                                <option value = "cash">Cash</option>
                                                            </select>
                                                        </td>

                                                    </tr>
                                                    <tr id = "check_type">
                                                        <td colspan="3" align="right">Check Type</td>
                                                        <td>
                                                            <select class="form-control check_type" id="check_type" name = "check_type">
                                                                <option value="">Select Check Type</option>
                                                                <option value = "acc_pay">Account Pay</option>
                                                                <option value = "cash_pay">Cash Pay</option>
                                                            </select>
                                                        </td>

                                                    </tr>
                                                    <tr id = "bank_acc">
                                                        <td colspan="3" align="right">Bank Accounts</td>
                                                        <td>
                                                            <select class="form-control bank_acc" id="bank_acc" name = "bank_acc">
                                                                <option value="">Select Bank Account</option>
                                                                <?php
                                                                foreach ($accounts as $a) {
                                                                    if ($a->account == 'bank') {
                                                                        ?>
                                                                        <option value = '<?php echo $a->acc_name ?>'><?php echo $a->acc_name; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </td>

                                                    </tr>
                                                    <tr id = "cash_acc">
                                                        <td colspan="3" align="right">Cash Accounts</td>
                                                        <td>
                                                            <select class="form-control cash_acc" id="cash_acc" name = "cash_acc">
                                                                <option value="">Select Cash Account</option>
                                                                <?php
                                                                foreach ($accounts as $a) {
                                                                    if ($a->account == 'cash') {
                                                                        ?>
                                                                        <option value = '<?php echo $a->acc_name ?>'><?php echo $a->acc_name; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </td>

                                                    </tr>

                                                    <tr id = "check_no">
                                                        <td colspan="3" align="right">Check Number</td>
                                                        <td><input type="text" name="check_no" id="check_no" class="form-control checkFields input-sm text-right"/></td>
                                                    </tr>

                                                    <tr id = "bank_name">
                                                        <td colspan="3" align="right">Bank Name</td>
                                                        <td><input type="text" name="bank_name" id="bank_name" class="form-control checkFields input-sm text-right"  /></td>
                                                    </tr>

                                                    <tr id = "check_date">
                                                        <td colspan="3" align="right">Check Date</td>
                                                        <td><input type="text" name="check_date" class="form-control input-sm text-right datepicker" value="<?php echo date('Y-m-d'); ?>"/></td>
                                                    </tr>

                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <?php
// ********************End payment form created by shaon*************************************
                            ?>


                            <div class="form-group">
                                <button class="btn btn-primary" type="submit"><span class="fa fa-save"></span>&nbsp; Pay Salary</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".numOnly").change(function () {
        var tAmount = $(this).val();
        tAmount = Number(tAmount).toFixed(2);
        $(this).val(tAmount);
    });
    $(".datepicker").datepicker({
        dateFormat: "yy-mm-dd"
    });


    $(document).ready(function () {

        $('.dailysSalary').keyup(function () {
            var presence = $("#presence").val();
            var daily_salary = $("#daily_salary").val();
            var earned_salary = Number(presence) * Number(daily_salary);
            earned_salary = earned_salary.toFixed(2);
            $("#earned_salary").val(earned_salary);
            calculate_salary();
        });

        $('.overtimeSalary').keyup(function () {
            var hourly_salary = $("#hourly_salary").val();
            var overtime = $("#overtime").val();
            var overtime_amount = Number(hourly_salary) * Number(overtime);
            overtime_amount = overtime_amount.toFixed(2);
            $("#overtime_amount").val(overtime_amount);
            calculate_salary();
        });

        $('.deductionSalary').keyup(function () {
            var hourly_salary_deduction = $("#hourly_salary_deduction").val();
            var deduction_hours = $("#deduction_hours").val();
            var deduction_amount = Number(hourly_salary_deduction) * Number(deduction_hours);
            deduction_amount = deduction_amount.toFixed(2);
            $("#deduction_amount").val(deduction_amount);
            calculate_salary();
        });
        $("#bonus_amount").keyup(function () {
            calculate_salary();
        });


    });
    function calculate_salary()
    {
        var earned_salary = $("#earned_salary").val();
        var overtime_amount = $("#overtime_amount").val();
        var deduction_amount = $("#deduction_amount").val();
        var bonus_amount = $("#bonus_amount").val();
        var net_payable = Number(earned_salary) + Number(overtime_amount) + Number(bonus_amount) - Number(deduction_amount);
        net_payable = net_payable.toFixed(2);
        $("#net_payable").val(net_payable);
    }
</script>


<script type="text/javascript">



    $('document').ready(function () {

        $('#check_type').hide();
        $('#bank_acc').hide();
        $('#cash_acc').hide();
        $('#bank_name').hide();
        $('#check_no').hide();
        $('#check_date').hide();

        $('#payment_type').change(account_type);
        $('#check_type').change(check_type);
    });

    function account_type()
    {
        var type = $('.payment_type').val();
        //alert(type);
        if (type == 'check')
        {
            $('#check_type').show();
            $('#check_no').show();
            $('#bank_name').show();
            $('#check_date').show();
            $('#bank_acc').hide();
            $('#cash_acc').hide();
        }
        else if (type == 'cash')
        {
            $('#cash_acc').show();
            $('#bank_acc').hide();
            $('#check_type').hide();
            $('#check_no').hide();
            $('#bank_name').hide();
            $('#check_date').hide();
        }
        else
        {
            $('#check_type').hide();
            $('#bank_acc').hide();
            $('#cash_acc').hide();
            $('#bank_name').hide();
            $('#check_no').hide();
            $('#check_date').hide();
        }

        return false;
    }

    function check_type()
    {
        var check_type = $('.check_type').val();
        if (check_type == 'acc_pay')
        {
            $('#bank_acc').show();
            $('#cash_acc').hide();
        }
        else if (check_type == 'cash_pay')
        {
            $('#bank_acc').hide();
            $('#cash_acc').show();
        }
        else
        {
            $('#bank_acc').hide();
            $('#cash_acc').hide();
        }
    }
</script>
