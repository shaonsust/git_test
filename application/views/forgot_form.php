<!DOCTYPE html>
<html class="bg-black">
<head>
    <meta charset="UTF-8">
    <title>FFML | Log in</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url();?>css/AdminLTE.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="bg-black">

<div class="form-box" id="login-box">
    <div class="header">

        <?php
        $msg=$this->session->userdata('message');

        if($msg)
        {
            echo $msg;
            $this->session->unset_userdata('message');
        }
        else{

            echo 'Your Email Please';

        }

        ?>



    </div>
    <form action="<?php echo base_url();?>login/forgot_password" method="post">
        <div class="body bg-gray">
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Your Email Address"/>
                <br/>
                <p class="text-center">
                   <em>Please provide your email address. We will send you information about reseting your password.</em>
                </p>
            </div>
        </div>
        <div class="footer">
            <button type="submit" class="btn bg-olive btn-block">Send</button>
        </div>
    </form>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js" type="text/javascript"></script>

</body>
</html>
