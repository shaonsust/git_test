<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>finished_good"><i class="fa fa-dashboard"></i> Finished Goods</a></li>
                <li class="active">Finished Good Details</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if(isset($message) && !empty($message))
{
    ?>
    <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message;?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="box-title">Finished Good Details </h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <form action="<?php echo base_url() . 'finished_good/details/'.$this->data['id']; ?>" method="post" class="form form-inline dateRangeForm">
                            <div class="form-group">
                                <label for="start_date">Start Date</label>
                                <input type="text" name="start_date" id="start_date" class="form-control datepicker input-sm"/>
                            </div>
                            <div class="form-group">
                                <label for="end_date">End Date</label>
                                <input type="text" name="end_date" id="end_date" class="form-control datepicker input-sm"/>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-default" type="submit"><span class="fa fa-refresh"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8">
                        <h3>Product Name : <?php echo $item['item']->name; ?> </h3>
                        <p><strong>Item Note : </strong><?php echo $item['item']->notes; ?></p>
                    </div>
                    <div class="col-md-4 text-right">
                        <?php
                            if($filtered == 'yes')
                            {
                                echo 'Showing Data From '.$start_date.' to '.$end_date;
                            }
                            else {
                                echo 'Showing Data From the beginning to till date';
                            }
                        ?>
                        <br/><strong>Total Purchase :</strong> <?php echo $item['purchase_stats']->total_qty.' '.$item['item']->unit; ?> <br/>
                        <strong>Total Sales :</strong> <?php echo $item['sale_stats']->total_qty.' '.$item['item']->unit; ?> <br/>
                        <strong>Total Usage :</strong> <?php echo $item['usage_stats']->total_qty.' '.$item['item']->unit; ?> <br/>

                    </div>
                </div>
                <table class="table table-bordered table-hover table-striped">
                    <tr>
                        <td>Measurement Unit</td>
                        <td align="center"><?php echo $item['item']->unit; ?></td>
                        <td>Unit Sell Price</td>
                        <td align="right"><?php echo number_format($item['item']->price, 2, '.', ','); ?> BDT</td>
                        <td>Primary Stock</td>
                        <td align="right"><?php echo number_format($item['item']->stock, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                        <td>Minimum Stock</td>
                        <td align="right"><?php echo number_format($item['item']->min_stock, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                        <td>Current Stock</td>
                        <td align="right"><?php
                            $current_stock = $item['item']->stock + $item['purchase_stats']->total_qty + $item['production_stats']->total_qty - $item['sale_stats']->total_qty - $item['usage_stats']->total_qty;
                            echo number_format($current_stock, 2, '.', ',').' ';
                            echo $item['item']->unit;
                            ?></td>
                    </tr>
                </table>

            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Sales History (<?php echo $item['sale_stats']->total_sales; ?> Sales)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped display">
                    <thead>
                    <tr>
                        <th>Sl.</th>
                        <th>Date-Time</th>
                        <th>Receipt No</th>
                        <th>Qty</th>
                        <th>Rate</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach($item['sales_history'] as $sales)
                    {
                        ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $sales->date.' '.$sales->time;?></td>
                            <td><a href="<?php echo base_url() . 'sales/receipt/' . $sales->order_id; ?>" title="Client: <?php echo $sales->name;?>"><?php echo $sales->receipt_no;?></a></td>
                            <td align="right"><?php echo number_format($sales->qty, 2, '.', ',').' '.$item['item']->unit;?></td>
                            <td align="right"><?php echo number_format($sales->rate, 2, '.', ',');?> BDT</td>
                            <td align="right"><?php echo number_format($sales->price, 2, '.', ',');?> BDT</td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Sales Statistics (<?php echo $item['sale_stats']->total_sales; ?> Sales)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped">
                    <tr>
                        <td>Max Qty</td>
                        <td align="right"><?php echo number_format($item['sale_stats']->max_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Min Qty</td>
                        <td align="right"><?php echo number_format($item['sale_stats']->min_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Avg Qty</td>
                        <td align="right"><?php echo number_format($item['sale_stats']->avg_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Total Qty</td>
                        <td align="right"><?php echo number_format($item['sale_stats']->total_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Max Rate</td>
                        <td align="right"><?php echo number_format($item['sale_stats']->max_rate, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Min Rate</td>
                        <td align="right"><?php echo number_format($item['sale_stats']->min_rate, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Avg Rate</td>
                        <td align="right"><?php echo number_format($item['sale_stats']->avg_rate, 2, '.', ','); ?> BDT</td>

                    </tr>
                    <tr>
                        <td>Total Price</td>
                        <td align="right"><?php echo number_format($item['sale_stats']->total_price, 2, '.', ','); ?> BDT</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="box box-success">
            <div class="box-header">
                <h4 class="box-title">Production History (<?php echo $item['production_stats']->total_productions; ?> Productions)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped display">
                    <thead>
                    <tr>
                        <th>Sl.</th>
                        <th>Date-Time</th>
                        <th>Receipt No</th>
                        <th>Qty</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach($item['production_history'] as $prod)
                    {
                        ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $prod->date.' '.$prod->time;?></td>
                            <td><?php echo $prod->receipt_no;?></td>
                            <td align="right"><?php echo number_format($prod->qty, 2, '.', ',').' '.$item['item']->unit;?></td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-success">
            <div class="box-header">
                <h4 class="box-title">Productions Statistics (<?php echo $item['production_stats']->total_productions; ?> Productions)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped">
                    <tr>
                        <td>Max Qty</td>
                        <td align="right"><?php echo number_format($item['production_stats']->max_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Min Qty</td>
                        <td align="right"><?php echo number_format($item['production_stats']->min_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Avg Qty</td>
                        <td align="right"><?php echo number_format($item['production_stats']->avg_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Total Qty</td>
                        <td align="right"><?php echo number_format($item['production_stats']->total_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="box box-info">
            <div class="box-header">
                <h4 class="box-title">Purchase History (<?php echo $item['purchase_stats']->total_purchase; ?> Purchases)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped display">
                    <thead>
                    <tr>
                        <th>Sl.</th>
                        <th>Date-Time</th>
                        <th>Receipt No</th>
                        <th>Qty</th>
                        <th>Rate</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach($item['purchase_history'] as $purchase)
                    {
                        ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $purchase->date.' '.$purchase->time;?></td>
                            <td><a href="<?php echo base_url() . 'purchase/receipt/' . $purchase->order_id; ?>" title="Supplier: <?php echo $purchase->name;?>"><?php echo $purchase->receipt_no;?></a></td>
                            <td align="right"><?php echo number_format($purchase->qty, 2, '.', ',').' '.$item['item']->unit;?></td>
                            <td align="right"><?php echo number_format($purchase->rate, 2, '.', ',');?> BDT</td>
                            <td align="right"><?php echo number_format($purchase->price, 2, '.', ',');?> BDT</td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-info">
            <div class="box-header">
                <h4 class="box-title">Purchase Statistics (<?php echo $item['purchase_stats']->total_purchase; ?> Purchases)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped">
                    <tr>
                        <td>Max Qty</td>
                        <td align="right"><?php echo number_format($item['purchase_stats']->max_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Min Qty</td>
                        <td align="right"><?php echo number_format($item['purchase_stats']->min_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Avg Qty</td>
                        <td align="right"><?php echo number_format($item['purchase_stats']->avg_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Total Qty</td>
                        <td align="right"><?php echo number_format($item['purchase_stats']->total_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Max Rate</td>
                        <td align="right"><?php echo number_format($item['purchase_stats']->max_rate, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Min Rate</td>
                        <td align="right"><?php echo number_format($item['purchase_stats']->min_rate, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Avg Rate</td>
                        <td align="right"><?php echo number_format($item['purchase_stats']->avg_rate, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Total Price</td>
                        <td align="right"><?php echo number_format($item['purchase_stats']->total_price, 2, '.', ','); ?> BDT</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="box box-success">
            <div class="box-header">
                <h4 class="box-title">Usage History (<?php echo $item['usage_stats']->total_usage; ?> Usage)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped display">
                    <thead>
                    <tr>
                        <th>Sl.</th>
                        <th>Date-Time</th>
                        <th>Receipt No</th>
                        <th>Qty</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach($item['usage_history'] as $usage)
                    {
                        ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $usage->date.' '.$usage->time;?></td>
                            <td><a href="<?php echo base_url() . 'usage/details/raw_material//' . $usage->usage_id; ?>" title="Purpose: <?php echo $usage->purpose;?>"><?php echo $usage->receipt_no;?></a></td>
                            <td align="right"><?php echo number_format($usage->qty, 2, '.', ',').' '.$item['item']->unit;?></td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-success">
            <div class="box-header">
                <h4 class="box-title">Usage Statistics (<?php echo $item['usage_stats']->total_usage; ?> Usage)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped">
                    <tr>
                        <td>Max Qty</td>
                        <td align="right"><?php echo number_format($item['usage_stats']->max_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Min Qty</td>
                        <td align="right"><?php echo number_format($item['usage_stats']->min_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Avg Qty</td>
                        <td align="right"><?php echo number_format($item['usage_stats']->avg_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Total Qty</td>
                        <td align="right"><?php echo number_format($item['usage_stats']->total_qty, 2, '.', ','); ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $( ".datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
</script>

