    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active">Finished Goods</li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Finished Goods &nbsp;
                    <a class="btn btn-primary" href="<?php echo base_url();?>finished_good/add_new"><span class="fa fa-plus"></span> &nbsp; Add New</a>
                    <a class="btn btn-primary" href="<?php echo base_url();?>purchase/new_purchase/finished_good"><span class="fa fa-barcode"></span> &nbsp; Purchase</a>
                    <a class="btn btn-primary" href="<?php echo base_url();?>usage/new_usage/finished_good"><span class="fa fa-exchange"></span> &nbsp; Use</a>
                    <a class="btn btn-primary" href="<?php echo base_url();?>sales/sale/finished_good"><span class="fa fa-usd"></span> &nbsp; Sale</a>
                </h4>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                    <tr>
                        <th>Sl.</th>
                        <th>Finished Good Name</th>
                        <th>Unit Sale Price</th>
                        <th>Current Stock</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach($items as $item)
                    {
                        ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><a href="<?php echo base_url() . 'finished_good/details/' . $item->id ?>" title="Details"><?php echo $item->name;?></a></td>
                            <td align="right"><?php echo number_format($item->price, 2, '.', ',').' BDT';?></td>
                            <td align="right">
                                <?php 
                                    $primary_stock = $item->primary_stock;
                                    $total_sold = $item->total_sold;
                                    $total_purchase = $item->total_purchase;
                                    $total_used = $item->total_used;
                                    $total_prod = $item->total_prod;
                                    $current_stock = $primary_stock + $total_purchase + $total_prod - $total_used - $total_sold;
                                    echo number_format($current_stock, 2, '.', ',').' '.$item->unit;
                                ?>
                            </td>  
                            <td align="center">
                                <a href="<?php echo base_url() . 'finished_good/details/' . $item->id ?>" class="btn btn-info btn-xs" title="Details"><i class="fa fa-list"></i></a>
                                <a href="<?php echo base_url() . 'finished_good/edit/' . $item->id ?>" class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo base_url() . 'finished_good/delete/' . $item->id ?>" class="btn btn-danger btn-xs" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                                <a href="<?php echo base_url() . 'production/new_production/' . $item->id ?>" class="btn btn-primary btn-xs" title="New Production"><i class="fa fa-plus"></i></a>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


