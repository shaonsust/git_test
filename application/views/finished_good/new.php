<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>finished_good"> Finished Goods</a></li>
                <li class="active">Add New Finished Good</li>
            </ol>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="content">
                <div class="row">
                    <div class="col-md-4">
                        <h4>Add New Finished Good</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo base_url() ?>finished_good/save" method="post">
                            <div class="form-group">
                                <label for="name">Finished Good Name:</label>
                                <input type="text" id="name" required="required" name="name" class="form-control" placeholder="Finished Good Name"/>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="unit">Measurement Unit:</label>
                                    <input type="text" id="unit" name="unit" class="form-control" placeholder="Measurement Unit eg: kg"/>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="price">Unit Price (Sales):</label>
                                    <input type="text" id="price" name="price" class="form-control" placeholder="Per unit Selling Price"/>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="stock">Primary Stock:</label>
                                    <input type="text" id="stock" name="stock" class="form-control" placeholder="Primary Stock"/>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="min_stock">Minimum Stock Limit:</label>
                                    <input type="text" id="min_stock" name="min_stock" class="form-control" placeholder="Minimum Stock limit for System Alert."/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="notes">Notes:</label>
                                <textarea class="form-control" name="notes" id="notes" cols="30" rows="2" placeholder="Notes eg: Any addtional Notes that need to be stored just for informative purpose"></textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit"><span class="fa fa-save"></span>&nbsp; Save Finished Good</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
