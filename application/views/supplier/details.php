<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>supplier"><i class="fa fa-dashboard"></i> Supplier</a></li>
                <li class="active">Supplier Details</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="box-title">Supplier Details </h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <form action="<?php echo base_url() . 'supplier/details/' . $this->data['id']; ?>" method="post" class="form form-inline dateRangeForm">
                            <div class="form-group">
                                <label for="start_date">Start Date</label>
                                <input type="text" name="start_date" id="start_date" class="form-control datepicker input-sm"/>
                            </div>
                            <div class="form-group">
                                <label for="end_date">End Date</label>
                                <input type="text" name="end_date" id="end_date" class="form-control datepicker input-sm"/>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-default" type="submit"><span class="fa fa-refresh"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8">
                        <h3>Supplier Name : <?php echo $company['company']->name; ?> </h3>
                        <div class="row">
                            <div class="col-md-7">
                                <p><strong>Contact Person : </strong><?php echo $company['company']->contact_person; ?></p>
                                <p><strong>Address : </strong><?php echo $company['company']->address; ?></p>
                                <p><strong>Notes : </strong><?php echo $company['company']->notes; ?></p>
                            </div>
                            <div class="col-md-5">
                                <p>
                                    <strong>Phone : </strong><?php echo $company['company']->contact_phone; ?>
                                    <?php
                                    if (isset($company['company']->secondary_phone) && !empty($company['company']->secondary_phone)) {
                                        echo ', ' . $company['company']->secondary_phone;
                                    }
                                    ?>
                                </p>
                                <p><strong>Email : </strong><?php echo $company['company']->email; ?></p>
                                <p><strong>Website : </strong><?php echo $company['company']->website; ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-right">
                        <?php
                        if ($filtered == 'yes') {
                            echo 'Showing Data From ' . $start_date . ' to ' . $end_date;
                        } else {
                            echo 'Showing Data From the beginning to till date';
                        }
                        ?>
                        <small>
                            <br/><strong>Total Purchase Orders :</strong> <?php echo $company['order_stats']->total_orders; ?> <br/>
                            <strong>Total Purchase Amount :</strong> <?php echo number_format($company['order_stats']->total_price, 2, '.', ','); ?> <br/>
                            <strong>Total Payed Amount :</strong> <?php echo number_format($company['order_stats']->vat, 2, '.', ','); ?> <br/>
                            <strong>Total VAT Payed :</strong> <?php echo number_format($company['order_stats']->discount, 2, '.', ','); ?> <br/>
                            <strong>Total Discount Received :</strong> <?php echo number_format($company['order_stats']->total_payment_amount, 2, '.', ','); ?> <br/>
                            <strong>Total Due Amount :</strong> <?php echo number_format($company['order_stats']->total_due_amount, 2, '.', ','); ?> <br/>
                            <strong>Total Due Payed :</strong> <?php echo number_format($company['accounting_stats']->total_due_payment, 2, '.', ','); ?> <br/>
                        </small>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="box box-warning">
            <div class="box-header">
                <h4 class="box-title">Account Summary</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped table-condensed">
                    <tr>
                        <td>Initial Due</td>
                        <td align="right"><?php echo number_format($company['company']->outgoing_amount, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td>Total Purchased Amount</td>
                        <td align="right"><?php echo number_format($company['order_stats']->total_price, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Total Vat</td>
                        <td align="right"> + <?php echo number_format($company['order_stats']->vat, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Total Discount</td>
                        <td align="right"> - <?php echo number_format($company['order_stats']->discount, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Total Payabale</td>
                        <th align="right" class="text-right">
                            <?php
                            $total_payable = $company['company']->outgoing_amount + $company['order_stats']->total_price + $company['order_stats']->vat - $company['order_stats']->discount;
                            echo number_format($total_payable, 2, '.', ',');
                            ?> BDT
                        </th>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td>Total Payments</td>
                        <td align="right"> <?php echo number_format($company['order_stats']->total_payment_amount, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Total Due Payments</td>
                        <td align="right"> + <?php echo number_format($company['accounting_stats']->total_due_payment, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Total Payed</td>
                        <th align="right" class="text-right">
                            <?php
                            $total_paid = $company['order_stats']->total_payment_amount + $company['accounting_stats']->total_due_payment;
                            echo number_format($total_paid, 2, '.', ',');
                            ?> BDT
                        </th>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <th>Total Due Payabale</th>
                        <th align="right" class="text-right">
                            <?php
                            $total_due_payable = $total_payable - $total_paid;
                            echo number_format($total_due_payable, 2, '.', ',');
                            ?> BDT
                        </th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="box box-danger">
            <div class="box-header">
                <h4 class="box-title">Supplier Statistics</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped table-condensed">
                    <tr>
                        <td>Maximum Purchase Amount</td>
                        <td align="right"><?php echo number_format($company['order_stats']->max_total_price, 2, '.', ','); ?> BDT</td>
                        <td>Maximum Payed Amount</td>
                        <td align="right"><?php echo number_format($company['order_stats']->max_payment_amount, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Minimum Purchase Amount</td>
                        <td align="right"><?php echo number_format($company['order_stats']->min_total_price, 2, '.', ','); ?> BDT</td>
                        <td>Minimum Payed Amount</td>
                        <td align="right"><?php echo number_format($company['order_stats']->min_payment_amount, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Average Purchase Amount</td>
                        <td align="right"><?php echo number_format($company['order_stats']->avg_total_price, 2, '.', ','); ?> BDT</td>
                        <td>Average Payed Amount</td>
                        <td align="right"><?php echo number_format($company['order_stats']->avg_payment_amount, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Total Purchase Amount</td>
                        <td align="right"><?php echo number_format($company['order_stats']->total_price, 2, '.', ','); ?> BDT</td>
                        <td>Total Payed Amount</td>
                        <td align="right"><?php echo number_format($company['order_stats']->total_payment_amount, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Maximum Discount Received</td>
                        <td align="right"><?php echo number_format($company['order_stats']->max_discount, 2, '.', ','); ?> BDT</td>
                        <td>Maximum Vat Payed</td>
                        <td align="right"><?php echo number_format($company['order_stats']->max_vat, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Minimum Discount Received</td>
                        <td align="right"><?php echo number_format($company['order_stats']->min_discount, 2, '.', ','); ?> BDT</td>
                        <td>Minimum Vat Payed</td>
                        <td align="right"><?php echo number_format($company['order_stats']->min_vat, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Average Discount Received</td>
                        <td align="right"><?php echo number_format($company['order_stats']->avg_discount, 2, '.', ','); ?> BDT</td>
                        <td>Average Vat Payed</td>
                        <td align="right"><?php echo number_format($company['order_stats']->avg_vat, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Total Discount Received</td>
                        <td align="right"><?php echo number_format($company['order_stats']->discount, 2, '.', ','); ?> BDT</td>
                        <td>Total Vat Payed</td>
                        <td align="right"><?php echo number_format($company['order_stats']->vat, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Maximum Due Amount</td>
                        <td align="right"><?php echo number_format($company['order_stats']->max_due_amount, 2, '.', ','); ?> BDT</td>
                        <td>Minimum Due Amount</td>
                        <td align="right"><?php echo number_format($company['order_stats']->min_due_amount, 2, '.', ','); ?> BDT</td>
                    </tr>
                    <tr>
                        <td>Average Due Amount</td>
                        <td align="right"><?php echo number_format($company['order_stats']->avg_due_amount, 2, '.', ','); ?> BDT</td>
                        <td>Total Due Amount</td>
                        <td align="right"><?php echo number_format($company['order_stats']->total_due_amount, 2, '.', ','); ?> BDT</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header">
                <h4 class="box-title">Purchase History (<?php echo $company['order_stats']->total_orders; ?> Purchases)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped display">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Date-Time</th>
                            <th>Receipt No</th>
                            <th>Total Price</th>
                            <th>VAT</th>
                            <th>Discount</th>
                            <th>Trans Type</th>
                            <th>Acc Name</th>
                            <th>Payed Amount</th>
                            <th>Due</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($company['order_history'] as $order) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $order->date . ' ' . $order->time; ?></td>
                                <td><a href="<?php echo base_url() . 'purchase/receipt/' . $order->id; ?>"><?php echo $order->receipt_no; ?></a></td>
                                <td align="right"><?php echo number_format($order->total_price, 2, '.', ','); ?> BDT</td>
                                <td align="right"><?php echo number_format($order->vat, 2, '.', ',') . ' BDT (' . number_format($order->vat_percentage, 2, '.', ',') . '%)'; ?></td>
                                <td align="right"><?php echo number_format($order->discount, 2, '.', ',') . ' BDT (' . number_format($order->discount_percentage, 2, '.', ',') . '%)'; ?></td>
                                <td align="right"><?php echo $order->tr_type; ?></td>
                                <td align="right"><?php echo $order->res_acc; ?></td>
                                <td align="right"><?php echo number_format($order->payment_amount, 2, '.', ','); ?> BDT</td>
                                <td align="right"><?php echo number_format($order->due_amount, 2, '.', ','); ?> BDT</td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="box box-success">
            <div class="box-header">
                <h4 class="box-title">Due Payment History (<?php echo $company['accounting_stats']->total_payments; ?> Payments)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped display">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Date-Time</th>
                            <th>Receipt No</th>
                            <th>Trans Type</th>
                            <th>Acc Name</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($company['accounting_history'] as $accounting) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $accounting->date . ' ' . $accounting->time; ?></td>
                                <td><a href="<?php echo base_url() . 'accounting/receipt/supplier_due/' . $accounting->id; ?>"><?php echo $accounting->receipt_no; ?></a></td>
                                <td align="right"><?php echo $accounting->tr_type; ?></td>
                                <td align="right"><?php echo $accounting->res_acc; ?></td>
                                <td align="right"><?php echo number_format($accounting->amount, 2, '.', ','); ?> BDT</td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(".datepicker").datepicker({
        dateFormat: "yy-mm-dd"
    });
</script>

