    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active">Suppliers</li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Suppliers &nbsp; <a class="btn btn-primary" href="<?php echo base_url();?>supplier/add_new"><span class="fa fa-plus"></span> &nbsp; Add New Supplier</a></h4>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                    <tr>
                        <th>Sl.</th>
                        <th>Supplier Name</th>
                        <th>Contact Person</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach($companies as $c)
                    {
                        ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><a href="<?php echo base_url() . 'supplier/details/' . $c->id ?>" title="Details"><?php echo $c->name;?></a></td>
                            <td><?php echo $c->contact_person;?></td>
                            <td><?php echo $c->contact_phone;?></td>
                            <td><?php echo $c->address;?></td>
                            <td align="center">
                                <a href="<?php echo base_url() . 'supplier/details/' . $c->id ?>" class="btn btn-info btn-xs" title="Details"><i class="fa fa-list"></i></a>
                                <a href="<?php echo base_url() . 'supplier/edit/' . $c->id ?>" class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo base_url() . 'supplier/delete/' . $c->id ?>" class="btn btn-danger btn-xs" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>





