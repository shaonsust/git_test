<?php
// echo "<pre>";
// print_r($this->data['payment']);
// die();
?>

<div class="row">
	<div class="col-md-12">
		<section class="content-header">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url(); ?>welcome"><i
						class="fa fa-dashboard"></i> Dashboard</a></li>
				<li><a href="<?php echo base_url().'accounting/account1/account'; ?>"> <?php echo $this->data['sub_title']?></a></li>
				<li class="active">Edit <?php echo $this->data['sub_title']?></li>
			</ol>
		</section>
	</div>
</div>
<div class="row">
	<div class="col-md-7">
		<div class="box box-primary">
			<div class="content">
				<div class="row">
					<div class="col-md-8">
						<h4>Edit <?php echo $this->data['sub_title']?></h4>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">

						<form
							action="<?php echo base_url() ?>accounting/account_update/<?php echo $this->data['payment']->id;?>"
							method="post" class="account">
							
							  <div class="form-group">
								<label for="purpose">Account Type:</label> <input type="text"
									id="account" name="account"
									class="form-control input-sm" required="required"
									value=<?php echo $this->data['payment']->account; ?> readonly>
							</div>
							

							<div class="form-group">
								<label for="company">Account Name:</label> <input type="text"
									id="acc_name" name="acc_name" class="form-control input-sm"
									required="required"
									value=<?php echo $this->data['payment']->acc_name; ?>>
							</div>                                                                        
                       				<?php
										if ($this->data ['payment']->account == 'bank') {
									?>
                                    <div class="form-group" id="acc_no">
								<label for="company">Account No:</label> <input type="text"
									id="acc_no" name="acc_no" class="form-control input-sm"
									value=<?php echo $this->data['payment']->acc_no; ?>>
							</div>

							<div class="form-group" id="bank_name">
								<label for="purpose">Bank Name:</label> <input type="text"
									id="bank_name" name="bank_name" class="form-control input-sm"
									value=<?php echo $this->data['payment']->bank_name; ?>>
							</div>
                                    <?php
									}
									?>
                                    
                                    <div class="form-group">
								<label for="purpose">Current Balance:</label> <input type="text"
									id="curr_balance" name="curr_balance"
									class="form-control input-sm" required="required"
									value=<?php echo $this->data['payment']->curr_balance; ?>>
							</div>
							<div class='row'>
								<div class="form-group col-md-12">
									<label for="account">Status:</label> <select
										class="form-control" id="status" name="status"
										onchange="return account_type()">
										<option value="1"
											<?php if($this->data['payment']->status == '1') { echo "selected"; }?>>Active</option>
										<option value="2"
											<?php if($this->data['payment']->status == '2') { echo "selected"; }?>>Inactive</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="description">Description/ Note:</label>
								<textarea class="form-control" name="description" id="description" cols="30" rows="4"><?php echo $this->data['payment']->description;?></textarea>
							</div>
							<div class="form-group">
								<button id="savePayment" class="btn btn-primary" type="submit">
									<span class="fa fa-save"></span>&nbsp; Save Payment
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



