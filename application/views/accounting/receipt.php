<style>
    @media screen {
        div.divFooter {
            display: none;
        }
    }
    @media print {
        .noPrint {
            display: none;
        }
        div.divFooter {
            position: fixed;
            bottom: 15px;
            right: 15px;
            page-break-after: always;
        }
        @page { margin: 0; }
        body { margin: 1.6cm; }
        .table-condensed tr td, .table-condensed tr th{
            font-size: 10px;
            line-height: 20px;
            padding: 2px 5px;
        }

    }
</style>
<div class="row noPrint">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url() . 'accounting/index/' . $this->data['type']; ?>"> <?php echo $this->data['sub_title'] ?></a></li>
                <li class="active">New <?php echo $this->data['sub_title']; ?> Receipt</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible noPrint" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>

<div class="row">
    <div class="col-md-10">
        <div class="box box-primary">
            <div class="box-header noPrint">
                <div class="row">
                    <div class="col-md-7">
                        <h4 class="box-title">
                            <?php echo $this->data['sub_title']; ?> Receipt - <?php echo $payment->receipt_no; ?>
                        </h4>
                    </div>
                    <div class="col-md-5 text-right">
                        <button class="btn btn-default" onclick="printMe()"> <i class="fa fa-print"></i> &nbsp;Print</button>
                        <a href="<?php echo base_url() . 'accounting/index/' . $this->data['type']; ?>" class="btn btn-info"> <i class="fa fa-arrow-left"></i> &nbsp;Back</a>
                        <a href="<?php echo base_url() . 'accounting/edit/' . $this->data['type'] . '/' . $payment->id; ?>" class="btn btn-primary"> <i class="fa fa-pencil"></i> &nbsp;Edit</a>
                        <a href="<?php echo base_url() . 'accounting/delete/' . $this->data['type'] . '/' . $payment->id; ?>" class="btn btn-danger"> <i class="fa fa-trash"></i> &nbsp;Delete</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%">
                            <tr>
                                <td width="20%" align="center">
                                    <img src="<?php echo base_url(); ?>logo.jpg" alt=""/>
                                </td>
                                <td width="60%" align="center">
                                    <h2>Bismillah Enterprise.</h2>
                                    <h3>Star Particle Board Mills Ltd.</h3>
                                    <p>55, Kazi Nazrul Islam Avenue, Kawran Bazar, Dhaka-1215</p>
                                </td>
                                <td width="20%" align="right">
                                    <p>01926-369889</br>
                                        9127586
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%">
                            <tr>
                                <td width="20%">
                                    <?php
                                    if ($this->data['type'] == 'supplier_due' || $this->data['type'] == 'client_due') {
                                        ?>
                                        Client<br/>
                                        Contact Person<br/>
                                        Address<br/>
                                        Phone
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td width="45%">
                                    <?php
                                    if ($this->data['type'] == 'supplier_due' || $this->data['type'] == 'client_due') {
                                        ?>
                                        : <?php echo $payment->name; ?> <br/>
                                        : <?php echo $payment->contact_person; ?> <br/>
                                        : <?php echo $payment->address; ?> <br/>
                                        : <?php echo $payment->contact_phone; ?>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td width="35%" align="right">
                                    Date: <?php echo $payment->date . ' ' . $payment->time; ?><br/>
                                    Receipt No: <?php echo $payment->receipt_no; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%">
                            <tr>
                                <td align="center">
                                    <h3><u><?php echo $this->data['sub_title']; ?> Receipt</u></h3>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-condensed" width="100%">
                            <tr>
                                <th width="5%">sl.</th>
                                <th width="35%">Purpose</th>
                                <th width="40%">Description</th>
                                <th width="20%">Amount</th>
                            </tr>
                            <tr>
                                <td width="5%">1</td>
                                <td width="35%"><?php echo $payment->purpose; ?></td>
                                <td width="40%"><?php echo $payment->description; ?></td>
                                <td width="20%" align="right"><?php echo number_format($payment->amount, 2, '.', ','); ?></td>
                            </tr>
                            <tr>
                                <td colspan="3" align="right"><strong>Total</strong></td>
                                <td align="right"><strong><?php echo number_format($payment->amount, 2, '.', ','); ?></strong></td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <strong>In Words: <?php echo $this->numbertowords->convert_currency($payment->amount); ?> </strong>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <tr>
                                <td width="10%">&nbsp;</td>
                                <td width="30%" align="center">
                                    <br/>
                                    <br/>
                                    <br/>
                                    ......................................................
                                    <br/>
                                    <?php
                                    if ($this->data['type'] == 'income') {
                                        echo "Payer's";
                                    } elseif ($this->data['type'] == 'expense') {
                                        echo "Receiver's";
                                    } elseif ($this->data['type'] == 'supplier_due') {
                                        echo "Supplier's";
                                    } elseif ($this->data['type'] == 'client_due') {
                                        echo "Client's";
                                    }
                                    ?> Signature
                                </td>
                                <td width="20%">&nbsp;</td>
                                <td width="30%" align="center">
                                    <br/>
                                    <br/>
                                    <br/>
                                    ......................................................
                                    <br/>
                                    For Bismillah Enterprise
                                </td>
                                <td width="20%">&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="divFooter">Software Developed By: SCORSHIA.COM (http://www.scorshia.com)</div>

<div class="row noPrint">
    <div class="col-md-12">
        <button class="btn btn-default" onclick="printMe()"> <i class="fa fa-print"></i> &nbsp;Print</button>
        <a href="<?php echo base_url() . 'accounting/index/' . $this->data['type']; ?>" class="btn btn-info"> <i class="fa fa-arrow-left"></i> &nbsp;Back</a>
        <a href="<?php echo base_url() . 'accounting/edit/' . $this->data['type'] . '/' . $payment->id; ?>" class="btn btn-primary"> <i class="fa fa-pencil"></i> &nbsp;Edit</a>
        <a href="<?php echo base_url() . 'accounting/delete/' . $this->data['type'] . '/' . $payment->id; ?>" class="btn btn-danger"> <i class="fa fa-trash"></i> &nbsp;Delete</a>
    </div>
</div>
<script>
    function printMe() {
        window.print();
    }
</script>


