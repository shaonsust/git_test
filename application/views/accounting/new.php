
<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <?php if ($this->data['type'] == 'account') { ?>
                    <li><a href="<?php echo base_url() . 'accounting/account1/account'; ?>"> <?php echo $this->data['sub_title'] ?></a></li>
                    <?php
                } else {
                    ?>
                    <li><a href="<?php echo base_url() . 'accounting/index/' . $this->data['type']; ?>"> <?php echo $this->data['sub_title'] ?></a></li>
                <?php } ?>
                <li class="active">New <?php echo $this->data['sub_title'] ?></li>
            </ol>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="content">
                <div class="row">
                    <div class="col-md-8">
                        <h4>New <?php echo $this->data['sub_title'] ?></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo base_url() ?>accounting/process/<?php echo $this->data['type'] ?>" method="post" class = "old">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="amount">Amount:</label>
                                    <input type="text" id="amount" name="amount" class="form-control input-sm numOnly" value="0.00" required="required"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="date">Date:</label>
                                    <input type="text" id="date" name="date" class="form-control input-sm datepicker" value="<?php echo date('Y-m-d'); ?>" required="required"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="time">Time:</label>
                                    <input type="time" id="time" name="time" class="form-control input-sm" value="<?php echo date('H:i:s'); ?>" required="required"/>
                                </div>
                            </div>
                            <?php
                            if ($this->data['type'] == 'supplier_due') {
                                ?>
                                <div class="form-group">
                                    <label for="company">Supplier:</label>
                                    <input type="text" id="company" name="company" class="form-control input-sm" required="required"/>
                                </div>                                                                        
                                <?php
                            } elseif ($this->data['type'] == 'client_due') {
                                ?>
                                <div class="form-group">
                                    <label for="company">Client:</label>
                                    <input type="text" id="company" name="company" class="form-control input-sm" required="required"/>
                                </div> 
                                <?php
                            } elseif ($this->data['type'] == 'income' || $this->data['type'] == 'expense') {
                                ?>
                                <div class="form-group">
                                    <label for="purpose">Purpose:</label>
                                    <input type="text" id="purpose" name="purpose" class="form-control input-sm" required="required" placeholder="The purpose/reason/cause or any title of the payment"/>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="form-group">
                                <label for="description">Description/ Note:</label>
                                <textarea class="form-control" name="description" id="description" cols="30"
                                          rows="4" placeholder="Any additional information/description or note that could be handy in future"></textarea>
                            </div>



                            <?php
// ********************Start payment form created by shaon*************************************
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-solid">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Payment</h3>
                                        </div>
                                        <div class="box-body">
                                            <table id="orderTable" class="table table-bordered table-condensed">

                                                <tfoot>
                                                    <tr id="payment_type">
                                                        <td colspan="3" align="right">Transaction Type</td>
                                                        <td>
                                                            <select class="form-control payment_type" id="payment_type" name = "payment_type" onchange = "return account_type()">
                                                                <option value="">Select Transaction Type</option>
                                                                <option value = "check">Cheque</option>
                                                                <option value = "cash">Cash</option>
                                                            </select>
                                                        </td>

                                                    </tr>
                                                    <tr id = "check_type">
                                                        <td colspan="3" align="right">Cheque Type</td>
                                                        <td>
                                                            <select class="form-control check_type" id="check_type" name = "check_type">
                                                                <option value="">Select Cheque Type</option>
                                                                <option value = "acc_pay">Account Pay</option>
                                                                <option value = "cash_pay">Cash Pay</option>
                                                            </select>
                                                        </td>

                                                    </tr>
                                                    <tr id = "bank_acc">
                                                        <td colspan="3" align="right">Bank Accounts</td>
                                                        <td>
                                                            <select class="form-control bank_acc" id="bank_acc" name = "bank_acc">
                                                                <option value="">Select Bank Account</option>
                                                                <?php
                                                                foreach ($accounts as $a) {
                                                                    if ($a->account == 'bank') {
                                                                        ?>
                                                                        <option value = '<?php echo $a->acc_name ?>'><?php echo $a->acc_name; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </td>

                                                    </tr>
                                                    <tr id = "cash_acc">
                                                        <td colspan="3" align="right">Cash Accounts</td>
                                                        <td>
                                                            <select class="form-control cash_acc" id="cash_acc" name = "cash_acc">
                                                                <option value="">Select Cash Account</option>
                                                                <?php
                                                                foreach ($accounts as $a) {
                                                                    if ($a->account == 'cash') {
                                                                        ?>
                                                                        <option value = '<?php echo $a->acc_name ?>'><?php echo $a->acc_name; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </td>

                                                    </tr>
                                                    <tr style="display:none;">
                                                        <td colspan="2" align="right">Vat</td>
                                                        <td>
                                                            <div class="input-group" id="vat_percentage_container" style="display: none;" >
                                                                <input type="text" class="form-control input-sm numOnly" id="vat_percentage" value="0.00"/>
                                                                <span class="input-group-addon" id="basic-addon1">%</span>
                                                            </div>

                                                            <select class="form-control input-sm" name="vat_type"
                                                                    id="vat_type">
                                                                <option value="amount">Amount</option>
                                                                <option value="percentage">Percentage</option>
                                                            </select>
                                                        </td>
                                                        <td><input type="text" name="vat" id="vat" class="form-control input-sm text-right adjustPrice" value="0.00"/></td>
                                                    </tr>

                                                    <tr id = "check_no">
                                                        <td colspan="3" align="right">Cheque Number</td>
                                                        <td><input type="text" name="check_no" id="check_no" class="form-control input-sm text-right " /></td>
                                                    </tr>

                                                    <tr id = "bank_name">
                                                        <td colspan="3" align="right">Bank Name</td>
                                                        <td><input type="text" name="bank_name" id="bank_name" class="form-control input-sm text-right "  /></td>
                                                    </tr>

                                                    <tr id = "check_date">
                                                        <td colspan="3" align="right">Cheque Date</td>
                                                        <td><input type="text" name="check_date" class="form-control input-sm text-right datepicker" value="<?php echo date('Y-m-d'); ?>"/></td>
                                                    </tr>

                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <?php
// ********************End payment form created by shaon*************************************
                            ?>

                            <div class = "form-group">
                                <button id = "savePayment" class = "btn btn-primary" type = "submit"><span class = "fa fa-save"></span>&nbsp;
                                    Save Payment</button>
                            </div>
                        </form>


                        <form action = "<?php echo base_url() ?>accounting/account/<?php echo $this->data['type'] ?>" method = "post" class = "account">
                            <div class = "row">
                                <div class = "form-group col-md-12" id = "account_type1">
                                    <label for = "account">Account Type:</label>
                                    <select class = "form-control" id = "account" name = "account">
                                        <option value = "nothing">Select Account Type</option>
                                        <option value = "bank">Bank</option>
                                        <option value = "cash">Cash</option>
                                    </select>
                                </div>

                            </div>

                            <div class = "form-group" id = "acc_name">
                                <label for = "company">Account Name:</label>
                                <input type = "text" id = "acc_name" name = "acc_name" class = "form-control input-sm"  placeholder = "Account Name"/>
                            </div>

                            <div class = "form-group" id = "acc_no">
                                <label for = "company">Account No:</label>
                                <input type = "text" id = "acc_no" name = "acc_no" class = "form-control input-sm" placeholder = "Account No"/>
                            </div>

                            <div class = "form-group" id = "bank_name1">
                                <label for = "purpose">Bank Name:</label>
                                <input type = "text" id = "bank_name1" name = "bank_name" class = "form-control input-sm" placeholder = "Bank Name"/>
                            </div>

                            <div class = "form-group" id = "curr_balance">
                                <label for = "purpose">Current Balance:</label>
                                <input type = "text" id = "curr_balance" name = "curr_balance" class = "form-control input-sm"  placeholder = "Current Balance"/>
                            </div>

                            <div class = "form-group" id = "description1">
                                <label for = "description">Description/ Note:</label>
                                <textarea class = "form-control" name = "description" id = "description" cols = "30"
                                          rows = "4" placeholder = "Any additional information/description or note that could be handy in future"></textarea>
                            </div>
                            <div class = "form-group">
                                <button id = "savePayment" class = "btn btn-primary" type = "submit"><span class = "fa fa-save"></span>&nbsp;
                                    Save Payment</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if (isset($companies) && !empty($companies)) {
    ?>
    <script>
        var companies = [<?php
    $total_companies = count($companies);
    $i = 1;
    foreach ($companies as $c) {
        if ($i == $total_companies) {
            echo '"' . $c->name . '"';
        } else {
            echo '"' . $c->name . '",';
        }
        $i++;
    }
    ?>];
        $("#company").autocomplete({
            source: companies
        });
    </script>
    <?php
}
?>

<script>
    $(".numOnly").change(function () {
        var tAmount = $(this).val();
        tAmount = Number(tAmount).toFixed(2);
        $(this).val(tAmount);
    });
    $(".datepicker").datepicker({
        dateFormat: "yy-mm-dd"
    });
</script>





<?php
if ($this->data['type'] == 'account') {
    ?>
    <script>
        $('document').ready(function () {
            $('.old').hide();
            $('.account').show();
    //            alert("jjjj");
            $('#acc_no').hide();
            $('#acc_name').hide();
            $('#bank_name1').hide();
            $('#curr_balance').hide();
            $('#description1').hide();

            $('#account').change(account_type1);
            //$('#check_type').change(check_type);
        });

    </script>
    <?php
} else {
    ?>
    <script>
        $('document').ready(function () {
            $('.account').hide();
            $('.old').show();

            $('#check_type').hide();
            $('#bank_acc').hide();
            $('#cash_acc').hide();
            $('#bank_name').hide();
            $('#check_no').hide();
            $('#check_date').hide();

            $('#account').change(account_type);
            $('#check_type').change(check_type);
        });

    </script>
    <?php
}
?>

<script type="text/javascript">

    function account_type()
    {
        var type = $('.payment_type').val();
        //alert(type);
        if (type == 'check')
        {
            $('#check_type').show();
            $('#check_no').show();
            $('#bank_name').show();
            $('#check_date').show();
            $('#bank_acc').hide();
            $('#cash_acc').hide();
        }
        else if (type == 'cash')
        {
            $('#cash_acc').show();
            $('#bank_acc').hide();
            $('#check_type').hide();
            $('#check_no').hide();
            $('#bank_name').hide();
            $('#check_date').hide();
        }
        else
        {
            $('#check_type').hide();
            $('#bank_acc').hide();
            $('#cash_acc').hide();
            $('#bank_name').hide();
            $('#check_no').hide();
            $('#check_date').hide();
        }

        return false;
    }

    function check_type()
    {
        var check_type = $('.check_type').val();
        if (check_type == 'acc_pay')
        {
            $('#bank_acc').show();
            $('#cash_acc').hide();
        }
        else if (check_type == 'cash_pay')
        {
            $('#bank_acc').hide();
            $('#cash_acc').show();
        }
        else
        {
            $('#bank_acc').hide();
            $('#cash_acc').hide();
        }
    }


    function account_type1()
    {
        var type = $('#account').val();
        if (type == 'bank')
        {
            $('#bank_name1').show();
            $('#acc_no').show();
            $('#curr_balance').show();
            $('#acc_name').show();
            $('#description1').show();
        }
        else if (type == 'cash')
        {
            $('#acc_name').show();
            $('#acc_no').hide();
            $('#bank_name1').hide();
            $('#curr_balance').show();
            $('#description1').show();
        }
        else
        {
            $('#curr_balance').hide();
            $('#acc_name').hide();
            $('#bank_name1').hide();
            $('#acc_no').hide();
            $('#description1').hide();
        }

        return false;
    }
</script>


