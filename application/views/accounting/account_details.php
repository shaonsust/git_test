<?php
//************************ Incoming sum account************************
$total_incoming = 0;
foreach ($order as $orders) {
    if (($orders['type'] === 'sales') && ($orders['res_acc'] === $details[0]['acc_name'])) {
        $total_incoming = $total_incoming + $orders['payment_amount'];
    }
}

foreach ($accounting as $acc) {
    if ($acc['res_acc'] == $details[0]['acc_name']) {
        if (($acc['type'] == 'outgoing_installment') || ($acc['type'] == 'client_due') || ($acc['type'] == 'income') || ($acc['type'] == 'incoming_loan')) {
            $total_incoming = $total_incoming + $acc['amount'];
        }
    }
}
//
//
//
//
////*********************outgoing sum account************************
//
$total_outgoing = 0;
foreach ($order as $orders) {
    if (($orders['type'] === 'purchase') && ($orders['res_acc'] === $details[0]['acc_name'])) {
        $total_outgoing = $total_outgoing + $orders['payment_amount'];
    }
}

foreach ($accounting as $acc) {
    if ($acc['res_acc'] == $details[0]['acc_name']) {
        if (($acc['type'] === 'incoming_installment') || ($acc['type'] === 'supplier_due') || ($acc['type'] === 'expense') || ($acc['type'] === 'outgoing_loan')) {
            $total_outgoing = $total_outgoing + $acc['amount'];
        }
    }
}

foreach ($salary as $s) {
    if ($s['res_acc'] === $details[0]['acc_name']) {
        $total_outgoing = $total_outgoing + $s['net_payable'];
    }
}
?>


<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>client"><i class="fa fa-dashboard"></i> Accounts</a></li>
                <li class="active">Accounts Details</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="box-title">Account Details </h4>
                    </div>

                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8">
                        <h3>Account Name : <?php echo $details[0]['acc_name']; ?> </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <p><strong>Account Type : </strong><?php echo $details[0]['account']; ?></p>
                                <?php if ($details[0]['account'] == 'bank') { ?>
                                    <p><strong>Bank name : </strong><?php echo $details[0]['bank_name']; ?></p>
                                    <p><strong>Account No : </strong><?php echo $details[0]['acc_no']; ?></p>
                                <?php } ?>
                                <?php if ($details[0]['status'] == 1) { ?>
                                    <p><strong>Status : </strong>Active</p>
                                <?php } else { ?>
                                    <p><strong>Status : </strong>Inactive</p>
                                <?php } ?>
                                <p><strong>Notes : </strong><?php echo $details[0]['description']; ?></p>
                                <h4><strong>Initial Balance : </strong><?php echo $details[0]['curr_balance']; ?></h4>                                
                                <h4><strong>Incoming Balance : </strong><?php echo $total_incoming; ?></h4>
                                <h4><strong>Outgoing Balance : </strong><?php echo $total_outgoing; ?></h4>
                                <h4><strong>Current Balance : </strong><?php echo ($details[0]['curr_balance'] + $total_incoming) - $total_outgoing; ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header">
                <h4 class="box-title">Incoming History</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped display">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Date-Time</th>
                            <th>Purpose</th>
                            <th>Receipt No</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>

                        <!--******************Start Data From Order Table********************-->
                        <?php
                        $i = 0;
                        foreach ($order as $orders) {
                            if (($orders['type'] === 'sales') && ($orders['res_acc'] === $details[0]['acc_name'])) {
                                ?>
                                <tr>
                                    <td><?php echo $i + 1; ?></td>
                                    <td><?php echo $orders['date'] . ' ' . $orders['time']; ?></td>
                                    <td><a target="_blank" href="<?php echo base_url() . 'sales/receipt/' . $orders['id']; ?>"><?php echo ucwords(str_replace("_", " ", $orders['type'])); ?></a></td>
                                    <td><a target="_blank" href="<?php echo base_url() . 'sales/receipt/' . $orders['id']; ?>"><?php echo $orders['receipt_no']; ?></a></td>
                                    <td><?php echo number_format($orders['payment_amount'], 2, '.', ', '); ?> BDT</td>

                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                        <!--******************End Data From Order Table********************-->



                        <!--******************Start Data From Accounting Table********************-->
                        <?php
                        foreach ($accounting as $acc) {
                            if ($acc['res_acc'] == $details[0]['acc_name']) {
                                if (($acc['type'] == 'outgoing_installment') || ($acc['type'] == 'client_due') || ($acc['type'] == 'income') || ($acc['type'] == 'incoming_loan')) {
                                    if ($acc['type'] === 'client_due' || $acc['type'] == 'income')
                                        $rc = base_url() . "accounting/receipt/" . $acc['type'] . '/' . $acc['id'];
                                    else if ($acc['type'] === 'incoming_loan')
                                        $rc = base_url() . 'loan/loan_details/' . $acc['type'] . '/' . $acc['id'];
                                    else
                                        $rc = "#";
                                    ?>
                                    <tr>
                                        <td><?php echo $i + 1; ?></td>
                                        <td><?php echo $acc['date'] . ' ' . $acc['time']; ?></td>
                                        <td><a target="_blank" href="<?php echo $rc ?>"><?php echo ucwords(str_replace("_", " ", $acc['type'])); ?></a></td>
                                        <td><a target="_blank" href="<?php echo $rc ?>"><?php echo $acc['receipt_no']; ?></a></td>
                                        <td><?php echo number_format($acc['amount'], 2, '.', ', '); ?> BDT</td>                                

                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                        }
                        ?>
                        <!--******************End Data From Accounting Table********************-->
                    </tbody>
                </table>
            </div>
        </div>
        <div class="box box-success">
            <div class="box-header">
                <h4 class="box-title">Outgoing History </h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped display">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Date-Time</th>
                            <th>Purpose</th>
                            <th>Receipt No</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!--******************Start Data From Accounting Table********************-->
                        <?php
                        $i = 0;
                        foreach ($accounting as $acc) {
                            if ($acc['res_acc'] === $details[0]['acc_name']) {
                                if (($acc['type'] === 'incoming_installment') || ($acc['type'] === 'supplier_due') || ($acc['type'] === 'expense') || ($acc['type'] === 'outgoing_loan')) {
                                    if ($acc['type'] === 'supplier_due' || $acc['type'] == 'expence')
                                        $rc = base_url() . "accounting/receipt/" . $acc['type'] . '/' . $acc['id'];
                                    else if ($acc['type'] === 'outgoing_loan')
                                        $rc = base_url() . 'loan/loan_details/' . $acc['type'] . '/' . $acc['id'];
                                    else
                                        $rc = "#";
                                    ?>
                                    <tr>
                                        <td><?php echo $i + 1; ?></td>
                                        <td><?php echo $acc['date'] . ' ' . $acc['time']; ?></td>
                                        <td><a target="_blank" href="<?php echo $rc; ?>"><?php echo ucwords(str_replace("_", " ", $acc['type'])); ?></a></td>
                                        <td><a target="_blank" href="<?php echo $rc; ?>"><?php echo $acc['receipt_no']; ?></a></td>
                                        <td><?php echo number_format($acc['amount'], 2, '.', ', '); ?> BDT</td>

                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                        }
                        ?>
                        <!--******************End Data From Accounting Table********************-->

                        <!--******************Start Data From Order Table********************-->
                        <?php
                        $j = 0;
                        foreach ($order as $order) {
                            if ($order['res_acc'] === $details[0]['acc_name']) {
                                if ($order['type'] === 'purchase') {
                                    ?>
                                    <tr>
                                        <td><?php echo $i + 1; ?></td>
                                        <td><?php echo $order['date'] . ' ' . $order['time']; ?></td>
                                        <td><a target="_blank" href="<?php echo base_url() . 'purchase/receipt/' . $order['id']; ?>"><?php echo ucwords(str_replace("_", " ", $order['type'])); ?></a></td>
                                        <td><a target="_blank" href="<?php echo base_url() . 'purchase/receipt/' . $order['id']; ?>"><?php echo $order['receipt_no']; ?></a></td>
                                        <td><?php echo number_format($order['payment_amount'], 2, '.', ', '); ?> BDT</td>

                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                        }
                        ?>
                        <!--******************End Data From Order Table********************-->

                        <!--******************Start Data From Salary Table********************-->
                        <?php
                        foreach ($salary as $order) {
                            if ($order['res_acc'] === $details[0]['acc_name']) {
                                ?>
                                <tr>
                                    <td><?php echo $i + 1; ?></td>
                                    <td><?php echo $order['date'] . ' ' . $order['time']; ?></td>
                                    <td><a target="_blank" href="<?php echo base_url() . 'hr/salary_details/' . $order['id']; ?>"><?php echo ucwords(str_replace("_", " ", $order['type'])); ?></a></td>
                                    <td><a target="_blank" href="<?php echo base_url() . 'hr/salary_details/' . $order['id']; ?>"><?php echo $order['receipt_no']; ?></a></td>
                                    <td><?php echo number_format($order['net_payable'], 2, '.', ', '); ?> BDT</td>

                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                        <!--******************End Data From Salary Table********************-->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(".datepicker").datepicker({
        dateFormat: "yy-mm-dd"
    });
</script>

