

<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active"><?php echo $this->data['sub_title']; ?></li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['sub_title']; ?> &nbsp;
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>accounting/new_payment/<?php echo $this->data['type']; ?>"><span class="fa fa-plus"></span> &nbsp; New <?php echo $this->data['sub_title']; ?></a>
                </h4>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Receipt</th>
                            <th>Date - Time</th>
                            <?php
                            if ($this->data['type'] == 'supplier_due' || $this->data['type'] == 'client_due') {
                                ?>
                                <th>Client Name</th>
                                <?php
                            }
                            ?>
                            <?php
                            if ($this->data['type'] == 'income' || $this->data['type'] == 'expense') {
                                ?>
                                <th>Purpose</th>
                                <th>Description</th>
                                <?php
                            }
                            ?>
                            <th>Trans Type</th>
                            <th>Acc Name</th>
                            <th>Amount</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($accounting as $a) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><a href="<?php echo base_url() . 'accounting/receipt/' . $this->data['type'] . '/' . $a->id; ?>" title="Details"><?php echo $a->receipt_no; ?></a></td>
                                <td><?php echo $a->date . ' - ' . $a->time; ?></td>
                                <?php
                                if ($this->data['type'] == 'supplier_due' || $this->data['type'] == 'client_due') {
                                    ?>
                                    <td><a href="<?php echo base_url() . 'client/details/' . $a->company_id; ?>" title="Details"><?php echo $a->name; ?></a></td>
                                    <?php
                                }
                                ?>
                                <?php
                                if ($this->data['type'] == 'income' || $this->data['type'] == 'expense') {
                                    ?>
                                    <td><?php echo $a->purpose; ?></td>
                                    <td><?php echo $a->description; ?></td>
                                    <?php
                                }
                                ?>
                                <td align="right"><?php echo $a->tr_type; ?></td>
                                <td align="right"><?php echo $a->res_acc; ?></td>
                                <td align="right"><?php echo number_format($a->amount, 2, '.', ','); ?></td>
                                <td align="center">
                                    <a href="<?php echo base_url() . 'accounting/edit/' . $this->data['type'] . '/' . $a->id ?>" class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-pencil"></i></a>
                                    <a href="<?php echo base_url() . 'accounting/delete/' . $this->data['type'] . '/' . $a->id ?>" class="btn btn-danger btn-xs" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


