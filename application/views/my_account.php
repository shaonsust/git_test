<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php
        $msg=$this->session->userdata('message');
        if($msg)
        {
            echo $msg;
            $this->session->unset_userdata('message');
        }
        else
        {
            echo ' My Account';
        }
        ?>
        <small></small>
    </h1>

    <?php

    $user_id=$this->session->userdata('user_id');

        ?>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>

            <li class="active">My account</li>
        </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
<!-- left column -->
<div class="col-md-6">
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Basic Info</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="<?php echo base_url();?>welcome/update_basic_info" method="post" >
            <div class="box-body">
                <div class="form-group">
                    <label for="full_name"> Full Name</label>
                    <input type="text" name="full_name" class="form-control" id="full_name" placeholder="Enter Full Name" value="<?php echo $user_info->full_name;?>">
                </div>
                <div class="form-group">
                    <label for="username"> User Name</label>
                    <input type="text" name="username" class="form-control" id="username" placeholder="Enter User Name" value="<?php echo $user_info->username;?>">
                </div>
                <div class="form-group">
                    <label for="email"> Email address</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="<?php echo $user_info->email;?>">
                </div>

            </div><!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div><!-- /.box -->

</div><!--/.col (left) -->
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Change Password</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo base_url();?>welcome/change_password" method="post">
                <div class="box-body" >
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Old Password</label>
                        <input type="password"  name="password" class="form-control " id="" placeholder="Your Current password" value="">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> New Password</label>
                        <input type="password"  name="user_password" class="form-control new_pass " id="" placeholder="Enter New password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Confirm Password</label>
                        <span class="error"></span>
                        <input type="password"  name="con_pass" class="form-control con_pass" id="" placeholder="Confirm New password">
                    </div>



                </div><!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit"  id="check_password" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div><!-- /.box -->
    </div>

</div>   <!-- /.row -->
</section><!-- /.content -->