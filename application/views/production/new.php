
<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url().'production'; ?>"> <?php echo $this->data['sub_title']?></a></li>
                <li class="active">New Production</li>
            </ol>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="content">
                <div class="row">
                    <div class="col-md-4">
                        <h4>New Production</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo base_url() ?>production/save/<?php echo $item->id;?>" method="post">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="amount">Quantity:</label>
                                    <div class="input-group">
                                        <input type="text" id="qty" name="qty" class="form-control input-sm numOnly" value="0.00" required="required"/>

                                        <span class="input-group-addon" id="basic-addon2"><?php echo $item->unit;?></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="date">Date:</label>
                                    <input type="text" id="date" name="date" class="form-control input-sm datepicker" value="<?php echo date('Y-m-d');?>" required="required"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="time">Time:</label>
                                    <input type="time" id="time" name="time" class="form-control input-sm" value="<?php echo date('H:i:s');?>" required="required"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="company">Finished Good's Name:</label>
                                <input type="text" id="finished_good" name="finished_good" class="form-control input-sm" value="<?php echo $item->name;?>" required="required" readonly/>
                                <input type="hidden" name="item_id" value="<?php echo $item->id;?>"/>
                            </div>
                            <div class="form-group">
                                <label for="description">Description/ Note:</label>
                                <textarea class="form-control" name="notes" id="notes" cols="30"
                                          rows="4" placeholder="Any additional information/description or note that could be handy in future"></textarea>
                            </div>
                            <div class="form-group">
                                <button id="saveProduction" class="btn btn-primary" type="submit"><span class="fa fa-save"></span>&nbsp; Save Production</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $( ".datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
</script>
