
<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url().'production'; ?>"> All Productions</a></li>
                <li class="active">Edit Production</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if(isset($message) && !empty($message))
{
    ?>
    <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message;?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="content">
                <div class="row">
                    <div class="col-md-4">
                        <h4>Edit Production</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo base_url() ?>production/update/<?php echo $production->id;?>" method="post">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="amount">Quantity:</label>
                                    <div class="input-group">
                                        <input type="text" id="qty" name="qty" class="form-control input-sm numOnly" value="<?php echo $production->qty; ?>" required="required"/>

                                        <span class="input-group-addon" id="basic-addon2"><?php echo $production->unit;?></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="date">Date:</label>
                                    <input type="text" id="date" name="date" class="form-control input-sm datepicker" value="<?php echo $production->date;?>" required="required"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="time">Time:</label>
                                    <input type="time" id="time" name="time" class="form-control input-sm" value="<?php echo $production->time;?>" required="required"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="company">Finished Good's Name:</label>
                                <input type="text" id="finished_good" name="finished_good" class="form-control input-sm" value="<?php echo $production->item_name;?>" required="required" readonly/>
                                <input type="hidden" name="item_id" value="<?php echo $production->item_id;?>"/>
                            </div>
                            <div class="form-group">
                                <label for="description">Description/ Note:</label>
                                <textarea class="form-control" name="notes" id="notes" cols="30"
                                          rows="4" placeholder="Any additional information/description or note that could be handy in future"><?php echo $production->notes;?></textarea>
                            </div>
                            <div class="form-group">
                                <button id="saveProduction" class="btn btn-primary" type="submit"><span class="fa fa-save"></span>&nbsp; Update Production</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $( ".datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
</script>
