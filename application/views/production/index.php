    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active">Productions</li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Usage - <?php echo $this->data['sub_title'];?> </h4>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                    <tr>
                        <th>Sl.</th>
                        <th>Receipt No.</th>
                        <th>Date - Time</th>
                        <th>Item</th>
                        <th>Quantity</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach($productions as $production)
                    {
                        ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $production->receipt_no;?></td>
                            <td><?php echo $production->date.' - '.$production->time;?></td>
                            <td><a href="<?php echo base_url() . 'finished_good/details/'. $production->item_id; ?>" title="Details"><?php echo $production->item_name;?></a></td>
                            <td><?php echo number_format($production->qty, 2, '.', ',').' '.$production->unit;?></td>
                            <td><?php echo $production->notes;?></td>
                            <td align="center">
                                <a href="<?php echo base_url() . 'production/edit/'  . $production->id ?>" class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo base_url() . 'production/delete/'  . $production->id ?>" class="btn btn-danger btn-xs" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


