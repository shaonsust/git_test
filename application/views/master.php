<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>BISMILLAH ENTERPRISE
            <?php
            if (isset($this->data['title']) && !empty($this->data['title'])) {
                echo ' | ' . $this->data['title'];
            }
            if (isset($this->data['sub_title']) && !empty($this->data['sub_title'])) {
                echo ' | ' . $this->data['sub_title'];
            }
            ?>
        </title>
        <link rel="icon" href="<?php echo base_url() ?>favicon.ico" type="image/x-icon">
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo base_url(); ?>css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="<?php echo base_url(); ?>css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!--    <link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />-->
        <!-- Ionicons -->



        <!-- Morris chart -->
        <link href="<?php echo base_url(); ?>css/morris/morris.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>jqueryui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>jqueryui/jquery-ui.theme.min.css" rel="stylesheet" type="text/css"/>
        <!-- jvectormap -->
        <link href="<?php echo base_url(); ?>css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css"/>
        <!-- Date Picker -->
        <link href="<?php echo base_url(); ?>css/datepicker/datepicker3.css" rel="stylesheet" type="text/css"/>
        <!-- Daterange picker -->
        <link href="<?php echo base_url(); ?>css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?php echo base_url(); ?>css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet"
              type="text/css"/>


        <!-- Theme style -->
        <link href="<?php echo base_url(); ?>css/AdminLTE.css" rel="stylesheet" type="text/css"/>
        <!--    <link href="--><?php //echo base_url();     ?><!--css/style.css" rel="stylesheet" type="text/css"/>-->
        <script src="<?php echo base_url() ?>js/jquery-2.1.1.min.js"></script>
    <!--    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>-->
    <!--    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>-->
        <script type="text/javascript" language="javascript" src="<?php echo base_url() ?>js/custom.js"></script>
    <!--    <script type="text/javascript" src="--><?php //echo base_url()     ?><!--ckeditor/ckeditor.js"></script>-->
        <script src="<?php echo base_url(); ?>js/jquery-ui.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url(); ?>js/plugins/morris/morris.min.js" type="text/javascript"></script>





        <!-- Theme style -->





        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="<?php echo base_url(); ?>welcome" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                BISMILLAH
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $this->session->userdata('user_name'); ?><i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="<?php echo base_url() . '' ?>img/avatar04.png" class="img-circle" alt="User Image"/>
                                </li>
                                <!-- Menu Body -->

                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?php echo base_url(); ?>welcome/my_account" class="btn btn-default btn-flat">My Account</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo base_url(); ?>welcome/logout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url(); ?>img/avatar04.png" class="img-circle" alt="User Image"/>
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo $this->session->userdata('user_name'); ?></p>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="<?php if (!empty($menu_active) && $menu_active == 'dashboard') echo 'active'; ?>">
                            <a href="<?php echo base_url(); ?>welcome">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li class=" <?php if (!empty($menu_active) && $menu_active == 'raw_material') echo 'active'; ?>">
                            <a href="<?php echo base_url(); ?>raw_material">
                                <i class="fa fa-sliders"></i> <span> Stock</span>
                            </a>
                        </li>

                        <li class=" <?php if (!empty($menu_active) && $menu_active == 'sales') echo 'active'; ?>">
                            <a href="<?php echo base_url(); ?>sales">
                                <i class="fa fa-sliders"></i> <span> Sales</span>
                            </a>
                        </li>
                        <li class=" <?php if (!empty($menu_active) && $menu_active == 'purchase') echo 'active'; ?>">
                            <a href="<?php echo base_url(); ?>purchase">
                                <i class="fa fa-sliders"></i> <span> Purchase</span>
                            </a>
                        </li>
                        <li class="treeview <?php if (!empty($menu_active) && $menu_active == 'accounting') echo 'active'; ?>">
                            <a href="#">
                                <i class="fa fa-laptop"></i>
                                <span>General Accounting</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php if (!empty($item_active) && $item_active == 'account') echo 'active'; ?>"><a href="<?php echo base_url(); ?>accounting/account1/account" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Accounts</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'income') echo 'active'; ?>"><a href="<?php echo base_url(); ?>accounting/index/income" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Income</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'expense') echo 'active'; ?>"><a href="<?php echo base_url(); ?>accounting/index/expense" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Expense</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'supplier_due') echo 'active'; ?>"><a href="<?php echo base_url(); ?>accounting/index/supplier_due" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Supplier Due Payment</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'client_due') echo 'active'; ?>"><a href="<?php echo base_url(); ?>accounting/index/client_due" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Client Due Payment</a></li>
                            </ul>
                        </li>
                        <li class="treeview <?php if (!empty($menu_active) && $menu_active == 'loan') echo 'active'; ?>">
                            <a href="#">
                                <i class="fa fa-laptop"></i>
                                <span>Loan Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php if (!empty($item_active) && $item_active == 'incoming_loan') echo 'active'; ?>"><a href="<?php echo base_url(); ?>loan/loans/incoming_loan" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Incoming Loans</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'outgoing_loan') echo 'active'; ?>"><a href="<?php echo base_url(); ?>loan/loans/outgoing_loan" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Outgoing Loans</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'loan_sources') echo 'active'; ?>"><a href="<?php echo base_url(); ?>loan/sources" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Loan Sources</a></li>
                            </ul>
                        </li>
                        <li class="treeview <?php if (!empty($menu_active) && $menu_active == 'hr') echo 'active'; ?>">
                            <a href="#">
                                <i class="fa fa-laptop"></i>
                                <span>Human Resource</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php if (!empty($item_active) && $item_active == 'employees') echo 'active'; ?>"><a href="<?php echo base_url(); ?>hr/employees" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Employees</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'salary') echo 'active'; ?>"><a href="<?php echo base_url(); ?>hr/salary" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Salary Payments</a></li>
                            </ul>
                        </li>
                        <li class=" <?php if (!empty($menu_active) && $menu_active == 'supplier') echo 'active'; ?>">
                            <a href="<?php echo base_url(); ?>supplier">
                                <i class="fa fa-sliders"></i> <span> Suppliers</span>
                            </a>
                        </li>
                        <li class=" <?php if (!empty($menu_active) && $menu_active == 'client') echo 'active'; ?>">
                            <a href="<?php echo base_url(); ?>client">
                                <i class="fa fa-sliders"></i> <span> Clients</span>
                            </a>
                        </li>   
                        <?php
                        $user_role = $this->session->userdata('user_role');
                        ?>
                        <li <?php
                        if ($user_role != 1) {
                            echo 'style="display:none"';
                        }
                        ?>class="treeview <?php if (!empty($menu_active) && $menu_active == 'reports') echo 'active'; ?>">
                            <a href="#">
                                <i class="fa fa-laptop"></i>
                                <span>Reports</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu reportsMenu">
                                <li class="<?php if (!empty($item_active) && $item_active == 'raw_material_summary') echo 'active'; ?>"><a href="<?php echo base_url(); ?>reports/raw_material_summary" style="margin-left: 5px;"><i class="fa fa-angle-double-right"></i>Stock Summary</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'client_summary') echo 'active'; ?>"><a href="<?php echo base_url(); ?>reports/client_summary" style="margin-left: 5px;"><i class="fa fa-angle-double-right"></i>All Clients Summary</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'supplier_summary') echo 'active'; ?>"><a href="<?php echo base_url(); ?>reports/supplier_summary" style="margin-left: 5px;"><i class="fa fa-angle-double-right"></i>All Suplier Summary</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'sales_summary') echo 'active'; ?>"><a href="<?php echo base_url(); ?>reports/sales_summary" style="margin-left: 5px;"><i class="fa fa-angle-double-right"></i>Sales Summary</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'purchase_summary') echo 'active'; ?>"><a href="<?php echo base_url(); ?>reports/purchase_summary" style="margin-left: 5px;"><i class="fa fa-angle-double-right"></i>Purchase Summary</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'general_income_summary') echo 'active'; ?>"><a href="<?php echo base_url(); ?>reports/general_income_summary" style="margin-left: 5px;"><i class="fa fa-angle-double-right"></i>General Income Summary</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'general_expense_summary') echo 'active'; ?>"><a href="<?php echo base_url(); ?>reports/general_expense_summary" style="margin-left: 5px;"><i class="fa fa-angle-double-right"></i>General Expense Summary</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'loan_in_summary') echo 'active'; ?>"><a href="<?php echo base_url(); ?>reports/loan_in_summary" style="margin-left: 5px;"><i class="fa fa-angle-double-right"></i>Incoming Loan Summary</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'loan_out_summary') echo 'active'; ?>"><a href="<?php echo base_url(); ?>reports/loan_out_summary" style="margin-left: 5px;"><i class="fa fa-angle-double-right"></i>Outgoing Loan Summary</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'overall_income_summary') echo 'active'; ?>"><a href="<?php echo base_url(); ?>reports/overall_income_summary" style="margin-left: 5px;"><i class="fa fa-angle-double-right"></i>Overall Income Summary</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'overall_expense_summary') echo 'active'; ?>"><a href="<?php echo base_url(); ?>reports/overall_expense_summary" style="margin-left: 5px;"><i class="fa fa-angle-double-right"></i>Overall Expense Summary</a></li>
                                <li class="<?php if (!empty($item_active) && $item_active == 'monthly_salary_sheet') echo 'active'; ?>"><a href="<?php echo base_url(); ?>reports/monthly_salary_sheet" style="margin-left: 5px;"><i class="fa fa-angle-double-right"></i>Monthly Salary Sheet</a></li>
                            </ul>
                        </li>
                        <li class="treeview <?php if (!empty($menu_active) && $menu_active == 'help') echo 'active'; ?>">
                            <a href="#">
                                <i class="fa fa-laptop"></i>
                                <span>Help Guide</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu reportsMenu">
                                <li class="<?php if (!empty($menu_active) && $menu_active == 'help' && !empty($item_active) && $item_active == 'overview') echo 'active'; ?>"><a href="<?php echo base_url(); ?>help/overview" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Overview</a></li>
                                <li class="<?php if (!empty($menu_active) && $menu_active == 'help' && !empty($item_active) && $item_active == 'account') echo 'active'; ?>"><a href="<?php echo base_url(); ?>help/account" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Account Settings</a></li>

                                <li class="<?php if (!empty($menu_active) && $menu_active == 'help' && !empty($item_active) && $item_active == 'raw_material') echo 'active'; ?>"><a href="<?php echo base_url(); ?>help/raw_material" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Raw Material</a></li>

                                <li class="<?php if (!empty($menu_active) && $menu_active == 'help' && !empty($item_active) && $item_active == 'finished_good') echo 'active'; ?>"><a href="<?php echo base_url(); ?>help/finished_good" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Finished Goods</a></li>

                                <li class="<?php if (!empty($menu_active) && $menu_active == 'help' && !empty($item_active) && $item_active == 'utility_item') echo 'active'; ?>"><a href="<?php echo base_url(); ?>help/utility_item" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Utility Items</a></li>

                                <li class="<?php if (!empty($menu_active) && $menu_active == 'help' && !empty($item_active) && $item_active == 'production') echo 'active'; ?>"><a href="<?php echo base_url(); ?>help/production" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> production</a></li>

                                <li class="<?php if (!empty($menu_active) && $menu_active == 'help' && !empty($item_active) && $item_active == 'usage') echo 'active'; ?>"><a href="<?php echo base_url(); ?>help/usage" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Item Usage Management </a></li>

                                <li class="<?php if (!empty($menu_active) && $menu_active == 'help' && !empty($item_active) && $item_active == 'sales') echo 'active'; ?>"><a href="<?php echo base_url(); ?>help/sales" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Sales Management</a></li>

                                <li class="<?php if (!empty($menu_active) && $menu_active == 'help' && !empty($item_active) && $item_active == 'purchase') echo 'active'; ?>"><a href="<?php echo base_url(); ?>help/purchase" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Purchase Management</a></li>

                                <li class="<?php if (!empty($menu_active) && $menu_active == 'help' && !empty($item_active) && $item_active == 'accounting') echo 'active'; ?>"><a href="<?php echo base_url(); ?>help/accounting" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> General Accounting</a></li>

                                <li class="<?php if (!empty($menu_active) && $menu_active == 'help' && !empty($item_active) && $item_active == 'loan') echo 'active'; ?>"><a href="<?php echo base_url(); ?>help/loan" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Loan Management </a></li>

                                <li class="<?php if (!empty($menu_active) && $menu_active == 'help' && !empty($item_active) && $item_active == 'hr') echo 'active'; ?>"><a href="<?php echo base_url(); ?>help/hr" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Human Resource </a></li>

                                <li class="<?php if (!empty($menu_active) && $menu_active == 'help' && !empty($item_active) && $item_active == 'supplier') echo 'active'; ?>"><a href="<?php echo base_url(); ?>help/supplier" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Suppliers Management</a></li>

                                <li class="<?php if (!empty($menu_active) && $menu_active == 'help' && !empty($item_active) && $item_active == 'client') echo 'active'; ?>"><a href="<?php echo base_url(); ?>help/client" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Clients Management</a></li>

                                <li class="<?php if (!empty($menu_active) && $menu_active == 'help' && !empty($item_active) && $item_active == 'reports') echo 'active'; ?>"><a href="<?php echo base_url(); ?>help/reports" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Reports</a></li>
                            </ul>
                        </li>
                        <li class=" <?php if (!empty($menu_active) && $menu_active == 'dbback') echo 'active'; ?>">
                            <a href="<?php echo base_url(); ?>welcome/dbback">
                                <i class="fa fa-sliders"></i> <span> Backup Database</span>
                            </a>
                        </li> 
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->



                <?php echo $main_content; ?>


                <!-- /.content -->
            </aside>
            <!-- /.right-side -->
        </div>
        <!-- ./wrapper -->

        <div class="noPrint" style="
             position: fixed;
             bottom: 3px;
             right: 3px;
             background: rgba(255,255,255,0.7);
             padding: 5px 10px;
             ">
            Software Developed By: <a href="http://www.scorshia.com">SCORSHIA.COM</a>
        </div>


        <script src="<?php echo base_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="<?php echo base_url(); ?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

        <!-- page script -->
        <script type="text/javascript">
            $(document).ready(function () {
                $('#table').dataTable({
                    "stateSave": true
                });
                $('#productTable').dataTable({
                    "scrollY": "500px",
                    "scrollCollapse": true,
                    "stateSave": true,
                    "ordering": false
                });

                $('table.display').dataTable();
            });
        </script>


        <!-- Morris.js charts -->
        <script src="<?php echo base_url(); ?>js/raphael-min.js"></script>



        <!-- Sparkline -->
        <script src="<?php echo base_url(); ?>js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="<?php echo base_url(); ?>js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"
        type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"
        type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="<?php echo base_url(); ?>js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="<?php echo base_url(); ?>js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- datepicker -->
        <script src="<?php echo base_url(); ?>js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo base_url(); ?>js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
        type="text/javascript"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url(); ?>js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>





        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="<?php echo base_url(); ?>js/AdminLTE/dashboard.js" type="text/javascript"></script>

        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>js/AdminLTE/demo.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/AdminLTE/style.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/warning.js" type="text/javascript"></script>



    </body>
</html>