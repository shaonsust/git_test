<?php
    $total_days = count($income);
?>
<script type="text/javascript">
    window.onload = function () {
        var chart = new CanvasJS.Chart("chartContainer",
            {
                theme: "theme3",
                animationEnabled: true,
                title:{
                    text: "Income Vs Expense, <?php echo $month_year;?>",
                    fontSize: 20
                },
                toolTip: {
                    shared: true
                },
                axisY: {
                    title: "Amount in Taka"
                },
                axisY2: {
                    title: "Amount in Taka"
                },
                data: [
                    {
                        type: "column",
                        name: "Overall Income",
                        legendText: "All incoming amount's summation",
                        showInLegend: true,
                        dataPoints:[
                            <?php
                                $i = 1;
                                foreach($income as $key=>$item)
                                {
                                if($i==$total_days)
                                    {
                            ?>
                            {label: "<?php echo $key;?>", y: <?php echo $item;?>}
                            <?php

                                    }
                                    else {
                            ?>
                            {label: "<?php echo $key;?>", y: <?php echo $item;?>},
                            <?php
                                    $i++;
                                    }
                                }
                            ?>
                        ]
                    },
                    {
                        type: "column",
                        name: "Overall Expense",
                        legendText: "All Outgoing amount's summation",
                        axisYType: "primary",
                        showInLegend: true,
                        dataPoints:[
                            <?php
                                $i = 1;
                                foreach($expense as $key=>$item)
                                {
                                if($i==$total_days)
                                    {
                            ?>
                            {label: "<?php echo $key;?>", y: <?php echo $item;?>}
                            <?php

                                    }
                                    else {
                            ?>
                            {label: "<?php echo $key;?>", y: <?php echo $item;?>},
                            <?php
                                    $i++;
                                    }
                                }
                            ?>


                        ]
                    }

                ],
                legend:{
                    cursor:"pointer",
                    itemclick: function(e){
                        if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                            e.dataSeries.visible = false;
                        }
                        else {
                            e.dataSeries.visible = true;
                        }
                        chart.render();
                    }
                }
            });

        chart.render();

        var chartIncome = new CanvasJS.Chart("chartContainerIncome",
            {
                title:{
                    text: "Overall Income Summary for <?php echo $month_year;?>",
                    fontFamily: "Arial",
                    fontWeight: "normal",
                    fontColor: "Green"
                },

                legend:{
                    verticalAlign: "bottom",
                    horizontalAlign: "center"
                },
                data: [
                    {
                        //startAngle: 45,
                        indexLabelFontSize: 20,
                        indexLabelFontFamily: "Garamond",
                        indexLabelFontColor: "darkgrey",
                        indexLabelLineColor: "darkgrey",
                        indexLabelPlacement: "outside",
                        type: "doughnut",
                        showInLegend: true,
                        dataPoints: [
                            <?php
                                $total_income_types = count($individual_income);
                                $total_income = 0;
                                foreach($individual_income as $income)
                                {
                                    $total_income += $income->Amount;
                                }
                                $i = 1;
                                foreach($individual_income as $inc)
                                {
                                    $type = str_replace('_', ' ', $inc->type);
                                    $type = ucwords($type);
                                    $percent = $inc->Amount*100/$total_income;
                                    $percent = number_format($percent, 2, '.', ',');
                                    if($i==$total_income_types)
                                        {
                                        echo '{  y: '.$percent.', legendText:"'.$type.' '.$percent.'%", indexLabel: "'.$type.' '.number_format($inc->Amount, 2, '.', ',').' BDT" }';
                                        }
                                        else{
                                        echo '{  y: '.$percent.', legendText:"'.$type.' '.$percent.'%", indexLabel: "'.$type.' '.number_format($inc->Amount, 2, '.', ',').' BDT" },';
                                        $i++;
                                        }
                                }
                            ?>
                        ]
                    }
                ]
            });

        chartIncome.render();

        var chartExpense = new CanvasJS.Chart("chartContainerExpense",
            {
                title:{
                    text: "Overall Expense Summary for <?php echo $month_year;?>",
                    fontFamily: "Arial",
                    fontWeight: "normal",
                    fontColor: "darkRed"
                },

                legend:{
                    verticalAlign: "bottom",
                    horizontalAlign: "center"
                },
                data: [
                    {
                        //startAngle: 45,
                        indexLabelFontSize: 20,
                        indexLabelFontFamily: "Garamond",
                        indexLabelFontColor: "darkgrey",
                        indexLabelLineColor: "darkgrey",
                        indexLabelPlacement: "outside",
                        type: "doughnut",
                        showInLegend: true,
                        dataPoints: [
                            <?php
                                $total_expense_types = count($individual_expense);
                                $total_expense = 0;
                                foreach($individual_expense as $expense)
                                {
                                    $total_expense += $expense->Amount;
                                }
                                $i = 1;
                                foreach($individual_expense as $exp)
                                {
                                    $type = str_replace('_', ' ', $exp->type);
                                    $type = ucwords($type);
                                    $percent = $exp->Amount*100/$total_income;
                                    $percent = number_format($percent, 2, '.', ',');
                                    if($i==$total_expense_types)
                                        {
                                        echo '{  y: '.$percent.', legendText:"'.$type.' '.$percent.'%", indexLabel: "'.$type.' '.number_format($exp->Amount, 2, '.', ',').' BDT" }';
                                        }
                                        else{
                                        echo '{  y: '.$percent.', legendText:"'.$type.' '.$percent.'%", indexLabel: "'.$type.' '.number_format($inc->Amount, 2, '.', ',').' BDT" },';
                                        $i++;
                                        }
                                }
                            ?>
                        ]
                    }
                ]
            });

        chartExpense.render();
    }
</script>
<script type="text/javascript" src="<?php echo base_url().'js/'?>canvasjs.min.js"></script>
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
</section>
<section class="content">

    <div class="row">
        <div class="col-md-12">
            <div id="chartContainer" style="height: 300px; width: 100%;">
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            <div id="chartContainerIncome" style="height: 300px; width: 100%;">
            </div>
        </div>
        <div class="col-md-6">
            <div id="chartContainerExpense" style="height: 300px; width: 100%;">
            </div>
        </div>
    </div>

</section>
