<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>raw_material"> Raw Material</a></li>
                <li class="active">Edit Raw Material</li>
            </ol>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="content">
                <div class="row">
                    <div class="col-md-4">
                        <h4>Edit Raw Material</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo base_url() ?>raw_material/update/<?php echo $item->id;?>" method="post">
                            <div class="form-group">
                                <label for="name">Raw Material Name:</label>
                                <input type="text" id="name" value="<?php echo $item->name;?>" required="required" name="name" class="form-control" placeholder="Raw Material Name"/>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="unit">Measurement Unit:</label>
                                    <input type="text" id="unit" name="unit" value="<?php echo $item->unit;?>" class="form-control" placeholder="Measurement Unit eg: kg"/>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="price">Unit Price (Purchase):</label>
                                    <input type="text" id="price" name="price" value="<?php echo $item->price;?>" class="form-control" placeholder="Per unit Price in case of Selling Raw Material"/>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="stock">Primary Stock:</label>
                                    <input type="text" id="stock" name="stock" value="<?php echo $item->stock;?>" class="form-control" placeholder="Primary Stock"/>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="min_stock">Minimum Stock Limit:</label>
                                    <input type="text" id="min_stock" name="min_stock" value="<?php echo $item->min_stock;?>" class="form-control" placeholder="Minimum Stock limit for System Alert."/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="notes">Notes:</label>
                                <textarea class="form-control" name="notes" id="notes" cols="30" rows="2" placeholder="Notes eg: Any addtional Notes that need to be stored just for informative purpose"><?php echo $item->notes;?></textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit"><span class="fa fa-save"></span>&nbsp; Update Raw Material</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
