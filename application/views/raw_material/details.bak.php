<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>raw_material"><i class="fa fa-dashboard"></i> Raw Materials</a></li>
                <li class="active">Raw Material Details</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if(isset($message) && !empty($message))
{
    ?>
    <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message;?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-5">
                        <h4 class="box-title">Raw Material Details </h4>
                    </div>
                    <div class="col-md-5">
                        <form action="<?php echo base_url() . 'raw_material/details/'.$this->data['id']; ?>" method="post" class="form form-inline">
                            <div class="form-group">
                                <label for="start_date">Start Date</label>
                                <input type="text" name="start_date" id="start_date" class="form-control datepicker input-sm"/>
                            </div>
                            <div class="form-group">
                                <label for="end_date">End Date</label>
                                <input type="text" name="end_date" id="end_date" class="form-control datepicker input-sm"/>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-default" type="submit"><span class="fa fa-eye"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <h3>Product Name : <?php echo $item['item']->name; ?> </h3>
                <p><strong>Item Note : </strong><?php echo $item['item']->notes; ?></p>
                <table class="table table-bordered table-hover table-striped">
                    <tr>
                        <td>Measurement Unit</td>
                        <td><?php echo $item['item']->unit; ?></td>
                        <td>Unit Sell Price</td>
                        <td><?php echo $item['item']->price; ?> BDT</td>
                        <td>Primary Stock</td>
                        <td><?php echo $item['item']->stock; ?> <?php echo $item['item']->unit; ?></td>
                        <td>Minimum Stock</td>
                        <td><?php echo $item['item']->min_stock; ?> <?php echo $item['item']->unit; ?></td>
                        <td>Current Stock</td>
                        <td><?php echo $item['item']->stock + $item['purchase_stats']->total_qty - $item['sale_stats']->total_qty - $item['usage_stats']->total_qty; ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                </table>

            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="box box-solid">
            <div class="box-header">
                <h4 class="box-title">Purchase Statistics (<?php echo $item['purchase_stats']->total_purchase; ?> Purchases)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped">
                    <tr>
                        <td>Max Qty</td>
                        <td><?php echo $item['purchase_stats']->max_qty; ?> <?php echo $item['item']->unit; ?></td>
                        <td>Min Qty</td>
                        <td><?php echo $item['purchase_stats']->min_qty; ?> <?php echo $item['item']->unit; ?></td>
                        <td>Avg Qty</td>
                        <td><?php echo $item['purchase_stats']->avg_qty; ?> <?php echo $item['item']->unit; ?></td>
                        <td>Total Qty</td>
                        <td><?php echo $item['purchase_stats']->total_qty; ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Max Rate</td>
                        <td><?php echo $item['purchase_stats']->max_rate; ?> BDT</td>
                        <td>Min Rate</td>
                        <td><?php echo $item['purchase_stats']->min_rate; ?> BDT</td>
                        <td>Avg Rate</td>
                        <td><?php echo $item['purchase_stats']->avg_rate; ?> BDT</td>
                        <td>Total Price</td>
                        <td><?php echo $item['purchase_stats']->total_price; ?> BDT</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="box box-solid">
            <div class="box-header">
                <h4 class="box-title">Purchase History (<?php echo $item['purchase_stats']->total_purchase; ?> Purchases)</h4>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                    <tr>
                        <th>Sl.</th>
                        <th>Date-Time</th>
                        <th>Receipt No</th>
                        <th>Qty</th>
                        <th>Rate</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach($item['purchase_history'] as $purchase)
                    {
                        ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $purchase->date.' '.$purchase->time;?></td>
                            <td><a href="<?php echo base_url() . 'purchase/receipt/' . $purchase->order_id; ?>" title="Supplier: <?php echo $purchase->name;?>"><?php echo $purchase->receipt_no;?></a></td>
                            <td><?php echo $purchase->qty;?></td>
                            <td><?php echo $purchase->rate;?></td>
                            <td><?php echo $purchase->price;?></td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="box box-solid">
            <div class="box-header">
                <h4 class="box-title">Sales Statistics (<?php echo $item['sale_stats']->total_sales; ?> Sales)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped">
                    <tr>
                        <td>Max Qty</td>
                        <td><?php echo $item['sale_stats']->max_qty; ?> <?php echo $item['item']->unit; ?></td>
                        <td>Min Qty</td>
                        <td><?php echo $item['sale_stats']->min_qty; ?> <?php echo $item['item']->unit; ?></td>
                        <td>Avg Qty</td>
                        <td><?php echo $item['sale_stats']->avg_qty; ?> <?php echo $item['item']->unit; ?></td>
                        <td>Total Qty</td>
                        <td><?php echo $item['sale_stats']->total_qty; ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Max Rate</td>
                        <td><?php echo $item['sale_stats']->max_rate; ?> BDT</td>
                        <td>Min Rate</td>
                        <td><?php echo $item['sale_stats']->min_rate; ?> BDT</td>
                        <td>Avg Rate</td>
                        <td><?php echo $item['sale_stats']->avg_rate; ?> BDT</td>
                        <td>Total Price</td>
                        <td><?php echo $item['sale_stats']->total_price; ?> BDT</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="box box-solid">
            <div class="box-header">
                <h4 class="box-title">Sales History (<?php echo $item['sale_stats']->total_sales; ?> Sales)</h4>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                    <tr>
                        <th>Sl.</th>
                        <th>Date-Time</th>
                        <th>Receipt No</th>
                        <th>Qty</th>
                        <th>Rate</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach($item['sales_history'] as $sales)
                    {
                        ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $sales->date.' '.$sales->time;?></td>
                            <td><a href="<?php echo base_url() . 'sales/receipt/' . $sales->order_id; ?>" title="Client: <?php echo $sales->name;?>"><?php echo $sales->receipt_no;?></a></td>
                            <td><?php echo $sales->qty;?></td>
                            <td><?php echo $sales->rate;?></td>
                            <td><?php echo $sales->price;?></td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="box box-solid">
            <div class="box-header">
                <h4 class="box-title">Usage Statistics (<?php echo $item['usage_stats']->total_usage; ?> Sales)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped">
                    <tr>
                        <td>Max Qty</td>
                        <td><?php echo $item['usage_stats']->max_qty; ?> <?php echo $item['item']->unit; ?></td>
                        <td>Min Qty</td>
                        <td><?php echo $item['usage_stats']->min_qty; ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                    <tr>
                        <td>Avg Qty</td>
                        <td><?php echo $item['usage_stats']->avg_qty; ?> <?php echo $item['item']->unit; ?></td>
                        <td>Total Qty</td>
                        <td><?php echo $item['usage_stats']->total_qty; ?> <?php echo $item['item']->unit; ?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="box box-solid">
            <div class="box-header">
                <h4 class="box-title">Usage History (<?php echo $item['usage_stats']->total_usage; ?> Sales)</h4>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                    <tr>
                        <th>Sl.</th>
                        <th>Date-Time</th>
                        <th>Receipt No</th>
                        <th>Qty</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach($item['usage_history'] as $usage)
                    {
                        ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $usage->date.' '.$usage->time;?></td>
                            <td><a href="<?php echo base_url() . 'usage/details/raw_material//' . $usage->usage_id; ?>" title="Purpose: <?php echo $usage->purpose;?>"><?php echo $usage->receipt_no;?></a></td>
                            <td><?php echo $usage->qty;?></td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $( ".datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
</script>

