<?php
//echo "<pre>";
//print_r($sales);
//die();
?>

<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Sales</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Sales &nbsp;
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>sales/sale"><span class="fa fa-plus"></span> &nbsp; New Sale</a>
                </h4>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                        <tr>
    <!--                        <th>Sl.</th>-->
                            <th>Receipt</th>
                            <th>Date - Time</th>
                            <th>Client Name</th>
                            <th>Trans Type</th>
                            <th>Acc Name</th>
                            <th>Total Price</th>
                            <th>Vat</th>
                            <th>Discount</th>
                            <th>Net Payable</th>
                            <th>Paid Amount</th>
                            <th>Due</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($sales as $sale) {
                            ?>
                            <tr>
    <!--                            <td>--><?php //echo $i;           ?><!--</td>-->
                                <td><a href="<?php echo base_url() . 'sales/receipt/' . $sale->id; ?>" title="Details"><?php echo $sale->receipt_no; ?></a></td>
                                <td><?php echo $sale->date . ' - ' . $sale->time; ?></td>
                                <td><a href="<?php echo base_url() . 'client/details/' . $sale->company_id; ?>" title="Details"><?php echo $sale->name; ?></a></td>
                                <td align="right"><?php echo $sale->tr_type; ?></td>
                                <td align="right"><?php echo $sale->res_acc; ?></td>
                                <td align="right"><?php echo $sale->total_price; ?></td>
                                <td align="right"><?php echo $sale->vat . ' ( ' . $sale->vat_percentage . ' % )'; ?></td>
                                <td align="right"><?php echo $sale->discount . ' ( ' . $sale->discount_percentage . ' % )'; ?></td>
                                <td align="right"><?php echo $sale->total_price + $sale->vat - $sale->discount; ?></td>
                                <td align="right"><?php echo $sale->payment_amount; ?></td>
                                <td align="right"><?php echo $sale->due_amount; ?></td>
                                <td align="center">
                                    <a href="<?php echo base_url() . 'sales/edit/' . $sale->id ?>" class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-pencil"></i></a>
                                    <a href="<?php echo base_url() . 'sales/delete/' . $sale->id ?>" class="btn btn-danger btn-xs" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                                </td>
                            </tr>
                            <?php
//                        $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


