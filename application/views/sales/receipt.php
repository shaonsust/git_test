<style>
    @media screen {
        div.divFooter {
            display: none;
        }
        .companyEnglish, .companyBangla {
            font-weight: bold;
            color: #102554;
        }
    }
    @media print {
        .box-body {
            padding:0;
        }
        .box-primary {
            border-top: none!important;
        }
        .box-header {
            border-bottom: none!important;
            padding: 0 !important;
        }
        .noPrint {
            display: none;
        }
        div.divFooter {
            position: fixed;
            bottom: 80px;
            right: 5px;
            page-break-after: always;
        }
        @page { margin: 0cm; }
        body { margin: 0cm; }
        .table-condensed tr td, .table-condensed tr th{
            font-size: 13px;
            line-height: 20px;
            padding: 2px 5px;
        }

    }
</style>
<div class="row noPrint">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>sales">Sales</a></li>
                <li class="active">Sales Receipt</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible noPrint" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-10">
        <div class="box box-primary">
            <div class="box-header noPrint">
                <div class="row">
                    <div class="col-md-7">
                        <h4 class="box-title">
                            Sales Receipt - <?php echo $order['order']->receipt_no; ?>
                        </h4>
                    </div>
                    <div class="col-md-5 text-right">
                        <button class="btn btn-default" onclick="printMe()"> <i class="fa fa-print"></i> &nbsp;Print</button>
                        <a href="<?php echo base_url() . 'sales'; ?>" class="btn btn-info"> <i class="fa fa-arrow-left"></i> &nbsp;Back</a>
                        <a href="<?php echo base_url() . 'sales/edit/' . $order['order']->id; ?>" class="btn btn-primary"> <i class="fa fa-pencil"></i> &nbsp;Edit</a>
                        <a href="<?php echo base_url() . 'sales/delete/' . $order['order']->id; ?>" class="btn btn-danger"> <i class="fa fa-trash"></i> &nbsp;Delete</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%">
                            <tr>
                                <td width="100%" align="center">
                                    <img src="<?php echo base_url(); ?>pad1.jpg" alt="" width="100%" height="27%"/>
                                </td>                                
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p><strong>Cell: 01926-369889, Phone :  9127586</strong></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%">
                            <tr>
                                <td width="20%">
                                    <strong>Client</strong><br/>
                                    <strong>Contact Person</strong><br/>
                                    <strong>Address</strong><br/>
                                    <strong>Phone</strong>
                                </td>
                                <td width="45%">
                                    : <?php echo $order['order']->company_name; ?> <br/>
                                    : <?php echo $order['order']->contact_person; ?> <br/>
                                    : <?php echo $order['order']->address; ?> <br/>
                                    : <?php echo $order['order']->contact_phone; ?>
                                </td>
                                <td width="35%" align="right">
                                    Date: <?php echo $order['order']->date . ' ' . $order['order']->time; ?><br/>
                                    Receipt No: <?php echo $order['order']->receipt_no; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%">
                            <tr>
                                <td align="center">
                                    <h3><u>Itemized Bill / Chalan</u></h3>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-condensed" width="100%">
                            <tr>
                                <th width="5%">sl.</th>
                                <th width="45%">Item Name</th>
                                <th width="15%">Rate</th>
                                <th width="15%">Qty</th>
                                <th width="20%">Price</th>
                            </tr>
                            <?php
                            $i = 1;
                            foreach ($order['sales'] as $sale) {
                                ?>
                                <tr>
                                    <td width="5%"><?php echo $i; ?></td>
                                    <td width="45%"><?php echo $sale->product_name; ?></td>
                                    <td width="15%" align="right"><?php echo number_format($sale->rate, 2, '.', ',') . ' BDT'; ?></td>
                                    <td width="15%" align="right"><?php echo number_format($sale->qty, 2, '.', ',') . ' ' . $sale->unit; ?></td>
                                    <td width="20%" align="right"><?php echo number_format($sale->price, 2, '.', ',') . ' BDT'; ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                            <tr>
                                <td colspan="4" align="right"><strong>Total Price</strong></td>
                                <td align="right"><strong><?php echo number_format($order['order']->total_price, 2, '.', ',') . ' BDT'; ?></strong></td>
                            </tr>
                            <!-- <tr>
                                <td colspan="4" align="right"><strong>VAT (<?php echo $order['order']->vat_percentage; ?> %)</strong></td>
                                <td align="right"><strong><?php echo $order['order']->vat; ?></strong></td>
                            </tr> -->
                            <tr>
                                <td colspan="4" align="right"><strong>Discount (<?php echo number_format($order['order']->discount_percentage, 2, '.', ','); ?> %)</strong></td>
                                <td align="right"><strong><?php echo number_format($order['order']->discount, 2, '.', ',') . ' BDT' ?></strong></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right"><strong>Previous Due</strong></td>
                                <td align="right"><strong>
                                        <?php
                                        $previous_due = $order['chalan_result']->incoming_amount + $order['chalan_result']->due_amount - $order['chalan_result']->acc_amount;
                                        echo number_format($previous_due, 2, '.', ',') . ' BDT'
                                        ?>
                                    </strong></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right"><strong>Net Payable</strong></td>
                                <?php
                                $net_payable = $order['order']->total_price + $order['order']->vat - $order['order']->discount + $previous_due;
                                ?>
                                <td align="right"><strong><?php echo number_format($net_payable, 2, '.', ',') . ' BDT'; ?></strong></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right"><strong>Paid Amount</strong></td>
                                <td align="right"><strong><?php echo number_format($order['order']->payment_amount, 2, '.', ',') . ' BDT'; ?></strong></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right"><strong>Due</strong></td>
                                <td align="right">
                                    <strong>
                                        <?php
                                        $final_due = $net_payable - $order['order']->payment_amount;
                                        echo number_format($final_due, 2, '.', ',') . ' BDT';
                                        ?>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <strong>Net Payable In Words: <?php echo $this->numbertowords->convert_currency($final_due); ?> </strong>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <tr>
                                <td width="10%">&nbsp;</td>
                                <td width="30%" align="center">
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    ......................................................
                                    <br/>
                                    Client's Signature
                                </td>
                                <td width="20%">&nbsp;</td>
                                <td width="30%" align="center">
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    ......................................................
                                    <br/>
                                    For Bismillah Enterprise
                                </td>
                                <td width="20%">&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="divFooter">Software Developed By: SCORSHIA (01712203022, 01799429789)</div>

<div class="row noPrint">
    <div class="col-md-12">
        <button class="btn btn-default" onclick="printMe()"> <i class="fa fa-print"></i> &nbsp;Print</button>
        <a href="<?php echo base_url() . 'sales'; ?>" class="btn btn-info"> <i class="fa fa-arrow-left"></i> &nbsp;Back</a>
        <a href="<?php echo base_url() . 'sales/edit/' . $order['order']->id; ?>" class="btn btn-primary"> <i class="fa fa-pencil"></i> &nbsp;Edit</a>
        <a href="<?php echo base_url() . 'sales/delete/' . $order['order']->id; ?>" class="btn btn-danger"> <i class="fa fa-trash"></i> &nbsp;Delete</a>
    </div>
</div>
<script>
    function printMe() {
        window.print();
    }
</script>


