<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>client"> Clients</a></li>
                <li class="active">Edit Client</li>
            </ol>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="content">
                <div class="row">
                    <div class="col-md-4">
                        <h4>Edit Client</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo base_url().'client/update/'.$company->id; ?>" method="post">
                            <div class="form-group">
                                <label for="name">Client Name:</label>
                                <input type="text" id="name" value="<?php echo $company->name;?>" required="required" name="name" class="form-control" placeholder="Clients Name eg: MS SS Enterprise"/>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="contact_person">Contact Person:</label>
                                    <input type="text" id="contact_person" value="<?php echo $company->contact_person;?>" name="contact_person" class="form-control" placeholder="Contact Person eg: Mr. John Doe"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="contact_phone">Contact Phone:</label>
                                    <input type="tel" id="contact_phone" value="<?php echo $company->contact_phone;?>" name="contact_phone" class="form-control" placeholder="Contact Phone eg: +880 1711 111 222"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address">Address:</label>
                                <textarea class="form-control" name="address" id="address" cols="30" rows="2" placeholder="Address eg: Suite# 34, Park Avenue, Street# 34, New York, USA"><?php echo $company->address;?></textarea>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="secondary_phone">Secondery Phone:</label>
                                    <input type="tel" id="secondary_phone" value="<?php echo $company->secondary_phone;?>" name="secondary_phone" class="form-control" placeholder="Secondary Phone eg: +880 1711 111 222"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Email Address:</label>
                                    <input type="email" id="email" value="<?php echo $company->email;?>" name="email" class="form-control" placeholder="Email Address eg: johndoe@mail.com"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="website">Website:</label>
                                    <input type="text" id="website" value="<?php echo $company->website;?>" name="website" class="form-control" placeholder="Website eg: http://www.example.com"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="incoming_amount">Due Receivable Till Date:</label>
                                    <input type="number" id="incoming_amount" value="<?php echo $company->incoming_amount;?>" name="incoming_amount" class="form-control" placeholder="Due Till Date eg: 1000"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="notes">Notes:</label>
                                <textarea class="form-control" name="notes" id="notes" cols="30" rows="2" placeholder="Notes eg: Any addtional Notes that need to be stored just for informative purpose"><?php echo $company->notes;?></textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit"><span class="fa fa-save"></span>&nbsp; Update Client</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
