<style>
    @media screen {
        div.divFooter {
            display: none;
        }
    }
    @media print {
        .noPrint {
            display: none;
        }
        div.divFooter {
            position: fixed;
            bottom: 15px;
            right: 15px;
            page-break-after: always;
        }
        @page { margin: 0; }
        body { margin: 1.6cm; }
        #table tr td, #table tr th{
            font-size: 12px;
            line-height: 25px;
            padding: 2px 5px;
        }

    }
</style>
<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active"><?php echo $sub_title ?></li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-5">
                        <h4 class="box-title"><?php echo $sub_title; ?></h4>
                    </div>
                    <div class="col-md-7 text-right noPrint">
                        <form action="<?php echo base_url() . 'reports/purchase_summary'; ?>" method="post" class="form form-inline dateRangeForm">
                            <div class="form-group">
                                <label for="start_date">Start Date</label>
                                <input type="text" name="start_date" id="start_date" class="form-control datepicker input-sm"/>
                            </div>
                            <div class="form-group">
                                <label for="end_date">End Date</label>
                                <input type="text" name="end_date" id="end_date" class="form-control datepicker input-sm"/>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-default" type="submit"><span class="fa fa-refresh"></span></button>
                                <button class="btn btn-default" onclick="printMe()"> <i class="fa fa-print"></i> &nbsp;Print</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <?php
                        $total_sales = 0;
                        $total_payment = 0;
                        $total_discount = 0;
                        $total_vat = 0;
                        $total_due = 0;
                        foreach ($sales as $sale) {
                            $total_sales += $sale->total_price;
                            $total_payment += $sale->payment_amount;
                            $total_discount += $sale->discount;
                            $total_vat += $sale->vat;
                            $total_due += $sale->due_amount;
                        }
                        $total_sales_amount = $total_sales - $total_discount + $total_vat;
                        ?>
                        <small style="padding-right: 22px;">Showing Data from <?php echo $start_date . ' to ' . $end_date; ?></small><br />
                        <small>Total Purchase : <strong><?php echo number_format($total_sales_amount, 2, '.', ',') . ' BDT'; ?></strong> </small> | 
                        <small>Total Payments : <strong> <?php echo number_format($total_payment, 2, '.', ',') . ' BDT'; ?></strong></small> | 
                        <small style="padding-right: 22px;">Total Dues : <strong> <?php echo number_format($total_due, 2, '.', ',') . ' BDT'; ?></strong></small>                       
                    </div>
                </div>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Receipt</th>
                            <th>Date - Time</th>
                            <th>Supplier Name</th>
                            <th>Total Price</th>
                            <!-- <th>Vat</th> -->
                            <th>Discount</th>
                            <th>Net Payable</th>
                            <th>Trans Type</th>
                            <th>Acc Name</th>
                            <th>Paid Amount</th>
                            <th>Due</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($sales as $sale) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $sale->receipt_no; ?></td>
                                <td><?php echo $sale->date . ' - ' . $sale->time; ?></td>
                                <td><?php echo $sale->name; ?></td>
                                <td align="right"><?php echo number_format($sale->total_price, 2, '.', ',') . ' BDT'; ?></td>
                                <!-- <td align="right"><?php echo number_format($sale->vat, 2, '.', ',') . ' ( ' . $sale->vat_percentage . ' % )'; ?></td> -->
                                <td align="right"><?php echo number_format($sale->discount, 2, '.', ',') . ' ( ' . $sale->discount_percentage . ' % )'; ?></td>
                                <td align="right"><?php
                                    $net_payable = $sale->total_price + $sale->vat - $sale->discount;
                                    echo number_format($net_payable, 2, '.', ',') . ' BDT';
                                    ?></td>
                                <td align="right"><?php echo $sale->tr_type; ?></td>
                                <td align="right"><?php echo $sale->res_acc; ?></td>
                                <td align="right"><?php echo number_format($sale->payment_amount, 2, '.', ',') . ' BDT'; ?></td>
                                <td align="right"><?php echo number_format($sale->due_amount, 2, '.', ',') . ' BDT'; ?></td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(".datepicker").datepicker({
        dateFormat: "yy-mm-dd"
    });
</script>
<script>
    function printMe() {
        window.print();
    }
</script>