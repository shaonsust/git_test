<style>
    @media screen {
        div.divFooter {
            display: none;
        }
    }
    @media print {
        .noPrint {
            display: none;
        }
        div.divFooter {
            position: fixed;
            bottom: 15px;
            right: 15px;
            page-break-after: always;
        }
        @page { margin: 0; }
        body { margin: 1.6cm; }
        #table tr td, #table tr th{
            font-size: 12px;
            line-height: 20px;
            padding: 2px 5px;
        }
    }
</style>
<style type="text/css">
    .table-condensed th, .table-condensed td {
        font-size: 12px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active"><?php echo $sub_title ?></li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="box-title"><?php echo $sub_title; ?></h4>
                    </div>
                    <div class="col-md-6 text-right noPrint">
                        <form action="<?php echo base_url() . 'reports/monthly_salary_sheet'; ?>" method="post" class="form form-inline dateRangeForm">
                            <div class="form-group">
                                <label for="month">Month</label>
                                <select name="month" id="month">
                                    <?php
                                    for ($i = 1; $i < 13; $i++) {
                                        $nmonth = date('F', mktime(0, 0, 0, $i));
                                        ?>
                                        <option value="<?php echo $nmonth; ?>" <?php
                                        if ($nmonth == $month) {
                                            echo 'selected';
                                        }
                                        ?>><?php echo $nmonth; ?></option>
                                                <?php
                                            }
                                            ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="year">Year</label>
                                <input type="text" name="year" id="year" class="form-control input-sm numOnly" value="<?php
                                if (isset($year) && !empty($year)) {
                                    echo $year;
                                }
                                ?>"/>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-default" type="submit"><span class="fa fa-refresh"></span></button>
                                <button class="btn btn-default" onclick="printMe()"> <i class="fa fa-print"></i> &nbsp;Print</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable table-condensed">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Card No</th>
                            <th>Name</th>
                            <th>Basic</th>
                            <th>Daily Salary</th>
                            <th>Hourly Salary</th>
                            <th>Present</th>
                            <th>OT Hours</th>
                            <th>OT Amount</th>
                            <th>Deduction Hours</th>
                            <th>Deduction Amount</th>
                            <th>Bonus</th>
                            <th>Trans Type</th>
                            <th>Acc Name</th>
                            <th>Net Salary</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        $total_payed = 0;
                        foreach ($salaries as $salary) {
                            ?>
                            <tr>
                                <td><?php echo $salary->date; ?></td>
                                <td><?php echo $salary->card_no; ?></td>
                                <td><?php echo $salary->name; ?></td>
                                <td align="right"><?php echo number_format($salary->basic_salary, 2, '.', ',') . ' /='; ?></td>
                                <td align="right"><?php echo number_format($salary->daily_salary, 2, '.', ',') . ' /='; ?></td>
                                <td align="right"><?php echo number_format($salary->hourly_salary, 2, '.', ',') . ' /='; ?></td>
                                <td align="right"><?php echo number_format($salary->presence, 2, '.', ','); ?></td>
                                <td align="right"><?php echo number_format($salary->overtime_hours, 2, '.', ','); ?></td>
                                <td align="right"><?php echo number_format($salary->overtime_amount, 2, '.', ',') . ' /='; ?></td>
                                <td align="right"><?php echo number_format($salary->deduction_hours, 2, '.', ','); ?></td>
                                <td align="right"><?php echo number_format($salary->deduction_amount, 2, '.', ',') . ' /='; ?></td>
                                <td align="right"><?php echo number_format($salary->bonus_amount, 2, '.', ',') . ' /='; ?></td>
                                <td align="right"><?php echo $salary->tr_type; ?></td>
                                <td align="right"><?php echo $salary->res_acc; ?></td>
                                <td align="right"><?php echo number_format($salary->net_payable, 2, '.', ',') . ' /='; ?></td>
                            </tr>
                            <?php
                            $i++;
                            $total_payed += $salary->net_payable;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function printMe() {
        window.print();
    }
</script>