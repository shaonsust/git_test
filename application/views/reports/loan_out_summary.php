<style>
    @media screen {
        div.divFooter {
            display: none;
        }
    }
    @media print {
        .noPrint {
            display: none;
        }
        div.divFooter {
            position: fixed;
            bottom: 15px;
            right: 15px;
            page-break-after: always;
        }
        @page { margin: 0; }
        body { margin: 1.6cm; }
        #table tr td, #table tr th{
            font-size: 12px;
            line-height: 25px;
            padding: 2px 5px;
        }

    }
</style>
<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active"><?php echo $sub_title ?></li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="box-title"><?php echo $sub_title; ?></h4>
                    </div>
                    <div class="col-md-6 text-right noPrint">
                        <form action="<?php echo base_url() . 'reports/loan_out_summary'; ?>" method="post" class="form form-inline dateRangeForm">
                            <div class="form-group">
                                <label for="start_date">Start Date</label>
                                <input type="text" name="start_date" id="start_date" class="form-control datepicker input-sm"/>
                            </div>
                            <div class="form-group">
                                <label for="end_date">End Date</label>
                                <input type="text" name="end_date" id="end_date" class="form-control datepicker input-sm"/>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-default" type="submit"><span class="fa fa-refresh"></span></button>
                                <button class="btn btn-default" onclick="printMe()"> <i class="fa fa-print"></i> &nbsp;Print</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <small style="padding-right: 22px;">Showing Data from <?php echo $start_date . ' to ' . $end_date; ?></small>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Transaction No</th>
                            <th>Date - Time</th>
                            <th>Loan Source</th>
                            <th>Purpose</th>
                            <th>Trans Type</th>
                            <th>Acc Name</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($loans as $loan) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $loan->receipt_no; ?></td>
                                <td><?php echo $loan->date . ' - ' . $loan->time; ?></td>
                                <td><?php echo $loan->name; ?></td>
                                <td><?php echo $loan->purpose; ?></td>
                                <td align="right"><?php echo $loan->tr_type; ?></td>
                                <td align="right"><?php echo $loan->res_acc; ?></td>
                                <td align="right"><?php echo number_format($loan->amount, 2, '.', ','); ?></td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(".datepicker").datepicker({
        dateFormat: "yy-mm-dd"
    });
</script>
<script>
    function printMe() {
        window.print();
    }
</script>