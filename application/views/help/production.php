    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a target="_blank" href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></h4>
            </div>
            <div class="box-body">
                <p>
				    FFML allowes you to keep track of productions. From the Finished goods page you may add new production data for a particular finished good.
                    <br />
                    On the left navigation there is an item "Production", Click on it. Or just <a target="_blank" href="<?php echo base_url(); ?>production">Click here</a> <br />
                    A new page will appear with all production details.
				</p>                 
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Production Actions</h4>
            </div>
            <div class="box-body">
                <p>
                    In the production page you will find a table with all production entries. In each row of that table you will have an action field with following buttons for each particular paroduction entry.                  
                </p>
                <div class="row">
                    <div class="col-md-1">                        
                        <a href="#" disabled="true" class="btn btn-warning btn-disabled" title="Edit"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="col-md-11">
                        Clicking this will open a new page with a form to edit that particular production enty. Note: You need administrative permission to proceed with this action <br />
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-1">                        
                        <a href="#" disabled="true" class="btn btn-danger btn-disabled" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                    </div>
                    <div class="col-md-11">
                        Clicking this allow you to delete that particular production entry. Note: You need administrative permission to proceed with this action. <br /> <br />
                    </div>
                </div>                               
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Adding new production entry</h4>
            </div>
            <div class="box-body">
                <p>
                    On the left navigation there is an item "Finished Goods", Click on it. Or just <a target="_blank" href="<?php echo base_url(); ?>finished_good">Click here</a> <br />
                    A new page will appear with all available Finished Goods in the inventory. In that page you will find a table with all available Finished Good and their basic informations. In each row of that table you will have an action field with the following button for each particular Finished Good                  
                </p>                               
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" disabled = "true" class="btn btn-primary btn-disabled" title="New Production"><i class="fa fa-plus"></i></a>
                    </div>
                    <div class="col-md-11">
                        Clicking this allow you to add production data for that particular finished good. Note: Adding new production will increase the stock of that particular finished good<br /> <br />
                    </div>
                </div>                  
            </div>
        </div>
    </div>    
</div>



