    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a target="_blank" href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></h4>
            </div>
            <div class="box-body">
                <p>
				    FFML allowes you to Add Utility Items to the Inventory. After adding Utility Item to inventory you can purchase Utility Items, use Utility Items, sell Utility Items. You may also edit or delete Utility Items from this application. 
                    <br />
                    On the left navigation there is an item "Utility Items", Click on it. Or just <a target="_blank" href="<?php echo base_url(); ?>utility_item">Click here</a> <br />
                    A new page will appear with all available Utility Items in the inventory. In that page you will find a table with available Utility Items. You can search for a specific Utility Item from the search box on the top right corner above the table.
				</p>                 
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Adding Utility Items to Inventory</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <a target="_blank" class="btn btn-primary btn-block" href="<?php echo base_url();?>utility_item/add_new"><span class="fa fa-plus"></span> &nbsp; Add New</a>
                    </div>
                    <div class="col-md-9">
                        There is a button on the top of the Utility Item table. That looks similar to the button on the left side. By clicking this (either from here or from the Utility Items page) you will have a form to add new Utility Item to the system.
                    </div>
                </div>             
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Purchasing Utility Item</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <a target="_blank" class="btn btn-primary btn-block" href="<?php echo base_url();?>purchase/new_purchase/utility_item"><span class="fa fa-barcode"></span> &nbsp; Purchase</a>
                    </div>
                    <div class="col-md-9">
                        There is a button on the top of the Utility Item table. That looks similar to the button on the left side. By clicking this (either from here or from the Utility Items page) you will have a form to purchase Utility Items. <br />
                        To Learn more about purchasing items <a href="<?php echo base_url()?>help/purchase"></a>
                    </div>
                </div>           
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Using Utility Items</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <a target="_blank" class="btn btn-primary btn-block" href="<?php echo base_url();?>usage/new_usage/utility_item"><span class="fa fa-exchange"></span> &nbsp; Use</a>
                    </div>
                    <div class="col-md-9">
                        There is a button on the top of the Utility Item table. That looks similar to the button on the left side. By clicking this (either from here or from the Utility Items page) you will have a form to use Utility Items for any purpose. <br />
                        To Learn more about using items <a href="<?php echo base_url()?>help/usage"></a>
                    </div>
                </div>                             
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Utility Item Actions</h4>
            </div>
            <div class="box-body">
                <p>
                    On the left navigation there is an item "Utility Items", Click on it. Or just <a target="_blank" href="<?php echo base_url(); ?>utility_item">Click here</a> <br />
                    A new page will appear with all available Utility Items in the inventory. In that page you will find a table with all available Utility Item and their basic informations. In each row of that table you will have an action field with following buttons for each particular Utility Item                  
                </p>                
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" disabled="true" class="btn btn-info btn-disabled" title="Details"><i class="fa fa-list"></i></a>                        
                    </div>
                    <div class="col-md-11">
                        Clicking this will open a new page with detailed information of that particular Utility Item. On that page by default data is shown from the begining to till date. If you want you can select custom date range for more specific details. <br /> <br />
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-1">                        
                        <a href="#" disabled="true" class="btn btn-warning btn-disabled" title="Edit"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="col-md-11">
                        Clicking this will open a new page with a form to edit that particular Utility Item. Note: You need administrative permission to proceed with this action <br />
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-1">                        
                        <a href="#" disabled="true" class="btn btn-danger btn-disabled" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                    </div>
                    <div class="col-md-11">
                        Clicking this allow you to delete that particular Utility Item. Note: You need administrative permission to proceed with this action. <br /> <br />
                    </div>
                </div>                               
            </div>
        </div>
    </div>    
</div>



