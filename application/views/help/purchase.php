    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a target="_blank" href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></h4>
            </div>
            <div class="box-body">
                <p>
				    FFML application allowes you to purchase raw materials, finished goods and Utility Items.
                    <br />
                    On the left navigation there is an item "Purchase", Click on it. or <a href="<?php echo base_url()?>purchase">Click here</a> to visit the purchase page. Here you will find a table with all the purchase entries with unique Receipt number and other details. By clicking on any receipt number you will be able to view that particular purchase receipt.
				</p>                 
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Purchase some item</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">                        
                        <a class="btn btn-primary" href="<?php echo base_url();?>purchase/new_purchase"><span class="fa fa-plus"></span> &nbsp; New Purchase</a>
                    </div>
                    <div class="col-md-9">
                        In the purchase page you can find this button above the purchase table. Click on it and you will find the purchase form. Add items to the order select a supplier and make a purchase order.
                    </div>
                </div>                             
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Purchase form</h4>
            </div>
            <div class="box-body">
                <p>
                    There are two parts in the purchase form. The left one have date, time, supplier, Purchase Bill/Chalan/Receipt No and a table for selected Items in the order. The supplier field has autocomplete features enabled. When you start typing any name in the supplier field it will suggest you supplier names maching your input. If there is an order made before for your particular supplier then you will surely find the supplier name in suggestions. If you find your supplier on the suggestions then select that particular supplier. In case of new supplier you just type the supplier's name. Select date and time of the purchase (By default it will show current date and time on that fields). In the Purchase Bill/Chalan/Receipt No provide necessary number for a document originally given by the supplier.
                    <br /><br />
                    On the right side of the form it shows available items. You can add any of the item by pressing the <button class="btn btn-sm bg-olive"><span class="fa fa-plus"></span></button> signed button next to each item. You can add quantity before or after adding an item to purchase order. There is a search box on the Available items box. You may try using it to find any item easily. 

                </p>                               
                <div class="row">
                    <div class="col-md-3">
                        <button disabled="true" class="btn btn-primary btn-block"><span class="fa fa-save"></span>&nbsp; Process Order</button>
                    </div>
                    <div class="col-md-9">
                        When you are done with adding items in purchase order with necessary quantity then press the process order button on the bottom of the left side box. The order will be processed and a purchase receipt with unique receipt number will be generated. If you want you can print it or leave it as it is.
                    </div>
                </div>                  
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Actions of Purchase Receipt</h4>
            </div>
            <div class="box-body">
                <p>
                    On the purchase main page there is a table with all purchase receipts. To view a purchase receipt you have to click on the receipt number. Then the purchase receipt will be shown. There are few buttons on the top right corner of the receipt.
                </p>   
                <div class="row">
                    <div class="col-md-2">
                        <button class="btn btn-default btn-disabled btn-block" disabled="true"> <i class="fa fa-print"></i> &nbsp;Print</button>
                    </div>
                    <div class="col-md-10">
                        <p>If you want, you may print the purchase receipt by clicking this button.</p> <br />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <a href="#" class="btn btn-primary btn-disabled btn-block" disabled="true"> <i class="fa fa-pencil"></i> &nbsp;Edit</a>
                    </div>
                    <div class="col-md-10">
                        <p>
                            If you think there is some correction needed in the purchase receipt then you may edit the purchase receipt by clicking this button.
                        </p><br />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <a href="#" class="btn btn-danger btn-disabled btn-block" disabled="true"> <i class="fa fa-trash"></i> &nbsp;Delete</a>
                    </div>
                    <div class="col-md-10">
                        <p>If in case a purchase receipt is no longer needed or may be generated by mistake then you may want to delete that purchase receipt. You may delete a purchase receipt by clicking this button</p>
                    </div>
                </div>
                <p>
                    Note: For Editing or Deleting a purchase receipt you must have to have administrative permission. If you have permission and you delete any purchase receipt it will also delete related information and adjust the regarding items stock, payments and dues in the system.
                </p>
                <p>
                    On the main purchase page there is a table with all purchase receipt. You can edit or delete a purchase receipt from there also.
                </p>  
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" disabled="true" class="btn btn-warning btn-disabled" title="Edit"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will be able to edit a purchase receipt.
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" disabled="true" class="btn btn-danger btn-disabled" title="Delete"><i class="fa fa-remove"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will be able to delete a purchase receipt.
                    </div>
                </div>                  
            </div>
        </div>
    </div>    
</div>



