    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a target="_blank" href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></h4>
            </div>
            <div class="box-body">
                <p>
				    FFML application has a module called Loan Management. Which allows you to keep track of Loans, wheather it is incoming or outgoing. It also allowes you to keep track of loan installments for both incoming and outgoing loans. To use loan module first of all you have to create loan sources. Loan sources are usually individual persons or companies. Whether you take a loan or give a loan to a person or a company then that individual person or company will be treated as a loan source. Loan management is quite bigger then the other modules so its recommended to read the loan management help guide thoroughly before using it.
                    <br />
                    On the left navigation there is an item "Loan Management", Click on it. It will have Incoming Loans, Outgoing Loans and Loan Sources. <br />
                    To take an incoming loan or view all incoming loans click on Incoming Loans from the menu or <a href="<?php echo base_url()?>loan/loans/incoming_loan">Click here</a> <br />
                    To give an outgoing loan or view all outgoing loans click on Outgoing Loans from the menu or <a href="<?php echo base_url()?>loan/loans/outgoing_loan">Click here</a> <br />
                    To create a loan source or view all loan sources click Loan Sources from the menu or <a href="<?php echo base_url()?>loan/sources">Click here</a>
				</p>                 
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Loan Sources</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <p>As mentioned above to use loan management module first of all you have to create loan sources. Which may be any individual person or a company. To create Loan Source go to Loan Management -> Loan Sources page from the menu or <a href="<?php echo base_url()?>loan/sources">Click here</a> You will find a table with existing loan sources. If there is any loan source available then you will see one row per loan source in the table. Click on the Source name and you will see a page with detailed information about all kind of transaction related with that particular loan source.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">                        
                        <a class="btn btn-primary" href="<?php echo base_url();?>loan/add_new_soucre"><span class="fa fa-plus"></span> &nbsp; Add New Loan Source</a>
                    </div>
                    <div class="col-md-9">
                        <p>To add a loan source click on the Add New Loan Source button from the Loan Sources page and fill out the form with necessary information. Then click on Save Loan source. A new loan source will be created in the system.</p>
                    </div>
                </div>                             
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Incoming Loans </h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <p>                        
                            Incoming lonas are the loans you have taken from any loan sources. To view all incoming loans or to create new incoming loan click on Loan Management -> Incoming Loans or <a href="<?php echo base_url()?>loan/loans/incoming_loan">Click here</a> You will have a list of all existing Incoming loans in a table. To view detailed information about any specific loan click on the Transaction number for that particular loan. You will see every details possible for that particular loan such as Loan source details, Loan Purpose, Description and all installments paid. You will also see a form that allowes you to pay installment for that particular loan.
                        </p>                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">                        
                        <a class="btn btn-primary" href="<?php echo base_url();?>loan/new_loan/incoming_loan"><span class="fa fa-plus"></span> &nbsp; New Incoming Loan</a>
                    </div>
                    <div class="col-md-9">
                    <h4>Create an Incoming Loan</h4>
                        <p>                            
                            In the Incoming Loans page you can find this button above the Loans table. Click on it and you will find the Incoming Loans form. Provide Amount, Date, Time, Purpose and description for a particular Incoming Loan. Type Loan source name in the Loan Source field. As Loan source field has autocomplete feature enabled you will find suggestion if the source name is available in the system. Then Click <button class="btn btn-primary btn-disabled" disabled="true"><span class="fa fa-save"></span>&nbsp; Save Payment</button>. A new entry will be submitted and a receipt will be generated for that particular transaction.
                        </p>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-12">
                        <h4>Making and Installment Payment</h4>
                        <p>
                            In the incoming loans page find your desired loan for paying installment. Click on the transaction number for that particular loan and a new page will be loaded with detailed information about that loan. You will see a form for "Pay New Installment for this Loan" on the top right corner of the page. Provide Amount, Date, Time and description (if needed). Then click on Save Installment button. A loan installment payment entry will be added.
                        </p>
                    </div>
                </div>                            
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Outgoing Loans </h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <p>                        
                            Outgoing lonas are the loans you have given to any loan sources. To view all outgoing loans or to create new outgoing loan click on Loan Management -> Outgoing Loans or <a href="<?php echo base_url()?>loan/loans/outgoing_loan">Click here</a> You will have a list of all existing outgoing loans in a table. To view detailed information about any specific loan click on the Transaction number for that particular loan. You will see every details possible for that particular loan such as Loan source details, Loan Purpose, Description and all installments received. You will also see a form that allowes you to receive installment for that particular loan.
                        </p>                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">                        
                        <a class="btn btn-primary" href="<?php echo base_url();?>loan/new_loan/outgoing_loan"><span class="fa fa-plus"></span> &nbsp; New Outgoing Loan</a>
                    </div>
                    <div class="col-md-9">
                    <h4>Create an Outgoing Loan</h4>
                        <p>                            
                            In the Outgoing Loans page you can find this button above the Loans table. Click on it and you will find the Incoming Loans form. Provide Amount, Date, Time, Purpose and description for a particular outgoing Loan. Type Loan source name in the Loan Source field. As Loan source field has autocomplete feature enabled you will find suggestion if the source name is available in the system. Then Click <button class="btn btn-primary btn-disabled" disabled="true"><span class="fa fa-save"></span>&nbsp; Save Payment</button>. A new entry will be submitted and a receipt will be generated for that particular transaction.
                        </p>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-12">
                        <h4>Making and Installment Payment(Receiving)</h4>
                        <p>
                            In the outgoing loans page find your desired loan for receiving installment. Click on the transaction number for that particular loan and a new page will be loaded with detailed information about that loan. You will see a form for "Pay New Installment for this Loan" on the top right corner of the page. Provide Amount, Date, Time and description (if needed). Then click on Save Installment button. A loan installment payment entry will be added.
                        </p>
                    </div>
                </div>                            
            </div>
        </div>
           
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Actions of Loan Management</h4>
            </div>
            <div class="box-body">
                <p>
                    In loan management there are loan sources and loan transactions. From any of the loan entries or installment tables click on any transaction number and you will see all details about that particular loan transactionand installments. By clicking on Any source name from any table you will find all incoming and outgoing loans and installments for that particular loan source.
                </p> 
                <p>
                    On source page and loan pages you will find following buttons in each row of tables.
                </p>  
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" disabled="true" class="btn btn-warning btn-disabled" title="Edit"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will be able to edit that corresponding entry(loan or source)
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" disabled="true" class="btn btn-danger btn-disabled" title="Delete"><i class="fa fa-remove"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will be able to delete that corresponding entry(loan or source)
                    </div>
                </div>   
                <p>
                    Note: For Editing or Deleting a loan/loan installment/source you must have to have administrative permission. If you have permission and you delete any loan/loan installment/source it will also delete related information and adjust the regarding payments and dues in the system.
                </p>               
            </div>
        </div>
    </div>    
</div>



