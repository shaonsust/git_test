    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a target="_blank" href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></h4>
            </div>
            <div class="box-body">
                <p>
				    FFML allowes you to Add Finished Goods to the Inventory. After adding Finished Good to inventory you can purchase Finished Goods, use Finished Goods, sell Finished Goods. You may also edit or delete Finished Goods from this application. 
                    <br />
                    On the left navigation there is an item "Finished Goods", Click on it. Or just <a target="_blank" href="<?php echo base_url(); ?>finished_good">Click here</a> <br />
                    A new page will appear with all available Finished Goods in the inventory. In that page you will find a table with available Finished Goods. You can search for a specific Finished Good from the search box on the top right corner above the table.
				</p>                 
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Adding Finished Goods to Inventory</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <a target="_blank" class="btn btn-primary btn-block" href="<?php echo base_url();?>finished_good/add_new"><span class="fa fa-plus"></span> &nbsp; Add New</a>
                    </div>
                    <div class="col-md-9">
                        There is a button on the top of the Finished Good table. That looks similar to the button on the left side. By clicking this (either from here or from the Finished Goods page) you will have a form to add new Finished Good to the system.
                    </div>
                </div>             
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Purchasing Finished Good</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <a target="_blank" class="btn btn-primary btn-block" href="<?php echo base_url();?>purchase/new_purchase/finished_good"><span class="fa fa-barcode"></span> &nbsp; Purchase</a>
                    </div>
                    <div class="col-md-9">
                        There is a button on the top of the Finished Good table. That looks similar to the button on the left side. By clicking this (either from here or from the Finished Goods page) you will have a form to purchase Finished Goods. <br />
                        To Learn more about purchasing items <a href="<?php echo base_url()?>help/purchase"></a>
                    </div>
                </div>           
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Using Finished Goods</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <a target="_blank" class="btn btn-primary btn-block" href="<?php echo base_url();?>usage/new_usage/finished_good"><span class="fa fa-exchange"></span> &nbsp; Use</a>
                    </div>
                    <div class="col-md-9">
                        There is a button on the top of the Finished Good table. That looks similar to the button on the left side. By clicking this (either from here or from the Finished Goods page) you will have a form to use Finished Goods for any purpose. <br />
                        To Learn more about using items <a href="<?php echo base_url()?>help/usage"></a>
                    </div>
                </div>                             
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Selling Finished Goods</h4>
            </div>
            <div class="box-body"> 
                <div class="row">
                    <div class="col-md-3">
                        <a target="_blank" class="btn btn-primary btn-block" href="<?php echo base_url();?>sales/sale/finished_good"><span class="fa fa-usd"></span> &nbsp; Sale</a>
                    </div>
                    <div class="col-md-9">
                        There is a button on the top of the Finished Good table. That looks similar to the button on the left side. By clicking this (either from here or from the Finished Goods page) you will have a form to sell Finished Goods. <br />
                        To Learn more about selling items <a href="<?php echo base_url()?>help/sales"></a>
                    </div>
                </div>               
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Finished Good Actions</h4>
            </div>
            <div class="box-body">
                <p>
                    On the left navigation there is an item "Finished Goods", Click on it. Or just <a target="_blank" href="<?php echo base_url(); ?>finished_good">Click here</a> <br />
                    A new page will appear with all available Finished Goods in the inventory. In that page you will find a table with all available Finished Good and their basic informations. In each row of that table you will have an action field with following buttons for each particular Finished Good                  
                </p>                
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" disabled="true" class="btn btn-info btn-disabled" title="Details"><i class="fa fa-list"></i></a>                        
                    </div>
                    <div class="col-md-11">
                        Clicking this will open a new page with detailed information of that particular Finished Good. On that page by default data is shown from the begining to till date. If you want you can select custom date range for more specific details. <br /> <br />
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-1">                        
                        <a href="#" disabled="true" class="btn btn-warning btn-disabled" title="Edit"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="col-md-11">
                        Clicking this will open a new page with a form to edit that particular Finished Good. Note: You need administrative permission to proceed with this action <br />
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-1">                        
                        <a href="#" disabled="true" class="btn btn-danger btn-disabled" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                    </div>
                    <div class="col-md-11">
                        Clicking this allow you to delete that particular product. Note: You need administrative permission to proceed with this action. <br /> <br />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" disabled="true" class="btn btn-primary btn-disabled" title="New Production"><i class="fa fa-plus"></i></a>
                    </div>
                    <div class="col-md-11">
                        Clicking this allow you to add production data for that particular finished good. Note: Adding new production will increase the stock of that particular finished good<br /> <br />
                    </div>
                </div>                  
            </div>
        </div>
    </div>    
</div>



