    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a target="_blank" href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></h4>
            </div>
            <div class="box-body">
                <p>
				    FFML application has a module called Human Resource Management. Which allowes you to create, edit and delete employees. It also allowes you to keep track of salary payments. It provides a monthly salary sheet report as well.
                    <br />
                    On the left navigation there is an item "Human Resource", Click on it. It will have Employees and Salary Payments. <br />
                    To view all employees or create new employee click on Human Resource -> Employees from the menu or <a href="<?php echo base_url()?>hr/employees">Click here</a> <br />
                    To view all salary payments or to make a new salary payment click on Human Resource -> Salary Payments from the menu or <a href="<?php echo base_url()?>loan/loans/outgoing_loan">Click here</a>
                    <br />
                    To view monthly salary sheet click on Reports -> Monthly Salary Sheet from the menu or <a href="<?php echo base_url()?>reports/monthly_salary_sheet">Click here</a> 
				</p>                 
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Create Employees</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">                        
                        <a class="btn btn-primary" href="<?php echo base_url();?>hr/add_new_employee"><span class="fa fa-plus"></span> &nbsp; Add New Employee</a>
                    </div>
                    <div class="col-md-9">
                        <p>To add a new employee click on Add new employee from Employees page and fill up the form with necessary data then save it by clicking <button class="btn btn-primary btn-disabled" disabled="true"><span class="fa fa-save"></span>&nbsp; Save Employee</button></p>
                    </div>
                </div>                             
            </div>
        </div>
           
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Actions available for employees</h4>
            </div>
            <div class="box-body">
                <p>
                    In Employees page you will see a table with all employees available in the system. In each row of the table you will find following action buttons.
                </p> 
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" class="btn btn-info btn-disabled" disabled="true" title="Details"><i class="fa fa-list"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will see a new page with all possible details of that particular employee including salary payments. <br /><br />
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" class="btn btn-primary btn-disabled" disabled="true" title="Edit"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will be able to edit an employee. <br /><br />
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" class="btn btn-danger btn-disabled" disabled="true" title="Delete"><i class="fa fa-remove"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will be able to delete an employee. <br /><br />
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" class="btn bg-purple btn-disabled" disabled="true" title="Pay Salary"><i class="fa fa-money"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will get a form to pay salary. Select month year and provide amounts then save the form. A salary payment receipt will be generated. Print it or edit/delete it if needed.
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" class="btn bg-olive btn-disabled" disabled="true" title="Current Status: Inactive | Mark as Active?"><i class="fa fa-chain"></i></a>
                    </div>
                    <div class="col-md-11">
                        This button is conditional. If the employee's current status is Inactive then you will see this button. By clicking it you will be able to mark an employee as Active.
                    </div>
                </div>   
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" class="btn bg-orange btn-disabled" disabled="true" title="Current Status: Active | Mark as Inactive?"><i class="fa fa-chain-broken"></i></a>
                    </div>
                    <div class="col-md-11">
                        This button is conditional. If the employee's current status is Active then you will see this button. By clicking it you will be able to mark an employee as Inactive.
                    </div>
                </div>   
                <p>
                    Note: For Editing or Deleting any employee or any salary payment you must have to have administrative permission. If you have permission and you delete any employee or salary payment it will also delete related information and adjust the regarding payments and dues in the system.
                </p>               
            </div>
        </div>
    </div>    
</div>



