    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a target="_blank" href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['title'];?> - Software Overview </h4>
            </div>
            <div class="box-body">
                <p>
                    FFML is an ERP (Enterprise Resource Planner) Application which is a combination of multiple stand alone softwares. FFML is an web application which consists of following stand alone softwares:                    
                </p>
                <ul>
                    <li><a target="_blank" href="<?php echo base_url()?>raw_material">Raw Material Management</a></li>
                    <li><a target="_blank" href="<?php echo base_url()?>finished_good">Finished Goods Management</a></li>
                    <li><a target="_blank" href="<?php echo base_url()?>utility_item">Utility Items Management</a></li>
                    <li><a target="_blank" href="<?php echo base_url()?>production">Production Management</a></li>
                    <li>
                        Item Usage Management <br />
                        <a target="_blank" href="<?php echo base_url()?>usage/index/raw_material">Raw Material</a> | 
                        <a target="_blank" href="<?php echo base_url()?>usage/index/finished_good">Finished Goods</a> | 
                        <a target="_blank" href="<?php echo base_url()?>usage/index/utility_item">Utility Items</a>
                    </li>
                    <li><a target="_blank" href="<?php echo base_url()?>sales">Sales Management</a></li>
                    <li>
                        General Accounting <br />
                        <a target="_blank" href="<?php echo base_url()?>accounting/index/income">Income</a> | 
                        <a target="_blank" href="<?php echo base_url()?>accounting/index/expense">Expense</a> | 
                        <a target="_blank" href="<?php echo base_url()?>accounting/index/supplier_due">Supplier Due Management</a> | 
                        <a target="_blank" href="<?php echo base_url()?>accounting/index/client_due">Client Due Management</a>
                    </li>
                    <li>
                        Loan Management <br />
                        <a target="_blank" href="<?php echo base_url()?>loan/loans/incoming_loan">Incoming Loan</a> | 
                        <a target="_blank" href="<?php echo base_url()?>loan/loans/outgoing_loan">Outgoing Loan</a> | 
                        <a target="_blank" href="<?php echo base_url()?>loan/sources">Loan Sources</a> | 
                    </li>
                    <li>
                        Human Resource Management <br />
                        <a target="_blank" href="<?php echo base_url()?>hr/employees">Employees</a> | 
                        <a target="_blank" href="<?php echo base_url()?>hr/salary">Salary Payments</a>
                    </li>
                    <li><a target="_blank" href="<?php echo base_url()?>supplier">Suppliers Management</a></li>
                    <li><a target="_blank" href="<?php echo base_url()?>client">Clients Management</a></li>
                    <li><a target="_blank" href="#">Reports</a></li>
                    <li><a target="_blank" href="#">Help</a></li>
                </ul>
                
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['title'];?> - Getting Started </h4>
            </div>
            <div class="box-body">
                <p>
                    This Help Guide will provide necessary Information about the sofware. If you have just started using this application or facing any problem finding any useful section, This help guide will surely help you to get familiar with this FFML Application. In the left side Navigation bar you can find an Item Named "Help Guide". Click on it and you will find help topics. Just click on your desired topic and you will see description about your topic. We recommend you to read all the topics before operating with the application.
                </p>
                <h4>Getting Started</h4>
                <p>
                    If you are first time using this software, This section will help you understand where to start. Asuming you want to produce and sell finished goods. So you need to add raw materials, suppliers to purchas raw material, clients to sell finished goods.
                </p>
                <ul>
                    <li><a href="<?php echo base_url(); ?>help/account">Complete your Profile.</a></li>
                    <li><a href="<?php echo base_url(); ?>help/raw_material">Add Raw Materials</a></li>
                    <li><a href="<?php echo base_url(); ?>help/finished_good">Add Finished Goods</a></li>
                    <li><a href="<?php echo base_url(); ?>help/supplier">Add Supplier</a></li>
                    <li><a href="<?php echo base_url(); ?>help/client">Add Clients</a></li>
                    <li><a href="<?php echo base_url(); ?>help/purchase">Purchase Raw Material</a></li>
                    <li><a href="<?php echo base_url(); ?>help/raw_material">Use Raw Material</a></li>
                    <li><a href="<?php echo base_url(); ?>help/finished_good">Produce Finished Goods</a></li>
                    <li><a href="<?php echo base_url(); ?>help/sales">Sell product</a></li>
                </ul>

            </div>
        </div>
    </div>
</div>


