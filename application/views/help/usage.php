    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a target="_blank" href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></h4>
            </div>
            <div class="box-body">
                <p>
				    FFML application allowes you to use items from the inventory.
                    <br />
                    On the left navigation there is an item "Item Usage", Click on it. A collapsible menu will be open with three links <a href="<?php echo base_url()?>usage/index/raw_material">Raw Material</a> , <a href="<?php echo base_url()?>usage/index/finished_good">Finished Goods</a>, <a href="<?php echo base_url()?>usage/index/utility_item">Utility Items</a> <br />
                    By clicking any of that link you will have a new page open with a table of usage details for your selected item. From that page you can add new item usage data by clicking on a button named "New <em>Item</em> Usage" (Item stands for your selected item type which may be Raw Material / Finished Good or Utility Item)
				</p>                 
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Item Usage Actions</h4>
            </div>
            <div class="box-body">
                <p>
                    In the Item usage page you will find a table with all usage entries. In each row of that table you will have an action field with following buttons for each particular usage entry.                  
                </p>
                <div class="row">
                    <div class="col-md-1">                        
                        <a href="#" disabled="true" class="btn btn-warning btn-disabled" title="Edit"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="col-md-11">
                        Clicking this will open a new page with a form to edit that particular usage enty. Note: You need administrative permission to proceed with this action <br />
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-1">                        
                        <a href="#" disabled="true" class="btn btn-danger btn-disabled" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                    </div>
                    <div class="col-md-11">
                        Clicking this allow you to delete that particular usage entry. Note: You need administrative permission to proceed with this action. <br /> <br />
                    </div>
                </div>                               
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Adding new usage entry</h4>
            </div>
            <div class="box-body">
                <p>
                    On the left navigation there is an item "Item Usage", Click on it. A collapsible menu will be open with three links <a href="<?php echo base_url()?>usage/index/raw_material">Raw Material</a> , <a href="<?php echo base_url()?>usage/index/finished_good">Finished Goods</a>, <a href="<?php echo base_url()?>usage/index/utility_item">Utility Items</a> <br />
                    By clicking any of that link you will have a new page open with a table of usage details for your selected item. From that page you can add new item usage data by clicking on a button named "New <em>Item</em> Usage" (Item stands for your selected item type which may be Raw Material / Finished Good or Utility Item)
                    <br />
                    Or you may add new usage data from any kind of item page. On each of the items pages you will have the following button above the table.

                </p>                               
                <div class="row">
                    <div class="col-md-3">
                        <a disabled="true" class="btn btn-primary btn-block" href="#"><span class="fa fa-exchange"></span> &nbsp; Use</a>
                    </div>
                    <div class="col-md-9">
                        There is a button on the top of the Finished Good table. That looks similar to the button on the left side. By clicking this you will have a form to use items for any purpose.
                    </div>
                </div>                  
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">The usage entry form</h4>
            </div>
            <div class="box-body">
                <p>
                    When ever you want to use any item (by following previously described steps) you will get a form to provide usage details.  <br />
                    There are two parts in that form. The left one have date, time, purpose, description and a table for selected Items for a particular usage order. Provide information for those fields in the left side. On the right side of the form it shows available items according to your item type. You can add any of the item by pressing the <button class="btn btn-sm bg-olive"><span class="fa fa-plus"></span></button> signed button next to each item. You can add quantity before or after adding a item to usage order. There is a search box on the Available items box. You may try using it to find any item easily. 

                </p>                               
                <div class="row">
                    <div class="col-md-3">
                        <button disabled="true" class="btn btn-primary btn-block"><span class="fa fa-save"></span>&nbsp; Process Order</button>
                    </div>
                    <div class="col-md-9">
                        When you are done with adding items in usage order with necessary quantity then press the process order button on the bottom of the left side box.
                    </div>
                </div>                  
            </div>
        </div>
    </div>    
</div>



