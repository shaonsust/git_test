    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a target="_blank" href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></h4>
            </div>
            <div class="box-body">
                <p>
				    As you are reading this so you have an user name and password to access the software for sure. You have logged in with your username and password. Before going to anywhere else you may like to complete your profile first. Which is very easy and very much important to do. 
				    In the top right corner of your browser view port ther is a button with an <span class="fa fa-user"></span> Icon with your username along side. Click on it and a pop up will appear right below the button. Here you can find a <a href="#" class="btn btn-default btn-flat btn-disabled" disabled="true">My Account</a> Button. Click on it and you will find the right place to Update your Name, Email address and User name. You may also change your password from there.
				    <br />
				    <em>If you are having trouble to find My account button you may just <a target="_blank" href="<?php echo base_url();?>welcome/my_account">Click Here</a></em>
				</p>                 
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">If in case you forgot your password</h4>
            </div>
            <div class="box-body">
                <p>
				    If in case you forgot your password then you will not be able to log in. If this happens there is a option for you to reset your password. In the Log In page you may find a link <a href="#" disabled="true">I Forgot My Password</a> Click on it provide your email address and you will receive an email with password reset link. Click on that link and follow instructions to reset your password.
				</p>                 
            </div>
        </div>
    </div>    
</div>



