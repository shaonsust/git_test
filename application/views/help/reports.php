    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a target="_blank" href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></h4>
            </div>
            <div class="box-body">
                <p>
				    FFML has a variety of reports available. Actually reports gives you the best overview of the system. FFML has a graphical chart based report on the dashboard which shows you Overall Income, Expense and Income VS Expense for the last month. And there are variety of reports available for any date range in the Reports menu. From the left navigation menu click on reports and try any of the reports available.
				</p>                 
                <h4>Some Important tips about reports</h4>
                <p>
                    By default all the report shows data for the last month (first day of last month to last day of last month). <br />
                    On the top right corner of report pages there is a date range selector with two text boxes which are calender enabled. Just click on the boxes to view the calender and select your required date range. Then click on <button class="btn btn-default btn-disabled" disabled="true" ><span class="fa fa-refresh"></span></button> and generate your desired report.
                </p>
            </div>
        </div>
    </div>    
</div>



