    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a target="_blank" href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></h4>
            </div>
            <div class="box-body">
                <p>
				    FFML allowes you to Add Raw Materials to the Inventory. After adding raw material to inventory you can purchase raw materials, use raw materials for production, sell raw materials. You may also edit or delete raw materials from this application. 
                    <br />
                    On the left navigation there is an item "Raw Materials", Click on it. Or just <a target="_blank" href="<?php echo base_url(); ?>raw_material">Click here</a> <br />
                    A new page will appear with all available raw materials in the inventory. In that page you will find a table with available raw materials. You can search for a specific raw material from the search box on the top right corner above the table.
				</p>                 
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Adding Raw Materials to Inventory</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <a target="_blank" class="btn btn-primary btn-block" href="<?php echo base_url();?>raw_material/add_new"><span class="fa fa-plus"></span> &nbsp; Add New</a>
                    </div>
                    <div class="col-md-9">
                        There is a button on the top of the raw material table. That looks similar to the button on the left side. By clicking this (either from here or from the raw materials page) you will have a form to add new raw material to the system.
                    </div>
                </div>             
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Purchasing Raw Material</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <a target="_blank" class="btn btn-primary btn-block" href="<?php echo base_url();?>purchase/new_purchase/raw_material"><span class="fa fa-barcode"></span> &nbsp; Purchase</a>
                    </div>
                    <div class="col-md-9">
                        There is a button on the top of the raw material table. That looks similar to the button on the left side. By clicking this (either from here or from the raw materials page) you will have a form to purchase raw materials. <br />
                        To Learn more about purchasing items <a href="<?php echo base_url()?>help/purchase"></a>
                    </div>
                </div>           
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Using Raw Materials</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <a target="_blank" class="btn btn-primary btn-block" href="<?php echo base_url();?>usage/new_usage/raw_material"><span class="fa fa-exchange"></span> &nbsp; Use</a>
                    </div>
                    <div class="col-md-9">
                        There is a button on the top of the raw material table. That looks similar to the button on the left side. By clicking this (either from here or from the raw materials page) you will have a form to use raw materials for production or any other purpose. <br />
                        To Learn more about using items <a href="<?php echo base_url()?>help/usage"></a>
                    </div>
                </div>                             
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Selling Raw Materials</h4>
            </div>
            <div class="box-body"> 
                <div class="row">
                    <div class="col-md-3">
                        <a target="_blank" class="btn btn-primary btn-block" href="<?php echo base_url();?>sales/sale/raw_material"><span class="fa fa-usd"></span> &nbsp; Sale</a>
                    </div>
                    <div class="col-md-9">
                        There is a button on the top of the raw material table. That looks similar to the button on the left side. By clicking this (either from here or from the raw materials page) you will have a form to sell raw materials. <br />
                        To Learn more about selling items <a href="<?php echo base_url()?>help/sales"></a>
                    </div>
                </div>               
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Raw Material Actions</h4>
            </div>
            <div class="box-body">
                <p>
                    On the left navigation there is an item "Raw Materials", Click on it. Or just <a target="_blank" href="<?php echo base_url(); ?>raw_material">Click here</a> <br />
                    A new page will appear with all available raw materials in the inventory. In that page you will find a table with all available raw material and their basic informations. In each row of that table you will have an action field with following buttons for each particular Raw material                  
                </p>                
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" disabled="true" class="btn btn-info btn-disabled" title="Details"><i class="fa fa-list"></i></a>                        
                    </div>
                    <div class="col-md-11">
                        Clicking this will open a new page with detailed information of that particular Raw material. On that page by default data is shown from the begining to till date. If you want you can select custom date range for more specific details. <br /> <br />
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-1">                        
                        <a href="#" disabled="true" class="btn btn-warning btn-disabled" title="Edit"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="col-md-11">
                        Clicking this will open a new page with a form to edit that particular raw material. Note: You need administrative permission to proceed with this action <br />
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-1">                        
                        <a href="#" disabled="true" class="btn btn-danger btn-disabled" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                    </div>
                    <div class="col-md-11">
                        Clicking this allow you to delete that particular raw material. Note: You need administrative permission to proceed with this action. <br /> <br />
                    </div>
                </div>               
            </div>
        </div>
    </div>    
</div>



