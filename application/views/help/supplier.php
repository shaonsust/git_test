    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a target="_blank" href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></h4>
            </div>
            <div class="box-body">
                <p>
				    FFML application has a module called Supplier Management. Which allowes you to create, edit and delete suppliers. It also allowes you to view all kind of transactions done with any particular supplier.
                    <br />
                    To view all suppliers or to create a new supplier click on Suppliers from the menu or <a href="<?php echo base_url()?>supplier">Click here</a>
                    In the supplier page you will see a table with all existing suppliers. You will also see a button for adding a new supplier
				</p>                 
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Create Supplier</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">                        
                        <a class="btn btn-primary" href="<?php echo base_url();?>supplier/add_new"><span class="fa fa-plus"></span> &nbsp; Add New Supplier</a>
                    </div>
                    <div class="col-md-9">
                        <p>To add a new supplier click on Add new supplier from Suppliers page and fill up the form with necessary data then save it by clicking <button class="btn btn-primary btn-disabled" disabled="true"><span class="fa fa-save"></span>&nbsp; Save Supplier</button></p>
                        <p>Note: <em>Supplier Names are unique. If in case you have multiple supplier with a same name then please use some kind of marking such as Mr.X1 Mr.X2 to keep a supplier names uniqe.</em></p>
                    </div>
                </div>                             
            </div>
        </div>
           
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Actions available for Suppliers</h4>
            </div>
            <div class="box-body">
                <p>
                    In Supplier page you will see a table with all supplier available in the system. By Clicking any supplier name you will see a detailed overview of any particular supplier.
                    In each row of the table you will find following action buttons.
                </p> 
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" class="btn btn-info btn-disabled" disabled="true" title="Details"><i class="fa fa-list"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will see a new page with all possible details of that particular supplier <br /><br />
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" class="btn btn-warning btn-disabled" disabled="true" title="Edit"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will be able to edit a supplier. <br /><br />
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" class="btn btn-danger btn-disabled" disabled="true" title="Delete"><i class="fa fa-remove"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will be able to delete a supplier. <br /><br />
                    </div>
                </div>                 
                <p>
                    Note: For Editing or Deleting any supplier you must have to have administrative permission. If you have permission and you delete any supplier it will also delete related information and adjust the regarding payments and dues in the system.
                </p>               
            </div>
        </div>
    </div>    
</div>



