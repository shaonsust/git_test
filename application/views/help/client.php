    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a target="_blank" href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></h4>
            </div>
            <div class="box-body">
                <p>
				    FFML application has a module called Client Management. Which allowes you to create, edit and delete clients. It also allowes you to view all kind of transactions done with any particular client.
                    <br />
                    To view all clients or to create a new client click on Clients from the menu or <a href="<?php echo base_url()?>client">Click here</a>
                    In the client page you will see a table with all existing clients. You will also see a button for adding a new client
				</p>                 
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Create Client</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">                        
                        <a class="btn btn-primary" href="<?php echo base_url();?>client/add_new"><span class="fa fa-plus"></span> &nbsp; Add New Client</a>
                    </div>
                    <div class="col-md-9">
                        <p>To add a new client click on Add New Client from clients page and fill up the form with necessary data then save it by clicking <button class="btn btn-primary btn-disabled" disabled="true"><span class="fa fa-save"></span>&nbsp; Save client</button></p>
                        <p>Note: <em>client Names are unique. If in case you have multiple client with a same name then please use some kind of marking such as Mr.X1 Mr.X2 to keep a client names uniqe.</em></p>
                    </div>
                </div>                             
            </div>
        </div>
           
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Actions available for Clients</h4>
            </div>
            <div class="box-body">
                <p>
                    In client page you will see a table with all client available in the system. By Clicking any client name you will see a detailed overview of any particular client.
                    In each row of the table you will find following action buttons.
                </p> 
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" class="btn btn-info btn-disabled" disabled="true" title="Details"><i class="fa fa-list"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will see a new page with all possible details of that particular client <br /><br />
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" class="btn btn-warning btn-disabled" disabled="true" title="Edit"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will be able to edit a client. <br /><br />
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" class="btn btn-danger btn-disabled" disabled="true" title="Delete"><i class="fa fa-remove"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will be able to delete a client. <br /><br />
                    </div>
                </div>                 
                <p>
                    Note: For Editing or Deleting any client you must have to have administrative permission. If you have permission and you delete any client it will also delete related information and adjust the regarding payments and dues in the system.
                </p>               
            </div>
        </div>
    </div>    
</div>



