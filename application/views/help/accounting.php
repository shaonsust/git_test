    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a target="_blank" href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['title'];?> - <?php echo $this->data['sub_title'];?></h4>
            </div>
            <div class="box-body">
                <p>
				    FFML application has a module called General Accounting. Which allows you to keep track of general income, expense and due payments. General Income and Expense is income and expenses beyond sales and purchase. And Due payments allow you to receive due payments from clients and pay dues to suppliers.
                    <br />
                    On the left navigation there is an item "General Accounting", Click on it. It will have Income, Expense, Supplier Due Payment, Client Due Payment. <br />
                    To make a general income entry or view existing income entries click on Income from the menu or <a href="<?php echo base_url()?>accounting/index/income">Click here</a> <br />
                    To make a general expense entry or view existing expense entries click on Expense from the menu or <a href="<?php echo base_url()?>accounting/index/expense">Click here</a> <br />
                    To make a Supplier due payment entry or view existing supplier due payment entries click on Supplier Due Payment from the menu or <a href="<?php echo base_url()?>accounting/index/supplier_due">Click here</a> <br />
                    To make a Client due payment entry or view existing client due payment entries click on Client Due Payment from the menu or <a href="<?php echo base_url()?>accounting/index/client_due">Click here</a> <br />
                    By clicking any of the link will take you to a page with a table of corresponding entries. In that table you will be able to see all existing entries with unique receipt number and other details. By clicking on any receipt number you will be able to view that particular transaction receipt. You can add new entries, edit or delete any existing entry from this page.
				</p>                 
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Making a General Income Entry </h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">                        
                        <a class="btn btn-primary" href="<?php echo base_url();?>accounting/new_payment/income"><span class="fa fa-plus"></span> &nbsp; New General Income</a>
                    </div>
                    <div class="col-md-9">
                        In the Income page you can find this button above the Income table. Click on it and you will find the General Income form. Provide Amount, Date, Time, Purpose and description for a particular Income and Click <button class="btn btn-primary btn-disabled" disabled="true"><span class="fa fa-save"></span>&nbsp; Save Payment</button>. A new entry will be submitted and a receipt will be generated for that particular transaction.
                    </div>
                </div>                             
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Making a General Expense Entry </h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">                        
                        <a class="btn btn-primary" href="<?php echo base_url();?>accounting/new_payment/expense"><span class="fa fa-plus"></span> &nbsp; New General Expense</a>
                    </div>
                    <div class="col-md-9">
                        In the Expense page you can find this button above the Expense table. Click on it and you will find the General Expense form. Provide Amount, Date, Time, Purpose and description for a particular Expense and Click <button class="btn btn-primary btn-disabled" disabled="true"><span class="fa fa-save"></span>&nbsp; Save Payment</button>. A new entry will be submitted and a receipt will be generated for that particular transaction.
                    </div>
                </div>                             
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Making a Supplier Due Payment </h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">                        
                        <a class="btn btn-primary" href="<?php echo base_url();?>accounting/new_payment/supplier_due"><span class="fa fa-plus"></span> &nbsp; New Supplier Due Payment</a>
                    </div>
                    <div class="col-md-9">
                        In the Supplier Due Payment page you can find this button above the Supplier Due Payment table. Click on it and you will find the Supplier Due Payment form. Provide Amount, Date and Time. The supplier field has autocomplete feature enabled. Start typing supplier name and you will get suggestions for your desired supplier. Select the correct one and provide description (if any) for a particular Due Payment and Click <button class="btn btn-primary btn-disabled" disabled="true"><span class="fa fa-save"></span>&nbsp; Save Payment</button>. A new entry will be submitted and a receipt will be generated for that particular transaction.
                    </div>
                </div>                             
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Making a Client Due Payment </h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">                        
                        <a class="btn btn-primary" href="<?php echo base_url();?>accounting/new_payment/client_due"><span class="fa fa-plus"></span> &nbsp; New Client Due Payment</a>
                    </div>
                    <div class="col-md-9">
                        In the Client Due Payment page you can find this button above the Client Due Payment table. Click on it and you will find the Client Due Payment form. Provide Amount, Date and Time. The client field has autocomplete feature enabled. Start typing client name and you will get suggestions for your desired client. Select the correct one and provide description (if any) for a particular Due Payment and Click <button class="btn btn-primary btn-disabled" disabled="true"><span class="fa fa-save"></span>&nbsp; Save Payment</button>. A new entry will be submitted and a receipt will be generated for that particular transaction.
                    </div>
                </div>                             
            </div>
        </div>        
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Actions of a transaction receipt</h4>
            </div>
            <div class="box-body">
                <p>
                    On any of the transaction's (Income, Expense, Supplier Due Payment, Client Due Payment) main page there is a table with all transaction receipts regarding the transaction type. To view a transaction receipt you have to click on the receipt number. Then the transaction receipt will be shown. There are few buttons on the top right corner of the receipt.
                </p>   
                <div class="row">
                    <div class="col-md-2">
                        <button class="btn btn-default btn-disabled btn-block" disabled="true"> <i class="fa fa-print"></i> &nbsp;Print</button>
                    </div>
                    <div class="col-md-10">
                        <p>If you want, you may print the transaction receipt by clicking this button.</p> <br />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <a href="#" class="btn btn-primary btn-disabled btn-block" disabled="true"> <i class="fa fa-pencil"></i> &nbsp;Edit</a>
                    </div>
                    <div class="col-md-10">
                        <p>
                            If you think there is some correction needed in the transaction receipt then you may edit the transaction receipt by clicking this button.
                        </p><br />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <a href="#" class="btn btn-danger btn-disabled btn-block" disabled="true"> <i class="fa fa-trash"></i> &nbsp;Delete</a>
                    </div>
                    <div class="col-md-10">
                        <p>If in case a transaction receipt is no longer needed or may be generated by mistake then you may want to delete that transaction receipt. You may delete a transaction receipt by clicking this button</p>
                    </div>
                </div>
                <p>
                    Note: For Editing or Deleting a transaction receipt you must have to have administrative permission. If you have permission and you delete any transaction receipt it will also delete related information and adjust the regarding payments and dues in the system.
                </p>
                <p>
                    On the main transaction pages there is a table with all transaction receipt. You can edit or delete a transaction receipt from there also.
                </p>  
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" disabled="true" class="btn btn-warning btn-disabled" title="Edit"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will be able to edit a transaction receipt.
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <a href="#" disabled="true" class="btn btn-danger btn-disabled" title="Delete"><i class="fa fa-remove"></i></a>
                    </div>
                    <div class="col-md-11">
                        By clicking this you will be able to delete a transaction receipt.
                    </div>
                </div>                  
            </div>
        </div>
    </div>    
</div>



