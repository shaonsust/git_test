<style>
    @media screen {
        div.divFooter {
            display: none;
        }
    }
    @media print {
        .noPrint {
            display: none;
        }
        div.divFooter {
            position: fixed;
            bottom: 15px;
            right: 15px;
            page-break-after: always;
        }
        @page { margin: 0; }
        body { margin: 1.6cm; }
        .table-condensed tr td, .table-condensed tr th{
            font-size: 10px;
            line-height: 20px;
            padding: 2px 5px;
        }

    }
</style>
<div class="row noPrint">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>purchase">Purchase</a></li>
                <li class="active">Purchase Receipt</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible noPrint" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-10">
        <div class="box box-primary">
            <div class="box-header noPrint">
                <div class="row">
                    <div class="col-md-7">
                        <h4 class="box-title">
                            Sales Receipt - <?php echo $order['order']->receipt_no; ?>
                        </h4>
                    </div>
                    <div class="col-md-5 text-right">
                        <button class="btn btn-default" onclick="printMe()"> <i class="fa fa-print"></i> &nbsp;Print</button>
                        <a href="<?php echo base_url() . 'purchase'; ?>" class="btn btn-info"> <i class="fa fa-arrow-left"></i> &nbsp;Back</a>
                        <a href="<?php echo base_url() . 'purchase/edit/' . $order['order']->id; ?>" class="btn btn-primary"> <i class="fa fa-pencil"></i> &nbsp;Edit</a>
                        <a href="<?php echo base_url() . 'purchase/delete/' . $order['order']->id; ?>" class="btn btn-danger"> <i class="fa fa-trash"></i> &nbsp;Delete</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%">
                            <tr>
                                <td width="20%" align="center">
<!--                                    <img src="<?php //echo base_url();          ?>logo.jpg" alt=""/>-->
                                </td>
                                <td width="60%" align="center">
                                    <h2>Bismillah Enterprise.</h2>
                                    <h3>Star Particle Board Mills Ltd.</h3>
                                    <p>55, Kazi Nazrul Islam Avenue, Kawran Bazar, Dhaka-1215</p>
                                </td>
                                <td width="20%" align="right">
                                    <p>01926-369889</br>
                                        9127586
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%">
                            <tr>
                                <td width="20%">
                                    Supplier<br/>
                                    Contact Person<br/>
                                    Address<br/>
                                    Phone
                                </td>
                                <td width="45%">
                                    : <?php echo $order['order']->company_name; ?> <br/>
                                    : <?php echo $order['order']->contact_person; ?> <br/>
                                    : <?php echo $order['order']->address; ?> <br/>
                                    : <?php echo $order['order']->contact_phone; ?>
                                </td>
                                <td width="35%" align="right">
                                    Date: <?php echo $order['order']->date . ' ' . $order['order']->time; ?><br/>
                                    Receipt No: <?php echo $order['order']->receipt_no; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%">
                            <tr>
                                <td align="center">
                                    <h3><u>Purchase Order</u></h3>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-condensed" width="100%">
                            <tr>
                                <th width="10%">sl.</th>
                                <th width="45%">Item Name</th>
                                <th width="15%">Rate</th>
                                <th width="15%">Qty</th>
                                <th width="15%">Price</th>
                            </tr>
                            <?php
                            $i = 1;
                            foreach ($order['sales'] as $sale) {
                                ?>
                                <tr>
                                    <td width="10%"><?php echo $i; ?></td>
                                    <td width="45%"><?php echo $sale->product_name; ?></td>
                                    <td width="15%" align="right"><?php echo $sale->rate; ?></td>
                                    <td width="15%" align="right"><?php echo $sale->qty . ' ' . $sale->unit; ?></td>
                                    <td width="15%" align="right"><?php echo $sale->price; ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                            <tr>
                                <td colspan="4" align="right"><strong>Total Price</strong></td>
                                <td align="right"><strong><?php echo $order['order']->total_price; ?></strong></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right"><strong>VAT (<?php echo $order['order']->vat_percentage; ?> %)</strong></td>
                                <td align="right"><strong><?php echo $order['order']->vat; ?></strong></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right"><strong>Discount (<?php echo $order['order']->discount_percentage; ?> %)</strong></td>
                                <td align="right"><strong><?php echo $order['order']->discount; ?></strong></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right"><strong>Net Payable</strong></td>
                                <td align="right"><strong><?php echo $net_payable = $order['order']->total_price + $order['order']->vat - $order['order']->discount; ?></strong></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right"><strong>Paid Amount</strong></td>
                                <td align="right"><strong><?php echo $order['order']->payment_amount; ?></strong></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right"><strong>Due</strong></td>
                                <td align="right"><strong><?php echo $order['order']->due_amount; ?></strong></td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <strong>Net Payable In Words: <?php echo $this->numbertowords->convert_currency($net_payable); ?> </strong>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <tr>
                                <td width="10%">&nbsp;</td>
                                <td width="30%" align="center">
                                    <br/>
                                    <br/>
                                    <br/>
                                    ......................................................
                                    <br/>
                                    Supplier's Signature
                                </td>
                                <td width="20%">&nbsp;</td>
                                <td width="30%" align="center">
                                    <br/>
                                    <br/>
                                    <br/>
                                    ......................................................
                                    <br/>
                                    For Bismillah Enterprise
                                </td>
                                <td width="20%">&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="divFooter">Software Developed By: SCORSHIA.COM (http://www.scorshia.com)</div>

<div class="row noPrint">
    <div class="col-md-12">
        <button class="btn btn-default" onclick="printMe()"> <i class="fa fa-print"></i> &nbsp;Print</button>
        <a href="<?php echo base_url() . 'purchase'; ?>" class="btn btn-info"> <i class="fa fa-arrow-left"></i> &nbsp;Back</a>
        <a href="<?php echo base_url() . 'purchase/edit/' . $order['order']->id; ?>" class="btn btn-primary"> <i class="fa fa-pencil"></i> &nbsp;Edit</a>
        <a href="<?php echo base_url() . 'purchase/delete/' . $order['order']->id; ?>" class="btn btn-danger"> <i class="fa fa-trash"></i> &nbsp;Delete</a>
    </div>
</div>
<script>
    function printMe() {
        window.print();
    }
</script>


