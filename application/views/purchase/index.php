<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Purchase</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Purchase &nbsp;
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>purchase/new_purchase"><span class="fa fa-plus"></span> &nbsp; New Purchase</a>
                </h4>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                        <tr>
    <!--                        <th>Sl.</th>-->
                            <th>Receipt</th>
                            <th>Date - Time</th>
                            <th>Supplier Name</th>
                            <th>Trans Type</th>
                            <th>Acc Name</th>
                            <th>Total Price</th>
                            <th>Vat</th>
                            <th>Discount</th>
                            <th>Net Payable</th>
                            <th>Paid Amount</th>
                            <th>Due</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($purchases as $purchase) {
                            ?>
                            <tr>
    <!--                            <td>--><?php //echo $i;   ?><!--</td>-->
                                <td><a href="<?php echo base_url() . 'purchase/receipt/' . $purchase->id; ?>" title="Details"><?php echo $purchase->receipt_no; ?></a></td>
                                <td><?php echo $purchase->date . ' - ' . $purchase->time; ?></td>
                                <td><a href="<?php echo base_url() . 'supplier/details/' . $purchase->company_id; ?>" title="Details"><?php echo $purchase->name; ?></a></td>
                                <td align="right"><?php echo $purchase->tr_type; ?></td>
                                <td align="right"><?php echo $purchase->res_acc; ?></td>
                                <td align="right"><?php echo $purchase->total_price; ?></td>
                                <td align="right"><?php echo $purchase->vat . ' ( ' . $purchase->vat_percentage . ' % )'; ?></td>
                                <td align="right"><?php echo $purchase->discount . ' ( ' . $purchase->discount_percentage . ' % )'; ?></td>
                                <td align="right"><?php echo $purchase->total_price + $purchase->vat - $purchase->discount; ?></td>
                                <td align="right"><?php echo $purchase->payment_amount; ?></td>
                                <td align="right"><?php echo $purchase->due_amount; ?></td>
                                <td align="center">
                                    <a href="<?php echo base_url() . 'purchase/edit/' . $purchase->id ?>" class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-pencil"></i></a>
                                    <a href="<?php echo base_url() . 'purchase/delete/' . $purchase->id ?>" class="btn btn-danger btn-xs" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                                </td>
                            </tr>
                            <?php
//                        $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


