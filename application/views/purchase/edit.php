
<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>purchase"> Purchase</a></li>
                <li class="active">Edit Order</li>
            </ol>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Edit Order - Receipt # <?php echo $order['order']->receipt_no;?></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo base_url().'purchase/update/'.$order['order']->id ?>" method="post">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="date">Date:</label>
                                    <input type="text" id="date" name="date" class="form-control input-sm datepicker" value="<?php echo $order['order']->date;?>" required="required"/>
                                </div>
                                <div class="form-group col-md-4 col-md-offset-4">
                                    <label for="time">Time:</label>
                                    <input type="time" id="time" name="time" class="form-control input-sm" value="<?php echo $order['order']->time;?>" required="required"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="company">Supplier:</label>
                                <input type="text" id="company" name="company" class="form-control input-sm" required="required" value="<?php echo $order['order']->company_name;?>"/>
                            </div>
                            <div class="form-group">
                                <label for="receipt_no">Purchase Bill/Chalan/Receipt No:</label>
                                <input type="text" id="receipt_no" name="receipt_no" class="form-control input-sm" value="<?php echo $order['order']->receipt_no;?>"required="required"/>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-solid">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Items in the Order</h3>
                                        </div>
                                        <div class="box-body">
                                            <table id="orderTable" class="table table-bordered table-condensed">
                                                <thead>
                                                <tr>
                                                    <th width="40%">Item</th>
                                                    <th width="20%">Rate</th>
                                                    <th width="20%">Qty</th>
                                                    <th width="20%">Price</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $i = 1;
                                                foreach($order['sales'] as $sale)
                                                {
                                                    ?>
                                                    <tr class="orderRow" id="orderProduct<?php echo $sale->item_id;?>">
                                                        <td>
                                                            <input type="hidden" name="item_id[]" value="<?php echo $sale->item_id;?>"/>
                                                            <div class="row">
                                                                <div class="col-md-9">
                                                                    <?php echo $sale->product_name;?>
                                                                    <input type="hidden" class="form-control input-sm" name="product_name[]" value="<?php echo $sale->product_name;?>" readonly/>
                                                                    </div>
                                                                <div class="col-md-3">
                                                                    <button type="button" class="btn btn-danger btn-sm orderRemove"><span class="fa fa-remove"></span></button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        <td><input type="number" class="form-control input-sm orderRate adjustPriceInd numOnly" name="rate[]" value="<?php echo $sale->rate;?>"/></td>
                                                        <td width="30%">
                                                            <div class="input-group">
                                                                <input type="number" class="form-control input-sm orderQty adjustPriceInd numOnly" name="qty[]" value="<?php echo $sale->qty;?>"/>
                                                                <span class="input-group-addon" id="basic-addon2"><?php echo $sale->unit; ?></span>
                                                            </div>
                                                        </td>
                                                        <td><input type="text" class="form-control input-sm orderPrice text-right" name="price[]" readonly value="<?php echo $sale->price;?>"/></td>
                                                        </tr>
                                                <?php
                                                }
                                                ?>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="3" align="right">Total Price</td>
                                                    <td><input type="text" name="total_price" id="totalPrice" class="form-control input-sm text-right" value="<?php echo $order['order']->total_price;?>" readonly required="required"/></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="right">Vat</td>
                                                    <td>
                                                        <div class="input-group" id="vat_percentage_container" style="display: none;" >
                                                            <input type="text" class="form-control input-sm numOnly" id="vat_percentage" value="<?php echo $order['order']->vat_percentage;?>"/>
                                                            <span class="input-group-addon" id="basic-addon1">%</span>
                                                        </div>

                                                        <select class="form-control input-sm" name="vat_type"
                                                                id="vat_type">
                                                            <option value="amount">Amount</option>
                                                            <option value="percentage">Percentage</option>
                                                        </select>
                                                    </td>
                                                    <td><input type="text" name="vat" id="vat" class="form-control input-sm text-right adjustPrice numOnly" value="<?php echo $order['order']->vat;?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="right">Discount</td>
                                                    <td>
                                                        <div class="input-group" id="discount_percentage_container" style="display: none;" >
                                                            <input type="text" class="form-control input-sm numOnly" id="discount_percentage" value="<?php echo $order['order']->discount_percentage;?>"/>
                                                            <span class="input-group-addon" id="basic-addon1">%</span>
                                                        </div>
                                                        <select class="form-control input-sm" name="discount_type"
                                                                id="discount_type">
                                                            <option value="amount">Amount</option>
                                                            <option value="percentage">Percentage</option>
                                                        </select>
                                                    </td>
                                                    <td><input type="text" name="discount" id="discount" class="form-control input-sm text-right adjustPrice numOnly" value="<?php echo $order['order']->discount;?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" align="right">Payment Amount</td>
                                                    <td><input type="text" name="payment_amount" id="paymentAmount" class="form-control input-sm text-right adjustPrice numOnly" value="<?php echo $order['order']->payment_amount;?>" required="required"/></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" align="right">Due Amount</td>
                                                    <td><input type="text" name="due_amount" id="dueAmount" class="form-control input-sm text-right" value="<?php echo $order['order']->due_amount;?>" readonly/></td>
                                                </tr>

                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button id="processOrder" class="btn btn-primary" type="submit"><span class="fa fa-save"></span>&nbsp; Update Order</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="box box-primary">
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Available Items</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table id="productTable" class="table table-bordered table-condensed table-hover table-striped dataTable">
                            <thead>
                            <tr>
                                <th>Item</th>
                                <th>qty</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($items as $item)
                            {
                                ?>
                                <tr>
                                    <td width="60%">
                                        <a class="productName" href="<?php echo base_url() . $item->type .'/details/' . $item->id ?>" target="_blank" title="Details"><?php echo $item->name;?></a>
                                        <input type="hidden" class="productRate" value="<?php echo $item->price; ?>"/>
                                        <input type="hidden" class="itemId" value="<?php echo $item->id; ?>"/>
                                        <input type="hidden" class="productUnit" value="<?php echo $item->unit; ?>"/>
                                    </td>
                                    <td width="30%">
                                        <div class="input-group">
                                            <input type="number" class="productQty form-control input-sm productTableInput numOnly" name="qty" value="1"/>
                                            <span class="input-group-addon" id="basic-addon2"><?php echo $item->unit; ?></span>
                                        </div>
                                    </td>
                                    <td width="10%"><button class="btn btn-sm btn-block bg-olive addProduct"><span class="fa fa-plus"></span></button></td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if(isset($companies) && !empty($companies))
{
    ?>
    <script>
        var companies = [<?php
                            $total_companies = count($companies);
                            $i = 1;
                            foreach($companies as $c)
                            {
                                if($i==$total_companies)
                                {
                                    echo '"'.$c->name.'"';
                                }
                                else
                                {
                                    echo '"'.$c->name.'",';
                                }
                                $i++;
                            }

                        ?>];
        $( "#company" ).autocomplete({
            source: companies
        });
    </script>
<?php
}
?>
<script>
$( ".datepicker" ).datepicker({
    dateFormat: "yy-mm-dd"
});
$(".addProduct").click(function(){
    $("tr.danger").remove();
    var itemId = $(this).closest('tr').find('.itemId').val();
    var productName = $(this).closest('tr').find('.productName').text();
    var productRate = $(this).closest('tr').find('.productRate').val();
    var productQty = $(this).closest('tr').find('.productQty').val();
    var productUnit = $(this).closest('tr').find('.productUnit').val();
    if($("#orderProduct"+itemId).length)
    {
        var prevQty = $("#orderProduct"+itemId).find(".orderQty").val();
        var newQty = Number(prevQty) + Number(productQty);
        $("#orderProduct"+itemId).find(".orderQty").val(newQty);
        adjustPrice("#orderProduct"+itemId);
    }
    else {
        var newRow = '<tr class="orderRow" id="orderProduct'+ itemId +'">' +
            '<td>' +
            '<input type="hidden" name="item_id[]" value="' + itemId + '"/>' +
            '<div class="row">' +
            '<div class="col-md-9">' +
            productName +
            '<input type="hidden" class="form-control input-sm" name="product_name[]" value="' + productName + '" readonly/>' +
            '</div>' +
            '<div class="col-md-3">' +
            '<button type="button" class="btn btn-danger btn-sm orderRemove"><span class="fa fa-remove"></span></button>' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td><input type="number" class="form-control input-sm orderRate adjustPriceInd numOnly" name="rate[]" value="' + productRate + '"/></td>' +
            '<td>' +
            '<div class="input-group">' +
            '<input type="number" class="form-control input-sm orderQty adjustPriceInd numOnly" name="qty[]" value="' + productQty + '"/>' +
            '<span class="input-group-addon" id="basic-addon1">' + productUnit + '</span>' +
            '</div>' +
            '</td>' +
            '<td><input type="text" class="form-control input-sm orderPrice text-right" name="price[]" readonly value="0.00"/></td>' +
            '</tr>';
        $("#orderTable").find('tbody').append(newRow);
        $('.numOnly').keypress(function(key) {
            if((key.charCode < 48 || key.charCode > 57) && (key.charCode != 46)) return false;
        });
        adjustPrice("#orderProduct"+itemId);
    }

});
$(".adjustPrice").keyup(function(){
    adjustPrice();
});
$(document).on('change', '.adjustPriceInd', function(){
    var orderRow = $(this).closest('tr').attr("id");
    var qty = $(this).closest('tr').find(".orderQty").val();
    var rate = $(this).closest('tr').find(".orderRate").val();
    var price = Number(qty) * Number(rate);
    price = price.toFixed(2);
    $(this).closest('tr').find(".orderPrice").val(price);

    var totalPrice = 0;
    var paymentAmount = $("#paymentAmount").val();


    $('.orderPrice').each(function() {
        totalPrice += Number($(this).val());
    });
    totalPrice = totalPrice.toFixed(2);
    $("#totalPrice").val(totalPrice);

    var discount = 0;
    var discount_percentage = 0;
    var discount_type = $("#discount_type").val();
    if(discount_type == 'percentage')
    {
        discount_percentage = $("#discount_percentage").val();
        discount = Number(totalPrice) * Number(discount_percentage) / 100;
        discount = discount.toFixed(2);
        $("#discount").val(discount);
    }
    else {
        discount = $("#discount").val();
    }

    var vat = 0;
    var vat_percentage = '';
    var vat_type = $("#vat_type").val();
    if(vat_type == 'percentage')
    {
        vat_percentage = $("#vat_percentage").val();
        vat = Number(totalPrice) * Number(vat_percentage) / 100;
        vat = vat.toFixed(2);
        $("#vat").val(vat);
    }
    else {
        vat =  $("#vat").val();
    }
    var dueAmount = Number(totalPrice) + Number(vat) - Number(discount) - Number(paymentAmount);
    dueAmount = dueAmount.toFixed(2);
    $("#dueAmount").val(dueAmount);

});

$(document).on("click", '.orderRemove', function(){
    $(this).closest("tr").remove();
    adjustPrice();
    if($(".orderRow").length)
    {
        return true;
    }
    else {
        var newRow = '<tr class="danger">' +
            '<td colspan="4" align="center">' +
            '<strong> Please Add Few Products to Process Order</strong>' +
            '</td>' +
            '</tr>';
        $("#orderTable").find('tbody').append(newRow);
    }
});

$("#processOrder").click(function(){
    if($(".orderRow").length)
    {
        return true;
    }
    else {
        $("tr.danger").remove();
        var newRow = '<tr class="danger">' +
            '<td colspan="4" align="center" valign="middle" >' +
            '<strong> Please Add Few Products to Process Order</strong>' +
            '</td>' +
            '</tr>';
        $("#orderTable").find('tbody').append(newRow);
        $("tr.danger").find('strong').animate({height: '100px', opacity: '0.7', lineHeight: '100px', fontSize: '22px'}, "slow", "swing");
        $("tr.danger").find('strong').animate({height: '35px', opacity: '1.0', lineHeight: '35px', fontSize: '14px'}, "slow", "swing");
        return false;
    }
});
$(".numOnly").change(function(){
    var tAmount = $(this).val();
    tAmount = Number(tAmount).toFixed(2);
    $(this).val(tAmount);
});

$("#discount_percentage").keyup(function(){
    adjustPrice();
});

$("#vat_percentage").keyup(function(){
    adjustPrice();
});
$("#discount_type").change(function(){
    var discount_type = $(this).val();
    if(discount_type == 'percentage')
    {
        $("#discount_percentage_container").show();
        $("#discount").attr('readonly', 'readonly');
    }
    else {
        $("#discount_percentage").val('0.00');
        $("#discount_percentage_container").hide();
        $("#discount").attr('readonly', false);
    }
    adjustPrice();
});
$("#vat_type").change(function(){
    var vat_type = $(this).val();
    if(vat_type == 'percentage')
    {
        $("#vat_percentage_container").show();
        $("#vat").attr('readonly', 'readonly');
    }
    else {
        $("#vat_percentage").val('0.00');
        $("#vat_percentage_container").hide();
        $("#vat").attr('readonly', false);
    }
    adjustPrice();
});
function adjustPrice(orderRow) {
    if(orderRow != "undefined")
    {
        var qty = $(orderRow).find(".orderQty").val();
        var rate = $(orderRow).find(".orderRate").val();
        var price = Number(qty) * Number(rate);
        price = price.toFixed(2);
        $(orderRow).find(".orderPrice").val(price);
    }

    var totalPrice = 0;
    var paymentAmount = $("#paymentAmount").val();


    $('.orderPrice').each(function() {
        totalPrice += Number($(this).val());
    });
    totalPrice = totalPrice.toFixed(2);
    $("#totalPrice").val(totalPrice);

    var discount = 0;
    var discount_percentage = 0;
    var discount_type = $("#discount_type").val();
    if(discount_type == 'percentage')
    {
        discount_percentage = $("#discount_percentage").val();
        discount = Number(totalPrice) * Number(discount_percentage) / 100;
        discount = discount.toFixed(2);
        $("#discount").val(discount);
    }
    else {
        discount = $("#discount").val();
    }

    var vat = 0;
    var vat_percentage = '';
    var vat_type = $("#vat_type").val();
    if(vat_type == 'percentage')
    {
        vat_percentage = $("#vat_percentage").val();
        vat = Number(totalPrice) * Number(vat_percentage) / 100;
        vat = vat.toFixed(2);
        $("#vat").val(vat);
    }
    else {
        vat =  $("#vat").val();
    }
    var dueAmount = Number(totalPrice) + Number(vat) - Number(discount) - Number(paymentAmount);
    dueAmount = dueAmount.toFixed(2);
    $("#dueAmount").val(dueAmount);
}
</script>