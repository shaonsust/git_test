<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active"><?php echo $this->data['sub_title']; ?></li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title"><?php echo $this->data['sub_title']; ?> &nbsp;
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>loan/new_loan/<?php echo $this->data['type']; ?>"><span class="fa fa-plus"></span> &nbsp; New <?php echo $this->data['sub_title']; ?></a>
                </h4>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Transaction No</th>
                            <th>Date - Time</th>
                            <th>Loan Source</th>
                            <th>Purpose</th>
                            <th>Tran Type</th>
                            <th>Acc_name</th>
                            <th>Amount</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($loans as $loan) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><a href="<?php echo base_url() . 'loan/loan_details/' . $this->data['type'] . '/' . $loan->id; ?>" title="Details"><?php echo $loan->receipt_no; ?></a></td>
                                <td><?php echo $loan->date . ' - ' . $loan->time; ?></td>
                                <td><a href="<?php echo base_url() . 'loan/source_details/' . $loan->company_id; ?>" title="Details"><?php echo $loan->name; ?></a></td>
                                <td><?php echo $loan->purpose; ?></td>
                                <td align="right"><?php echo $loan->tr_type; ?></td>
                                <td align="right"><?php echo $loan->res_acc; ?></td>
                                <td align="right"><?php echo number_format($loan->amount, 2, '.', ','); ?></td>
                                <td align="center">
                                    <a href="<?php echo base_url() . 'loan/edit_loan/' . $this->data['type'] . '/' . $loan->id ?>" class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-pencil"></i></a>
                                    <a href="<?php echo base_url() . 'loan/delete_loan/' . $this->data['type'] . '/' . $loan->id ?>" class="btn btn-danger btn-xs" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


