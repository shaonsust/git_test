
<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url().'loan/loan_details/'.$this->data['type'].'/'.$this->data['id']; ?>"> <?php echo $this->data['sub_title']?></a></li>
                <li class="active">Edit Installment</li>
            </ol>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="content">
                <div class="row">
                    <div class="col-md-4">
                        <h4>Edit Installment</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo base_url() ?>loan/update_installment/<?php echo $this->data['type'].'/'.$parent_id.'/'.$loan->id;?>" method="post">
                            <div class="form-group">
                                <label for="company">Loan Source:</label>
                                <input type="text" id="company" name="company" class="form-control input-sm" required="required" value="<?php echo $loan->name;?>" readonly/>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="amount">Installment Amount:</label>
                                    <input type="text" id="amount" name="amount" class="form-control input-sm numOnly" value="<?php echo $loan->amount;?>" required="required"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="date">Installment Date:</label>
                                    <input type="text" id="date" name="date" class="form-control input-sm datepicker" value="<?php echo $loan->date;?>" required="required"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="time">Installment Time:</label>
                                    <input type="time" id="time" name="time" class="form-control input-sm" value="<?php echo $loan->time;?>" required="required"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description">Description/ Note:</label>
                                <textarea class="form-control" name="description" id="description" cols="30"
                                          rows="4" placeholder="Any additional information/description or note that could be handy in future"><?php echo $loan->description;?></textarea>
                            </div>
                            <div class="form-group">
                                <button id="savePayment" class="btn btn-primary" type="submit"><span class="fa fa-save"></span>&nbsp; Update Installment</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".numOnly").change(function(){
        var tAmount = $(this).val();
        tAmount = Number(tAmount).toFixed(2);
        $(this).val(tAmount);
    });
    $( ".datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
</script>
