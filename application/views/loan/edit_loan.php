
<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url().'loan/loans/'.$this->data['type']; ?>"> <?php echo $this->data['sub_title']?></a></li>
                <li class="active">Edit <?php echo $this->data['sub_title']?></li>
            </ol>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="content">
                <div class="row">
                    <div class="col-md-4">
                        <h4>Edit <?php echo $this->data['sub_title']?></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo base_url() ?>loan/update_loan/<?php echo $this->data['type'].'/'.$loan->id;?>" method="post">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="amount">Amount:</label>
                                    <input type="text" id="amount" name="amount" class="form-control input-sm numOnly" value="<?php echo $loan->amount;?>" required="required"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="date">Date:</label>
                                    <input type="text" id="date" name="date" class="form-control input-sm datepicker" value="<?php echo $loan->date;?>" required="required"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="time">Time:</label>
                                    <input type="time" id="time" name="time" class="form-control input-sm" value="<?php echo $loan->time;?>" required="required"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="company">Loan Source:</label>
                                <input type="text" id="company" name="company" class="form-control input-sm" required="required" value="<?php echo $loan->name;?>"/>
                            </div>

                            <div class="form-group">
                                <label for="purpose">Purpose:</label>
                                <input type="text" id="purpose" name="purpose" class="form-control input-sm" required="required" placeholder="The purpose/reason/cause or any title of the payment" value="<?php echo $loan->purpose;?>"/>
                            </div>
                            <div class="form-group">
                                <label for="description">Description/ Note:</label>
                                <textarea class="form-control" name="description" id="description" cols="30"
                                          rows="4" placeholder="Any additional information/description or note that could be handy in future"><?php echo $loan->description;?></textarea>
                            </div>
                            <div class="form-group">
                                <button id="savePayment" class="btn btn-primary" type="submit"><span class="fa fa-save"></span>&nbsp; Update Loan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if(isset($companies) && !empty($companies))
{
    ?>
    <script>
        var companies = [<?php
                            $total_companies = count($companies);
                            $i = 1;
                            foreach($companies as $c)
                            {
                                if($i==$total_companies)
                                {
                                    echo '"'.$c->name.'"';
                                }
                                else
                                {
                                    echo '"'.$c->name.'",';
                                }
                                $i++;
                            }

                        ?>];

        $( "#company" ).autocomplete({
            source: companies
        });

    </script>
<?php
}
?>
<script>
    $(".numOnly").change(function(){
        var tAmount = $(this).val();
        tAmount = Number(tAmount).toFixed(2);
        $(this).val(tAmount);
    });
    $( ".datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
</script>
