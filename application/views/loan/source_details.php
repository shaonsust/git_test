<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>loan/sources"><i class="fa fa-dashboard"></i> Loan Sources</a></li>
                <li class="active">Loan Source Details</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header">
                <h4 class="box-title">
                    Loan Source Name: <?php echo $loan['company']->name; ?>
                </h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4">
                                <strong>Contact Person</strong>
                            </div>
                            <div class="col-md-8">
                                <?php echo $loan['company']->name; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <strong>Address</strong>
                            </div>
                            <div class="col-md-8">
                                <?php echo $loan['company']->address; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <strong>Phone Number</strong>
                            </div>
                            <div class="col-md-8">
                                <?php echo $loan['company']->contact_phone; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4">
                                <strong>Secondary Phone</strong>
                            </div>
                            <div class="col-md-8">
                                <?php echo $loan['company']->secondary_phone; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <strong>Email</strong>
                            </div>
                            <div class="col-md-8">
                                <?php echo $loan['company']->email; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <strong>Website</strong>
                            </div>
                            <div class="col-md-8">
                                <?php echo $loan['company']->website; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <strong>Notes</strong>
                    </div>
                    <div class="col-md-10">
                        <?php echo $loan['company']->notes; ?>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-solid">
                            <div class="box-header">
                                <h4 class="box-title">Accounting Summary</h4>
                            </div>
                            <div class="box-body">
                                <table class="table table-bordered table-condensed">
                                    <tr>
                                        <td>Initial Due Payable to Source</td>
                                        <th class="text-right"><?php echo number_format($loan['company']->outgoing_amount, 2, '.', ','); ?> BDT</th>
                                        <td rowspan="6"></td>
                                        <td>Initial Due Receivable from Source</td>
                                        <th class="text-right"><?php echo number_format($loan['company']->incoming_amount, 2, '.', ','); ?> BDT</th>
                                    </tr>
                                    <tr>
                                        <td>Total Loans Taken from Source</td>
                                        <th class="text-right"><?php echo number_format($loan['total_incoming_loan']->total_incoming_loan, 2, '.', ','); ?> BDT</th>
                                        <td>Total Loans Given to Source</td>
                                        <th class="text-right"><?php echo number_format($loan['total_outgoing_loan']->total_outgoing_loan, 2, '.', ','); ?> BDT</th>
                                    </tr>
                                    <tr>
                                        <td>Total Payable to Source</td>
                                        <th class="text-right">
                                            <?php
                                            $total_payable = $loan['company']->outgoing_amount + $loan['total_incoming_loan']->total_incoming_loan;
                                            echo number_format($total_payable, 2, '.', ',') . ' BDT';
                                            ?>
                                        </th>
                                        <td>Total Receivable from Source</td>
                                        <th class="text-right">
                                            <?php
                                            $total_receivable = $loan['company']->incoming_amount + $loan['total_outgoing_loan']->total_outgoing_loan;
                                            echo number_format($total_receivable, 2, '.', ',') . ' BDT';
                                            ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>Total Installments Payed to Source</td>
                                        <th class="text-right"><?php echo number_format($loan['total_incoming_installment']->total_incoming_installment, 2, '.', ','); ?> BDT</th>
                                        <td>Total Installments Received from Source</td>
                                        <th class="text-right"><?php echo number_format($loan['total_outgoing_installment']->total_outgoing_installment, 2, '.', ','); ?> BDT</th>
                                    </tr>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <th>Total Due Payable to Source</th>
                                        <th class="text-right">
                                            <?php
                                            $total_due_payable = $total_payable - $loan['total_incoming_installment']->total_incoming_installment;
                                            echo number_format($total_due_payable, 2, '.', ',') . ' BDT';
                                            ?>
                                        </th>
                                        <th>Total Due Receivable from Source</th>
                                        <th class="text-right">
                                            <?php
                                            $total_due_receivable = $total_receivable - $loan['total_outgoing_installment']->total_outgoing_installment;
                                            echo number_format($total_due_receivable, 2, '.', ',') . ' BDT';
                                            ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td colspan="5"></td>
                                    </tr>
                                    <tr>
                                        <th colspan="5" class="text-center">
                                            <?php
                                            $overall_balance = $total_due_receivable - $total_due_payable;
                                            ?>
                                            Overall Amount
                                            <?php
                                            if ($overall_balance > 0) {
                                                echo '(Receivable) from Source';
                                            } else {
                                                echo '(Payable) to Source';
                                                $overall_balance = $overall_balance * (-1);
                                            }
                                            ?>
                                            :
                                            <?php
                                            echo number_format($overall_balance, 2, '.', ',') . ' BDT';
                                            ?>
                                            |
                                            In Words: <?php echo $this->numbertowords->convert_currency($overall_balance); ?>
                                        </th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Loans Taken from Source (<?php echo $loan['total_incoming_loan']->total_incoming_loans; ?>)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped dataTable display table-small-text">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Transaction No</th>
                            <th>Date - Time</th>
                            <th>Purpose</th>
                            <th>Tran Type</th>
                            <th>Acc Name</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($loan['incoming_loan'] as $il) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><a href="<?php echo base_url() . 'loan/loan_details/' . $il->type . '/' . $il->id; ?>" title="Details"><?php echo $il->receipt_no; ?></a></td>
                                <td><?php echo $il->date . ' - ' . $il->time; ?></td>
                                <td><?php echo $il->purpose; ?></td>
                                <td><?php echo $ii->tr_type; ?></td>
                                <td><?php echo $ii->res_acc; ?></td>
                                <td align="right"><?php echo number_format($il->amount, 2, '.', ','); ?> BDT</td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Loans Given to Source (<?php echo $loan['total_outgoing_loan']->total_outgoing_loans; ?>)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped dataTable display table-small-text">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Transaction No</th>
                            <th>Date - Time</th>
                            <th>Purpose</th>
                            <th>Tran Type</th>
                            <th>Acc Name</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($loan['outgoing_loan'] as $il) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><a href="<?php echo base_url() . 'loan/loan_details/' . $il->type . '/' . $il->id; ?>" title="Details"><?php echo $il->receipt_no; ?></a></td>
                                <td><?php echo $il->date . ' - ' . $il->time; ?></td>
                                <td><?php echo $il->purpose; ?></td>
                                <td><?php echo $il->tr_type; ?></td>
                                <td><?php echo $il->res_acc; ?></td>
                                <td align="right"><?php echo number_format($il->amount, 2, '.', ','); ?> BDT</td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Installment Payed to Source (<?php echo $loan['total_incoming_installment']->total_incoming_installments; ?>)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped dataTable display table-small-text">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Transaction No</th>
                            <th>Date - Time</th>
                            <th>Tran Type</th>
                            <th>Acc Name</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($loan['incoming_installment'] as $oi) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $oi->receipt_no; ?></td>
                                <td><?php echo $oi->date . ' - ' . $oi->time; ?></td>
                                <td><?php echo $oi->tr_type; ?></td>
                                <td><?php echo $oi->res_acc; ?></td>
                                <td align="right"><?php echo number_format($oi->amount, 2, '.', ','); ?> BDT</td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Installment Received from Source (<?php echo $loan['total_outgoing_installment']->total_outgoing_installments; ?>)</h4>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover table-striped dataTable display table-small-text">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Transaction No</th>
                            <th>Date - Time</th>
                            <th>Tran Type</th>
                            <th>Acc Name</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($loan['outgoing_installment'] as $ii) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $ii->receipt_no; ?></td>
                                <td><?php echo $ii->date . ' - ' . $ii->time; ?></td>
                                <td><?php echo $ii->tr_type; ?></td>
                                <td><?php echo $ii->res_acc; ?></td>
                                <td align="right"><?php echo number_format($ii->amount, 2, '.', ','); ?> BDT</td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>