<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>loan/sources"> Loan Sources</a></li>
                <li class="active">Add New Loan Source</li>
            </ol>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
            <div class="content">
                <div class="row">
                    <div class="col-md-4">
                        <h4>Add New Loan Source</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo base_url() ?>loan/save" method="post">
                            <div class="form-group">
                                <label for="name">Source Name:</label>
                                <input type="text" id="name" required="required" name="name" class="form-control" placeholder="Source Name eg: MS SS Enterprise"/>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="contact_person">Contact Person:</label>
                                    <input type="text" id="contact_person" name="contact_person" class="form-control" placeholder="Contact Person eg: Mr. John Doe"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="contact_phone">Contact Phone:</label>
                                    <input type="tel" id="contact_phone" name="contact_phone" class="form-control" placeholder="Contact Phone eg: +880 1711 111 222"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address">Address:</label>
                                <textarea class="form-control" name="address" id="address" cols="30" rows="2" placeholder="Address eg: Suite# 34, Park Avenue, Street# 34, New York, USA"></textarea>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="secondary_phone">Secondery Phone:</label>
                                    <input type="tel" id="secondary_phone" name="secondary_phone" class="form-control" placeholder="Secondary Phone eg: +880 1711 111 222"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="email">Email Address:</label>
                                    <input type="email" id="email" name="email" class="form-control" placeholder="Email Address eg: johndoe@mail.com"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="website">Website:</label>
                                    <input type="text" id="website" name="website" class="form-control" placeholder="Website eg: http://www.example.com"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="outgoing_amount">Due Payable Till Date:</label>
                                    <input type="number" id="outgoing_amount" name="outgoing_amount" class="form-control" placeholder="Due Till Date eg: 1000"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="incoming_amount">Due Receivable Till Date:</label>
                                    <input type="number" id="incoming_amount" name="incoming_amount" class="form-control" placeholder="Due Till Date eg: 1000"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="notes">Notes:</label>
                                <textarea class="form-control" name="notes" id="notes" cols="30" rows="2" placeholder="Notes eg: Any addtional Notes that need to be stored just for informative purpose"></textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit"><span class="fa fa-save"></span>&nbsp; Save Loan Source</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
