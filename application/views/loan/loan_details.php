<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>loan/loans/<?php echo $type; ?>"><i class="fa fa-dashboard"></i> <?php echo $this->data['sub_title']; ?>s</a></li>
                <li class="active">Loan Details</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header">
                <h4 class="box-title">
                    <?php echo $sub_title; ?> Details
                    | <span class="text-right">Transaction No: <?php echo $loan['loan']->receipt_no; ?></span>
                </h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <strong>Loan Source Name</strong>
                    </div>
                    <div class="col-md-8">
                        <a href="<?php echo base_url() . 'loan/source_details/' . $loan['loan']->company_id; ?>" title="Details"><?php echo $loan['loan']->name; ?></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Contact Person</strong>
                    </div>
                    <div class="col-md-8">
                        <?php echo $loan['loan']->name; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Phone Number</strong>
                    </div>
                    <div class="col-md-8">
                        <?php echo $loan['loan']->contact_phone; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Address</strong>
                    </div>
                    <div class="col-md-8">
                        <?php echo $loan['loan']->address; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Loan Date Time</strong>
                    </div>
                    <div class="col-md-8">
                        <?php echo $loan['loan']->date . '-' . $loan['loan']->time; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Loan Purpose</strong>
                    </div>
                    <div class="col-md-8">
                        <?php echo $loan['loan']->purpose; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Description / Note</strong>
                    </div>
                    <div class="col-md-8">
                        <?php echo $loan['loan']->description; ?>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-condensed">
                            <tr>
                                <td>Loan Amount</td>
                                <th class="text-right">
                                    <?php echo number_format($loan['loan']->amount, 2, '.', ',') . ' BDT'; ?>
                                </th>
                            </tr>
                            <tr>
                                <td>Payed (Installments) Amount</td>
                                <th class="text-right">
                                    <?php echo number_format($loan['payed_amount']->payed_amount, 2, '.', ',') . ' BDT'; ?>
                                </th>
                            </tr>
                            <tr>
                                <td>Due Amount</td>
                                <th class="text-right">
                                    <?php
                                    $due_amount = $loan['loan']->amount - $loan['payed_amount']->payed_amount;
                                    echo number_format($due_amount, 2, '.', ',') . ' BDT';
                                    ?>
                                </th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header">
                <h4 class="box-title">Pay New Installment For this loan</h4>
            </div>
            <div class="box-body">
                <form action="<?php echo base_url() ?>loan/installment/<?php echo $type . '/' . $loan['loan']->id; ?>" method="post">
                    <input type="hidden" name="company_id" value="<?php echo $loan['loan']->company_id; ?>"/>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="amount">Amount:</label>
                            <input type="text" id="amount" name="amount" class="form-control input-sm numOnly" value="0.00" required="required"/>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="date">Date:</label>
                            <input type="text" id="date" name="date" class="form-control input-sm datepicker" value="<?php echo date('Y-m-d'); ?>" required="required"/>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="time">Time:</label>
                            <input type="time" id="time" name="time" class="form-control input-sm" value="<?php echo date('H:i:s'); ?>" required="required"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Description/ Note:</label>
                        <textarea class="form-control" name="description" id="description" cols="30"
                                  rows="4" placeholder="Any additional information/description or note that could be handy in future"></textarea>
                    </div>



                    <?php
                    // ********************Start payment form created by shaon*************************************
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Payment</h3>
                                </div>
                                <div class="box-body">
                                    <table id="orderTable" class="table table-bordered table-condensed">

                                        <tfoot>
                                            <tr id="payment_type">
                                                <td colspan="3" align="right">Transaction Type</td>
                                                <td>
                                                    <select class="form-control payment_type" id="payment_type" name = "payment_type">
                                                        <option value="">Select Transaction Type</option>
                                                        <option value = "check">Cheque</option>
                                                        <option value = "cash">Cash</option>
                                                    </select>
                                                </td>

                                            </tr>
                                            <tr id = "check_type">
                                                <td colspan="3" align="right">Cheque Type</td>
                                                <td>
                                                    <select class="form-control check_type" id="check_type" name = "check_type">
                                                        <option value="">Select Cheque Type</option>
                                                        <option value = "acc_pay">Account Pay</option>
                                                        <option value = "cash_pay">Cash Pay</option>
                                                    </select>
                                                </td>

                                            </tr>
                                            <tr id = "bank_acc">
                                                <td colspan="3" align="right">Bank Accounts</td>
                                                <td>
                                                    <select class="form-control bank_acc" id="bank_acc" name = "bank_acc">
                                                        <option>Select Bank Account</option>
                                                        <?php
                                                        foreach ($accounts as $a) {
                                                            if ($a->account == 'bank') {
                                                                ?>
                                                                <option value = '<?php echo $a->acc_name ?>'><?php echo $a->acc_name; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </td>

                                            </tr>
                                            <tr id = "cash_acc">
                                                <td colspan="3" align="right">Cash Accounts</td>
                                                <td>
                                                    <select class="form-control cash_acc" id="cash_acc" name = "cash_acc">
                                                        <option>Select Cash Account</option>
                                                        <?php
                                                        foreach ($accounts as $a) {
                                                            if ($a->account == 'cash') {
                                                                ?>
                                                                <option value = '<?php echo $a->acc_name ?>'><?php echo $a->acc_name; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </td>

                                            </tr>
                                            <tr style="display:none;">
                                                <td colspan="2" align="right">Vat</td>
                                                <td>
                                                    <div class="input-group" id="vat_percentage_container" style="display: none;" >
                                                        <input type="text" class="form-control input-sm numOnly" id="vat_percentage" value="0.00"/>
                                                        <span class="input-group-addon" id="basic-addon1">%</span>
                                                    </div>

                                                    <select class="form-control input-sm" name="vat_type"
                                                            id="vat_type">
                                                        <option value="amount">Amount</option>
                                                        <option value="percentage">Percentage</option>
                                                    </select>
                                                </td>
                                                <td><input type="text" name="vat" id="vat" class="form-control input-sm text-right adjustPrice numOnly" value="0.00"/></td>
                                            </tr>

                                            <tr id = "check_no">
                                                <td colspan="3" align="right">Cheque Number</td>
                                                <td><input type="text" name="check_no" id="check_no" class="form-control input-sm text-right adjustPrice"  /></td>
                                            </tr>

                                            <tr id = "bank_name">
                                                <td colspan="3" align="right">Bank Name</td>
                                                <td><input type="text" name="bank_name" id="bank_name" class="form-control input-sm text-right adjustPrice"  /></td>
                                            </tr>

                                            <tr id = "check_date">
                                                <td colspan="3" align="right">Cheque Date</td>
                                                <td><input type="text" name="check_date" class="form-control input-sm text-right datepicker" value="<?php echo date('Y-m-d'); ?>"/></td>
                                            </tr>

                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                    <?php
// ********************End payment form created by shaon*************************************
                    ?>



                    <div class="form-group">
                        <button id="savePayment" class="btn btn-primary" type="submit"><span class="fa fa-save"></span>&nbsp; Save Installment</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Loan Installments</h4>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Transaction No</th>
                            <th>Date - Time</th>
                            <th>Description</th>
                            <th>Tran Type</th>
                            <th>Acc Name</th>
                            <th>Amount</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($loan['installments'] as $installment) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $installment->receipt_no ?></td>
                                <td><?php echo $installment->date . ' - ' . $installment->time; ?></td>
                                <td><?php echo $installment->description; ?></td>
                                <td><?php echo $installment->tr_type; ?></td>
                                <td><?php echo $installment->res_acc; ?></td>
                                <td align="right"><?php echo number_format($installment->amount, 2, '.', ','); ?> BDT</td>
                                <td align="center">
                                    <a href="<?php echo base_url() . 'loan/edit_installment/' . $type . '/' . $installment->parent_id . '/' . $installment->id; ?>" class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-pencil"></i></a>
                                    <a href="<?php echo base_url() . 'loan/delete_installment/' . $type . '/' . $installment->parent_id . '/' . $installment->id; ?>" class="btn btn-danger btn-xs" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    $(".numOnly").change(function () {
        var tAmount = $(this).val();
        tAmount = Number(tAmount).toFixed(2);
        $(this).val(tAmount);
    });
    $(".datepicker").datepicker({
        dateFormat: "yy-mm-dd"
    });
</script>


<script type="text/javascript">



    $('document').ready(function () {

        $('#check_type').hide();
        $('#bank_acc').hide();
        $('#cash_acc').hide();
        $('#bank_name').hide();
        $('#check_no').hide();
        $('#check_date').hide();

        $('#payment_type').change(account_type);
        $('#check_type').change(check_type);
    });

    function account_type()
    {
        var type = $('.payment_type').val();
        //alert(type);
        if (type == 'check')
        {
            $('#check_type').show();
            $('#check_no').show();
            $('#bank_name').show();
            $('#check_date').show();
            $('#bank_acc').hide();
            $('#cash_acc').hide();
        }
        else if (type == 'cash')
        {
            $('#cash_acc').show();
            $('#bank_acc').hide();
            $('#check_type').hide();
            $('#check_no').hide();
            $('#bank_name').hide();
            $('#check_date').hide();
        }
        else
        {
            $('#check_type').hide();
            $('#bank_acc').hide();
            $('#cash_acc').hide();
            $('#bank_name').hide();
            $('#check_no').hide();
            $('#check_date').hide();
        }

        return false;
    }

    function check_type()
    {
        var check_type = $('.check_type').val();
        if (check_type == 'acc_pay')
        {
            $('#bank_acc').show();
            $('#cash_acc').hide();
        }
        else if (check_type == 'cash_pay')
        {
            $('#bank_acc').hide();
            $('#cash_acc').show();
        }
        else
        {
            $('#bank_acc').hide();
            $('#cash_acc').hide();
        }
    }
</script>
