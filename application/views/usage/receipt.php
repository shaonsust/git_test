<style>
    @media screen {
        div.divFooter {
            display: none;
        }
    }
    @media print {
        .noPrint {
            display: none;
        }
        div.divFooter {
            position: fixed;
            bottom: 15px;
            right: 15px;
            page-break-after: always;
        }
        @page { margin: 0; }
        body { margin: 1.6cm; }
        .table-condensed tr td, .table-condensed tr th{
            font-size: 10px;
            line-height: 20px;
            padding: 2px 5px;
        }

    }
</style>
<div class="row noPrint">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>usage/index/<?php echo $this->data['type'] ?>"> <?php echo $this->data['sub_title'] ?> Usage</a></li>
                <li class="active"><?php echo $this->data['sub_title'] ?> Usage Details</li>
            </ol>
        </section>
    </div>
</div>
<?php
$message = $this->session->userdata('message');
$class = $this->session->userdata('class');
if (isset($message) && !empty($message)) {
    ?>
    <div class="alert alert-<?php echo $class; ?> alert-dismissible noPrint" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
    <?php
    $this->session->unset_userdata('message');
    $this->session->unset_userdata('class');
}
?>
<div class="row">
    <div class="col-md-10">
        <div class="box box-primary">
            <div class="box-header noPrint">
                <div class="row">
                    <div class="col-md-5">
                        <h4 class="box-title">
                            Usage Details - <?php echo $usage['usage']->receipt_no; ?>
                        </h4>
                    </div>
                    <div class="col-md-7 text-right">
                        <button class="btn btn-default" onclick="printMe()"> <i class="fa fa-print"></i> &nbsp;Print</button>
                        <a href="<?php echo base_url() . 'usage/index/' . $this->data['type']; ?>" class="btn btn-info"> <i class="fa fa-arrow-left"></i> &nbsp;Back</a>
                        <a href="<?php echo base_url() . 'usage/edit/' . $this->data['type'] . '/' . $usage['usage']->id; ?>" class="btn btn-primary"> <i class="fa fa-pencil"></i> &nbsp;Edit</a>
                        <a href="<?php echo base_url() . 'usage/delete/' . $this->data['type'] . '/' . $usage['usage']->id; ?>" class="btn btn-danger"> <i class="fa fa-trash"></i> &nbsp;Delete</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%">
                            <tr>
                                <td width="20%" align="center">
<!--                                    <img src="<?php echo base_url(); ?>logo.jpg" alt=""/>-->
                                </td>
                                <td width="60%" align="center">
                                    <h2>Bismillah Enterprise.</h2>
                                    <h3>Star Particle Board Mills Ltd.</h3>
                                    <p>55, Kazi Nazrul Islam Avenue, Kawran Bazar, Dhaka-1215</p>
                                </td>
                                <td width="20%" align="right">
                                    <p>01926-369889</br>
                                        9127586
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%">
                            <tr>
                                <td width="20%">
                                    Purpose<br/>
                                    Description
                                </td>
                                <td width="45%">
                                    : <?php echo $usage['usage']->purpose; ?> <br/>
                                    : <?php echo $usage['usage']->description; ?>
                                </td>
                                <td width="35%" align="right">
                                    Date: <?php echo $usage['usage']->date . ' ' . $usage['usage']->time; ?><br/>
                                    Receipt No: <?php echo $usage['usage']->receipt_no; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%">
                            <tr>
                                <td align="center">
                                    <h3><u>Usage Details</u></h3>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-condensed" width="100%">
                            <tr>
                                <th width="5%">sl.</th>
                                <th width="50%">Item Name</th>
                                <th width="15%">Qty</th>
                            </tr>
                            <?php
                            $i = 1;
                            foreach ($usage['items'] as $ui) {
                                ?>
                                <tr>
                                    <td width="5%"><?php echo $i; ?></td>
                                    <td width="50%"><?php echo $ui->name; ?></td>
                                    <td width="15%" align="right"><?php echo $ui->qty . ' ' . $ui->unit; ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <tr>
                                <td width="10%">&nbsp;</td>
                                <td width="30%" align="center">
                                    <br/>
                                    <br/>
                                    <br/>
                                    ......................................................
                                    <br/>
                                    Items Received By
                                </td>
                                <td width="20%">&nbsp;</td>
                                <td width="30%" align="center">
                                    <br/>
                                    <br/>
                                    <br/>
                                    ......................................................
                                    <br/>
                                    For Bismillah Enterprise Office
                                </td>
                                <td width="20%">&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="divFooter">Software Developed By: SCORSHIA.COM (http://www.scorshia.com)</div>

<div class="row noPrint">
    <div class="col-md-12">
        <button class="btn btn-default" onclick="printMe()"> <i class="fa fa-print"></i> &nbsp;Print</button>
        <a href="<?php echo base_url() . 'usage/index/' . $this->data['type']; ?>" class="btn btn-info"> <i class="fa fa-arrow-left"></i> &nbsp;Back</a>
        <a href="<?php echo base_url() . 'usage/edit/' . $this->data['type'] . '/' . $usage['usage']->id; ?>" class="btn btn-primary"> <i class="fa fa-pencil"></i> &nbsp;Edit</a>
        <a href="<?php echo base_url() . 'usage/delete/' . $this->data['type'] . '/' . $usage['usage']->id; ?>" class="btn btn-danger"> <i class="fa fa-trash"></i> &nbsp;Delete</a>
    </div>
</div>
<script>
    function printMe() {
        window.print();
    }
</script>


