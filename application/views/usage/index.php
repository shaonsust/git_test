    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active">Usage</li>
                </ol>
            </section>
        </div>
    </div>
    <?php
        $message = $this->session->userdata('message');
        $class = $this->session->userdata('class');
        if(isset($message) && !empty($message))
        {
    ?>
            <div class="alert alert-<?php echo $class;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $message;?>
            </div>
    <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('class');
        }
    ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Usage - <?php echo $this->data['sub_title'];?> &nbsp;
                    <a class="btn btn-primary" href="<?php echo base_url();?>usage/new_usage/<?php echo $this->data['type'];?>"><span class="fa fa-plus"></span> &nbsp; New <?php echo $this->data['sub_title'];?> Usage</a>
                </h4>
            </div>
            <div class="box-body">
                <table id="table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                    <tr>
                        <th>Sl.</th>
                        <th>Receipt No.</th>
                        <th>Date - Time</th>
                        <th>Purpose</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach($usage as $u)
                    {
                        ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><a href="<?php echo base_url() . 'usage/details/'. $this->data['type'] . '/' . $u->id; ?>" title="Details"><?php echo $u->receipt_no;?></a></td>
                            <td><?php echo $u->date.' - '.$u->time;?></td>
                            <td><a href="<?php echo base_url() . 'usage/details/'. $this->data['type'] . '/' . $u->id; ?>" title="Details"><?php echo $u->purpose;?></a></td>
                            <td><?php echo $u->description;?></td>
                            <td align="center">
                                <a href="<?php echo base_url() . 'usage/edit/'. $this->data['type'] . '/'  . $u->id ?>" class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo base_url() . 'usage/delete/'. $this->data['type'] . '/'  . $u->id ?>" class="btn btn-danger btn-xs" onclick="return CheckDelete()" title="Delete"><i class="fa fa-remove"></i></a>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


