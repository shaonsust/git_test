
<div class="row">
    <div class="col-md-12">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>welcome"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>usage/index/<?php echo $this->data['type']?>"> <?php echo $this->data['sub_title']?> Usage</a></li>
                <li class="active">Edit <?php echo $this->data['sub_title']?> Usage</li>
            </ol>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Edit <?php echo $this->data['sub_title']?> Usage - <?php echo $usage['usage']->receipt_no;?></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo base_url() ?>usage/update/<?php echo $this->data['type'].'/'.$usage['usage']->id;?>" method="post">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="date">Date:</label>
                                    <input type="text" id="date" name="date" class="form-control input-sm datepicker" value="<?php echo $usage['usage']->date;?>" required="required" />
                                </div>
                                <div class="form-group col-md-4 col-md-offset-4">
                                    <label for="time">Time:</label>
                                    <input type="time" id="time" name="time" class="form-control input-sm" value="<?php echo $usage['usage']->time;?>" required="required"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="purpose">Purpose:</label>
                                <input type="text" id="purpose" name="purpose" class="form-control input-sm" required="required" placeholder="The purpose of items usage" value="<?php echo $usage['usage']->purpose;?>"/>
                            </div>
                            <div class="form-group">
                                <label for="description">Description:</label>
                                <textarea class="form-control" name="description" id="description" cols="30"
                                          rows="3" placeholder="How and where are the below items going to be used?"><?php echo $usage['usage']->description;?></textarea>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-solid">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Items in the Usage Order</h3>
                                        </div>
                                        <div class="box-body">
                                            <table id="orderTable" class="table table-bordered table-condensed">
                                                <thead>
                                                <tr>
                                                    <th width="70%">Item</th>
                                                    <th width="30%">Qty</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach($usage['items'] as $ui)
                                                {
                                                    ?>
                                                    <tr class="orderRow" id="orderProduct<?php echo $ui->item_id;?>">
                                                        <td>
                                                            <input type="hidden" name="item_id[]" value="<?php echo $ui->item_id;?>"/>
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <?php echo $ui->name;?>
                                                                    <input type="hidden" class="form-control input-sm" name="product_name[]" value="<?php echo $ui->name;?>" readonly/>
                                                                    </div>
                                                                <div class="col-md-2 text-right">
                                                                    <button type="button" class="btn btn-danger btn-sm orderRemove"><span class="fa fa-remove"></span></button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        <td>
                                                            <div class="input-group">
                                                                <input type="number" class="form-control input-sm orderQty numOnly" name="qty[]" value="<?php echo $ui->qty;?>"/>
                                                                <span class="input-group-addon" id="basic-addon1"><?php echo $ui->unit;?></span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button id="processOrder" class="btn btn-primary" type="submit"><span class="fa fa-save"></span>&nbsp; Update Order</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="box box-primary">
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Available Items</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table id="productTable" class="table table-bordered table-condensed table-hover table-striped dataTable">
                            <thead>
                            <tr>
                                <th>Item</th>
                                <th>qty</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($items as $item)
                            {
                                ?>
                                <tr>
                                    <td width="60%">
                                        <a class="productName" href="<?php echo base_url() . $item->type .'/details/' . $item->id ?>" target="_blank" title="Details"><?php echo $item->name;?></a>
                                        <input type="hidden" class="itemId" value="<?php echo $item->id; ?>"/>
                                        <input type="hidden" class="productUnit" value="<?php echo $item->unit; ?>"/>
                                    </td>
                                    <td width="30%">
                                        <div class="input-group">
                                            <input type="number" class="productQty form-control input-sm productTableInput numOnly" name="qty" value="1"/>
                                            <span class="input-group-addon" id="basic-addon2"><?php echo $item->unit; ?></span>
                                        </div>
                                    </td>
                                    <td width="10%"><button class="btn btn-sm btn-block bg-olive addProduct"><span class="fa fa-plus"></span></button></td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $( ".datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
    $(".addProduct").click(function(){
        $("tr.danger").remove();
        var itemId = $(this).closest('tr').find('.itemId').val();
        var productName = $(this).closest('tr').find('.productName').text();
        var productQty = $(this).closest('tr').find('.productQty').val();
        var productUnit = $(this).closest('tr').find('.productUnit').val();
        if($("#orderProduct"+itemId).length)
        {
            var prevQty = $("#orderProduct"+itemId).find(".orderQty").val();
            var newQty = Number(prevQty) + Number(productQty);
            $("#orderProduct"+itemId).find(".orderQty").val(newQty);
            adjustPrice("#orderProduct"+itemId);
        }
        else {
            var newRow = '<tr class="orderRow" id="orderProduct'+ itemId +'">' +
                '<td>' +
                '<input type="hidden" name="item_id[]" value="' + itemId + '"/>' +
                '<div class="row">' +
                '<div class="col-md-10">' +
                productName +
                '<input type="hidden" class="form-control input-sm" name="product_name[]" value="' + productName + '" readonly/>' +
                '</div>' +
                '<div class="col-md-2 text-right">' +
                '<button type="button" class="btn btn-danger btn-sm orderRemove"><span class="fa fa-remove"></span></button>' +
                '</div>' +
                '</div>' +
                '</td>' +
                '<td>' +
                '<div class="input-group">' +
                '<input type="number" class="form-control input-sm orderQty numOnly" name="qty[]" value="' + productQty + '"/>' +
                '<span class="input-group-addon" id="basic-addon1">' + productUnit + '</span>' +
                '</div>' +
                '</td>' +
                '</tr>';
            $("#orderTable").find('tbody').append(newRow);
            $('.numOnly').keypress(function(key) {
                if((key.charCode < 48 || key.charCode > 57) && (key.charCode != 46)) return false;
            });
        }

    });


    $(document).on("click", '.orderRemove', function(){
        $(this).closest("tr").remove();
        adjustPrice();
        if($(".orderRow").length)
        {
            return true;
        }
        else {
            var newRow = '<tr class="danger">' +
                '<td colspan="4" align="center">' +
                '<strong> Please Add Few Products to Process Order</strong>' +
                '</td>' +
                '</tr>';
            $("#orderTable").find('tbody').append(newRow);
        }
    });

    $("#processOrder").click(function(){
        if($(".orderRow").length)
        {
            return true;
        }
        else {
            $("tr.danger").remove();
            var newRow = '<tr class="danger">' +
                '<td colspan="4" align="center" valign="middle" >' +
                '<strong> Please Add Few Products to Process Order</strong>' +
                '</td>' +
                '</tr>';
            $("#orderTable").find('tbody').append(newRow);
            $("tr.danger").find('strong').animate({height: '100px', opacity: '0.7', lineHeight: '100px', fontSize: '22px'}, "slow", "swing");
            $("tr.danger").find('strong').animate({height: '35px', opacity: '1.0', lineHeight: '35px', fontSize: '14px'}, "slow", "swing");
            return false;
        }
    });
    $(".numOnly").change(function(){
        var tAmount = $(this).val();
        tAmount = Number(tAmount).toFixed(2);
        $(this).val(tAmount);
    });
</script>