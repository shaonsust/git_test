<?php
$config = array(
    'signin' => array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required'
        ),
    ),
    'setting/update_title' => array(
        array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'required'
        ),
        array(
            'field' => 'slogan',
            'label' => 'Slogan',
            'rules' => 'required'
        ),
    ),
    'update_contact_details' => array(
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|valid_email'
        ),
        array(
            'field' => 'number',
            'label' => 'Number',
            'rules' => 'required|numeric'
        ),
        array(
            'field' => 'address',
            'label' => 'Address',
            'rules' => 'required'
        ),
        array(
            'field' => 'map',
            'label' => 'Map',
            'rules' => ''
        ),
    ),
    'update_social_links' => array(
        array(
            'field' => 'facebook',
            'label' => 'Facebook',
            'rules' => 'prep_url'
        ),
        array(
            'field' => 'twitter',
            'label' => 'Twitter',
            'rules' => 'prep_url'
        ),
        array(
            'field' => 'linkedin',
            'label' => 'Linkedin',
            'rules' => 'prep_url'
        ),
        array(
            'field' => 'other',
            'label' => 'Other',
            'rules' => 'prep_url'
        )
    ),
    'page' => array(
        array(
            'field' => 'page_name',
            'label' => 'Page_name',
            'rules' => 'required'
        ),
        array(
            'field' => 'page_title',
            'label' => 'Page_title',
            'rules' => 'required'
        ),
        array(
            'field' => 'page_subtitle',
            'label' => 'Page_subtitle',
            'rules' => ''
        ),
        array(
            'field' => 'page_content',
            'label' => 'Page_content',
            'rules' => ''
        ),
        array(
            'field' => 'comment_status',
            'label' => 'Comment_status',
            'rules' => ''
        ),
        array(
            'field' => 'featured_image',
            'label' => 'Featured_image',
            'rules' => ''
        ),
        array(
            'field' => 'slider_id',
            'label' => 'Slider_id',
            'rules' => ''
        ),
        array(
            'field' => 'page_parent_id',
            'label' => 'Page_parent_id',
            'rules' => ''
        ),
        array(
            'field' => 'tags',
            'label' => 'Tags',
            'rules' => ''
        ),
        array(
            'field' => 'meta_key',
            'label' => 'Meta_key',
            'rules' => ''
        ),
        array(
            'field' => 'meta_description',
            'label' => 'Meta_description',
            'rules' => ''
        )
    ),
    'slider' => array(
        array(
            'field' => 'slider_name',
            'label' => 'Slider Name',
            'rules' => 'required'
        ),
        array(
            'field' => 'slider_details',
            'label' => 'Slider Details',
            'rules' => ''
        )
    ),
    'slide' => array(
        array(
            'field' => 'slide_order',
            'label' => 'Slide Order',
            'rules' => 'required'
        ),
        array(
            'field' => 'slide_name',
            'label' => 'Slide name',
            'rules' => 'required'
        ),
        array(
            'field' => 'slide_description',
            'label' => 'Slide Description',
            'rules' => ''
        ),
        array(
            'field' => 'tags',
            'label' => 'Tags',
            'rules' => ''
        ),
        array(
            'field' => 'meta_key',
            'label' => 'Meta_key',
            'rules' => ''
        ),
        array(
            'field' => 'meta_description',
            'label' => 'Meta_description',
            'rules' => ''
        )
    ),
    'gallery' => array(
        array(
            'field' => 'gallery_name',
            'label' => 'Gallery Name',
            'rules' => 'required'
        ),
        array(
            'field' => 'gallery_details',
            'label' => 'Gallery Details',
            'rules' => ''
        )
    ),
    'photo' => array(
        array(
            'field' => 'photo_order',
            'label' => 'Photo Order',
            'rules' => 'required'
        ),
        array(
            'field' => 'photo_name',
            'label' => 'Photo name',
            'rules' => 'required'
        ),
        array(
            'field' => 'photo_description',
            'label' => 'Photo Description',
            'rules' => ''
        ),
        array(
            'field' => 'tags',
            'label' => 'Tags',
            'rules' => ''
        ),
        array(
            'field' => 'meta_key',
            'label' => 'Meta_key',
            'rules' => ''
        ),
        array(
            'field' => 'meta_description',
            'label' => 'Meta_description',
            'rules' => ''
        )
    ),
    'postCat' => array(
        array(
            'field' => 'category_name',
            'label' => 'Category Name',
            'rules' => 'required'
        ),
        array(
            'field' => 'category_details',
            'label' => 'Category Details',
            'rules' => ''
        ),
        array(
            'field' => 'category_parent_id',
            'label' => 'Category Parent ID',
            'rules' => ''
        )
    ),
    'post' => array(
        array(
            'field' => 'post_title',
            'label' => 'Post Title',
            'rules' => 'required'
        ),
        array(
            'field' => 'post_subtitle',
            'label' => 'Page_subtitle',
            'rules' => ''
        ),
        array(
            'field' => 'post_content',
            'label' => 'Page_content',
            'rules' => ''
        ),
        array(
            'field' => 'comment_status',
            'label' => 'Comment_status',
            'rules' => ''
        ),
        array(
            'field' => 'featured_image',
            'label' => 'Featured_image',
            'rules' => ''
        ),
        array(
            'field' => 'slider_id',
            'label' => 'Slider_id',
            'rules' => ''
        ),
        array(
            'field' => 'category_id',
            'label' => 'Page_parent_id',
            'rules' => ''
        ),
        array(
            'field' => 'tags',
            'label' => 'Tags',
            'rules' => ''
        ),
        array(
            'field' => 'meta_key',
            'label' => 'Meta_key',
            'rules' => ''
        ),
        array(
            'field' => 'meta_description',
            'label' => 'Meta_description',
            'rules' => ''
        )
    ),
    'portfolio' => array(
        array(
            'field' => 'portfolio_title',
            'label' => 'Portfolio Title',
            'rules' => 'required'
        ),
        array(
            'field' => 'portfolio_subtitle',
            'label' => 'Portfolio Sub-Title',
            'rules' => ''
        ),
        array(
            'field' => 'portfolio_subtitle',
            'label' => 'Portfolio Sub-Title',
            'rules' => ''
        )
    ),
    'packageCat' => array(
        array(
            'field' => 'category_name',
            'label' => 'Category Name',
            'rules' => 'required'
        ),
        array(
            'field' => 'category_details',
            'label' => 'Category Details',
            'rules' => ''
        )
    ),
    'package' => array(
        array(
            'field' => 'package_name',
            'label' => 'Package Name',
            'rules' => 'required'
        ),
        array(
            'field' => 'post_title',
            'label' => 'Post Title',
            'rules' => 'required'
        ),
        array(
            'field' => 'post_subtitle',
            'label' => 'Page_subtitle',
            'rules' => ''
        ),
        array(
            'field' => 'post_content',
            'label' => 'Page_content',
            'rules' => ''
        ),
        array(
            'field' => 'package_table',
            'label' => 'Page_content',
            'rules' => ''
        ),
        array(
            'field' => 'category_id',
            'label' => 'Page_parent_id',
            'rules' => ''
        ),
        array(
            'field' => 'tags',
            'label' => 'Tags',
            'rules' => ''
        ),
        array(
            'field' => 'meta_key',
            'label' => 'Meta_key',
            'rules' => ''
        ),
        array(
            'field' => 'meta_description',
            'label' => 'Meta_description',
            'rules' => ''
        )
    ),
    'client' => array(
        array(
            'field' => 'first_name',
            'label' => 'First Name',
            'rules' => 'required'
        ),
        array(
            'field' => 'last_name',
            'label' => 'Last Name',
            'rules' => 'required'
        ),
        array(
            'field' => 'client_userid',
            'label' => 'Client User Id',
            'rules' => 'required'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => ''
        ),
        array(
            'field' => 'client_category',
            'label' => 'Client Category',
            'rules' => ''
        ),
        array(
            'field' => 'client_email',
            'label' => 'Client Email',
            'rules' => ''
        ),
        array(
            'field' => 'client_mobile',
            'label' => 'Client Mobile',
            'rules' => ''
        ),
        array(
            'field' => 'client_website',
            'label' => 'Client Website',
            'rules' => ''
        ),
        array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => ''
        ),
        array(
            'field' => 'is_featured',
            'label' => 'Is Featured',
            'rules' => ''
        )
    ),
    'member' => array(
        array(
            'field' => 'first_name',
            'label' => 'First Name',
            'rules' => 'required'
        ),
        array(
            'field' => 'last_name',
            'label' => 'Last Name',
            'rules' => 'required'
        ),
        array(
            'field' => 'member_category',
            'label' => 'Member Category',
            'rules' => 'required'
        ),
        array(
            'field' => 'designation',
            'label' => 'Designation',
            'rules' => 'required'
        ),
        array(
            'field' => 'member_email',
            'label' => 'Member Email',
            'rules' => ''
        ),
        array(
            'field' => 'member_mobile',
            'label' => 'Member Mobile',
            'rules' => ''
        ),
        array(
            'field' => 'facebook_link',
            'label' => 'Page_content',
            'rules' => ''
        ),
        array(
            'field' => 'twitter_link',
            'label' => 'Page_content',
            'rules' => ''
        ),
        array(
            'field' => 'google_plus_link',
            'label' => 'Page_content',
            'rules' => ''
        ),
        array(
            'field' => 'linkedin_link',
            'label' => 'Page_content',
            'rules' => ''
        ),
        array(
            'field' => 'description',
            'label' => 'Page_parent_id',
            'rules' => ''
        ),
        array(
            'field' => 'is_featured',
            'label' => 'Tags',
            'rules' => ''
        )
    ),
    'menu' => array(
        array(
            'field' => 'video_link',
            'label' => 'Video Link',
            'rules' => 'alpha_dash'
        ),
        array(
            'field' => 'vi_menu',
            'label' => 'Video Link',
            'rules' => ''
        ),
        array(
            'field' => 'menu_type',
            'label' => 'Video Link',
            'rules' => ''
        ),
        array(
            'field' => 'menu_image',
            'label' => 'Video Link',
            'rules' => ''
        ),
        array(
            'field' => 'old_menu_image',
            'label' => 'Video Link',
            'rules' => ''
        ),
        array(
            'field' => 'pages',
            'label' => 'Video Link',
            'rules' => ''
        )
    ),
    'sign_up' => array(
        array(
            'field' => 'first_name',
            'label' => 'First Name',
            'rules' => 'required'
        ),
        array(
            'field' => 'last_name',
            'label' => 'Last Name',
            'rules' => 'required'
        ),
        array(
            'field' => 'client_userid',
            'label' => 'Client User Id',
            'rules' => 'required'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required'
        ),
        array(
            'field' => 'client_email',
            'label' => 'Client Email',
            'rules' => ''
        ),
        array(
            'field' => 'client_mobile',
            'label' => 'Client Mobile',
            'rules' => ''
        ),
        array(
            'field' => 'client_website',
            'label' => 'Client Website',
            'rules' => ''
        ),
    )
);

?>