<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hr extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('login');
        }
        $this->load->model('hr_model', 'hm');
        $this->load->model('accounting_model', 'am');
        $this->data['user_role'] = $this->session->userdata('user_role');
        $this->data['title'] = 'Human Resource';
        $this->data['menu_active'] = 'hr';
    }

    public function employees() {
        $this->data['sub_title'] = 'All Employees';
        $this->data['employees'] = $this->hm->getAll();
        $this->data['main_content'] = $this->load->view('hr/employees', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function add_new_employee() {
        $this->data['sub_title'] = 'New Employee';
        $this->data['main_content'] = $this->load->view('hr/new_employee', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function save_employee() {
        $save = $this->hm->save();
        if ($save) {
            if ($save == 'exist') {
                $sdata['message'] = '<strong>Failure:</strong> Duplicate Entry Detected. Please try a different Name';
                $sdata['class'] = 'danger';
            } else {
                $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
                $sdata['class'] = 'success';
            }
        } else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('hr/employees');
    }

    public function edit_staff($id) {
        if ($this->data['user_role'] == 1 OR $this->data['user_role'] == 2) {
            $this->data['sub_title'] = 'Edit Employee';
            $this->data['employee'] = $this->hm->get_individual($id);
            $this->data['main_content'] = $this->load->view('hr/edit_employee', $this->data, true);
            $this->load->view('master', $this->data);
        } else {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('supplier');
        }
    }

    public function update_employee($id) {
        $update = $this->hm->update_employee($id);
        if ($update) {
            if ($update == 'exist') {
                $sdata['message'] = '<strong>Failure:</strong> Duplicate Entry Detected. Please try a different Name';
                $sdata['class'] = 'danger';
            } else {
                $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
                $sdata['class'] = 'success';
            }
        } else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('hr/employees');
    }

    public function status_update($status, $id) {
        if ($this->data['user_role'] == 1 OR $this->data['user_role'] == 2) {
            $update = $this->hm->status_update($status, $id);
            if ($update) {
                $sdata['message'] = '<strong>Success:</strong> Data Updated Successfully';
                $sdata['class'] = 'success';
            } else {
                $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
                $sdata['class'] = 'warning';
            }
        } else {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('hr/employees');
    }

    public function delete_staff($id) {
        if ($this->data['user_role'] == 1 OR $this->data['user_role'] == 2) {
            $delete = $this->hm->delete_staff($id);
            if ($delete) {
                $sdata['message'] = '<strong>Success:</strong> Data Deleted Successfully';
                $sdata['class'] = 'success';
            } else {
                $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
                $sdata['class'] = 'warning';
            }
        } else {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('hr/employees');
    }

    public function pay_salary($id) {
        $this->data['sub_title'] = 'Salary Payment';
        $this->data['employee'] = $this->hm->get_individual($id);
        $this->data['accounts'] = $this->am->account_info();
        $this->data['main_content'] = $this->load->view('hr/pay_salary', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function process_salary($id) {
        $save = $this->hm->process_salary($id);
        if ($save) {
            if ($save == 'exist') {
                $sdata['message'] = '<strong>Failure:</strong> Duplicate Entry Detected. Please try a different Month';
                $sdata['class'] = 'danger';
                $this->session->set_userdata($sdata);
                redirect('hr/pay_salary/' . $id);
            } else {
                $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
                $sdata['class'] = 'success';
                $this->session->set_userdata($sdata);
                redirect('hr/salary_details/' . $save);
            }
        } else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('hr/pay_salary/' . $id);
        }
    }

    public function salary() {
        $this->data['sub_title'] = 'All Salaries';
        $this->data['salaries'] = $this->hm->getAllSalaries();
        $this->data['main_content'] = $this->load->view('hr/salary', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function salary_details($id) {
        $this->data['sub_title'] = 'Salary Details';
        $this->data['salary'] = $this->hm->getSalary($id);
        $this->data['main_content'] = $this->load->view('hr/salary_details', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function edit_salary($id) {
        if ($this->data['user_role'] == 1 OR $this->data['user_role'] == 2) {
            $this->data['sub_title'] = 'Edit Details';
            $this->data['salary'] = $this->hm->getSalary($id);
            $this->data['main_content'] = $this->load->view('hr/edit_salary', $this->data, true);
            $this->load->view('master', $this->data);
        } else {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('hr/salary');
        }
    }

    public function update_salary($id) {
        $update = $this->hm->update_salary($id);
        if ($update) {
            if ($update == 'exist') {
                $sdata['message'] = '<strong>Failure:</strong> Duplicate Entry Detected. Please try a different Month';
                $sdata['class'] = 'danger';
                $this->session->set_userdata($sdata);
                redirect('hr/edit_salary/' . $id);
            } else {
                $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
                $sdata['class'] = 'success';
                $this->session->set_userdata($sdata);
                redirect('hr/salary_details/' . $id);
            }
        } else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('hr/edit_salary/' . $id);
        }
    }

    public function delete_salary($id) {
        if ($this->data['user_role'] == 1 OR $this->data['user_role'] == 2) {
            $delete = $this->hm->delete_salary($id);
            if ($delete) {
                $sdata['message'] = '<strong>Success:</strong> Data Deleted Successfully';
                $sdata['class'] = 'success';
            } else {
                $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
                $sdata['class'] = 'warning';
            }
        } else {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('hr/salary');
    }

    public function details($id) {
        $this->data['id'] = $id;
        $this->data['sub_title'] = 'Employee Details';
        $this->data['staff'] = $this->hm->get_staff_details($id);
        $this->data['main_content'] = $this->load->view('hr/employee_details', $this->data, true);
        $this->load->view('master', $this->data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */