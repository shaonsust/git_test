<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utility_Item extends CI_Controller
{

    public $data;
    public function __construct()
    {
        parent::__construct();
        $user_id=$this->session->userdata('user_id');
        if(empty($user_id))
        {
            redirect('login');
        }
        $this->load->model('items_model', 'im');
        $this->load->model('order_model', 'om');
        $this->data['user_role'] = $this->session->userdata('user_role');
        $this->data['menu_active'] = 'utility_item';
        $this->data['title'] = 'Utility Items';
    }

    public function index()
    {
        $this->data['item_active'] = 'all_utility_item';
        $this->data['sub_title'] = 'All Utility Items';
        $this->data['items'] = $this->im->get_items_index('utility_item');
        $this->data['main_content'] = $this->load->view('utility_item/index', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function add_new()
    {
        $this->data['sub_title'] = 'New Utility Item';
        $this->data['main_content'] = $this->load->view('utility_item/new', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function save()
    {
        $ndata['name'] = $this->input->post('name');
        $ndata['unit'] = $this->input->post('unit');
        $ndata['price'] = $this->input->post('price');
        $ndata['stock'] = $this->input->post('stock');
        $ndata['min_stock'] = $this->input->post('min_stock');
        $ndata['type'] = 'utility_item';
        $ndata['notes'] = $this->input->post('notes');

        $save = $this->im->save($ndata);
        if($save)
        {
            if($save == 'exist')
            {
                $sdata['message'] = '<strong>Failure:</strong> Duplicate Entry Detected. Please try a different Name';
                $sdata['class'] = 'danger';
            }
            else {
                $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
                $sdata['class'] = 'success';
            }
        }
        else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('utility_item');
    }
    public function edit($id)
    {
        if($this->data['user_role'] == 1 OR $this->data['user_role'] == 2)
        {
            $this->data['sub_title'] = 'Edit Utility Item';
            $this->data['item'] = $this->im->get_individual($id);
            $this->data['main_content'] = $this->load->view('utility_item/edit', $this->data, true);
            $this->load->view('master', $this->data);
        }
        else
        {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('utility_item');
        }

    }
    public function update($id)
    {
        $udata['name'] = $this->input->post('name');
        $udata['unit'] = $this->input->post('unit');
        $udata['price'] = $this->input->post('price');
        $udata['stock'] = $this->input->post('stock');
        $udata['min_stock'] = $this->input->post('min_stock');
        $udata['notes'] = $this->input->post('notes');
        $update = $this->im->update($udata, $id);
        if($update)
        {
            if($update == 'exist')
            {
                $sdata['message'] = '<strong>Failure:</strong> Duplicate Entry Detected. Please try a different Name';
                $sdata['class'] = 'danger';
            }
            else {
                $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
                $sdata['class'] = 'success';
            }
        }
        else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';

        }
        $this->session->set_userdata($sdata);
        redirect('utility_item');
    }
    public function delete($id)
    {
        if($this->data['user_role'] == 1 OR $this->data['user_role'] == 2)
        {
            $delete = $this->im->delete($id);
            if($delete)
            {
                $sdata['message'] = '<strong>Success:</strong> Data Deleted Successfully';
                $sdata['class'] = 'success';
            }
            else {
                $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
                $sdata['class'] = 'warning';
            }
        }
        else
        {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('utility_item');

    }
    public function details($id)
    {
        $this->data['id'] = $id;
        $star_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        if(isset($star_date) && !empty($star_date))
        {
            $this->data['start_date'] = date('Y-m-d', strtotime($star_date));
            $this->data['filtered'] = 'yes';
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('0000-00-00'));
            $this->data['filtered'] = 'no';
        }
        if(isset($end_date) && !empty($end_date))
        {
            $this->data['end_date'] = date('Y-m-d', strtotime($end_date));
        }
        else {
            $this->data['end_date'] = date('Y-m-d');
        }
        $this->data['sub_title'] = 'Utility Item Details';
        $this->data['item'] = $this->im->get_details($id, $this->data['start_date'], $this->data['end_date']);
        $this->data['main_content'] = $this->load->view('utility_item/details', $this->data, true);
        $this->load->view('master', $this->data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */