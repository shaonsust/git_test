<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sales extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('login');
        }
        $this->load->model('company_model', 'cm');
        $this->load->model('order_model', 'om');
        $this->load->model('accounting_model', 'am');
        $this->data['user_role'] = $this->session->userdata('user_role');
        $this->data['menu_active'] = 'sales';
        $this->data['title'] = 'Sales';
    }

    public function index() {
        $this->data['sub_title'] = 'All Sales';
        $this->data['sales'] = $this->om->getAll('sales');
        $this->data['main_content'] = $this->load->view('sales/index', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function sale($type = false) {
        $this->data['sub_title'] = 'New Sale';
        $this->data['items'] = $this->om->get_items($type, 'sales');
        $this->data['companies'] = $this->cm->get('client');
        $this->data['accounts'] = $this->am->account_info();
        $this->data['main_content'] = $this->load->view('sales/new', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function process() {
        $order_id = $this->om->process('sales');
        if ($order_id) {
            $sdata['message'] = '<strong>Success:</strong> The Sales Order has been Processed Successfully.';
            $sdata['class'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('sales/receipt/' . $order_id);
        } else {
            $sdata['message'] = '<strong>Failure:</strong> Something went wrong while processing the sales order. Please Try again.';
            $sdata['class'] = 'danger';
            $this->session->set_userdata($sdata);
            redirect('sales');
        }
    }

    public function delete($id) {
        if ($this->data['user_role'] == 1 OR $this->data['user_role'] == 2) {
            $delete = $this->om->delete($id);
            if ($delete) {
                $sdata['message'] = '<strong>Success:</strong> The Sales Order has been deleted with all related information.';
                $sdata['class'] = 'success';
                $this->session->set_userdata($sdata);
                redirect('sales');
            } else {
                $sdata['message'] = '<strong>Failure:</strong> Something went wrong while deleting the sales order. Please Try again.';
                $sdata['class'] = 'danger';
                $this->session->set_userdata($sdata);
                redirect('sales');
            }
        } else {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('sales');
        }
    }

    public function receipt($id) {
        $this->data['sub_title'] = 'Receipt';
        $this->data['order'] = $this->om->getOrder($id);
        // echo '<pre>';
        // print_r($this->data['order']);exit;
        $this->data['main_content'] = $this->load->view('sales/receipt', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function edit($id) {
        if ($this->data['user_role'] == 1 OR $this->data['user_role'] == 2) {
            $this->data['sub_title'] = 'Edit Sales';
            $this->data['order'] = $this->om->getOrder($id);
            $this->data['items'] = $this->om->get_items();
            $this->data['companies'] = $this->cm->get('client');
            $this->data['main_content'] = $this->load->view('sales/edit', $this->data, true);
            $this->load->view('master', $this->data);
        } else {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('sales');
        }
    }

    public function update($id) {
        $order_id = $this->om->update($id, 'sales');
        if ($order_id) {
            $sdata['message'] = '<strong>Success:</strong> The Sales Order has been Updated Successfully';
            $sdata['class'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('sales/receipt/' . $order_id);
        } else {
            $sdata['message'] = '<strong>Failure:</strong> Something went wrong while updating the sales order. Please Try again.';
            $sdata['class'] = 'danger';
            $this->session->set_userdata($sdata);
            redirect('sales');
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */