<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Production extends CI_Controller
{

    public $data;
    public function __construct()
    {
        parent::__construct();
        $user_id=$this->session->userdata('user_id');
        if(empty($user_id))
        {
            redirect('login');
        }
        $this->load->model('production_model', 'pm');
        $this->data['user_role'] = $this->session->userdata('user_role');
        $this->data['menu_active'] = 'production';
        $this->data['title'] = 'Productions';
    }

    public function index()
    {
        $this->data['sub_title'] = 'All Productions';
        $this->data['productions'] = $this->pm->get_productions();
        $this->data['main_content'] = $this->load->view('production/index', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function new_production($id)
    {
        $this->data['item'] = $this->pm->get_item($id);
        $this->data['sub_title'] = 'New Production';
        $this->data['main_content'] = $this->load->view('production/new', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function save($id)
    {
        $save = $this->pm->save();
        if($save)
        {
            $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
            $sdata['class'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('production');

        }
        else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('production/new_production/'.$id);
        }

    }
    public function edit($id)
    {
        if($this->data['user_role'] == 1 OR $this->data['user_role'] == 2)
        {
            $this->data['sub_title'] = 'Edit Production';
            $this->data['production'] = $this->pm->get_individual($id);
            $this->data['main_content'] = $this->load->view('production/edit', $this->data, true);
            $this->load->view('master', $this->data);
        }
        else
        {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('finished_good');
        }

    }
    public function update($id)
    {
        $update = $this->pm->update($id);
        if($update)
        {
            $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
            $sdata['class'] = 'success';
        }
        else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';

        }
        $this->session->set_userdata($sdata);
        redirect('production/edit/'.$id);
    }
    public function delete($id)
    {
        if($this->data['user_role'] == 1 OR $this->data['user_role'] == 2)
        {
            $delete = $this->pm->delete($id);
            if($delete)
            {
                $sdata['message'] = '<strong>Success:</strong> Data Deleted Successfully';
                $sdata['class'] = 'success';
            }
            else {
                $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
                $sdata['class'] = 'warning';
            }
        }
        else
        {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('production');

    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */