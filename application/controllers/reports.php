<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller
{
    public $data;
    public function __construct()
    {
        parent::__construct();
        $user_id=$this->session->userdata('user_id');
        if(empty($user_id))
        {
            redirect('login');
        }
        $this->load->model('reports_model', 'rm');
        $this->data['user_role'] = $this->session->userdata('user_role');
        $this->data['title'] = 'Reports';
        $this->data['menu_active'] = 'reports';

    }

    public function client_summary()
    {
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        if(isset($this->data['start_date']) && !empty($this->data['start_date']))
        {
            $this->data['start_date'] = $this->input->post('start_date');
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('first day of previous month'));
        }
        if(isset($this->data['end_date']) && !empty($this->data['end_date']))
        {
            $this->data['end_date'] = $this->input->post('end_date');
        }
        else {
            $this->data['end_date'] = date('Y-m-d', strtotime('last day of previous month'));
        }
        $this->data['sub_title'] = 'All Clients Summary Report';
        $this->data['item_active'] = 'client_summary';
        $this->data['companies'] = $this->rm->client_summary($this->data['start_date'], $this->data['end_date']);
        $this->data['main_content'] = $this->load->view('reports/client_summary', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function supplier_summary()
    {
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        if(isset($this->data['start_date']) && !empty($this->data['start_date']))
        {
            $this->data['start_date'] = $this->input->post('start_date');
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('first day of previous month'));
        }
        if(isset($this->data['end_date']) && !empty($this->data['end_date']))
        {
            $this->data['end_date'] = $this->input->post('end_date');
        }
        else {
            $this->data['end_date'] = date('Y-m-d', strtotime('last day of previous month'));
        }

        $this->data['sub_title'] = 'All Supplier Summary Report';
        $this->data['item_active'] = 'supplier_summary';
        $this->data['companies'] = $this->rm->supplier_summary($this->data['start_date'], $this->data['end_date']);
        $this->data['main_content'] = $this->load->view('reports/supplier_summary', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function raw_material_summary()
    {
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        if(isset($this->data['start_date']) && !empty($this->data['start_date']))
        {
            $this->data['start_date'] = $this->input->post('start_date');
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('first day of previous month'));
        }
        if(isset($this->data['end_date']) && !empty($this->data['end_date']))
        {
            $this->data['end_date'] = $this->input->post('end_date');
        }
        else {
            $this->data['end_date'] = date('Y-m-d', strtotime('last day of previous month'));
        }

        $this->data['sub_title'] = 'All Raw Materials Summary Report';
        $this->data['item_active'] = 'raw_material_summary';
        $this->data['items'] = $this->rm->raw_material_summary($this->data['start_date'], $this->data['end_date']);
        $this->data['main_content'] = $this->load->view('reports/raw_material_summary', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function finished_goods_summary()
    {
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        if(isset($this->data['start_date']) && !empty($this->data['start_date']))
        {
            $this->data['start_date'] = $this->input->post('start_date');
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('first day of previous month'));
        }
        if(isset($this->data['end_date']) && !empty($this->data['end_date']))
        {
            $this->data['end_date'] = $this->input->post('end_date');
        }
        else {
            $this->data['end_date'] = date('Y-m-d', strtotime('last day of previous month'));
        }

        $this->data['sub_title'] = 'All Finished Goods Summary Report';
        $this->data['item_active'] = 'finished_goods_summary';
        $this->data['items'] = $this->rm->finished_goods_summary($this->data['start_date'], $this->data['end_date']);
        $this->data['main_content'] = $this->load->view('reports/finished_goods_summary', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function utility_items_summary()
    {
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        if(isset($this->data['start_date']) && !empty($this->data['start_date']))
        {
            $this->data['start_date'] = $this->input->post('start_date');
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('first day of previous month'));
        }
        if(isset($this->data['end_date']) && !empty($this->data['end_date']))
        {
            $this->data['end_date'] = $this->input->post('end_date');
        }
        else {
            $this->data['end_date'] = date('Y-m-d', strtotime('last day of previous month'));
        }

        $this->data['sub_title'] = 'All Utility Items Summary Report';
        $this->data['item_active'] = 'utility_items_summary';
        $this->data['items'] = $this->rm->utility_items_summary($this->data['start_date'], $this->data['end_date']);
        $this->data['main_content'] = $this->load->view('reports/utility_items_summary', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function sales_summary()
    {
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        if(isset($this->data['start_date']) && !empty($this->data['start_date']))
        {
            $this->data['start_date'] = $this->input->post('start_date');
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('first day of previous month'));
        }
        if(isset($this->data['end_date']) && !empty($this->data['end_date']))
        {
            $this->data['end_date'] = $this->input->post('end_date');
        }
        else {
            $this->data['end_date'] = date('Y-m-d', strtotime('last day of previous month'));
        }

        $this->data['sub_title'] = 'Sales Summary Report';
        $this->data['item_active'] = 'sales_summary';
        $this->data['sales'] = $this->rm->sales_summary($this->data['start_date'], $this->data['end_date']);
        $this->data['main_content'] = $this->load->view('reports/sales_summary', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function purchase_summary()
    {
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        if(isset($this->data['start_date']) && !empty($this->data['start_date']))
        {
            $this->data['start_date'] = $this->input->post('start_date');
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('first day of previous month'));
        }
        if(isset($this->data['end_date']) && !empty($this->data['end_date']))
        {
            $this->data['end_date'] = $this->input->post('end_date');
        }
        else {
            $this->data['end_date'] = date('Y-m-d', strtotime('last day of previous month'));
        }

        $this->data['sub_title'] = 'Purchase Summary Report';
        $this->data['item_active'] = 'purchase_summary';
        $this->data['sales'] = $this->rm->purchase_summary($this->data['start_date'], $this->data['end_date']);
        $this->data['main_content'] = $this->load->view('reports/purchase_summary', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function general_income_summary()
    {
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        if(isset($this->data['start_date']) && !empty($this->data['start_date']))
        {
            $this->data['start_date'] = $this->input->post('start_date');
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('first day of previous month'));
        }
        if(isset($this->data['end_date']) && !empty($this->data['end_date']))
        {
            $this->data['end_date'] = $this->input->post('end_date');
        }
        else {
            $this->data['end_date'] = date('Y-m-d', strtotime('last day of previous month'));
        }

        $this->data['sub_title'] = 'General Income Report';
        $this->data['item_active'] = 'general_income_summary';
        $this->data['accounting'] = $this->rm->general_income_summary($this->data['start_date'], $this->data['end_date']);
        $this->data['main_content'] = $this->load->view('reports/general_income_summary', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function general_expense_summary()
    {
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        if(isset($this->data['start_date']) && !empty($this->data['start_date']))
        {
            $this->data['start_date'] = $this->input->post('start_date');
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('first day of previous month'));
        }
        if(isset($this->data['end_date']) && !empty($this->data['end_date']))
        {
            $this->data['end_date'] = $this->input->post('end_date');
        }
        else {
            $this->data['end_date'] = date('Y-m-d', strtotime('last day of previous month'));
        }

        $this->data['sub_title'] = 'General Expense Report';
        $this->data['item_active'] = 'general_expense_summary';
        $this->data['accounting'] = $this->rm->general_expense_summary($this->data['start_date'], $this->data['end_date']);
        $this->data['main_content'] = $this->load->view('reports/general_expense_summary', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function loan_in_summary()
    {
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        if(isset($this->data['start_date']) && !empty($this->data['start_date']))
        {
            $this->data['start_date'] = $this->input->post('start_date');
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('first day of previous month'));
        }
        if(isset($this->data['end_date']) && !empty($this->data['end_date']))
        {
            $this->data['end_date'] = $this->input->post('end_date');
        }
        else {
            $this->data['end_date'] = date('Y-m-d', strtotime('last day of previous month'));
        }

        $this->data['sub_title'] = 'Incoming Loan Summary Report';
        $this->data['item_active'] = 'loan_in_summary';
        $this->data['loans'] = $this->rm->loan_in_summary($this->data['start_date'], $this->data['end_date']);
        $this->data['main_content'] = $this->load->view('reports/loan_in_summary', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function loan_out_summary()
    {
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        if(isset($this->data['start_date']) && !empty($this->data['start_date']))
        {
            $this->data['start_date'] = $this->input->post('start_date');
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('first day of previous month'));
        }
        if(isset($this->data['end_date']) && !empty($this->data['end_date']))
        {
            $this->data['end_date'] = $this->input->post('end_date');
        }
        else {
            $this->data['end_date'] = date('Y-m-d', strtotime('last day of previous month'));
        }

        $this->data['sub_title'] = 'Outgoing Loan Summary Report';
        $this->data['item_active'] = 'loan_out_summary';
        $this->data['loans'] = $this->rm->loan_out_summary($this->data['start_date'], $this->data['end_date']);
        $this->data['main_content'] = $this->load->view('reports/loan_out_summary', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function overall_income_summary()
    {
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        if(isset($this->data['start_date']) && !empty($this->data['start_date']))
        {
            $this->data['start_date'] = $this->input->post('start_date');
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('first day of previous month'));
        }
        if(isset($this->data['end_date']) && !empty($this->data['end_date']))
        {
            $this->data['end_date'] = $this->input->post('end_date');
        }
        else {
            $this->data['end_date'] = date('Y-m-d', strtotime('last day of previous month'));
        }
        $this->data['sub_title'] = 'Overall Income Report';
        $this->data['item_active'] = 'overall_income_summary';
        $income = $this->rm->overall_income_summary($this->data['start_date'], $this->data['end_date']);
        $overall_income = array();
        foreach($income as $item)
        {
            if(!in_array($item->date, $overall_income))
            {
                $overall_income[$item->date] = array(
                    'income' => 0,
                    'incoming_loan' => 0,
                    'incoming_installment' => 0,
                    'client_due' => 0,
                    'sales' => 0
                );
            }
        }
        foreach($income as $item)
        {
            $overall_income[$item->date][$item->type] = $item->Amount;
        }
        $this->data['income'] = $overall_income;
        $this->data['main_content'] = $this->load->view('reports/overall_income_summary', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function overall_expense_summary()
    {
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        if(isset($this->data['start_date']) && !empty($this->data['start_date']))
        {
            $this->data['start_date'] = $this->input->post('start_date');
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('first day of previous month'));
        }
        if(isset($this->data['end_date']) && !empty($this->data['end_date']))
        {
            $this->data['end_date'] = $this->input->post('end_date');
        }
        else {
            $this->data['end_date'] = date('Y-m-d', strtotime('last day of previous month'));
        }
        $this->data['sub_title'] = 'Overall Expense Report';
        $this->data['item_active'] = 'overall_expense_summary';
        $expense = $this->rm->overall_expense_summary($this->data['start_date'], $this->data['end_date']);
        $overall_expense = array();
        foreach($expense as $item)
        {
            if(!in_array($item->date, $overall_expense))
            {
                $overall_expense[$item->date] = array(
                    'expense' => 0,
                    'outgoing_loan' => 0,
                    'outgoing_installment' => 0,
                    'supplier_due' => 0,
                    'purchase' => 0,
                    'salary' => 0
                );
            }
        }
        foreach($expense as $item)
        {
            $overall_expense[$item->date][$item->type] = $item->Amount;
        }
        $this->data['expense'] = $overall_expense;
        $this->data['main_content'] = $this->load->view('reports/overall_expense_summary', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function monthly_salary_sheet()
    {
        $this->data['sub_title'] = 'Monthly Salary Sheet';
        $this->data['item_active'] = 'monthly_salary_sheet';
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        if(isset($month) && !empty($month))
        {
            $this->data['month'] = $this->input->post('month');
        }
        else {
            $this->data['month'] = date('F', strtotime('last day of previous month'));
        }
        if(isset($year) && !empty($year))
        {
            $this->data['year'] = $this->input->post('year');
        }
        else {
            $this->data['year'] = date('Y', strtotime('last day of previous month'));
        }
        $this->data['sub_title'] = 'Monthly Salary Sheet of '.$this->data['month'].' - '.$this->data['year'];
        $this->data['item_active'] = 'monthly_salary_sheet';
        $this->data['salaries'] = $this->rm->get_all_salary($this->data['month'],$this->data['year']);
        $this->data['main_content'] = $this->load->view('reports/monthly_salary_sheet', $this->data, true);
        $this->load->view('master', $this->data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */