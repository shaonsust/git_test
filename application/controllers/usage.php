<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usage extends CI_Controller
{

    public $data;
    public function __construct()
    {
        parent::__construct();
        $user_id=$this->session->userdata('user_id');
        if(empty($user_id))
        {
            redirect('login');
        }
        $this->load->model('usage_model', 'um');
        $this->load->model('items_model', 'im');
        $this->data['user_role'] = $this->session->userdata('user_role');
        $this->data['menu_active'] = 'usage';
        $this->data['title'] = 'Usage';
    }

    public function index($type)
    {
        if($type == 'raw_material')
        {
            $this->data['item_active'] = 'use_raw_material';
            $this->data['sub_title'] = 'Raw Materials';
            $this->data['type'] = 'raw_material';
        }
        elseif($type == 'finished_good')
        {
            $this->data['item_active'] = 'use_finished_good';
            $this->data['sub_title'] = 'Finished Goods';
            $this->data['type'] = 'finished_good';
        }
        elseif($type == 'utility_item')
        {
            $this->data['item_active'] = 'use_utility_item';
            $this->data['sub_title'] = 'Utility Items';
            $this->data['type'] = 'utility_item';
        }
        $this->data['usage'] = $this->um->getAll($type);
        $this->data['main_content'] = $this->load->view('usage/index', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function new_usage($type)
    {
        if($type == 'raw_material')
        {
            $this->data['item_active'] = 'use_raw_material';
            $this->data['sub_title'] = 'Raw Materials';
            $this->data['type'] = 'raw_material';
        }
        elseif($type == 'finished_good')
        {
            $this->data['item_active'] = 'use_finished_good';
            $this->data['sub_title'] = 'Finished Goods';
            $this->data['type'] = 'finished_good';
        }
        elseif($type == 'utility_item')
        {
            $this->data['item_active'] = 'use_utility_item';
            $this->data['sub_title'] = 'Utility Items';
            $this->data['type'] = 'utility_item';
        }
        $this->data['items'] = $this->um->get_items($type);
        $this->data['main_content'] = $this->load->view('usage/new', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function process($type)
    {
        $usage_id = $this->um->process($type);
        if($usage_id)
        {
            $sdata['message'] = '<strong>Success:</strong> The Usage Quee has been Processed Successfully.';
            $sdata['class'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('usage/details/'.$type.'/'.$usage_id);
        }
        else
        {
            $sdata['message'] = '<strong>Failure:</strong> Something went wrong while processing the Usage Quee. Please Try again.';
            $sdata['class'] = 'danger';
            $this->session->set_userdata($sdata);
            redirect('usage/index/'.$type);
        }
    }
    public function details($type, $id)
    {
        if($type == 'raw_material')
        {
            $this->data['item_active'] = 'use_raw_material';
            $this->data['sub_title'] = 'Raw Materials';
            $this->data['type'] = 'raw_material';
        }
        elseif($type == 'finished_good')
        {
            $this->data['item_active'] = 'use_finished_good';
            $this->data['sub_title'] = 'Finished Goods';
            $this->data['type'] = 'finished_good';
        }
        elseif($type == 'utility_item')
        {
            $this->data['item_active'] = 'use_utility_item';
            $this->data['sub_title'] = 'Utility Items';
            $this->data['type'] = 'utility_item';
        }
        $this->data['usage'] = $this->um->getUsage($id);
        $this->data['main_content'] = $this->load->view('usage/receipt', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function edit($type, $id)
    {
        if($this->data['user_role'] == 1 OR $this->data['user_role'] == 2)
        {
            if($type == 'raw_material')
            {
                $this->data['item_active'] = 'use_raw_material';
                $this->data['sub_title'] = 'Raw Materials';
                $this->data['type'] = 'raw_material';
            }
            elseif($type == 'finished_good')
            {
                $this->data['item_active'] = 'use_finished_good';
                $this->data['sub_title'] = 'Finished Goods';
                $this->data['type'] = 'finished_good';
            }
            elseif($type == 'utility_item')
            {
                $this->data['item_active'] = 'use_utility_item';
                $this->data['sub_title'] = 'Utility Items';
                $this->data['type'] = 'utility_item';
            }
            $this->data['items'] = $this->um->get_items($type);
            $this->data['usage'] = $this->um->getUsage($id);
            $this->data['main_content'] = $this->load->view('usage/edit', $this->data, true);
            $this->load->view('master', $this->data);
        }
        else
        {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('usage/index/'.$type);
        }
    }
    public function update($type, $id)
    {

        $usage_id = $this->um->update($id, $type);
        if($usage_id)
        {
            $sdata['message'] = '<strong>Success:</strong> The Usage Order has been Updated Successfully';
            $sdata['class'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('usage/details/'.$type.'/'.$usage_id);
        }
        else
        {
            $sdata['message'] = '<strong>Failure:</strong> Something went wrong while updating the usage order. Please Try again.';
            $sdata['class'] = 'danger';
            $this->session->set_userdata($sdata);
            redirect('usage/index/'.$type);
        }
    }
    public function delete($type, $id)
    {
        if($this->data['user_role'] == 1 OR $this->data['user_role'] == 2)
        {
            $delete = $this->um->delete($id);
            if($delete)
            {
                $sdata['message'] = '<strong>Success:</strong> The Usage Order has been deleted with all related information.';
                $sdata['class'] = 'success';
                $this->session->set_userdata($sdata);
                redirect('usage/index/'.$type);
            }
            else
            {
                $sdata['message'] = '<strong>Failure:</strong> Something went wrong while deleting the usage order. Please Try again.';
                $sdata['class'] = 'danger';
                $this->session->set_userdata($sdata);
                redirect('usage/index/'.$type);
            }
        }
        else
        {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('usage/index/'.$type);
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */