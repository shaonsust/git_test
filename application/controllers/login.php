<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $user_id=$this->session->userdata('user_id');
        if(!empty($user_id))
        {
            redirect('welcome');
        }
        $this->load->helper('cookie');
    }

    public function index()
    {
        $user_id=get_cookie('user_id');
        $user_role=get_cookie('user_role');
        if(!empty($user_id) && !empty($user_role))
        {
            $sdata['user_id']=$user_id;
            $sdata['user_role']=$user_role;
            $this->session->set_userdata($sdata);

            if($user_role==1)
            {
                redirect('super_admin_c');
            }
            elseif($user_role==2)
            {
                redirect('admin_c');
            }
            elseif($user_role==3)
            {
                redirect('main_c');
            }
        }
        else{
        $this->load->view('login');
        }
    }

    public function login_check()
    {
        $data=array();

        $username=$this->input->post('username',true);
        $password=$this->input->post('password',true);

        $loggedin=$this->login_model->login_check($username,$password);


        if(!empty($loggedin))
        {
            $sdata=array();
            $remember_me=$this->input->post('remember_me',true);
            if($remember_me=='on')
            {
                $user_id= array(
                    'name'   => 'user_id',
                    'value'  => $loggedin->user_id,
                    'expire' => time() + (86400 *30)
                );
                $user_role= array(
                    'name'   => 'user_role',
                    'value'  => $loggedin->user_role,
                    'expire' => time() + (86400 *30)
                );
                set_cookie($user_id);
                set_cookie($user_role);


            }

            $sdata['user_id']=$loggedin->id;
            $sdata['user_name']=$loggedin->username;
            $sdata['user_role']=$loggedin->role;

            $this->session->set_userdata($sdata);
            redirect('welcome');
        }
        else{
            $sdata=array();
            $sdata['message']='Your Username Or Password is Invalid !';
            $this->session->set_userdata($sdata);
            redirect('login');
        }


    }


    public function show_forgot_form()
    {
        $this->load->view('forgot_form');
    }

    public function forgot_password()
    {
        $email=$this->input->post('email',true);
        $check_email=$this->login_model->get_email($email);

        if(!empty($check_email))
        {
            $sender_name='Admin';
            $sender_email='admin@ffmlbd.com';
            $config = Array(
                'protocol' => 'mail',
                'mailtype'  => 'html',
                'charset' => 'utf-8',
                'wordwrap' => TRUE
            );


            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $email_body ="
                           <strong>Password reset Request recieved:</strong><br>
                           <p>If you really want to reset your password then please click on the bellow link and provide new
                        password. If you don't want to reset your password then please disregard this message</p>
                           ";

            $subject='Reset Password for FFML';
            $pass=rand('0000000','999999');
            $mdpass = md5($pass);

            $password_reset_link = base_url().'login/reset_password/'.$check_email->user_id.'/'.$mdpass;
            $email_body .= '<b>Password Reset Link : </b> <a href="'.$password_reset_link.'">'.
                $password_reset_link.'</a>\r\n';

           // echo $email_body;
           // exit();
            $this->email->from($sender_email, $sender_name);
            $this->email->to($email);
            $this->email->subject($subject);
            $this->email->message($email_body);
            $this->email->send();

            $data['token']=$mdpass;
            $data['token_type']='forgot_password';
            $this->login_model->update_token($check_email->user_id,$data);
            $sdata=array();
            $sdata['message']='Please Check Email !';
            $this->session->set_userdata($sdata);
            redirect('login/show_forgot_form');





        }
        else
        {
            $sdata=array();
            $sdata['message']='Your Email Invalid !';
            $this->session->set_userdata($sdata);
            redirect('login/show_forgot_form');
        }


    }


    public function reset_password($id,$token)
    {
        $data['check_user_id']=$this->login_model->get_check_token($id,$token);

        if($data['check_user_id'])
        {
            $this->load->view('reset_password',$data);
        }

    }

    public function update_password()
    {
        $user_id=$this->input->post('user_id',true);
        $data['token'] = '';
        $data['token_type'] = '';
        $data['password']=md5($this->input->post('password',true));

        $this->login_model->update_password($user_id,$data);
        $sdata['message']='Your Password has been changed successfully. You can now log in with your new
        password';

        $this->session->set_userdata($sdata);
        redirect('login');

    }


}


/*echo '<pre>';
print_r($check_email);
exit();*/