<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Loan extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('login');
        }
        $this->load->model('loan_model', 'lm');
        $this->load->model('accounting_model', 'am');
        $this->data['user_role'] = $this->session->userdata('user_role');
        $this->data['title'] = 'Loan';
        $this->data['menu_active'] = 'loan';
    }

    public function sources() {
        $this->data['item_active'] = 'loan_sources';
        $this->data['sub_title'] = 'Loan Sources';
        $this->data['companies'] = $this->lm->get('loan_source');
        $this->data['main_content'] = $this->load->view('loan/sources', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function source_details($id) {
        $this->data['item_active'] = 'loan_sources';
        $this->data['sub_title'] = 'Loan Sources';
        $this->data['loan'] = $this->lm->get_source($id);
        $this->data['main_content'] = $this->load->view('loan/source_details', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function add_new_soucre() {
        $this->data['sub_title'] = 'New Loan Source';
        $this->data['item_active'] = 'loan_sources';
        $this->data['main_content'] = $this->load->view('loan/new', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function save() {
        $ndata['name'] = $this->input->post('name');
        $ndata['contact_person'] = $this->input->post('contact_person');
        $ndata['contact_phone'] = $this->input->post('contact_phone');
        $ndata['address'] = $this->input->post('address');
        $ndata['secondary_phone'] = $this->input->post('secondary_phone');
        $ndata['email'] = $this->input->post('email');
        $ndata['website'] = $this->input->post('website');
        $ndata['outgoing_amount'] = $this->input->post('outgoing_amount');
        $ndata['incoming_amount'] = $this->input->post('incoming_amount');
        $ndata['notes'] = $this->input->post('notes');
        $ndata['type'] = 'loan_source';
        $ndata['created_at'] = date('Y-m-d');

        $save = $this->lm->save($ndata);
        if ($save) {
            if ($save == 'exist') {
                $sdata['message'] = '<strong>Failure:</strong> Duplicate Entry Detected. Please try a different Name';
                $sdata['class'] = 'danger';
            } else {
                $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
                $sdata['class'] = 'success';
            }
        } else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('loan/sources');
    }

    public function edit($id) {
        if ($this->data['user_role'] == 1 OR $this->data['user_role'] == 2) {
            $this->data['sub_title'] = 'Edit Loan Sources';
            $this->data['item_active'] = 'loan_sources';
            $this->data['company'] = $this->lm->get_individual($id);
            $this->data['main_content'] = $this->load->view('loan/edit', $this->data, true);
            $this->load->view('master', $this->data);
        } else {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('supplier');
        }
    }

    public function update($id) {
        $udata['name'] = $this->input->post('name');
        $udata['contact_person'] = $this->input->post('contact_person');
        $udata['contact_phone'] = $this->input->post('contact_phone');
        $udata['address'] = $this->input->post('address');
        $udata['secondary_phone'] = $this->input->post('secondary_phone');
        $udata['email'] = $this->input->post('email');
        $udata['website'] = $this->input->post('website');
        $udata['incoming_amount'] = $this->input->post('incoming_amount');
        $udata['outgoing_amount'] = $this->input->post('outgoing_amount');
        $udata['notes'] = $this->input->post('notes');

        $update = $this->lm->update($udata, $id);
        if ($update) {
            if ($update == 'exist') {
                $sdata['message'] = '<strong>Failure:</strong> Duplicate Entry Detected. Please try a different Name';
                $sdata['class'] = 'danger';
            } else {
                $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
                $sdata['class'] = 'success';
            }
        } else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('loan/sources');
    }

    public function delete($id) {
        if ($this->data['user_role'] == 1 OR $this->data['user_role'] == 2) {
            $delete = $this->lm->delete($id);
            if ($delete) {
                $sdata['message'] = '<strong>Success:</strong> Data Deleted Successfully';
                $sdata['class'] = 'success';
            } else {
                $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
                $sdata['class'] = 'warning';
            }
        } else {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('loan/sources');
    }

    public function loans($type) {
        $this->data['item_active'] = $type;
        if ($type == 'incoming_loan') {
            $this->data['sub_title'] = 'Incoming Loans';
        } else {
            $this->data['sub_title'] = 'Outgoing Loans';
        }
        $this->data['type'] = $type;
        $this->data['loans'] = $this->lm->get_loans($type);
        $this->data['main_content'] = $this->load->view('loan/loans', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function new_loan($type) {
        $this->data['item_active'] = $type;
        if ($type == 'incoming_loan') {
            $this->data['sub_title'] = 'Incoming Loan';
        } else {
            $this->data['sub_title'] = 'Outgoing Loan';
        }
        $this->data['type'] = $type;
        $this->data['loans'] = $this->lm->get($type);
        $this->data['companies'] = $this->lm->get('loan_source');
        $this->data['accounts'] = $this->am->account_info();
        $this->data['main_content'] = $this->load->view('loan/new_loan', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function process($type) {
        $save = $this->lm->process($type);
        if ($save) {
            $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
            $sdata['class'] = 'success';
        } else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('loan/loans/' . $type);
    }

    public function delete_loan($type, $id) {
        $save = $this->lm->delete_loan($id);
        if ($save) {
            $sdata['message'] = '<strong>Success:</strong> Loan Deleted Successfully';
            $sdata['class'] = 'success';
        } else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem while deleting the loan!! Please Try again';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('loan/loans/' . $type);
    }

    public function edit_loan($type, $id) {
        if ($this->data['user_role'] == 1 OR $this->data['user_role'] == 2) {
            if ($type == 'incoming_loan') {
                $this->data['sub_title'] = 'Incoming Loans';
            } else {
                $this->data['sub_title'] = 'Outgoing Loans';
            }
            $this->data['item_active'] = $type;
            $this->data['type'] = $type;
            $this->data['companies'] = $this->lm->get('loan_source');
            $this->data['loan'] = $this->lm->get_loan($id);
            $this->data['main_content'] = $this->load->view('loan/edit_loan', $this->data, true);
            $this->load->view('master', $this->data);
        } else {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('loan/loans/' . $type);
        }
    }

    public function update_loan($type, $id) {
        $loan_id = $this->lm->update_loan($type, $id);
        if ($loan_id) {
            $sdata['message'] = '<strong>Success:</strong> The Loan has been updated Successfully';
            $sdata['class'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('loan/loans/' . $type);
        } else {
            $sdata['message'] = '<strong>Failure:</strong> Something went wrong while updating the loan. Please Try again.';
            $sdata['class'] = 'danger';
            $this->session->set_userdata($sdata);
            redirect('loan/loans/' . $type);
        }
    }

    public function loan_details($type, $id) {
        if ($type == 'incoming_loan') {
            $this->data['sub_title'] = 'Incoming Loan';
        } else {
            $this->data['sub_title'] = 'Outgoing Loan';
        }
        $this->data['accounts'] = $this->am->account_info();
        $this->data['item_active'] = $type;
        $this->data['type'] = $type;
        $this->data['loan'] = $this->lm->get_loan_details($id);
        $this->data['main_content'] = $this->load->view('loan/loan_details', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function installment($type, $id) {
        $save = $this->lm->process_installment($type, $id);
        if ($save) {
            $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
            $sdata['class'] = 'success';
        } else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('loan/loan_details/' . $type . '/' . $id);
    }

    public function edit_installment($type, $parent_id, $id) {
        if ($this->data['user_role'] == 1 OR $this->data['user_role'] == 2) {
            if ($type == 'incoming_loan') {
                $this->data['sub_title'] = 'Incoming Loans';
            } else {
                $this->data['sub_title'] = 'Outgoing Loans';
            }
            $this->data['item_active'] = $type;
            $this->data['type'] = $type;
            $this->data['id'] = $id;
            $this->data['parent_id'] = $parent_id;
            $this->data['loan'] = $this->lm->get_installment($id);
            $this->data['main_content'] = $this->load->view('loan/edit_installment', $this->data, true);
            $this->load->view('master', $this->data);
        } else {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('loan/loan_details/' . $type . '/' . $parent_id);
        }
    }

    public function update_installment($type, $parent_id, $id) {
        $loan_id = $this->lm->update_installment($id);
        if ($loan_id) {
            $sdata['message'] = '<strong>Success:</strong> The Installment has been updated Successfully';
            $sdata['class'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('loan/loan_details/' . $type . '/' . $parent_id);
        } else {
            $sdata['message'] = '<strong>Failure:</strong> Something went wrong while updating the Installment. Please Try again.';
            $sdata['class'] = 'danger';
            $this->session->set_userdata($sdata);
            redirect('loan/loan_details/' . $type . '/' . $parent_id);
        }
    }

    public function delete_installment($type, $parent_id, $id) {
        $delete = $this->lm->delete_installment($id);
        if ($delete) {
            $sdata['message'] = '<strong>Success:</strong> Installment Deleted Successfully';
            $sdata['class'] = 'success';
        } else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem while deleting the Installment!! Please Try again';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('loan/loan_details/' . $type . '/' . $parent_id);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */