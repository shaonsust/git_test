<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Purchase extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('login');
        }
        $this->load->model('company_model', 'cm');
        $this->load->model('accounting_model', 'am');
        $this->load->model('order_model', 'om');
        $this->data['user_role'] = $this->session->userdata('user_role');
        $this->data['menu_active'] = 'purchase';
        $this->data['title'] = 'Purchase';
    }

    public function index() {
        $this->data['sub_title'] = 'All Purchase';
        $this->data['purchases'] = $this->om->getAll('purchase');
        $this->data['main_content'] = $this->load->view('purchase/index', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function new_purchase($type = false) {
        $this->data['sub_title'] = 'New Purchase';
        $this->data['items'] = $this->om->get_items($type);
        $this->data['companies'] = $this->cm->get('supplier');
        $this->data['accounts'] = $this->am->account_info();
        $this->data['main_content'] = $this->load->view('purchase/new', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function process() {
        $order_id = $this->om->process('purchase');
        if ($order_id) {
            $sdata['message'] = '<strong>Success:</strong> The Purchase Order has been Processed Successfully.';
            $sdata['class'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('purchase/receipt/' . $order_id);
        } else {
            $sdata['message'] = '<strong>Failure:</strong> Something went wrong while processing the purchase order. Please Try again.';
            $sdata['class'] = 'danger';
            $this->session->set_userdata($sdata);
            redirect('purchase');
        }
    }

    public function delete($id) {
        if ($this->data['user_role'] == 1 OR $this->data['user_role'] == 2) {
            $delete = $this->om->delete($id);
            if ($delete) {
                $sdata['message'] = '<strong>Success:</strong> The Purchase Order has been deleted with all related information.';
                $sdata['class'] = 'success';
                $this->session->set_userdata($sdata);
                redirect('purchase');
            } else {
                $sdata['message'] = '<strong>Failure:</strong> Something went wrong while deleting the purchase order. Please Try again.';
                $sdata['class'] = 'danger';
                $this->session->set_userdata($sdata);
                redirect('purchase');
            }
        } else {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('purchase');
        }
    }

    public function receipt($id) {
        $this->data['sub_title'] = 'Receipt';
        $this->data['order'] = $this->om->getOrder($id);
        $this->data['main_content'] = $this->load->view('purchase/receipt', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function edit($id) {
        if ($this->data['user_role'] == 1 OR $this->data['user_role'] == 2) {
            $this->data['sub_title'] = 'Edit Purchase';
            $this->data['order'] = $this->om->getOrder($id);
            $this->data['items'] = $this->om->get_items();
            $this->data['companies'] = $this->cm->get('supplier');
            $this->data['main_content'] = $this->load->view('purchase/edit', $this->data, true);
            $this->load->view('master', $this->data);
        } else {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('purchase');
        }
    }

    public function update($id) {
        $order_id = $this->om->update($id, 'purchase');
        if ($order_id) {
            $sdata['message'] = '<strong>Success:</strong> The Purchase Order has been Updated Successfully';
            $sdata['class'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('purchase/receipt/' . $order_id);
        } else {
            $sdata['message'] = '<strong>Failure:</strong> Something went wrong while updating the purchase order. Please Try again.';
            $sdata['class'] = 'danger';
            $this->session->set_userdata($sdata);
            redirect('purchase');
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */