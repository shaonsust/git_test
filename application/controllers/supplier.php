<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Supplier extends CI_Controller
{

    public $data;
    public function __construct()
    {
        parent::__construct();
        $user_id=$this->session->userdata('user_id');
        if(empty($user_id))
        {
            redirect('login');
        }
        $this->load->model('company_model', 'cm');
        $this->data['user_role'] = $this->session->userdata('user_role');
        $this->data['menu_active'] = 'supplier';
        $this->data['title'] = 'Suppliers';

    }

    public function index()
    {
        $this->data['sub_title'] = 'All Suppliers';
        $this->data['companies'] = $this->cm->get('supplier');
        $this->data['main_content'] = $this->load->view('supplier/index', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function add_new()
    {
        $this->data['sub_title'] = 'New Supplier';
        $this->data['main_content'] = $this->load->view('supplier/new', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function save()
    {
        $ndata['name'] = $this->input->post('name');
        $ndata['contact_person'] = $this->input->post('contact_person');
        $ndata['contact_phone'] = $this->input->post('contact_phone');
        $ndata['address'] = $this->input->post('address');
        $ndata['secondary_phone'] = $this->input->post('secondary_phone');
        $ndata['email'] = $this->input->post('email');
        $ndata['website'] = $this->input->post('website');
        $ndata['outgoing_amount'] = $this->input->post('outgoing_amount');
        $ndata['notes'] = $this->input->post('notes');
        $ndata['type'] = 'supplier';
        $ndata['created_at'] = date('Y-m-d');

        $save = $this->cm->save($ndata);
        if($save)
        {
            if($save == 'exist')
            {
                $sdata['message'] = '<strong>Failure:</strong> Duplicate Entry Detected. Please try a different Name';
                $sdata['class'] = 'danger';
            }
            else {
                $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
                $sdata['class'] = 'success';
            }
        }
        else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('supplier');
    }
    public function edit($id)
    {
        if($this->data['user_role'] == 1 OR $this->data['user_role'] == 2)
        {
            $this->data['sub_title'] = 'Edit Supplier';
            $this->data['company'] = $this->cm->get_individual($id);
            $this->data['main_content'] = $this->load->view('supplier/edit', $this->data, true);
            $this->load->view('master', $this->data);
        }
        else
        {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('supplier');
        }

    }
    public function update($id)
    {
        $udata['name'] = $this->input->post('name');
        $udata['contact_person'] = $this->input->post('contact_person');
        $udata['contact_phone'] = $this->input->post('contact_phone');
        $udata['address'] = $this->input->post('address');
        $udata['secondary_phone'] = $this->input->post('secondary_phone');
        $udata['email'] = $this->input->post('email');
        $udata['website'] = $this->input->post('website');
        $udata['outgoing_amount'] = $this->input->post('outgoing_amount');
        $udata['notes'] = $this->input->post('notes');
        $update = $this->cm->update($udata, $id);
        if($update)
        {
            if($update == 'exist')
            {
                $sdata['message'] = '<strong>Failure:</strong> Duplicate Entry Detected. Please try a different Name';
                $sdata['class'] = 'danger';
            }
            else {
                $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
                $sdata['class'] = 'success';
            }
        }
        else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';

        }
        $this->session->set_userdata($sdata);
        redirect('supplier');
    }
    public function delete($id)
    {
        if($this->data['user_role'] == 1 OR $this->data['user_role'] == 2)
        {
            $delete = $this->cm->delete($id);
            if($delete)
            {
                $sdata['message'] = '<strong>Success:</strong> Data Deleted Successfully';
                $sdata['class'] = 'success';
            }
            else {
                $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
                $sdata['class'] = 'warning';
            }
        }
        else
        {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('supplier');

    }
    public function details($id)
    {
        $this->data['id'] = $id;
        $star_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        if(isset($star_date) && !empty($star_date))
        {
            $this->data['start_date'] = date('Y-m-d', strtotime($star_date));
            $this->data['filtered'] = 'yes';
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('0000-00-00'));
            $this->data['filtered'] = 'no';
        }
        if(isset($end_date) && !empty($end_date))
        {
            $this->data['end_date'] = date('Y-m-d', strtotime($end_date));
        }
        else {
            $this->data['end_date'] = date('Y-m-d');
        }
        $this->data['sub_title'] = 'Supplier Details';
        $this->data['company'] = $this->cm->get_details($id, $this->data['start_date'], $this->data['end_date']);
        $this->data['main_content'] = $this->load->view('supplier/details', $this->data, true);
        $this->load->view('master', $this->data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */