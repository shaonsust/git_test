<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    public $notification;
    public $data;
    public function __construct()
    {
        parent::__construct();
        $user_id=$this->session->userdata('user_id');
        if(empty($user_id))
        {
           redirect('login');
        }
        $this->load->model('login_model');
        $this->load->model('welcome_model', 'wm');

        $this->load->helper('cookie');

        $this->data['user_role'] = $this->session->userdata('user_role');
    }

    public function index()
    {
        $lu['menu_active'] = 'dashboard';
        $this->session->set_userdata($lu);

        $this->data['start_date'] = date('Y-m-d', strtotime('first day of previous month'));
        $this->data['end_date'] = date('Y-m-d', strtotime('last day of previous month'));
        $this->data['month_year'] = date('F Y', strtotime('last day of previous month'));
        $aryRange = array();

        $iDateFrom=mktime(1,0,0,substr($this->data['start_date'],5,2),     substr($this->data['start_date'],8,2),substr($this->data['start_date'],0,4));
        $iDateTo=mktime(1,0,0,substr($this->data['end_date'],5,2),     substr($this->data['end_date'],8,2),substr($this->data['end_date'],0,4));

        if ($iDateTo>=$iDateFrom)
        {
            $aryRange[date('Y-m-d',$iDateFrom)] = 0;
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                $aryRange[date('Y-m-d',$iDateFrom)] = 0;
            }
        }
        $incomeRange = $aryRange;
        $expenseRange = $aryRange;

        $income = $this->wm->overall_income_summary($this->data['start_date'], $this->data['end_date']);
        foreach($income as $item)
        {
            $incomeRange[$item->date] = round($item->Amount, 2);
        }
        $this->data['income'] = $incomeRange;

        $expense = $this->wm->overall_expense_summary($this->data['start_date'], $this->data['end_date']);
        foreach($expense as $item)
        {
            $expenseRange[$item->date] = round($item->Amount, 2);
        }
        $this->data['expense'] = $expenseRange;

        $this->data['individual_income'] = $this->wm->get_individual_income($this->data['start_date'], $this->data['end_date']);
        $this->data['individual_expense'] = $this->wm->get_individual_expense($this->data['start_date'], $this->data['end_date']);
//        echo '<pre>';
//        print_r($this->data['individual_income']);exit;
        $data['main_content'] = $this->load->view('dashboard', $this->data, true);
        $this->load->view('master', $data);
    }

    public function my_account()
    {
        $user_id = $this->session->userdata('user_id');


        $data = array();
        $data['user_info']=$this->wm->user_info($user_id);

        $data['main_content'] = $this->load->view('my_account', $data, true);
        $this->load->view('master', $data);
    }

    public function update_basic_info()
    {
        $user_id = $this->session->userdata('user_id');
        $data['username'] = $this->input->post('username', true);
        $data['full_name'] = $this->input->post('full_name', true);
        $data['email'] = $this->input->post('email', true);


        $k = $this->wm->update_user($user_id, $data);


        $sdata = array();
        $sdata['message'] = 'Successfully name Change Thank you !';

        $this->session->set_userdata($sdata);

        redirect('welcome/my_account');
    }

    public function change_password()
    {
        $user_id = $this->session->userdata('user_id');
        $password = md5($this->input->post('password', true));


        $check_pass=$this->wm->check_pass($user_id,$password);



        if(!empty($check_pass))
        {
            $data['password'] = md5($this->input->post('user_password', true));
            $this->wm->update_user($user_id, $data);


            $sdata = array();
            $sdata['message'] = 'Password Changed successfully';

            $this->session->set_userdata($sdata);
        }
        else{




            $sdata = array();
            $sdata['message'] = 'Old Password is not correct  !';

            $this->session->set_userdata($sdata);
        }



        redirect('welcome/my_account');
    }



    public function logout()
    {
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('user_role');

        delete_cookie('user_id');
        delete_cookie('user_role');

        $sdata=array();
        $sdata['message']='You Successfully loggedout Thank you!';
        $this->session->set_userdata($sdata);
        redirect('login');



    }
    public function dbback()
    {
        $prefs = array(
                'format'      => 'zip',             // gzip, zip, txt
                'filename'    => 'dbbackup.sql',    // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
                'newline'     => "\r\natcasesort(array)"               // Newline character used in backup file
              );
        $this->load->dbutil($prefs);

        // Backup your entire database and assign it to a variable
        $backup =& $this->dbutil->backup(); 

        // Load the file helper and write the file to your server
        $this->load->helper('file');
        write_file('/backup/dbbackup.gz', $backup); 

        // Load the download helper and send the file to your desktop
        $this->load->helper('download');
        force_download('dbbackup.gz', $backup);
    }
    public function ajaxTesting()
    {
        $this->load->view('ajaxTesting');
    }
    public function ajaxResult()
    {
        $data['output'] = $this->input->post('name');
        echo $data['output'];
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */