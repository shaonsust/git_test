<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Help extends CI_Controller
{
    public $data;
    public function __construct()
    {
        parent::__construct();
        $user_id=$this->session->userdata('user_id');
        if(empty($user_id))
        {
            redirect('login');
        }
        $this->data['user_role'] = $this->session->userdata('user_role');
        $this->data['title'] = 'Help Guide';
        $this->data['menu_active'] = 'help';

    }

    public function overview()
    {
        $this->data['sub_title'] = 'Overview';
        $this->data['item_active'] = 'overview';
        $this->data['main_content'] = $this->load->view('help/overview', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function account()
    {
        $this->data['sub_title'] = 'Account Settings';
        $this->data['item_active'] = 'account';
        $this->data['main_content'] = $this->load->view('help/account', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function raw_material()
    {
        $this->data['sub_title'] = 'Raw Material Management';
        $this->data['item_active'] = 'raw_material';
        $this->data['main_content'] = $this->load->view('help/raw_material', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function finished_good()
    {
        $this->data['sub_title'] = 'Finished Good Management';
        $this->data['item_active'] = 'finished_good';
        $this->data['main_content'] = $this->load->view('help/finished_good', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function utility_item()
    {
        $this->data['sub_title'] = 'Utility Item Management';
        $this->data['item_active'] = 'utility_item';
        $this->data['main_content'] = $this->load->view('help/utility_item', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function production()
    {
        $this->data['sub_title'] = 'Production Management';
        $this->data['item_active'] = 'production';
        $this->data['main_content'] = $this->load->view('help/production', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function usage()
    {
        $this->data['sub_title'] = 'Item Usage Management';
        $this->data['item_active'] = 'usage';
        $this->data['main_content'] = $this->load->view('help/usage', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function sales()
    {
        $this->data['sub_title'] = 'Sales Management';
        $this->data['item_active'] = 'sales';
        $this->data['main_content'] = $this->load->view('help/sales', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function purchase()
    {
        $this->data['sub_title'] = 'Purchase Management';
        $this->data['item_active'] = 'purchase';
        $this->data['main_content'] = $this->load->view('help/purchase', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function accounting()
    {
        $this->data['sub_title'] = 'General Accounting Management';
        $this->data['item_active'] = 'accounting';
        $this->data['main_content'] = $this->load->view('help/accounting', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function loan()
    {
        $this->data['sub_title'] = 'Loan Management';
        $this->data['item_active'] = 'loan';
        $this->data['main_content'] = $this->load->view('help/loan', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function hr()
    {
        $this->data['sub_title'] = 'Human Resource Management';
        $this->data['item_active'] = 'hr';
        $this->data['main_content'] = $this->load->view('help/hr', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function supplier()
    {
        $this->data['sub_title'] = 'Supplier Management';
        $this->data['item_active'] = 'supplier';
        $this->data['main_content'] = $this->load->view('help/supplier', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function client()
    {
        $this->data['sub_title'] = 'Client Management';
        $this->data['item_active'] = 'client';
        $this->data['main_content'] = $this->load->view('help/client', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function reports()
    {
        $this->data['sub_title'] = 'Reports';
        $this->data['item_active'] = 'reports';
        $this->data['main_content'] = $this->load->view('help/reports', $this->data, true);
        $this->load->view('master', $this->data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */