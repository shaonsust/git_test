<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Accounting extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('login');
        }
        $this->load->model('company_model', 'cm');
        $this->load->model('accounting_model', 'am');
        $this->data['user_role'] = $this->session->userdata('user_role');
        $this->data['menu_active'] = 'accounting';
        $this->data['title'] = 'Accounting';
    }

    public function index($type) {
        //die($type);
        if ($type == 'income') {
            $this->data['item_active'] = 'income';
            $this->data['sub_title'] = 'General Income';
            $this->data['type'] = 'income';
        } elseif ($type == 'account') {
            $this->data['item_active'] = 'account';
            $this->data['sub_title'] = 'Account';
            $this->data['type'] = 'account';
        } elseif ($type == 'expense') {
            $this->data['item_active'] = 'expense';
            $this->data['sub_title'] = 'General Expense';
            $this->data['type'] = 'expense';
        } elseif ($type == 'supplier_due') {
            $this->data['item_active'] = 'supplier_due';
            $this->data['sub_title'] = 'Supplier Due Payment';
            $this->data['type'] = 'supplier_due';
        } elseif ($type == 'client_due') {
            $this->data['item_active'] = 'client_due';
            $this->data['sub_title'] = 'Client Due Payment';
            $this->data['type'] = 'client_due';
        }
        $this->data['accounting'] = $this->am->getAll($type);
        $this->data['main_content'] = $this->load->view('accounting/index', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function new_payment($type) {
        if ($type == 'income') {
            $this->data['item_active'] = 'income';
            $this->data['sub_title'] = 'General Income';
            $this->data['type'] = 'income';
        } elseif ($type == 'expense') {
            $this->data['item_active'] = 'expense';
            $this->data['sub_title'] = 'General Expense';
            $this->data['type'] = 'expense';
        } elseif ($type == 'account') {
            $this->data['item_active'] = 'account';
            $this->data['sub_title'] = 'Account';
            $this->data['type'] = 'account';
        } elseif ($type == 'supplier_due') {
            $this->data['item_active'] = 'supplier_due';
            $this->data['sub_title'] = 'Supplier Due Payment';
            $this->data['type'] = 'supplier_due';
            $this->data['companies'] = $this->cm->get('supplier');
        } elseif ($type == 'client_due') {
            $this->data['item_active'] = 'client_due';
            $this->data['sub_title'] = 'Client Due Payment';
            $this->data['type'] = 'client_due';
            $this->data['companies'] = $this->cm->get('client');
        }
        $this->data['accounts'] = $this->am->account_info();
        $this->data['main_content'] = $this->load->view('accounting/new', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function account1($type) {
        $this->data['item_active'] = 'account';
        $this->data['sub_title'] = 'Account';
        $this->data['type'] = 'account';

        $this->data['accounting'] = $this->am->account_info();
        $this->data['main_content'] = $this->load->view('accounting/account_index', $this->data, true);
        $this->load->view('master', $this->data);
        //     	echo "working good";
        //      	die();
    }

    public function account($type) {
        $accounting_id = $this->am->account_insert($type);
        if ($accounting_id) {
            $sdata['message'] = '<strong>Success:</strong> The account created successfully.';
            $sdata['class'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('accounting/account1/' . $type);
        } else {
            $sdata['message'] = '<strong>Failure:</strong> Something went wrong while creating the account. Please try again.';
            $sdata['class'] = 'danger';
            $this->session->set_userdata($sdata);
            redirect('accounting/index/' . $type);
        }
    }

    public function account_update($id) {
        $adata['account'] = $this->input->post('account');
        if ($adata['account'] == 'bank') {
            $adata['acc_no'] = $this->input->post('acc_no');
            $adata['bank_name'] = $this->input->post('bank_name');
        } else {
            $adata['acc_no'] = '';
            $adata['bank_name'] = '';
        }

        $adata['acc_name'] = $this->input->post('acc_name');
        $adata['date'] = date("Y-m-d");
        $adata['time'] = date("h:i:sa");
        $adata['curr_balance'] = $this->input->post('curr_balance');
        $adata['description'] = $this->input->post('description');
        $adata['acc_type'] = 'account';
        $adata['status'] = $this->input->post('status');
        ;

        $aid = $this->am->account_update($adata, $id);
        if ($aid) {
            //die('working good');
            $sdata['message'] = '<strong>Success:</strong> The account updated successfully.';
            $sdata['class'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('accounting/account1/account');
        } else {
            die('something wrong');
            $sdata['message'] = '<strong>Failure:</strong> Something went wrong while updating the account. Please try again.';
            $sdata['class'] = 'danger';
            $this->session->set_userdata($sdata);
            redirect('accounting/index/' . $type);
        }
    }

    public function account_edit($type, $id) {
        $this->data['item_active'] = 'account';
        $this->data['sub_title'] = 'Account';
        $this->data['type'] = 'account';
        $this->data['payment'] = $this->am->account_id($type, $id);
        $this->data['main_content'] = $this->load->view('accounting/account_edit', $this->data, true);
        $this->load->view('master', $this->data);
//     	echo $id;
//     	die();
    }

    public function process($type) {
        $accounting_id = $this->am->process($type);
        if ($accounting_id) {
            $sdata['message'] = '<strong>Success:</strong> The payment has been processed successfully.';
            $sdata['class'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('accounting/receipt/' . $type . '/' . $accounting_id);
        } else {
            $sdata['message'] = '<strong>Failure:</strong> Something went wrong while processing the payment. Please try again.';
            $sdata['class'] = 'danger';
            $this->session->set_userdata($sdata);
            redirect('accounting/index/' . $type);
        }
    }

    public function receipt($type, $id) {
        if ($type == 'income') {
            $this->data['item_active'] = 'income';
            $this->data['sub_title'] = 'General Income';
            $this->data['type'] = 'income';
        } elseif ($type == 'expense') {
            $this->data['item_active'] = 'expense';
            $this->data['sub_title'] = 'General Expense';
            $this->data['type'] = 'expense';
        } elseif ($type == 'account') {
            $this->data['item_active'] = 'account';
            $this->data['sub_title'] = 'Account';
            $this->data['type'] = 'account';
        } elseif ($type == 'supplier_due') {
            $this->data['item_active'] = 'supplier_due';
            $this->data['sub_title'] = 'Supplier Due Payment';
            $this->data['type'] = 'supplier_due';
            $this->data['companies'] = $this->cm->get('supplier');
        } elseif ($type == 'client_due') {
            $this->data['item_active'] = 'client_due';
            $this->data['sub_title'] = 'Client Due Payment';
            $this->data['type'] = 'client_due';
            $this->data['companies'] = $this->cm->get('client');
        }
        $this->data['payment'] = $this->am->getOrder($type, $id);
        $this->data['main_content'] = $this->load->view('accounting/receipt', $this->data, true);
        $this->load->view('master', $this->data);
    }

    public function edit($type, $id) {
        if ($this->data['user_role'] == 1 OR $this->data['user_role'] == 2) {
            if ($type == 'income') {
                $this->data['item_active'] = 'income';
                $this->data['sub_title'] = 'General Income';
                $this->data['type'] = 'income';
            } elseif ($type == 'expense') {
                $this->data['item_active'] = 'expense';
                $this->data['sub_title'] = 'General Expense';
                $this->data['type'] = 'expense';
            } elseif ($type == 'supplier_due') {
                $this->data['item_active'] = 'supplier_due';
                $this->data['sub_title'] = 'Supplier Due Payment';
                $this->data['type'] = 'supplier_due';
                $this->data['companies'] = $this->cm->get('supplier');
            } elseif ($type == 'client_due') {
                $this->data['item_active'] = 'client_due';
                $this->data['sub_title'] = 'Client Due Payment';
                $this->data['type'] = 'client_due';
                $this->data['companies'] = $this->cm->get('client');
            }
            $this->data['payment'] = $this->am->getOrder($type, $id);
            $this->data['main_content'] = $this->load->view('accounting/edit', $this->data, true);
            $this->load->view('master', $this->data);
        } else {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('accounting/index' . $type);
        }
    }

    public function update($type, $id) {
        $payment_id = $this->am->update($type, $id);
        if ($payment_id) {
            $sdata['message'] = '<strong>Success:</strong> The Payment has been updated Successfully';
            $sdata['class'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('accounting/receipt/' . $type . '/' . $payment_id);
        } else {
            $sdata['message'] = '<strong>Failure:</strong> Something went wrong while updating the payment. Please Try again.';
            $sdata['class'] = 'danger';
            $this->session->set_userdata($sdata);
            redirect('accounting/index/' . $type);
        }
    }

    public function delete($type, $id) {
        $delete = $this->am->delete($id);
        if ($delete) {
            if ($type == 'account')
                $sdata['message'] = '<strong>Success:</strong> The account deleted with all related information.';
            else
                $sdata['message'] = '<strong>Success:</strong> The payment has been deleted with all related information.';
            $sdata['class'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('accounting/index/' . $type);
        }
        else {
            $sdata['message'] = '<strong>Failure:</strong> Something went wrong while deleting the payment. Please Try again.';
            $sdata['class'] = 'danger';
            $this->session->set_userdata($sdata);
            redirect('accounting/index/' . $type);
        }
    }

    public function account_delete($id) {
        $delete = $this->am->account_delete($id);
        if ($delete) {

            $sdata['message'] = '<strong>Success:</strong> The account deleted with all related information.';
            $sdata['class'] = 'success';
            $this->session->set_userdata($sdata);
            redirect('accounting/account1/account');
        } else {
            $sdata['message'] = '<strong>Failure:</strong> Something went wrong while deleting the account. Please Try again.';
            $sdata['class'] = 'danger';
            $this->session->set_userdata($sdata);
            redirect('accounting/account1/account');
        }
    }

    public function details($id, $res_acc) {
//        echo $id;
//        die();
        $this->data['acc_id'] = $this->am->account_details($id);
//        echo "<pre>";
//        print_r($this->data['acc_id']);
        $res_acc = $this->data['acc_id'][0]['acc_name'];
        //die($res_acc);

        $star_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        if (isset($star_date) && !empty($star_date)) {
            $this->data['start_date'] = date('Y-m-d', strtotime($star_date));
            $this->data['filtered'] = 'yes';
        } else {
            $this->data['start_date'] = date('Y-m-d', strtotime('0000-00-00'));
            $this->data['filtered'] = 'no';
        }
        if (isset($end_date) && !empty($end_date)) {
            $this->data['end_date'] = date('Y-m-d', strtotime($end_date));
        } else {
            $this->data['end_date'] = date('Y-m-d');
        }
        $this->data['sub_title'] = 'Account Details';
        $this->data['details'] = $this->am->account_details($id);

        $rr = $this->data['order'] = $this->am->get_order_details($res_acc);
//        var_dump($res_acc);
//        //var_dump($rr);
//        exit();
        $this->data['salary'] = $this->am->get_salary_details($res_acc);
        $this->data['accounting'] = $this->am->get_account_details($res_acc);
//        echo "<pre>";
//        print_r($this->data);
//        die();
        //$this->data['company'] = $this->cm->get_details($id, $this->data['start_date'], $this->data['end_date']);
        $this->data['main_content'] = $this->load->view('accounting/account_details', $this->data, true);
        $this->load->view('master', $this->data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */