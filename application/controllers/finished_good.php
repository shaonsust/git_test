<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Finished_Good extends CI_Controller
{

    public $data;
    public function __construct()
    {
        parent::__construct();
        $user_id=$this->session->userdata('user_id');
        if(empty($user_id))
        {
            redirect('login');
        }
        $this->load->model('items_model', 'im');
        $this->data['user_role'] = $this->session->userdata('user_role');
        $this->data['menu_active'] = 'finished_good';
        $this->data['title'] = 'Finished Goods';
    }

    public function index()
    {
        $this->data['item_active'] = 'all_finished_goods';
        $this->data['sub_title'] = 'All Finished Goods';
        $this->data['items'] = $this->im->get_items_index('finished_good');
        $this->data['main_content'] = $this->load->view('finished_good/index', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function add_new()
    {
        $this->data['sub_title'] = 'New Finished Good';
        $this->data['main_content'] = $this->load->view('finished_good/new', $this->data, true);
        $this->load->view('master', $this->data);
    }
    public function save()
    {
        $ndata['name'] = $this->input->post('name');
        $ndata['unit'] = $this->input->post('unit');
        $ndata['price'] = $this->input->post('price');
        $ndata['stock'] = $this->input->post('stock');
        $ndata['min_stock'] = $this->input->post('min_stock');
        $ndata['type'] = 'finished_good';
        $ndata['notes'] = $this->input->post('notes');

        $save = $this->im->save($ndata);
        if($save)
        {
            if($save == 'exist')
            {
                $sdata['message'] = '<strong>Failure:</strong> Duplicate Entry Detected. Please try a different Name';
                $sdata['class'] = 'danger';
            }
            else {
                $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
                $sdata['class'] = 'success';
            }
        }
        else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('finished_good');
    }
    public function edit($id)
    {
        if($this->data['user_role'] == 1 OR $this->data['user_role'] == 2)
        {
            $this->data['sub_title'] = 'Edit Finished Good';
            $this->data['item'] = $this->im->get_individual($id);
            $this->data['main_content'] = $this->load->view('finished_good/edit', $this->data, true);
            $this->load->view('master', $this->data);
        }
        else
        {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
            $this->session->set_userdata($sdata);
            redirect('finished_good');
        }

    }
    public function update($id)
    {
        $udata['name'] = $this->input->post('name');
        $udata['unit'] = $this->input->post('unit');
        $udata['price'] = $this->input->post('price');
        $udata['stock'] = $this->input->post('stock');
        $udata['min_stock'] = $this->input->post('min_stock');
        $udata['notes'] = $this->input->post('notes');
        $update = $this->im->update($udata, $id);
        if($update)
        {
            if($update == 'exist')
            {
                $sdata['message'] = '<strong>Failure:</strong> Duplicate Entry Detected. Please try a different Name';
                $sdata['class'] = 'danger';
            }
            else {
                $sdata['message'] = '<strong>Success:</strong> Data Saved Successfully';
                $sdata['class'] = 'success';
            }
        }
        else {
            $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
            $sdata['class'] = 'warning';

        }
        $this->session->set_userdata($sdata);
        redirect('finished_good');
    }
    public function delete($id)
    {
        if($this->data['user_role'] == 1 OR $this->data['user_role'] == 2)
        {
            $delete = $this->im->delete($id);
            if($delete)
            {
                $sdata['message'] = '<strong>Success:</strong> Data Deleted Successfully';
                $sdata['class'] = 'success';
            }
            else {
                $sdata['message'] = '<strong>Warning:</strong> Server encountered a problem during the process!! Please Try again';
                $sdata['class'] = 'warning';
            }
        }
        else
        {
            $sdata['message'] = '<strong>Warning:</strong> You are not allowed to do this. Please contact administrator.';
            $sdata['class'] = 'warning';
        }
        $this->session->set_userdata($sdata);
        redirect('finished_good');

    }
    public function details($id)
    {
        $this->data['id'] = $id;
        $star_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        if(isset($star_date) && !empty($star_date))
        {
            $this->data['start_date'] = date('Y-m-d', strtotime($star_date));
            $this->data['filtered'] = 'yes';
        }
        else {
            $this->data['start_date'] = date('Y-m-d', strtotime('0000-00-00'));
            $this->data['filtered'] = 'no';
        }
        if(isset($end_date) && !empty($end_date))
        {
            $this->data['end_date'] = date('Y-m-d', strtotime($end_date));
        }
        else {
            $this->data['end_date'] = date('Y-m-d');
        }
        $this->data['sub_title'] = 'Finished Goods Details';
        $this->data['item'] = $this->im->get_details($id, $this->data['start_date'], $this->data['end_date']);
        $this->data['main_content'] = $this->load->view('finished_good/details', $this->data, true);
        $this->load->view('master', $this->data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */