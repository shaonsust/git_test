<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Numbertowords {
    public function convert_currency($number)
    {
        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        $string .= $this->convert_number_to_words($number);

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= ' Taka, ';
            $string .= $this->convert_number_to_words(round($fraction,2)).' Paisa Only';
        }
        else {
            $string .= ' Taka Only';
        }
        return $string;
    }
    public function convert_number_to_words($number) {
        $hyphen      = '-';
        $conjunction = ' and ';
        $separator   = ' ';
        $negative    = 'negative ';
        $decimal     = ' point ';
        $dictionary  = array(
            0                   => 'Zero',
            1                   => 'One',
            2                   => 'Two',
            3                   => 'Three',
            4                   => 'Four',
            5                   => 'Five',
            6                   => 'Six',
            7                   => 'Seven',
            8                   => 'Eight',
            9                   => 'Nine',
            10                  => 'Ten',
            11                  => 'Eleven',
            12                  => 'Twelve',
            13                  => 'Thirteen',
            14                  => 'Fourteen',
            15                  => 'Fifteen',
            16                  => 'Sixteen',
            17                  => 'Seventeen',
            18                  => 'Eighteen',
            19                  => 'Nineteen',
            20                  => 'Twenty',
            30                  => 'Thirty',
            40                  => 'Fourty',
            50                  => 'Fifty',
            60                  => 'Sixty',
            70                  => 'Seventy',
            80                  => 'Eighty',
            90                  => 'Ninety',
            100                 => 'Hundred',
            1000                => 'Thousand',
            100000                => 'Lakh',
//        1000000             => 'Million',
            10000000            => 'Koti',
//        1000000000          => 'Billion',
//        1000000000000       => 'Trillion',
//        1000000000000000    => 'Quadrillion',
//        1000000000000000000 => 'Quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . $this->convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . $this->convert_number_to_words($remainder);
                }
                break;
            case $number < 100000:
                $thousand  = (int) ($number / 1000);
                $remainder = $number % 1000;
                $k_remainder= $thousand % 10;
                if($k_remainder)
                {
                    $string .= $this->convert_number_to_words($thousand) . ' ' . $dictionary[1000];
                }else{
                    $string = $dictionary[$thousand] . ' ' . $dictionary[1000];
                }
                if ($remainder) {
                    $string .= $separator . $this->convert_number_to_words($remainder);
                }
                break;
            case $number < 10000000:
                $lakh  = (int) ($number / 100000);
                $remainder = $number % 100000;
                $k_remainder= $lakh % 10;
                if($k_remainder)
                {
                    $string .= $this->convert_number_to_words($lakh) . ' ' . $dictionary[100000];
                }else{
                    $string = $dictionary[$lakh] . ' ' . $dictionary[100000];
                }
                if ($remainder) {
                    $string .= $separator . $this->convert_number_to_words($remainder);
                }
                break;
            case $number < 1000000000:
                $koti  = (int) ($number / 10000000);
                $remainder = $number % 10000000;
                $k_remainder= $koti % 10;
                if($k_remainder)
                {
                    $string .= $this->convert_number_to_words($koti) . ' ' . $dictionary[10000000];
                }else{
                    $string = $dictionary[$koti] . ' ' . $dictionary[10000000];
                }
                if ($remainder) {
                    $string .= $separator . $this->convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= $this->convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= ' Taka, ';
            $string .= $this->convert_number_to_words(round($fraction,2)).' Paisa Only';
        }

        return $string;
    }
}
?>