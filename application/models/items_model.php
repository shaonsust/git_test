<?php

class Items_Model extends CI_Model {

    public function get_items_index($type) {
        $query = $this->db->query("
            Select
                item.id, item.name, item.unit, item.price
                , IFNULL(item.stock, 0) as primary_stock,
                IFNULL(ordstemp.total_sold, 0) as total_sold
                , IFNULL(ordptemp.total_purchase, 0) as total_purchase
                , IFNULL(usgtemp.total_used, 0) as total_used
                , IFNULL(prodtemp.total_prod, 0) as total_prod
            from items item
            left join
                (
                    Select ords.item_id
                    , SUM(ords.qty) as total_sold
                    from order_items ords
                    where  ords.type = 0
                    GROUP BY  ords.item_id
                ) ordstemp on ordstemp.item_id = item.id

            left join
                (
                    Select ordp.item_id
                    , SUM(ordp.qty) as total_purchase
                    from order_items ordp
                    where ordp.type = 1
                    GROUP BY  ordp.item_id
                ) ordptemp on ordptemp.item_id = item.id

            left join
                (
                    Select usg.item_id
                    , SUM(usg.qty)  as total_used
                    from usage_items usg
                    group by usg.item_id
                ) usgtemp on usgtemp.item_id = item.id
            left join
                (
                    Select prod.item_id
                    , SUM(prod.qty)  as total_prod
                    from production prod
                    group by prod.item_id
                ) prodtemp on prodtemp.item_id = item.id
              WHERE item.`type` = '$type'");
        return $query->result();    
    }

    public function get_items($type)
    {
        $this->db->select('*');
        $this->db->where('type',$type);
        $this->db->from('items');
        $query_result =  $this->db->get();
        return $query_result->result();
    }
    public function save($data)
    {
        $this->db->select('*');
        $this->db->where('name',$data['name']);
        $this->db->from('items');
        $query_result=  $this->db->get();
        $item =  $query_result->num_rows();

        if($item == 0)
        {
            $this->db->insert('items',$data);
            return $this->db->insert_id();
        }
        else {
            return 'exist';
        }
    }
    public function get_individual($id)
    {
        $this->db->select('*');
        $this->db->where('id',$id);
        $this->db->from('items');
        $query_result=  $this->db->get();
        return $query_result->row();
    }
    public function update($data, $id)
    {
        $this->db->select('*');
        $this->db->where('name',$data['name']);
        $this->db->where('id !=',$id);
        $this->db->from('items');
        $query_result=  $this->db->get();
        $item =  $query_result->num_rows();

        if($item == 0)
        {
            $this->db->where('id',$id);
            $this->db->update('items',$data);
            return $this->db->affected_rows();
        }
        else {
            return 'exist';
        }

    }
    public function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('items');
        return $this->db->affected_rows();
    }
    public function get_details($id, $start_date, $end_date)
    {
        $this->db->select('*');
        $this->db->where('id',$id);
        $this->db->from('items');
        $query_result =  $this->db->get();
        $data['item'] = $query_result->row();

        $this->db->select('MAX(qty) as max_qty, MAX(rate) as max_rate, MIN(qty) as min_qty, MIN(rate) as min_rate, AVG(qty) as avg_qty, AVG(rate) as avg_rate, SUM(qty) as total_qty, SUM(price) as total_price, COUNT(item_id) as total_sales');
        $this->db->where('item_id', $id);
        $this->db->where('type', 0);
        $this->db->where('date >=', $start_date);
        $this->db->where('date <=', $end_date);
        $this->db->from('order_items');
        $sale_stats = $this->db->get();
        $data['sale_stats'] = $sale_stats->row();

        $this->db->select('MAX(qty) as max_qty, MAX(rate) as max_rate, MIN(qty) as min_qty, MIN(rate) as min_rate, AVG(qty) as avg_qty, AVG(rate) as avg_rate, SUM(qty) as total_qty, SUM(price) as total_price, COUNT(item_id) as total_purchase');
        $this->db->where('item_id', $id);
        $this->db->where('type', 1);
        $this->db->where('date >=', $start_date);
        $this->db->where('date <=', $end_date);
        $this->db->from('order_items');
        $purchase_stats = $this->db->get();
        $data['purchase_stats'] = $purchase_stats->row();

        $this->db->select('MAX(qty) as max_qty, MIN(qty) as min_qty, AVG(qty) as avg_qty, SUM(qty) as total_qty, COUNT(item_id) as total_productions');
        $this->db->where('item_id', $id);
        $this->db->where('date >=', $start_date);
        $this->db->where('date <=', $end_date);
        $this->db->from('production');
        $production_stats = $this->db->get();
        $data['production_stats'] = $production_stats->row();
        
        $this->db->select('MAX(qty) as max_qty, MIN(qty) as min_qty, AVG(qty) as avg_qty, SUM(qty) as total_qty, COUNT(item_id) as total_usage');
        $this->db->where('item_id', $id);
        $this->db->where('date >=', $start_date);
        $this->db->where('date <=', $end_date);
        $this->db->from('usage_items');
        $usage_stats = $this->db->get();
        $data['usage_stats'] = $usage_stats->row();

        $this->db->select('order_items.order_id, order_items.rate, order_items.qty, order_items.price, order.date, order.time, order.receipt_no, companies.name');
        $this->db->where('order_items.date >=', $start_date);
        $this->db->where('order_items.date <=', $end_date);
        $this->db->where('order_items.item_id', $id);
        $this->db->where('order_items.type', 1);
        $this->db->join('order', 'order.id = order_items.order_id');
        $this->db->join('companies', 'companies.id = order.company_id');
        $this->db->from('order_items');
        $purchase_history = $this->db->get();
        $data['purchase_history'] = $purchase_history->result();

        $this->db->select('order_items.order_id, order_items.rate, order_items.qty, order_items.price, order.date, order.time, order.receipt_no, companies.name');
        $this->db->where('order_items.date >=', $start_date);
        $this->db->where('order_items.date <=', $end_date);
        $this->db->where('order_items.item_id', $id);
        $this->db->where('order_items.type', 0);
        $this->db->join('order', 'order.id = order_items.order_id');
        $this->db->join('companies', 'companies.id = order.company_id');
        $this->db->from('order_items');
        $sales_history = $this->db->get();
        $data['sales_history'] = $sales_history->result();

        $this->db->select('usage_items.usage_id, usage_items.qty, usage.receipt_no, usage.date, usage.time, usage.purpose');
        $this->db->where('usage_items.date >=', $start_date);
        $this->db->where('usage_items.date <=', $end_date);
        $this->db->where('usage_items.item_id', $id);
        $this->db->join('usage', 'usage.id = usage_items.usage_id');
        $this->db->from('usage_items');
        $usage_history = $this->db->get();
        $data['usage_history'] = $usage_history->result();

        $this->db->select('production.id, production.qty, production.date, production.time, production.receipt_no');
        $this->db->where('production.date >=', $start_date);
        $this->db->where('production.date <=', $end_date);
        $this->db->where('production.item_id', $id);
        $this->db->from('production');
        $production_history = $this->db->get();
        $data['production_history'] = $production_history->result();

        return $data;
    }

} 