<?php

class Order_Model extends CI_Model {

    public function getAll($type) {
        $this->db->select('order.id, order.receipt_no, order.company_id, order.date, order.time, order.total_price, order.discount_percentage, order.discount, order.vat_percentage, order.vat, order.payment_amount, order.due_amount, companies.name, order.tr_type, order.res_acc');
        $this->db->from('order');
        $this->db->where('order.type', $type);
        $this->db->join('companies', 'companies.id = order.company_id');
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function type($type) {
        $this->db->select('accounting.type, accounting.res_acc');
        $this->db->from('accounting');
        $this->db->where('order.type', $type);
        $this->db->join('companies', 'companies.id = order.company_id');
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function get_items($type = false, $sales = false) {
        if ($sales == 'sales') {
            $query = $this->db->query("
            Select
                item.id, item.name, item.unit, item.price, item.type
                , IFNULL(item.stock, 0) as primary_stock,
                IFNULL(ordstemp.total_sold, 0) as total_sold
                , IFNULL(ordptemp.total_purchase, 0) as total_purchase
                , IFNULL(usgtemp.total_used, 0) as total_used
                , IFNULL(prodtemp.total_production, 0) as total_production
            from items item
            left join
                (
                    Select ords.item_id
                    , SUM(ords.qty) as total_sold
                    from order_items ords
                    where  ords.type = 0
                    GROUP BY  ords.item_id
                ) ordstemp on ordstemp.item_id = item.id

            left join
                (
                    Select ordp.item_id
                    , SUM(ordp.qty) as total_purchase
                    from order_items ordp
                    where ordp.type = 1
                    GROUP BY  ordp.item_id
                ) ordptemp on ordptemp.item_id = item.id

            left join
                (
                    Select prod.item_id
                    , SUM(prod.qty)  as total_production
                    from production prod
                    group by prod.item_id
                ) prodtemp on prodtemp.item_id = item.id

            left join
                (
                    Select usg.item_id
                    , SUM(usg.qty)  as total_used
                    from usage_items usg
                    group by usg.item_id
                ) usgtemp on usgtemp.item_id = item.id 
            WHERE item.`type` = 'finished_good'");
        } else if ($type == false && $sales == false) {
            $query = $this->db->query("
            Select
                item.id, item.name, item.unit, item.price, item.type
                , IFNULL(item.stock, 0) as primary_stock,
                IFNULL(ordstemp.total_sold, 0) as total_sold
                , IFNULL(ordptemp.total_purchase, 0) as total_purchase
                , IFNULL(usgtemp.total_used, 0) as total_used
            from items item
            left join
                (
                    Select ords.item_id
                    , SUM(ords.qty) as total_sold
                    from order_items ords
                    where  ords.type = 0
                    GROUP BY  ords.item_id
                ) ordstemp on ordstemp.item_id = item.id

            left join
                (
                    Select ordp.item_id
                    , SUM(ordp.qty) as total_purchase
                    from order_items ordp
                    where ordp.type = 1
                    GROUP BY  ordp.item_id
                ) ordptemp on ordptemp.item_id = item.id

            left join
                (
                    Select usg.item_id
                    , SUM(usg.qty)  as total_used
                    from usage_items usg
                    group by usg.item_id
                ) usgtemp on usgtemp.item_id = item.id
                WHERE item.`type` != 'finished_good'");
        } else {
            $query = $this->db->query("
            Select
                item.id, item.name, item.unit, item.price, item.type
                , IFNULL(item.stock, 0) as primary_stock,
                IFNULL(ordstemp.total_sold, 0) as total_sold
                , IFNULL(ordptemp.total_purchase, 0) as total_purchase
                , IFNULL(usgtemp.total_used, 0) as total_used
            from items item
            left join
                (
                    Select ords.item_id
                    , SUM(ords.qty) as total_sold
                    from order_items ords
                    where  ords.type = 0
                    GROUP BY  ords.item_id
                ) ordstemp on ordstemp.item_id = item.id

            left join
                (
                    Select ordp.item_id
                    , SUM(ordp.qty) as total_purchase
                    from order_items ordp
                    where ordp.type = 1
                    GROUP BY  ordp.item_id
                ) ordptemp on ordptemp.item_id = item.id

            left join
                (
                    Select usg.item_id
                    , SUM(usg.qty)  as total_used
                    from usage_items usg
                    group by usg.item_id
                ) usgtemp on usgtemp.item_id = item.id
              WHERE item.`type` = '$type'");
        }


        return $query->result();
        // echo '<pre>';
        // print_r($query->result());exit;
    }

    public function process($type) {
        if ($type == 'sales') {
            $odata['receipt_no'] = 'BISE-S' . time();
        } elseif ($type == 'purchase') {
            $odata['receipt_no'] = $this->input->post('receipt_no');
        }
        $odata['date'] = $this->input->post('date');
        $odata['time'] = $this->input->post('time');
        $total_price = $this->input->post('total_price');
        $odata['total_price'] = number_format($total_price, 2, '.', '');
        $discount = $this->input->post('discount');
        $odata['discount'] = number_format($discount, 2, '.', '');
        $discount_type = $this->input->post('discount_type');
        if ($discount_type == 'amount') {
            $discount_percentage = $odata['discount'] * 100 / $odata['total_price'];
            $odata['discount_percentage'] = number_format($discount_percentage, 2, '.', '');
        } else {
            $discount_percentage = $this->input->post('discount_percentage');
            $odata['discount_percentage'] = number_format($discount_percentage, 2, '.', '');
        }
        $vat = $this->input->post('vat');
        $odata['vat'] = number_format($vat, 2, '.', '');
        $vat_type = $this->input->post('vat_type');
        if ($vat_type == 'amount') {
            $vat_percentage = $odata['vat'] * 100 / $odata['total_price'];
            $odata['vat_percentage'] = number_format($vat_percentage, 2, '.', '');
        } else {
            $vat_percentage = $this->input->post('vat_percentage');
            $odata['vat_percentage'] = number_format($vat_percentage, 2, '.', '');
        }
        $payment_amount = $this->input->post('payment_amount');
        $odata['payment_amount'] = number_format($payment_amount, 2, '.', '');

        $due_amount = $this->input->post('due_amount');
        $odata['due_amount'] = number_format($due_amount, 2, '.', '');
        $odata['type'] = $type;

        $company_name = $this->input->post('company');
        $this->db->select('id');
        $this->db->where('name', $company_name);
        $this->db->from('companies');
        $company_result = $this->db->get();
        $num_company = $company_result->num_rows();
        if ($num_company == 1) {
            $company = $company_result->row();
            $odata['company_id'] = $company->id;
        } else {
            $cdata['name'] = $company_name;
            if ($type == 'sales') {
                $cdata['type'] = 'client';
                $cdata['notes'] = 'This Client was created from the sales page while processing a new sales order';
            } elseif ($type == 'purchase') {
                $cdata['type'] = 'supplier';
                $cdata['notes'] = 'This Supplier was created from the purchase page while processing a new purchase order';
            }

            $cdata['status'] = 1;
            $cdata['created_at'] = $odata['date'];
            $this->db->insert('companies', $cdata);
            $odata['company_id'] = $this->db->insert_id();
        }

        // transaction type form calculation
        $odata['tr_type'] = $this->input->post('payment_type');
        $odata['check_type'] = $this->input->post('check_type');
        if ($odata['check_type'] == 'acc_pay')
            $odata['res_acc'] = $this->input->post('bank_acc');
        else
            $odata['res_acc'] = $this->input->post('cash_acc');
        //$price = $this->input->post('cash_acc');
        $odata['check_no'] = $this->input->post('check_no');
        $odata['bank_name'] = $this->input->post('bank_name');
        if ($odata['tr_type'] == 'cash')
            $odata['check_date'] = '0000-00-00';
        else
            $odata['check_date'] = $this->input->post('check_date');
        //$price = $this->input->post('cash_acc');
//        echo "<pre>";
//        print_r($odata);
//        // die();

        $this->db->insert('order', $odata);
        $sdata['order_id'] = $this->db->insert_id();

        $item_id = $this->input->post('item_id');
        $rate = $this->input->post('rate');
        $qty = $this->input->post('qty');
        $price = $this->input->post('price');
        $num_products = count($item_id);
        for ($i = 0; $i < $num_products; $i++) {
            $sdata['item_id'] = $item_id[$i];
            $sdata['rate'] = number_format($rate[$i], 2, '.', '');
            $sdata['qty'] = number_format($qty[$i], 2, '.', '');
            $sdata['price'] = number_format($price[$i], 2, '.', '');
            if ($type == 'sales') {
                $sdata['type'] = 0;
            } elseif ($type == 'purchase') {
                $sdata['type'] = 1;
            }
            $sdata['date'] = $odata['date'];
            $this->db->insert('order_items', $sdata);
        }
        return $sdata['order_id'];
    }

    public function update($id, $type) {
        if ($type == 'purchase') {
            $odata['receipt_no'] = $this->input->post('receipt_no');
        }
        $odata['date'] = $this->input->post('date');
        $odata['time'] = $this->input->post('time');
        $total_price = $this->input->post('total_price');
        $odata['total_price'] = number_format($total_price, 2, '.', '');
        $discount = $this->input->post('discount');
        $odata['discount'] = number_format($discount, 2, '.', '');
        $discount_type = $this->input->post('discount_type');
        if ($discount_type == 'amount') {
            $discount_percentage = $odata['discount'] * 100 / $odata['total_price'];
            $odata['discount_percentage'] = number_format($discount_percentage, 2, '.', '');
        } else {
            $discount_percentage = $this->input->post('discount_percentage');
            $odata['discount_percentage'] = number_format($discount_percentage, 2, '.', '');
        }
        $vat = $this->input->post('vat');
        $odata['vat'] = number_format($vat, 2, '.', '');
        $vat_type = $this->input->post('vat_type');
        if ($vat_type == 'amount') {
            $vat_percentage = $odata['vat'] * 100 / $odata['total_price'];
            $odata['vat_percentage'] = number_format($vat_percentage, 2, '.', '');
        } else {
            $vat_percentage = $this->input->post('vat_percentage');
            $odata['vat_percentage'] = number_format($vat_percentage, 2, '.', '');
        }
        $payment_amount = $this->input->post('payment_amount');
        $odata['payment_amount'] = number_format($payment_amount, 2, '.', '');

        $due_amount = $this->input->post('due_amount');
        $odata['due_amount'] = number_format($due_amount, 2, '.', '');

        $company_name = $this->input->post('company');
        $this->db->select('id');
        $this->db->where('name', $company_name);
        $this->db->from('companies');
        $company_result = $this->db->get();
        $num_company = $company_result->num_rows();
        if ($num_company == 1) {
            $company = $company_result->row();
            $odata['company_id'] = $company->id;
        } else {
            $cdata['name'] = $company_name;
            if ($type == 'sales') {
                $cdata['type'] = 'client';
                $cdata['notes'] = 'This Client was created from the sales page while processing a new sales order';
            } elseif ($type == 'purchase') {
                $cdata['type'] = 'supplier';
                $cdata['notes'] = 'This Supplier was created from the purchase page while processing a new purchase order';
            }
            $cdata['status'] = 1;
            $cdata['created_at'] = $odata['date'];
            $this->db->insert('companies', $cdata);
            $odata['company_id'] = $this->db->insert_id();
        }

        $this->db->where('id', $id);
        $this->db->update('order', $odata);

        $this->db->where('order_id', $id);
        $this->db->delete('order_items');

        $sdata['order_id'] = $id;
        $item_id = $this->input->post('item_id');
        $rate = $this->input->post('rate');
        $qty = $this->input->post('qty');
        $price = $this->input->post('price');
        $num_products = count($item_id);
        for ($i = 0; $i < $num_products; $i++) {
            $sdata['item_id'] = $item_id[$i];
            $sdata['rate'] = number_format($rate[$i], 2, '.', '');
            $sdata['qty'] = number_format($qty[$i], 2, '.', '');
            $sdata['price'] = number_format($price[$i], 2, '.', '');
            if ($type == 'sales') {
                $sdata['type'] = 0;
            } elseif ($type == 'purchase') {
                $sdata['type'] = 1;
            }
            $sdata['date'] = $odata['date'];
            $this->db->insert('order_items', $sdata);
        }


        return $sdata['order_id'];
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('order');
        $delete = $this->db->affected_rows();
        $this->db->where('order_id', $id);
        $this->db->delete('order_items');
        return $delete;
    }

    public function getOrder($id) {
        $this->db->select('order.id, order.receipt_no, order.date, order.time, order.total_price, order.discount_percentage, order.discount, order.vat_percentage, order.vat, order.payment_amount, order.due_amount, companies.id as company_id, companies.name as company_name, companies.contact_person, companies.contact_phone, companies.address');
        $this->db->where('order.id', $id);
        $this->db->from('order');
        $this->db->join('companies', 'companies.id = order.company_id');
        $order_result = $this->db->get();
        $data['order'] = $order_result->row();
        $order_date = $data['order']->date;
        $company_id = $data['order']->company_id;

        $this->db->select('order_items.item_id, order_items.rate, order_items.qty, order_items.price, items.name as product_name, items.unit');
        $this->db->where('order_id', $id);
        $this->db->from('order_items');
        $this->db->join('items', 'items.id = order_items.item_id');
        $sales_result = $this->db->get();
        $data['sales'] = $sales_result->result();

        // $this->db->select('SUM(order.due_amount) as total_due, SUM(accounting.amount) as total_due_payment, companies.incoming_amount as initial_due');
        // $this->db->where('companies.id', $data['order']->company_id);
        // $this->db->where('order.date <=', $data['order']->date);
        // $this->db->where('accounting.date <=', $data['order']->date);
        // $this->db->from('companies');
        // $this->db->join('order', 'order.company_id = companies.id');
        // $this->db->join('accounting', 'accounting.company_id = companies.id');

        $query = $this->db->query("
            Select
                comp.id, comp.incoming_amount,
                temp.due_amount,
                acctemp.acc_amount

            from companies comp
            left join
                (
                    Select ord.company_id, SUM(ord.due_amount) as due_amount
                    from `order` ord
                    where  ord.date <= '$order_date' AND ord.id != '$id'
                    GROUP BY  ord.company_id
                ) temp on temp.company_id = comp.id

            left join
                (
                    Select acc.company_id ,SUM(acc.amount)  as acc_amount
                    from accounting acc
                    where  acc.date <= '$order_date'
                    group by acc.company_id
                ) acctemp on comp.id = acctemp.company_id
              WHERE comp.`id` = '$company_id'");
        $chalan_result = $query->row();
        $data['chalan_result'] = $chalan_result;

        return $data;
    }

}
