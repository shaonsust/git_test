<?php

class Reports_Model extends CI_Model {

    public function client_summary($start_date, $end_date) {
        $query = $this->db->query("
Select
	comp.id, comp.name, comp.contact_person, comp.address, comp.contact_phone,
	comp.incoming_amount,
	temp.total_price, temp.due_amount, temp.discount, temp.vat, temp.payment_amount,
	acctemp.acc_amount

from companies comp
left join
	(
		Select ord.company_id, SUM(ord.due_amount) as due_amount, SUM(ord.discount) as discount,
		SUM(ord.vat) as vat, SUM(ord.payment_amount) as payment_amount, SUM(ord.total_price) as total_price
		from `order` ord
		where  ord.date between '$start_date' and '$end_date'
		GROUP BY  ord.company_id
	) temp on temp.company_id = comp.id

left join
	(
		Select acc.company_id ,SUM(acc.amount)  as acc_amount
		from accounting acc
		where  acc.date between '$start_date' and '$end_date'
		group by acc.company_id
	) acctemp on comp.id = acctemp.company_id
  WHERE comp.`type` = 'client'");

        return $query->result();
    }

    public function supplier_summary($start_date, $end_date) {
        $query = $this->db->query("
Select
	comp.id, comp.name, comp.contact_person, comp.address, comp.contact_phone,
	comp.outgoing_amount,
	temp.total_price, temp.due_amount, temp.discount, temp.vat, temp.payment_amount,
	acctemp.acc_amount

from companies comp
left join
	(
		Select ord.company_id, SUM(ord.due_amount) as due_amount, SUM(ord.discount) as discount,
		SUM(ord.vat) as vat, SUM(ord.payment_amount) as payment_amount, SUM(ord.total_price) as total_price
		from `order` ord
		where  ord.date between '$start_date' and '$end_date'
		GROUP BY  ord.company_id
	) temp on temp.company_id = comp.id

left join
	(
		Select acc.company_id ,SUM(acc.amount)  as acc_amount
		from accounting acc
		where  acc.date between '$start_date' and '$end_date'
		group by acc.company_id
	) acctemp on comp.id = acctemp.company_id
  WHERE comp.`type` = 'supplier'");

        return $query->result();
    }

    public function raw_material_summary($start_date, $end_date) {
        $query = $this->db->query("
Select
	item.id, item.name, item.unit
	, IFNULL(item.stock, 0) as primary_stock,
	IFNULL(ordstemp.total_sold, 0) as total_sold
	, IFNULL(ordstemp.max_sold_rate, 0) as max_sold_rate
	, IFNULL(ordstemp.min_sold_rate, 0) as min_sold_rate
	, IFNULL(ordstemp.avg_sold_rate, 0) as avg_sold_rate
	, IFNULL(ordstemp.total_sold_amount, 0) as total_sold_amount
	, IFNULL(ordptemp.total_purchase, 0) as total_purchase
	, IFNULL(ordptemp.max_pur_rate, 0) as max_pur_rate
	, IFNULL(ordptemp.min_pur_rate, 0) as min_pur_rate
	, IFNULL(ordptemp.avg_pur_rate, 0) as avg_pur_rate
	, IFNULL(ordptemp.total_purchase_amount, 0) as total_purchase_amount
	, IFNULL(usgtemp.total_used, 0) as total_used
from items item
left join
	(
		Select ords.item_id
		, SUM(ords.qty) as total_sold
		, MAX(ords.rate) as max_sold_rate
		, Min(ords.rate) as min_sold_rate
		, AVG(ords.rate) as avg_sold_rate
		, SUM(ords.price) as total_sold_amount
		from order_items ords
		where  (ords.date between '$start_date' and '$end_date') AND (ords.type = 0)
		GROUP BY  ords.item_id
	) ordstemp on ordstemp.item_id = item.id

left join
	(
		Select ordp.item_id
		, SUM(ordp.qty) as total_purchase
		, MAX(ordp.rate) as max_pur_rate
		, Min(ordp.rate) as min_pur_rate
		, AVG(ordp.rate) as avg_pur_rate
		, SUM(ordp.price) as total_purchase_amount
		from order_items ordp
		where  (ordp.date between '$start_date' and '$end_date') AND (ordp.type = 1)
		GROUP BY  ordp.item_id
	) ordptemp on ordptemp.item_id = item.id

left join
	(
		Select usg.item_id
		, SUM(usg.qty)  as total_used
		from usage_items usg
		where  usg.date between '$start_date' and '$end_date'
		group by usg.item_id
	) usgtemp on usgtemp.item_id = item.id
  WHERE item.`type` = 'raw_material'");

        return $query->result();
    }

    public function finished_goods_summary($start_date, $end_date) {
        $query = $this->db->query("
Select
	item.id, item.name, item.unit
	, IFNULL(item.stock, 0) as primary_stock,
	IFNULL(ordstemp.total_sold, 0) as total_sold
	, IFNULL(ordstemp.max_sold_rate, 0) as max_sold_rate
	, IFNULL(ordstemp.min_sold_rate, 0) as min_sold_rate
	, IFNULL(ordstemp.avg_sold_rate, 0) as avg_sold_rate
	, IFNULL(ordstemp.total_sold_amount, 0) as total_sold_amount
	, IFNULL(ordptemp.total_purchase, 0) as total_purchase
	, IFNULL(ordptemp.max_pur_rate, 0) as max_pur_rate
	, IFNULL(ordptemp.min_pur_rate, 0) as min_pur_rate
	, IFNULL(ordptemp.avg_pur_rate, 0) as avg_pur_rate
	, IFNULL(ordptemp.total_purchase_amount, 0) as total_purchase_amount
	, IFNULL(usgtemp.total_used, 0) as total_used
	, IFNULL(prodtemp.total_production, 0) as total_production
from items item
left join
	(
		Select ords.item_id
		, SUM(ords.qty) as total_sold
		, MAX(ords.rate) as max_sold_rate
		, Min(ords.rate) as min_sold_rate
		, AVG(ords.rate) as avg_sold_rate
		, SUM(ords.price) as total_sold_amount
		from order_items ords
		where  (ords.date between '$start_date' and '$end_date') AND (ords.type = 0)
		GROUP BY  ords.item_id
	) ordstemp on ordstemp.item_id = item.id

left join
	(
		Select ordp.item_id
		, SUM(ordp.qty) as total_purchase
		, MAX(ordp.rate) as max_pur_rate
		, Min(ordp.rate) as min_pur_rate
		, AVG(ordp.rate) as avg_pur_rate
		, SUM(ordp.price) as total_purchase_amount
		from order_items ordp
		where  (ordp.date between '$start_date' and '$end_date') AND (ordp.type = 1)
		GROUP BY  ordp.item_id
	) ordptemp on ordptemp.item_id = item.id

left join
	(
		Select usg.item_id
		, SUM(usg.qty)  as total_used
		from usage_items usg
		where  usg.date between '$start_date' and '$end_date'
		group by usg.item_id
	) usgtemp on usgtemp.item_id = item.id
left join
	(
		Select prod.item_id
		, SUM(prod.qty)  as total_production
		from production prod
		where  prod.date between '$start_date' and '$end_date'
		group by prod.item_id
	) prodtemp on prodtemp.item_id = item.id
  WHERE item.`type` = 'finished_good'");

        return $query->result();
    }

    public function utility_items_summary($start_date, $end_date) {
        $query = $this->db->query("
Select
	item.id, item.name, item.unit
	, IFNULL(item.stock, 0) as primary_stock
	, IFNULL(ordptemp.total_purchase, 0) as total_purchase
	, IFNULL(ordptemp.max_pur_rate, 0) as max_pur_rate
	, IFNULL(ordptemp.min_pur_rate, 0) as min_pur_rate
	, IFNULL(ordptemp.avg_pur_rate, 0) as avg_pur_rate
	, IFNULL(ordptemp.total_purchase_amount, 0) as total_purchase_amount
	, IFNULL(usgtemp.total_used, 0) as total_used
from items item

left join
	(
		Select ordp.item_id
		, SUM(ordp.qty) as total_purchase
		, MAX(ordp.rate) as max_pur_rate
		, Min(ordp.rate) as min_pur_rate
		, AVG(ordp.rate) as avg_pur_rate
		, SUM(ordp.price) as total_purchase_amount
		from order_items ordp
		where  (ordp.date between '$start_date' and '$end_date') AND (ordp.type = 1)
		GROUP BY  ordp.item_id
	) ordptemp on ordptemp.item_id = item.id

left join
	(
		Select usg.item_id
		, SUM(usg.qty)  as total_used
		from usage_items usg
		where  usg.date between '$start_date' and '$end_date'
		group by usg.item_id
	) usgtemp on usgtemp.item_id = item.id
  WHERE item.`type` = 'utility_item'");

        return $query->result();
    }

    public function sales_summary($start_date, $end_date) {
        $this->db->select('order.id, order.receipt_no, order.company_id, order.date, order.time, order.total_price, order.discount_percentage, order.discount, order.vat_percentage, order.vat, order.payment_amount, order.due_amount, companies.name, order.tr_type, order.res_acc');
        $this->db->from('order');
        $this->db->where('order.type', 'sales');
        $this->db->where('order.date >=', $start_date);
        $this->db->where('order.date <=', $end_date);
        $this->db->join('companies', 'companies.id = order.company_id');
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function purchase_summary($start_date, $end_date) {
        $this->db->select('order.id, order.receipt_no, order.company_id, order.date, order.time, order.total_price, order.discount_percentage, order.discount, order.vat_percentage, order.vat, order.payment_amount, order.due_amount, companies.name, order.tr_type, order.res_acc');
        $this->db->from('order');
        $this->db->where('order.type', 'purchase');
        $this->db->where('order.date >=', $start_date);
        $this->db->where('order.date <=', $end_date);
        $this->db->join('companies', 'companies.id = order.company_id');
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function general_income_summary($start_date, $end_date) {
        $this->db->select('*');
        $this->db->from('accounting');
        $this->db->where('type', 'income');
        $this->db->where('date >=', $start_date);
        $this->db->where('date <=', $end_date);
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function general_expense_summary($start_date, $end_date) {
        $this->db->select('*');
        $this->db->from('accounting');
        $this->db->where('type', 'expense');
        $this->db->where('date >=', $start_date);
        $this->db->where('date <=', $end_date);
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function loan_in_summary($start_date, $end_date) {
        $this->db->select('accounting.id, accounting.amount, accounting.receipt_no, accounting.company_id, accounting.date, accounting.time, accounting.purpose, accounting.description, companies.name, accounting.tr_type, accounting.res_acc');
        $this->db->from('accounting');
        $this->db->where('accounting.date >=', $start_date);
        $this->db->where('accounting.date <=', $end_date);
        $this->db->where('accounting.type', 'incoming_loan');
        $this->db->join('companies', 'companies.id = accounting.company_id');
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function loan_out_summary($start_date, $end_date) {
        $this->db->select('accounting.id, accounting.amount, accounting.receipt_no, accounting.company_id, accounting.date, accounting.time, accounting.purpose, accounting.description, companies.name, accounting.tr_type, accounting.res_acc');
        $this->db->from('accounting');
        $this->db->where('accounting.date >=', $start_date);
        $this->db->where('accounting.date <=', $end_date);
        $this->db->where('accounting.type', 'outgoing_loan');
        $this->db->join('companies', 'companies.id = accounting.company_id');
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function overall_income_summary($start_date, $end_date) {
        $query = $this->db->query("select
	acc.date , 'Accounting' as TableType , acc.type, sum(acc.amount) Amount
	from	accounting	acc
	where acc.date between '$start_date' and '$end_date'
    AND type IN ('income', 'incoming_loan', 'incoming_installment', 'client_due')
	group by acc.date, acc.type

	UNION
select
	ord.date , 'Order' as TableType, ord.type, sum(ord.payment_amount) Amount
	from	`order` ord
	where ord.date between '$start_date' and '$end_date'
    AND type = 'sales'
	group by ord.date, ord.type");

        return $query->result();
    }

    public function overall_expense_summary($start_date, $end_date) {
        $query = $this->db->query("select
	acc.date , 'Accounting' as TableType , acc.type, sum(acc.amount) Amount
	from	accounting	acc
	where acc.date between '$start_date' and '$end_date'
    AND type IN ('expense', 'outgoing_loan', 'outgoing_installment', 'supplier_due')
	group by acc.date, acc.type

	UNION
select
	ord.date , 'Order' as TableType, ord.type, sum(ord.payment_amount) Amount
	from	`order` ord
	where ord.date between '$start_date' and '$end_date'
    AND type = 'purchase'
	group by ord.date, ord.type

	UNION
select
	sal.date , 'Salary' as TableType, sal.type, sum(sal.net_payable) Amount
	from	`salary` sal
	where sal.date between '$start_date' and '$end_date'
    AND type = 'salary'
	group by sal.date, sal.type
	");
        return $query->result();
    }

    public function get_all_salary($month, $year) {
        $this->db->select('salary.*,staff.card_no,staff.type,staff.name');
        $this->db->where('salary.month', $month);
        $this->db->where('salary.year', $year);
        $this->db->from('salary');
        $this->db->join('staff', 'salary.staff_id = staff.id');
        $query_result = $this->db->get();
        return $query_result->result();
//        echo '<pre>';
//        print_r($query_result->result());exit;
    }

}
