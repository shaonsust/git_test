<?php

class Hr_Model extends CI_Model {

    public function getAll() {
        $this->db->select('*');
        $this->db->from('staff');
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function save($data) {
        $data['name'] = $this->input->post('name');
        $data['card_no'] = $this->input->post('card_no');
        $data['type'] = $this->input->post('type');
        $data['designation'] = $this->input->post('designation');
        $data['basic_salary'] = $this->input->post('basic_salary');
        $data['daily_salary'] = $this->input->post('daily_salary');
        $data['hourly_salary'] = $this->input->post('hourly_salary');
        $data['notes'] = $this->input->post('notes');
        $data['joining_date'] = $this->input->post('joining_date');
        $data['dob'] = $this->input->post('dob');
        $data['phone'] = $this->input->post('phone');
        $data['present_address'] = $this->input->post('present_address');
        $data['permanent_address'] = $this->input->post('permanent_address');

        $this->db->select('*');
        $this->db->where('name', $data['name']);
        $this->db->from('staff');
        $query_result = $this->db->get();
        $company = $query_result->num_rows();

        if ($company == 0) {
            $this->db->insert('staff', $data);
            return $this->db->insert_id();
        } else {
            return 'exist';
        }
    }

    public function get_individual($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('staff');
        $query_result = $this->db->get();
        return $query_result->row();
    }

    public function update_employee($id) {
        $data['name'] = $this->input->post('name');
        $data['card_no'] = $this->input->post('card_no');
        $data['type'] = $this->input->post('type');
        $data['designation'] = $this->input->post('designation');
        $data['basic_salary'] = $this->input->post('basic_salary');
        $data['daily_salary'] = $this->input->post('daily_salary');
        $data['hourly_salary'] = $this->input->post('hourly_salary');
        $data['notes'] = $this->input->post('notes');
        $data['joining_date'] = $this->input->post('joining_date');
        $data['dob'] = $this->input->post('dob');
        $data['phone'] = $this->input->post('phone');
        $data['present_address'] = $this->input->post('present_address');
        $data['permanent_address'] = $this->input->post('permanent_address');

        $this->db->select('*');
        $this->db->where('name', $data['name']);
        $this->db->where('id !=', $id);
        $this->db->from('staff');
        $query_result = $this->db->get();
        $company = $query_result->num_rows();

        if ($company == 0) {
            $this->db->where('id', $id);
            $this->db->update('staff', $data);
            return $this->db->affected_rows();
        } else {
            return 'exist';
        }
    }

    public function status_update($status, $id) {
        $data['status'] = $status;

        $this->db->where('id', $id);
        $this->db->update('staff', $data);
        return $this->db->affected_rows();
    }

    public function delete_staff($id) {
        $this->db->where('id', $id);
        $this->db->delete('staff');
        $delete = $this->db->affected_rows();

        $this->db->where('staff_id', $id);
        $this->db->delete('salary');

        return $delete;
    }

    public function process_salary($id) {
        $data['staff_id'] = $id;
        $data['date'] = $this->input->post('date');
        $data['time'] = $this->input->post('time');
        $data['month'] = $this->input->post('month');
        $data['year'] = $this->input->post('year');
        $data['basic_salary'] = $this->input->post('basic_salary');
        $data['daily_salary'] = $this->input->post('daily_salary');
        $data['presence'] = $this->input->post('presence');
        $data['earned_salary'] = $this->input->post('earned_salary');
        $data['hourly_salary'] = $this->input->post('hourly_salary');
        $data['overtime_hours'] = $this->input->post('overtime');
        $data['overtime_amount'] = $this->input->post('overtime_amount');
        $data['hourly_salary_deduction'] = $this->input->post('hourly_salary_deduction');
        $data['deduction_hours'] = $this->input->post('deduction_hours');
        $data['deduction_amount'] = $this->input->post('deduction_amount');
        $data['bonus_amount'] = $this->input->post('bonus_amount');
        $data['net_payable'] = $this->input->post('net_payable');
        $data['notes'] = $this->input->post('notes');
        $data['receipt_no'] = 'BISE-MS' . time();


        // transaction type form calculation
        $data['tr_type'] = $this->input->post('payment_type');
        $data['check_type'] = $this->input->post('check_type');
        if ($data['check_type'] == 'acc_pay')
            $data['res_acc'] = $this->input->post('bank_acc');
        else
            $data['res_acc'] = $this->input->post('cash_acc');
        //$price = $this->input->post('cash_acc');
        $data['check_no'] = $this->input->post('check_no');
        $data['bank_name'] = $this->input->post('bank_name');
        if ($data['tr_type'] == 'cash')
            $data['check_date'] = '0000-00-00';
        else
            $data['check_date'] = $this->input->post('check_date');
//        $price = $this->input->post('cash_acc');
//        echo "<pre>";
//        print_r($data);
//        die();

        $this->db->select('*');
        $this->db->where('month', $data['month']);
        $this->db->where('year', $data['year']);
        $this->db->where('staff_id', $data['staff_id']);
        $this->db->from('salary');
        $query_result = $this->db->get();
        $salary = $query_result->num_rows();

        if ($salary == 0) {
            $this->db->insert('salary', $data);
            return $this->db->insert_id();
        } else {
            return 'exist';
        }
    }

    public function update_salary($id) {
        $data['date'] = $this->input->post('date');
        $data['time'] = $this->input->post('time');
        $data['month'] = $this->input->post('month');
        $data['year'] = $this->input->post('year');
        $data['basic_salary'] = $this->input->post('basic_salary');
        $data['daily_salary'] = $this->input->post('daily_salary');
        $data['presence'] = $this->input->post('presence');
        $data['earned_salary'] = $this->input->post('earned_salary');
        $data['hourly_salary'] = $this->input->post('hourly_salary');
        $data['overtime_hours'] = $this->input->post('overtime');
        $data['overtime_amount'] = $this->input->post('overtime_amount');
        $data['hourly_salary_deduction'] = $this->input->post('hourly_salary_deduction');
        $data['deduction_hours'] = $this->input->post('deduction_hours');
        $data['deduction_amount'] = $this->input->post('deduction_amount');
        $data['bonus_amount'] = $this->input->post('bonus_amount');
        $data['net_payable'] = $this->input->post('net_payable');
        $data['notes'] = $this->input->post('notes');

        $this->db->select('*');
        $this->db->where('month', $data['month']);
        $this->db->where('year', $data['year']);
        $this->db->where('id !=', $id);
        $this->db->from('salary');
        $query_result = $this->db->get();
        $salary = $query_result->num_rows();

        if ($salary == 0) {
            $this->db->where('id', $id);
            $this->db->update('salary', $data);
            return $this->db->affected_rows();
        } else {
            return 'exist';
        }
    }

    public function getAllSalaries() {
        $this->db->select('salary.id as salary_id, salary.staff_id, salary.month, salary.year, salary.basic_salary, salary.daily_salary, salary.presence, salary.earned_salary, salary.hourly_salary, salary.overtime_hours, salary.overtime_amount, salary.hourly_salary_deduction, salary.deduction_hours, salary.deduction_amount, salary.bonus_amount, salary.net_payable, salary.date, salary.time, salary.receipt_no, staff.id as staff_id, staff.card_no, staff.name, salary.tr_type, salary.res_acc');
        $this->db->from('salary');
        $this->db->join('staff', 'salary.staff_id = staff.id');
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function getSalary($id) {
        $this->db->select('salary.id as salary_id, salary.staff_id, salary.month, salary.year, salary.basic_salary, salary.daily_salary, salary.presence, salary.earned_salary, salary.hourly_salary, salary.overtime_hours, salary.overtime_amount, salary.hourly_salary_deduction, salary.deduction_hours, salary.deduction_amount, salary.bonus_amount, salary.net_payable, salary.date, salary.time, salary.receipt_no, salary.notes, staff.id as staff_id, staff.card_no, staff.name, staff.designation');
        $this->db->where('salary.id', $id);
        $this->db->from('salary');
        $this->db->join('staff', 'salary.staff_id = staff.id');
        $query_result = $this->db->get();
        return $query_result->row();
    }

    public function delete_salary($id) {
        $this->db->where('id', $id);
        $this->db->delete('salary');
        return $this->db->affected_rows();
    }

    public function get_staff_details($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('staff');
        $staff_result = $this->db->get();
        $data['staff'] = $staff_result->row();

        $this->db->select('*');
        $this->db->where('staff_id', $id);
        $this->db->from('salary');
        $salary_result = $this->db->get();
        $data['salary'] = $salary_result->result();

        return $data;
    }

}
