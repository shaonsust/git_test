<?php

class Production_Model extends CI_Model {

    public function get_productions() {
        $this->db->select('production.*, items.name as item_name, items.unit');
        $this->db->from('production');
        $this->db->join('items', 'items.id = production.item_id');
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function get_individual($id) {
        $this->db->select('production.*, items.name as item_name, items.unit');
        $this->db->where('production.id', $id);
        $this->db->from('production');
        $this->db->join('items', 'items.id = production.item_id');
        $query_result = $this->db->get();
        return $query_result->row();
    }

    public function get_finished_goods() {
        $this->db->select('*');
        $this->db->where('type', 'finished_good');
        $this->db->from('items');
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function save() {
        $data['receipt_no'] = "BISE-PF" . time();
        $data['item_id'] = $this->input->post('item_id');
        $data['qty'] = $this->input->post('qty');
        $data['date'] = $this->input->post('date');
        $data['time'] = $this->input->post('time');
        $data['notes'] = $this->input->post('notes');
        $this->db->insert('production', $data);
        return $this->db->insert_id();
    }

    public function get_item($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('items');
        $query_result = $this->db->get();
        return $query_result->row();
    }

    public function update($id) {
        $data['qty'] = $this->input->post('qty');
        $data['date'] = $this->input->post('date');
        $data['time'] = $this->input->post('time');
        $data['notes'] = $this->input->post('notes');
        $this->db->where('id', $id);
        $this->db->update('production', $data);
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('production');
        return $this->db->affected_rows();
    }

}
