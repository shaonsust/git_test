<?php

class Loan_Model extends CI_Model {

    public function get($type) {
        $this->db->select('*');
        $this->db->where('type', $type);
        $this->db->from('companies');
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function save($data) {
        $this->db->select('*');
        $this->db->where('name', $data['name']);
        $this->db->from('companies');
        $query_result = $this->db->get();
        $company = $query_result->num_rows();

        if ($company == 0) {
            $this->db->insert('companies', $data);
            return $this->db->insert_id();
        } else {
            return 'exist';
        }
    }

    public function get_individual($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('companies');
        $query_result = $this->db->get();
        return $query_result->row();
    }

    public function update($data, $id) {
        $this->db->select('*');
        $this->db->where('name', $data['name']);
        $this->db->where('id !=', $id);
        $this->db->from('companies');
        $query_result = $this->db->get();
        $company = $query_result->num_rows();

        if ($company == 0) {
            $this->db->where('id', $id);
            $this->db->update('companies', $data);
            return $this->db->affected_rows();
        } else {
            return 'exist';
        }
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('companies');
        return $this->db->affected_rows();
    }

    public function get_loans($type) {
        $this->db->select('accounting.id, accounting.amount, accounting.receipt_no, accounting.company_id, accounting.date, accounting.time, accounting.purpose, accounting.description, companies.name, accounting.tr_type, accounting.res_acc');
        $this->db->from('accounting');
        $this->db->where('accounting.type', $type);
        $this->db->join('companies', 'companies.id = accounting.company_id');
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function process($type) {
        $ldata['date'] = $this->input->post('date');
        $ldata['time'] = $this->input->post('time');
        $ldata['amount'] = $this->input->post('amount');
        $ldata['purpose'] = $this->input->post('purpose');
        $ldata['description'] = $this->input->post('description');
        $ldata['type'] = $type;

        $company_name = $this->input->post('company');
        $this->db->select('id');
        $this->db->where('name', $company_name);
        $this->db->from('companies');
        $company_result = $this->db->get();
        $num_company = $company_result->num_rows();
        if ($num_company == 1) {
            $company = $company_result->row();
            $ldata['company_id'] = $company->id;
        } else {
            $cdata['name'] = $company_name;
            $cdata['type'] = 'loan_source';
            $cdata['notes'] = 'This Loan Source was created while processing a loan payment';
            $cdata['status'] = 1;
            $cdata['created_at'] = $ldata['date'];
            $this->db->insert('companies', $cdata);
            $ldata['company_id'] = $this->db->insert_id();
        }
        if ($type == 'incoming_loan') {
            $ldata['receipt_no'] = 'BISE-IL' . time();
        } elseif ($type == 'outgoing_loan') {
            $ldata['receipt_no'] = 'BISE-OL' . time();
        } elseif ($type == 'outgoing_installment') {
            $ldata['receipt_no'] = 'BISE-OI' . time();
        } elseif ($type == 'incoming_installment') {
            $ldata['receipt_no'] = 'BISE-II' . time();
        }

        // transaction type form calculation
        $ldata['tr_type'] = $this->input->post('payment_type');
        $ldata['check_type'] = $this->input->post('check_type');
        if ($ldata['check_type'] == 'acc_pay')
            $ldata['res_acc'] = $this->input->post('bank_acc');
        else
            $ldata['res_acc'] = $this->input->post('cash_acc');
        //$price = $this->input->post('cash_acc');
        $ldata['check_no'] = $this->input->post('check_no');
        $ldata['bank_name'] = $this->input->post('bank_name');
        if ($ldata['tr_type'] == 'cash')
            $ldata['check_date'] = '0000-00-00';
        else
            $ldata['check_date'] = $this->input->post('check_date');
        //$price = $this->input->post('cash_acc');
//        echo "<pre>";
//        print_r($ldata);
//        die();


        $this->db->insert('accounting', $ldata);
        return $this->db->insert_id();
    }

    public function delete_loan($id) {
        $this->db->where('id', $id);
        $this->db->or_where('parent_id', $id);
        $this->db->delete('accounting');
        $delete = $this->db->affected_rows();
        return $delete;
    }

    public function get_loan($id) {
        $this->db->select('accounting.id, accounting.amount, accounting.date, accounting.time, accounting.purpose, accounting.description, accounting.company_id, accounting.receipt_no, companies.name');
        $this->db->where('accounting.id', $id);
        $this->db->from('accounting');
        $this->db->join('companies', 'companies.id = accounting.company_id');
        $query_result = $this->db->get();
        return $query_result->row();
    }

    public function update_loan($type, $id) {
        $ldata['date'] = $this->input->post('date');
        $ldata['time'] = $this->input->post('time');
        $ldata['amount'] = $this->input->post('amount');
        $ldata['purpose'] = $this->input->post('purpose');
        $ldata['description'] = $this->input->post('description');

        $company_name = $this->input->post('company');
        $this->db->select('id');
        $this->db->where('name', $company_name);
        $this->db->from('companies');
        $company_result = $this->db->get();
        $num_company = $company_result->num_rows();
        if ($num_company == 1) {
            $company = $company_result->row();
            $ldata['company_id'] = $company->id;
        } else {
            $cdata['name'] = $company_name;
            $cdata['type'] = 'loan_source';
            $cdata['notes'] = 'This Loan Source was created while processing a loan payment';
            $cdata['status'] = 1;
            $cdata['created_at'] = $ldata['date'];
            $this->db->insert('companies', $cdata);
            $ldata['company_id'] = $this->db->insert_id();
        }
        $this->db->where('id', $id);
        $this->db->update('accounting', $ldata);
        return $this->db->affected_rows();
    }

    public function get_loan_details($id) {
        $this->db->select('accounting.id, accounting.res_acc, accounting.tr_type, accounting.amount, accounting.date, accounting.time, accounting.purpose, accounting.description, accounting.company_id, accounting.receipt_no, companies.name, companies.contact_person, companies.address, companies.contact_phone, companies.name');
        $this->db->where('accounting.id', $id);
        $this->db->from('accounting');
        $this->db->join('companies', 'companies.id = accounting.company_id');
        $loan_result = $this->db->get();
        $data['loan'] = $loan_result->row();

        $this->db->select('*');
        $this->db->where('parent_id', $id);
        $this->db->from('accounting');
        $installment_result = $this->db->get();
        $data['installments'] = $installment_result->result();

        $this->db->select('SUM(amount) as payed_amount');
        $this->db->where('parent_id', $id);
        $this->db->from('accounting');
        $installment_result = $this->db->get();
        $data['payed_amount'] = $installment_result->row();

        return $data;
    }

    public function process_installment($type, $id) {
        $data['amount'] = $this->input->post('amount');
        $data['date'] = $this->input->post('date');
        $data['time'] = $this->input->post('time');
        $data['description'] = $this->input->post('description');
        $data['company_id'] = $this->input->post('company_id');
        $data['parent_id'] = $id;
        if ($type == 'incoming_loan') {
            $data['type'] = 'incoming_installment';
            $data['receipt_no'] = 'BISE-II' . time();
        } else {
            $data['type'] = 'outgoing_installment';
            $data['receipt_no'] = 'BISE-OI' . time();
        }

        // transaction type form calculation
        $data['tr_type'] = $this->input->post('payment_type');
        $data['check_type'] = $this->input->post('check_type');
        if ($data['check_type'] == 'acc_pay')
            $data['res_acc'] = $this->input->post('bank_acc');
        else
            $data['res_acc'] = $this->input->post('cash_acc');
        //$price = $this->input->post('cash_acc');
        $data['check_no'] = $this->input->post('check_no');
        $data['bank_name'] = $this->input->post('bank_name');
        if ($data['tr_type'] == 'cash')
            $data['check_date'] = '0000-00-00';
        else
            $data['check_date'] = $this->input->post('check_date');
        //$price = $this->input->post('cash_acc');
//        echo "<pre>";
//        print_r($data);
//        die();


        $this->db->insert('accounting', $data);
        return $this->db->insert_id();
    }

    public function get_installment($id) {
        $this->db->select('accounting.id, accounting.amount, accounting.date, accounting.time, accounting.description, accounting.company_id, accounting.receipt_no, companies.name, companies.contact_person, companies.address, companies.contact_phone, companies.name');
        $this->db->where('accounting.id', $id);
        $this->db->from('accounting');
        $this->db->join('companies', 'companies.id = accounting.company_id');
        $installment_result = $this->db->get();
        return $installment_result->row();
    }

    public function update_installment($id) {
        $ldata['date'] = $this->input->post('date');
        $ldata['time'] = $this->input->post('time');
        $ldata['amount'] = $this->input->post('amount');
        $ldata['purpose'] = $this->input->post('purpose');
        $ldata['description'] = $this->input->post('description');


        $this->db->where('id', $id);
        $this->db->update('accounting', $ldata);
        return $this->db->affected_rows();
    }

    public function delete_installment($id) {
        $this->db->where('id', $id);
        $this->db->delete('accounting');
        $delete = $this->db->affected_rows();
        return $delete;
    }

    public function get_source($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('companies');
        $company = $this->db->get();
        $data['company'] = $company->row();

        $this->db->select('*');
        $this->db->where('company_id', $id);
        $this->db->where('type', 'incoming_loan');
        $this->db->from('accounting');
        $incoming_loan = $this->db->get();
        $data['incoming_loan'] = $incoming_loan->result();

        $this->db->select('SUM(amount) as total_incoming_loan, count(id) as total_incoming_loans');
        $this->db->where('company_id', $id);
        $this->db->where('type', 'incoming_loan');
        $this->db->from('accounting');
        $total_incoming_loan = $this->db->get();
        $data['total_incoming_loan'] = $total_incoming_loan->row();

        $this->db->select('*');
        $this->db->where('company_id', $id);
        $this->db->where('type', 'outgoing_loan');
        $this->db->from('accounting');
        $outgoing_loan = $this->db->get();
        $data['outgoing_loan'] = $outgoing_loan->result();

        $this->db->select('SUM(amount) as total_outgoing_loan, count(id) as total_outgoing_loans');
        $this->db->where('company_id', $id);
        $this->db->where('type', 'outgoing_loan');
        $this->db->from('accounting');
        $total_outgoing_loan = $this->db->get();
        $data['total_outgoing_loan'] = $total_outgoing_loan->row();

        $this->db->select('*');
        $this->db->where('company_id', $id);
        $this->db->where('type', 'incoming_installment');
        $this->db->from('accounting');
        $incoming_installment = $this->db->get();
        $data['incoming_installment'] = $incoming_installment->result();

        $this->db->select('SUM(amount) as total_incoming_installment, count(id) as total_incoming_installments');
        $this->db->where('company_id', $id);
        $this->db->where('type', 'incoming_installment');
        $this->db->from('accounting');
        $total_incoming_installment = $this->db->get();
        $data['total_incoming_installment'] = $total_incoming_installment->row();

        $this->db->select('*');
        $this->db->where('company_id', $id);
        $this->db->where('type', 'outgoing_installment');
        $this->db->from('accounting');
        $outgoing_installment = $this->db->get();
        $data['outgoing_installment'] = $outgoing_installment->result();

        $this->db->select('SUM(amount) as total_outgoing_installment, count(id) as total_outgoing_installments');
        $this->db->where('company_id', $id);
        $this->db->where('type', 'outgoing_installment');
        $this->db->from('accounting');
        $total_outgoing_installment = $this->db->get();
        $data['total_outgoing_installment'] = $total_outgoing_installment->row();
        return $data;
    }

}
