<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 1/22/2015
 * Time: 2:11 PM
 */

class Welcome_Model extends CI_Model {

        public function update_user($user_id,$data)
        {
            $this->db->where('id',$user_id)
                    ->update('users',$data);
        }

    public function user_info($user_id)
    {
        $this->db->select('*')
            ->from('users')
            ->where('id',$user_id);


        $querq_result=$this->db->get();
        $result=$querq_result->row();
        return $result;
    }

    public function check_pass($user_id,$password)
    {

        $this->db->select('*')
            ->from('users')
            ->where('id',$user_id)
            ->where('password',$password);


        $querq_result=$this->db->get();
        $result=$querq_result->row();

        return $result;
    }

    public function overall_income_summary($start_date, $end_date)
    {
        $query = $this->db->query("Select temp.date ,SUM(Amount) Amount from
(

select
	acc.date , 'Accounting' as TableType , sum(acc.amount) Amount
	from	accounting	acc
	where acc.date between '$start_date' and '$end_date'
	AND type IN ('income', 'incoming_loan', 'incoming_installment', 'client_due')
	group by acc.date

	UNION
select
	ord.date , 'Order' as TableType , sum(ord.payment_amount) Amount
	from	`order` ord
	where ord.date between '$start_date' and '$end_date'
	AND type = 'sales'
	group by ord.date
) AS temp

Group by temp.date
order by temp.date ASC");

        return $query->result();
    }

    public function overall_expense_summary($start_date, $end_date)
    {
        $query = $this->db->query("Select temp.date ,SUM(Amount) Amount from
(

select
	acc.date , 'Accounting' as TableType , sum(acc.amount) Amount
	from	accounting	acc
	where acc.date between '$start_date' and '$end_date'
	AND type IN ('expense', 'outgoing_loan', 'outgoing_installment', 'supplier_due')
	group by acc.date

	UNION
select
	ord.date , 'Order' as TableType , sum(ord.payment_amount) Amount
	from	`order` ord
	where ord.date between '$start_date' and '$end_date'
	AND type = 'purchase'
	group by ord.date

	UNION

select
	sal.date , 'Salary' as TableType , sum(sal.net_payable) Amount
	from	salary sal
	where sal.date between '$start_date' and '$end_date'
	AND type = 'salary'
	group by sal.date
) AS temp

Group by temp.date
order by temp.date ASC");

        return $query->result();
    }

    public function get_individual_income($start_date, $end_date)
    {
        $query = $this->db->query("select
	'Accounting' as TableType , acc.type, sum(acc.amount) Amount
	from	accounting	acc
	where acc.date between '2015-01-01' and '2015-06-31'
    AND type IN ('income', 'incoming_loan', 'incoming_installment', 'client_due')
	group by acc.type

	UNION
select
	'Order' as TableType, ord.type, sum(ord.payment_amount) Amount
	from	`order` ord
	where ord.date between '2015-01-01' and '2015-06-31'
    AND type = 'sales'
	group by ord.type");

        return $query->result();
    }

    public function get_individual_expense($start_date, $end_date)
    {
        $query = $this->db->query("select
	'Accounting' as TableType , acc.type, sum(acc.amount) Amount
	from	accounting	acc
	where acc.date between '2015-01-01' and '2015-06-31'
    AND type IN ('expense', 'outgoing_loan', 'outgoing_installment', 'supplier_due')
	group by acc.type

	UNION
select
	'Order' as TableType, ord.type, sum(ord.payment_amount) Amount
	from	`order` ord
	where ord.date between '2015-01-01' and '2015-06-31'
    AND type = 'purchase'
	group by ord.type

	UNION
select
	'Salary' as TableType, sal.type, sum(sal.net_payable) Amount
	from	`salary` sal
	where sal.date between '2015-01-01' and '2015-06-31'
    AND type = 'salary'
	group by sal.type
	");
        return $query->result();
    }

} 