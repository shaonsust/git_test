<?php

class Usage_Model extends CI_Model {

    public function getAll($type) {
        $this->db->select('*');
        $this->db->from('usage');
        $this->db->where('type', $type);
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function get_items($type) {
        $query = $this->db->query("
        Select
            item.id, item.name, item.unit, item.price, item.type
            , IFNULL(item.stock, 0) as primary_stock,
            IFNULL(ordstemp.total_sold, 0) as total_sold
            , IFNULL(ordptemp.total_purchase, 0) as total_purchase
            , IFNULL(usgtemp.total_used, 0) as total_used
        from items item
        left join
            (
                Select ords.item_id
                , SUM(ords.qty) as total_sold
                from order_items ords
                where  ords.type = 0
                GROUP BY  ords.item_id
            ) ordstemp on ordstemp.item_id = item.id

        left join
            (
                Select ordp.item_id
                , SUM(ordp.qty) as total_purchase
                from order_items ordp
                where ordp.type = 1
                GROUP BY  ordp.item_id
            ) ordptemp on ordptemp.item_id = item.id

        left join
            (
                Select usg.item_id
                , SUM(usg.qty)  as total_used
                from usage_items usg
                group by usg.item_id
            ) usgtemp on usgtemp.item_id = item.id
          WHERE item.`type` = '$type'");
        return $query->result();
    }

    public function process($type) {
        $udata['receipt_no'] = 'BISE-U' . time();
        ;
        $udata['date'] = $this->input->post('date');
        $udata['time'] = $this->input->post('time');
        $udata['purpose'] = $this->input->post('purpose');
        $udata['description'] = $this->input->post('description');
        $udata['type'] = $type;
        $this->db->insert('usage', $udata);

        $idata['usage_id'] = $this->db->insert_id();

        $item_id = $this->input->post('item_id');
        $qty = $this->input->post('qty');
        $num_items = count($item_id);
        for ($i = 0; $i < $num_items; $i++) {
            $idata['item_id'] = $item_id[$i];
            $idata['qty'] = $qty[$i];
            $idata['date'] = $udata['date'];
            $this->db->insert('usage_items', $idata);
        }
        return $idata['usage_id'];
    }

    public function update($id, $type) {
        $udata['date'] = $this->input->post('date');
        $udata['time'] = $this->input->post('time');
        $udata['purpose'] = $this->input->post('purpose');
        $udata['description'] = $this->input->post('description');

        $this->db->where('id', $id);
        $this->db->update('usage', $udata);

        $this->db->where('usage_id', $id);
        $this->db->delete('usage_items');


        $idata['usage_id'] = $id;
        $idata['type'] = $type;
        $item_id = $this->input->post('item_id');
        $qty = $this->input->post('qty');
        $num_items = count($item_id);
        for ($i = 0; $i < $num_items; $i++) {
            $idata['item_id'] = $item_id[$i];
            $idata['qty'] = $qty[$i];
            $idata['date'] = $udata['date'];
            $this->db->insert('usage_items', $idata);
        }
        return $id;
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('usage');
        $delete = $this->db->affected_rows();
        $this->db->where('usage_id', $id);
        $this->db->delete('usage_items');
        return $delete;
    }

    public function getUsage($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('usage');
        $usage_result = $this->db->get();
        $data['usage'] = $usage_result->row();

        $this->db->select('usage_items.item_id, usage_items.qty, items.name, items.unit');
        $this->db->where('usage_items.usage_id', $id);
        $this->db->from('usage_items');
        $this->db->join('items', 'items.id = usage_items.item_id');
        $items_result = $this->db->get();
        $data['items'] = $items_result->result();
        return $data;
    }

}
