<?php

class Company_Model extends CI_Model {

    public function get($type) {
        $this->db->select('*');
        $this->db->where('type', $type);
        $this->db->from('companies');
        $query_result = $this->db->get();
        return $query_result->result();
    }

    public function save($data) {
        $this->db->select('*');
        $this->db->where('name', $data['name']);
        $this->db->from('companies');
        $query_result = $this->db->get();
        $company = $query_result->num_rows();

        if ($company == 0) {
            $this->db->insert('companies', $data);
            return $this->db->insert_id();
        } else {
            return 'exist';
        }
    }

    public function get_individual($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('companies');
        $query_result = $this->db->get();
        return $query_result->row();
    }

    public function update($data, $id) {
        $this->db->select('*');
        $this->db->where('name', $data['name']);
        $this->db->where('id !=', $id);
        $this->db->from('companies');
        $query_result = $this->db->get();
        $company = $query_result->num_rows();

        if ($company == 0) {
            $this->db->where('id', $id);
            $this->db->update('companies', $data);
            return $this->db->affected_rows();
        } else {
            return 'exist';
        }
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('companies');
        return $this->db->affected_rows();
    }

    public function search($keyword, $type) {
        $query = $this->db->query("SELECT * FROM companies WHERE type = '$type' AND (name LIKE '%$keyword%' OR contact_person LIKE '%$keyword%')");
        $query_result = $query->result();
        return $query_result;
    }

    public function get_details($id, $start_date, $end_date) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('companies');
        $query_result = $this->db->get();
        $data['company'] = $query_result->row();

        $this->db->select('MAX(total_price) as max_total_price, MAX(discount) as max_discount, MAX(vat) as max_vat, MAX(payment_amount) as max_payment_amount, MAX(due_amount) as max_due_amount, MIN(total_price) as min_total_price, MIN(discount) as min_discount, MIN(vat) as min_vat,MIN(payment_amount) as min_payment_amount, MIN(due_amount) as min_due_amount, AVG(total_price) as avg_total_price, AVG(discount) as avg_discount, AVG(vat) as avg_vat, AVG(payment_amount) as avg_payment_amount, AVG(due_amount) as avg_due_amount, SUM(total_price) as total_price, SUM(discount) as discount, SUM(vat) as vat, SUM(payment_amount) as total_payment_amount, SUM(due_amount) as total_due_amount, COUNT(company_id) as total_orders');
        $this->db->where('company_id', $id);
        $this->db->where('date >=', $start_date);
        $this->db->where('date <=', $end_date);
        $this->db->from('order');
        $order_stats = $this->db->get();
        $data['order_stats'] = $order_stats->row();

        $this->db->select('SUM(amount) as total_due_payment, COUNT(company_id) as total_payments');
        $this->db->where('company_id', $id);
        $this->db->where('date >=', $start_date);
        $this->db->where('date <=', $end_date);
        $this->db->from('accounting');
        $accounting_stats = $this->db->get();
        $data['accounting_stats'] = $accounting_stats->row();

        $this->db->select('*');
        $this->db->where('date >=', $start_date);
        $this->db->where('date <=', $end_date);
        $this->db->where('company_id', $id);
        $this->db->from('order');
        $order_history = $this->db->get();
        $data['order_history'] = $order_history->result();

        $this->db->select('*');
        $this->db->where('date >=', $start_date);
        $this->db->where('date <=', $end_date);
        $this->db->where('company_id', $id);
        $this->db->from('accounting');
        $accounting_history = $this->db->get();
        $data['accounting_history'] = $accounting_history->result();

        return $data;
    }

}
