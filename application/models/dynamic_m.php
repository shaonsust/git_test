<?php
include(APPPATH.'libraries/dynamic.php');
class Dynamic_M extends Dynamic {


	public function save($table,$data)
    {
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }
    
    public function update($column,$condition,$table,$data)
    {
        $this->db->where($column,$condition);
        $this->db->update($table,$data);
    }
    // here u can update any data using only one condition.
    public function delete($column,$condition,$table)
    {
        $this->db->where($column,$condition);
        $this->db->delete($table);
    }
    // for delete any data from database
    public function get_all_data($tabel)
    {
        $this->db->select('*');
        $this->db->from($tabel);
        $query_result=  $this->db->get();
        return $query_result->result();
    }
    // this function return an array
    public function get_data_desc($tabel,$colmun)
    {
        $this->db->select('*');
        $this->db->from($tabel);
        $this->db->order_by($colmun,'DESC');
        $query_result=  $this->db->get();
        return $query_result->result();
    }
    // this function return u descending order array. u have to define whice colmun u want to orderd.
    public function get_data_by_condition($colmun,$conditon,$tabel)
    {
        $this->db->select('*');
        $this->db->where($colmun,$conditon);
        $this->db->from($tabel);
        $query_result=  $this->db->get();
        return $query_result->row();
    }
	// this function returns specific data using only one condition.
	public function get_specific_data_by_condition_array($field,$colmun,$conditon,$tabel)
	{
		$this->db->select($field);
		$this->db->where($colmun,$conditon);
		$this->db->from($tabel);
		$query_result=  $this->db->get();
		return $query_result->result();
	}
    // this function return u only one conditional row.
    public function get_data_by_condition_array($colmun,$conditon,$tabel)
    {
        $this->db->select('*');
        $this->db->where($colmun,$conditon);
        $this->db->from($tabel);
        $query_result=  $this->db->get();
        return $query_result->result();
    }
    // if condition is true then this function always return u an array.
    public function get_data_by_condition_array_desc($colmun,$conditon,$tabel,$orderd_colmun)
    {
        $this->db->select('*');
        $this->db->where($colmun,$conditon);
        $this->db->from($tabel);
        $this->db->order_by($orderd_colmun,'DESC');
        $query_result=  $this->db->get();
        return $query_result->result();
    }
    // same as previous. this function return descending orderd value.
    public function get_data_by_double_condition_array($colmun1,$conditon1,$colmun2,$conditon2,$tabel)
    {
        $this->db->select('*');
        $this->db->where($colmun1,$conditon1);
        $this->db->where($colmun2,$conditon2);
        $this->db->from($tabel);
        $query_result=  $this->db->get();
        return $query_result->result();
    }
    // here u can apply double condition 
    public function get_data_by_double_condition_array_desc($colmun1,$conditon1,$colmun2,$conditon2,$tabel)
    {
        $this->db->select('*');
        $this->db->where($colmun1,$conditon1);
        $this->db->where($colmun2,$conditon2);
        $this->db->from($tabel);
        $query_result=  $this->db->get();
        return $query_result->result();
    }
    // here u can apply double condition 
    public function get_data_by_double_condition_array_or($colmun1,$conditon1,$colmun2,$conditon2,$tabel,$orderd_colmun)
    {
        $this->db->select('*');
        $this->db->where($colmun1,$conditon1);
        $this->db->or_where($colmun2,$conditon2);
        $this->db->from($tabel);
        $this->db->order_by($orderd_colmun,'DESC');
        $query_result=  $this->db->get();
        return $query_result->result();
    }
    // here u can apply optional condition. any true will return value.
    public function get_data_by_double_condition_array_or_desc($colmun1,$conditon1,$colmun2,$conditon2,$tabel,$orderd_colmun)
    {
        $this->db->select('*');
        $this->db->where($colmun1,$conditon1);
        $this->db->or_where($colmun2,$conditon2);
        $this->db->from($tabel);
        $this->db->order_by($orderd_colmun,'DESC');
        $query_result=  $this->db->get();
        return $query_result->result();
    }
    // same as previous. this function return descending order value.
    public function get_data_by_condition_result_last_limit($colmun,$conditon,$tabel,$orderd_colmun,$as_or_des,$limit)
    {
        $this->db->select('*');
        $this->db->where($colmun,$conditon);
        $this->db->from($tabel);
        $this->db->order_by($orderd_colmun,$as_or_des);
        $this->db->limit($limit);
        $query_result=  $this->db->get();
        return $query_result->result();
    }
    // this function return u limited data as u define asscending or descending orderd.
    
    
    
    
    public function count_rows($table)
    {
        $count = $this->db->count_all($table);
        return $count;
    }
    // for counting row in a table.
    
    public function count_rows_by_condition($clomun1,$condition1,$table)
    {
        $this->db->where($clomun1,$condition1);
        $result=$this->db->count_all_results($table);
        return $result;
    }
    public function count_rows_by_double_condition($clomun1,$condition1,$clomun2,$condition2,$table)
    {
        $this->db->select('*');
        $this->db->where($clomun1,$condition1);
        $this->db->where($clomun2,$condition2);
        $result=$this->db->count_all_results($table);
        return $result;
    }
    public function count_rows_by_double_condition_notin($clomun1,$condition1,$clomun2,$condition2,$table)
    {
        $this->db->select('*');
        $this->db->where($clomun1,$condition1);
        $this->db->where_not_in($clomun2,$condition2);
        $result=$this->db->count_all_results($table);
        return $result;
    }
    public function count_rows_by_double_condition_or($clomun1,$condition1,$clomun2,$condition2,$table)
    {
        $this->db->select('*');
        $this->db->where($clomun1,$condition1);
        $this->db->or_where($clomun2,$condition2);
        $result=$this->db->count_all_results($table);
        return $result;
    }
    public function get_data_for_pagination($table,$num,$offset)
    {
        $query_result=$this->db->get($table,$num,$offset);
        $result=$query_result->result();
        return $result;
    }
    // for pagination
    public function get_data_for_pagination_desc($orderd_colmun,$table,$num,$offset)
    {
        $this->db->select('*');
        $this->db->order_by($orderd_colmun,'DESC');
        $query_result=$this->db->get($table,$num,$offset);
        $result=$query_result->result();
        return $result;
    }
    // return value for pagination orderd by descending
    public function get_data_for_pagination_condition($column,$condition1,$table,$num,$offset)
    {
        $this->db->select('*');
        $this->db->where($column,$condition1);
        $query_result=$this->db->get($table,$num,$offset);
        $result=$query_result->result();
        return $result;
    }
    // conditional value for pagination
    public function get_data_for_pagination_condition_desc($colmun1,$condition1,$orderd_colmun,$table,$num,$offset)
    {
        $this->db->select('*');
        $this->db->where($colmun1,$condition1);
        $this->db->order_by($orderd_colmun,'DESC');
        $query_result=$this->db->get($table,$num,$offset);
        $result=$query_result->result();
        //print_r($result);exit;
        return $result;
    }
    // descending orderd conditional data return this function
    public function get_data_for_pagination_double_condition($clomun1,$condition1,$clomun2,$condition2,$table,$num,$offset)
    {
        $this->db->select('*');
        $this->db->where($clomun1,$condition1);
        $this->db->where($clomun2,$condition2);
        $query_result=$this->db->get($table,$num,$offset);
        $result=$query_result->result();
        return $result;
    }
    // double condtional value for pagination
    public function get_data_for_pagination_double_condition_desc($colmun1,$condition1,$colmun2,$condition2,$orderd_colmun,$table,$num,$offset)
    {
        $this->db->select('*');
        $this->db->where($colmun1,$condition1);
        $this->db->where($colmun2,$condition2);
        $this->db->order_by($orderd_colmun,'DESC');
        $query_result=$this->db->get($table,$num,$offset);
        $result=$query_result->result();
        //print_r($result);exit;
        return $result;
    }
    // double condtional value for pagination
    public function get_data_for_pagination_double_condition_notin_desc($colmun1,$condition1,$colmun2,$condition2,$orderd_colmun,$table,$num,$offset)
    {
        $this->db->select('*');
        $this->db->where($colmun1,$condition1);
        $this->db->where_not_in($colmun2,$condition2);
        $this->db->order_by($orderd_colmun,'DESC');
        $query_result=$this->db->get($table,$num,$offset);
        $result=$query_result->result();
        /*echo "<pre>";
        print_r($result);exit;*/
        return $result;
    }
    // descending orderd double conditional data return this function
    public function get_data_for_pagination_double_condition_or($clomun1,$condition1,$clomun2,$condition2,$table,$num,$offset)
    {
        $this->db->select('*');
        $this->db->where($clomun1,$condition1);
        $this->db->or_where($clomun2,$condition2);
        $query_result=$this->db->get($table,$num,$offset);
        $result=$query_result->result();
        return $result;
    }
    // this function, only one true return value within double condtion
    public function get_data_for_pagination_double_condition_or_desc($colmun1,$condition1,$colmun2,$condition2,$orderd_colmun,$table,$num,$offset)
    {
        $this->db->select('*');
        $this->db->where($colmun1,$condition1);
        $this->db->or_where($colmun2,$condition2);
        $this->db->order_by($orderd_colmun,'DESC');
        $query_result=$this->db->get($table,$num,$offset);        
        $result=$query_result->result();
        return $result;
    }
    // same as previous. descending orderd value return by this function

    public function get_data_for_pagination_like($column, $condition, $table, $num, $offset)
    {
        $this->db->select('*');
        $this->db->like($column, $condition, 'both');
        $query_result=$this->db->get($table,$num,$offset);
        $result=$query_result->result();
        return $result;
    }
    public function get_data_where_not_in($clomun1,$condition1,$clomun2,$condition2,$order_column,$table,$num,$offset)
    {
        $this->db->select('*');
        $this->db->where($clomun1,$condition1);
        $this->db->where_not_in($clomun2,$condition2);
        $this->db->order_by($order_column,'DESC');
        $query_result=$this->db->get($table,$num,$offset);
        $result=$query_result->result();
        return $result;
    }
    public function get_subscriber_query()
    {
        $this->db->select('email');
        $this->db->where('role',4);
        $this->db->order_by('id','DESC');
        return $this->db->get('user');
    }
}

?>
