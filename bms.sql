-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 24, 2016 at 07:51 PM
-- Server version: 5.6.30-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bms`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) NOT NULL,
  `acc_type` varchar(200) NOT NULL,
  `acc_name` varchar(200) NOT NULL,
  `acc_no` varchar(200) NOT NULL,
  `bank_name` varchar(200) NOT NULL,
  `curr_balance` varchar(200) NOT NULL,
  `status` int(10) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `account`, `acc_type`, `acc_name`, `acc_no`, `bank_name`, `curr_balance`, `status`, `description`, `date`, `time`) VALUES
(2, 'cash', 'account', 'office', '', '', '1234500', 2, 'fjkdsfdkjfdsk                                                                                                                                                                        ', '2016-04-24', '03:00:02'),
(3, 'bank', 'account', 'fdfdsfds', '233243534432', 'Brac', '44654654', 1, 'hghjghtyhg', '2016-04-24', '09:55:24'),
(4, 'cash', 'account', 'dfdsfds', '', '', '32312', 1, 'dfdsfdsfds aaaaaaaaaaaaaaaaaa                                                                                  ', '2016-04-24', '02:56:54'),
(6, 'cash', 'account', 'dfdsfsdfds', '', '', '3321312', 1, 'dfddsfds', '2016-04-24', '10:40:32'),
(10, 'bank', 'account', 'habijabi', '654321', 'Bangladesh Bank', '876543', 1, 'dfsfsddfss', '2016-04-24', '01:42:24');

-- --------------------------------------------------------

--
-- Table structure for table `accounting`
--

CREATE TABLE IF NOT EXISTS `accounting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `date` date NOT NULL,
  `time` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `purpose` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `receipt_no` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `accounting`
--

INSERT INTO `accounting` (`id`, `amount`, `date`, `time`, `purpose`, `description`, `company_id`, `type`, `receipt_no`, `parent_id`) VALUES
(1, 63000, '2015-09-17', '09:12:15', 'To buy computer', '', 0, 'expense', 'FFML-GE1436685222', 0),
(2, 10000, '2015-07-11', '09:15:00', 'Supplier Due Payment', '', 6, 'supplier_due', 'FFML-SD1436685372', 0),
(3, 4027, '2015-07-12', '18:26:55', 'Office Purpose', '', 112, 'incoming_loan', 'FFML-IL1436718484', 0),
(4, 1600000, '2015-07-12', '18:28:24', 'Office Purpose', '', 113, 'incoming_loan', 'FFML-IL1436718516', 0),
(5, 1000000, '2015-07-12', '18:28:56', 'Office Purpose', '', 114, 'incoming_loan', 'FFML-IL1436718552', 0),
(7, 400643, '2015-07-12', '18:29:53', 'Office Purpose', '', 116, 'incoming_loan', 'FFML-IL1436718602', 0),
(12, 10000, '2015-07-12', '18:45:26', 'Office Purpose', '', 112, 'incoming_loan', 'FFML-IL1436719539', 0),
(13, 30000, '2015-12-14', '13:10:39', '', '', 112, 'incoming_installment', 'FFML-II1450077054', 3),
(14, 50000, '2015-12-14', '13:14:13', 'Supplier Due Payment', '', 2, 'supplier_due', 'FFML-SD1450077262', 0),
(15, 5000, '2016-03-11', '16:10:42', 'Net Bill', '', 0, 'expense', 'FFML-GE1457691054', 0),
(16, 3000, '2016-03-11', '16:11:03', 'Wastage Sales', '', 0, 'income', 'FFML-GI1457691074', 0),
(17, 0, '0000-00-00', '0', '', '', 0, 'account', '', 0),
(18, 0, '0000-00-00', '0', '', 'fkdkfsdlksd', 0, 'account', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('6cbc5d13ef318d23e419d458947ec3ae', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36', 1461497830, 'a:5:{s:9:"user_data";s:0:"";s:9:"user_name";s:5:"admin";s:11:"menu_active";s:9:"dashboard";s:7:"user_id";s:1:"1";s:9:"user_role";s:1:"1";}');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `secondary_phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `incoming_amount` float NOT NULL,
  `outgoing_amount` float NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL COMMENT '1 = new 0 = old',
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=118 ;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `contact_person`, `contact_phone`, `address`, `secondary_phone`, `email`, `website`, `incoming_amount`, `outgoing_amount`, `type`, `notes`, `status`, `created_at`) VALUES
(1, 'MS Dyuti Enterprise', 'Choto Hujur', '01711352950', 'Faltita Bazar, Fakirhat, Bagerhat', '', '', '', 4606350, 0, 'client', '', 0, '2015-07-11'),
(2, 'MS Al Morshed Enterprise', 'Md. Shahbuddin Ahmed', '01712777930', 'Savar, Dhaka', '', '', '', 0, 978672, 'supplier', '', 0, '2015-07-11'),
(3, 'Ms Islam Enterpise', 'Afridul islam', '01711827001', 'Khulna', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(4, 'Ms Unisan Markiting Ince', 'Md Mossaraf hossain', '01727061001', 'Dhaka', '01761752545', '', '', 0, 1460530, 'supplier', '', 0, '2015-07-12'),
(5, 'Ms Sumi Enterprise', 'Md Shaidul Islam', '01922680996', 'Kustia', '01922690254', '', '', 0, 666561, 'supplier', '', 0, '2015-07-12'),
(6, 'Ms sonaullah Enterprise', 'Md kamrul huq', '01716004966', 'Kustia', '', '', '', 0, 747886, 'supplier', '', 0, '2015-07-12'),
(7, 'Ms Azad Brothers', 'Abul kalam azad', '01718327709', 'Dinajpur', '', '', '', 0, 174330, 'supplier', '', 0, '2015-07-12'),
(8, 'Shekh enayet Ali', 'Shekh enayet Ali', '01717667937', 'Chaksri Bazar', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(9, 'Ms Ray Vandar', 'Babu shapon ray', '01718733307', 'Malsha Bazar', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(10, 'MS Dyuti Enterprise (s)', 'Choto Hujur', '01711352950', 'Faltita Bazar', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(11, 'Ms Khalek Enterprise', 'Md Roni Hawlader', '07682247558', 'Labanchora Khulna', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(12, 'Ms Rafiq Store', 'Md Rafiqul Islam Khan', '01712658432', 'Khulna', '', '', '', 0, 716480, 'supplier', '', 0, '2015-07-12'),
(13, 'Ms Rafi Enterprise', 'Sayed belal Hossain ', '01718550541', 'Khulna', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(14, 'Ms sumon Enterprise', 'Md Sumon Sheak', '01710788272', 'Sheali Rupsha', '', '', '', 2887250, 0, 'client', '', 0, '2015-07-12'),
(15, 'Ms ray vandar(c)', 'Babu shopon ray', '01718733307', 'Mansha', '', '', '', 2498350, 0, 'client', '', 0, '2015-07-12'),
(16, 'Ms sheikh Braders', 'Md Ferdaus Sheikh', '01715604885', 'Chitolmari', '', '', '', 1373260, 0, 'client', '', 0, '2015-07-12'),
(17, 'Ms Fakirhat Enterprise', 'Haji Addul gafur', '01717666904', 'Fakirhat', '', '', '', 1711980, 0, 'client', '', 0, '2015-07-12'),
(19, 'MS Anower traders', 'Al Haj Anower hossain', '', 'Kalibari bazar', '', '', '', 0, 0, 'supplier', 'This Supplier was created from the purchase page while processing a new purchase order', 1, '2015-07-11'),
(21, 'Ms Firoza Enterprise', 'Md shariful Islam', '', 'Jatrapu Bazar', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(22, 'Md Fuad Hasan', 'Fuad Hasan', '01711398726', 'Fakirhat', '', '', '', 0, 37, 'supplier', '', 0, '2015-07-12'),
(23, 'Ms Gour Enterprise', 'Shadhon Pal', '01720330524', 'Malsha bazar', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(24, 'Md Shah Alom Khan', 'Shah Alom Khan', '01718209056', 'Rainda Bagerhat', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(25, 'Ms Ara Filling Station', 'Md Anisr Rahman', '01852274541', 'Pagla Fakirhat', '', '', '', 0, 183600, 'supplier', '', 0, '2015-07-12'),
(26, 'ACI Limited', '', '01755658735', 'Dhaka', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(27, 'Rals Agro Ltd.', '', '01823020666', 'Dhaka', '', '', '', 0, 66000, 'supplier', '', 0, '2015-07-12'),
(28, 'Fishtek Bnagladesh Ltd', '', '01722221257', 'Dhaka', '', '', '', 0, 358500, 'supplier', '', 0, '2015-07-12'),
(29, 'Max Pro', 'Pronob Babu', '01711789789', 'Dhaka', '', '', '', 0, 544000, 'supplier', '', 0, '2015-07-12'),
(30, 'Md Abdullah Sheikh', 'Abdullah Sheikh', '01730967840', 'Mougop Fakirhat', '', '', '', 0, 520, 'supplier', '', 0, '2015-07-12'),
(31, 'Md Liton Sheikh', 'Liton Sheikh', '01740577674', 'Fakirhat', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(32, 'Ms Nurul Traders', 'Sheikh Nurul Islam', '017711571635', 'Fakirhat', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(33, 'Mono Bag Mills Ltd', '', '01926872877', 'Gazipur Dhaka', '', '', '', 0, 68368, 'supplier', '', 0, '2015-07-12'),
(34, 'Sino Bag Mills Ltd', 'Md Sohrab Hossain', '0177698344', 'Munshigonj Dhaka', '', '', '', 0, 161110, 'supplier', '', 0, '2015-07-12'),
(35, 'Sotota Engineering Workshop', 'Md Alamgir Hossain', '01848056447', 'Koia Bazar Khulna', '', '', '', 0, 128064, 'supplier', '', 0, '2015-07-12'),
(36, 'Ntional Iron Industries', 'Md Abul Bashar', '01718015093', 'Farazipara Road Khulna', '', '', '', 0, 20422, 'supplier', '', 0, '2015-07-12'),
(37, 'Voice Int', 'Md ziaur Rahman Sobuj', '01712018708', 'Dhaka', '', '', '', 0, 50000, 'supplier', '', 0, '2015-07-12'),
(38, 'Bd Graph Tech', 'Md Tanvr Ahmed', '01716050872', 'Dhaka', '', '', '', 0, 280000, 'supplier', '', 0, '2015-07-12'),
(39, 'Brothers Int', 'Zm Shahjahan Hawladar', '01713363315', 'Dhaka', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(40, 'H mill Machinaries', '', '01720512350', 'Nawabpur Road Dhaka', '', '', '', 0, 11869, 'supplier', '', 0, '2015-07-12'),
(41, 'Gramin Balish Ghar', '', '01720330524', 'Kushtia', '', '', '', 0, 11678, 'supplier', '', 0, '2015-07-12'),
(42, 'Chunnu mia', 'Chunnu Mia', '01739320037', 'Kathaltola Fakirhat', '', '', '', 0, 1500, 'supplier', '', 0, '2015-07-12'),
(43, 'Md Mokhlesur Rahman', 'Mokhlesur Rahman', '01715999136', 'Jatrapur Bazar', '', '', '', 0, 273500, 'supplier', '', 0, '2015-07-12'),
(44, 'Md Abdul Mazid', 'Md Abdul Mazid', '01764248039', 'Shambagar', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(45, 'Sayad Dinar Hossain', 'Sayad Dinar Hossain', '01749600600', 'Chaksri Bazar', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(46, 'Ms Sumon Enetprise(s)', 'Md Sumon Sheikh', '01710788272', 'Shieli Bazar', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(47, 'Md Amir Ali', 'Md Amir Ali', '01740582666', 'Khalsi Bagerhat', '', '', '', 0, 0, 'supplier', '', 0, '2015-07-12'),
(48, 'Md Dulal Hawlader', 'Md Dulal Hawlader', '01713956024', 'Pathorghata Borgona', '', '', '', 0, 239886, 'supplier', '', 0, '2015-07-12'),
(49, 'Ms Rpsha Enterprise', 'Md Yunus Talukdar', '01811795187', 'Labonchora Bandabazar', '', '', '', 0, 243275, 'supplier', '', 0, '2015-07-12'),
(50, 'Ms Bismillah Traders', 'Sheikh Rezwan Hossain', '01712549369', 'Khornia Bazar Dumuria', '', '', '', 1538740, 0, 'client', '', 0, '2015-07-12'),
(51, 'Ms Hakim Store', 'Md Abdul Hakim', '01713144635', 'Mansha Bazar, Fakirhat', '', '', '', 1095540, 0, 'client', '', 0, '2015-07-12'),
(52, 'Ms Gour Enterprise(c)', 'Shadhon Pal', '01740551011', 'Mansha Bazar', '', '', '', 1635970, 0, 'client', '', 0, '2015-07-12'),
(53, 'Ms Unique Traders', 'Md Farhad Hossain', '01917670591', 'Fakirhat Bazar', '', '', '', 823779, 0, 'client', '', 0, '2015-07-12'),
(54, 'Ms Sonaton Enterprise', 'Sonaton Dam', '01798237681', 'Alaipur Bazar', '', '', '', 973335, 0, 'client', '', 0, '2015-07-12'),
(55, 'Ms Munna Fish', 'Md Maruf Sheikh', '01747530756', 'Jessore more, Pabla, Doulotpur, Khulna', '', '', '', 607355, 0, 'client', '', 0, '2015-07-12'),
(56, 'Md Sariful Shikdar', 'Sariful Shikdar', '01731344690', 'Kochatola Rupsha', '', '', '', 727520, 0, 'client', '', 0, '2015-07-12'),
(57, 'Ms Shahid Traders', 'Sheikh Shahidul Islam', '01713144633', 'C&B Bazar', '', '', '', 1304340, 0, 'client', '', 0, '2015-07-12'),
(58, 'Ms Friends Enterprise', 'Babu Narayon', '01713921252', 'C&B Bazar', '', '', '', 180600, 0, 'client', '', 0, '2015-07-12'),
(59, 'Ms Bhai Bhai Banijjo Vandar', 'Babu Mukul Kabiraj', '01716099367', 'Koia Bazar Khulna', '', '', '', 422193, 0, 'client', '', 0, '2015-07-12'),
(60, 'Ms Pranto Traders', 'Ashok Kumar Shaha', '01934504768', 'Fakirhat Bazar', '', '', '', 724149, 0, 'client', '', 0, '2015-07-12'),
(61, 'Ms Masud Store', 'Md Bacchu Sheikh', '01740955663', 'Tomaltola', '', '', '', 654605, 0, 'client', '', 0, '2015-07-12'),
(62, 'Vai Vai Poltry & Fish Food Traders', 'Md Kamrul Islam', '01714586775', 'Dorga Bagerhat', '', '', '', 972709, 0, 'client', '', 0, '2015-07-12'),
(63, 'Ms Gazi Enterprise', 'Md Helal Gazi', '01716406691', 'Aronghata Bypass Doulotpur', '', '', '', 117118, 0, 'client', '', 0, '2015-07-12'),
(64, 'Ms Dyuti (2)', 'Md Obaydul Haque', '01711643756', 'Shantiganj More Fakirhat', '', '', '', 1036450, 0, 'client', '', 0, '2015-07-12'),
(65, 'Ms Mukit Tradesr', 'Md Saifuzzaman', '01714695306', 'Fakirhat Bazar', '', '', '', 212250, 0, 'client', '', 0, '2015-07-12'),
(66, 'Ms RM Polltry', 'Md Tarikul Islam', '01761743985', 'Chulkathi Bazar', '', '', '', 350350, 0, 'client', '', 0, '2015-07-12'),
(67, 'Md Rafiqul Islam', 'Rafiqul Islam', '01716207869', 'Kandhapara bagerhat', '', '', '', 101000, 0, 'client', '', 0, '2015-07-12'),
(68, 'Ms Joukhali Enterprise', 'Md Muslimuddin', '01711786581', 'SriGhat Bagerhat', '', '', '', 344725, 0, 'client', '', 0, '2015-07-12'),
(69, 'Al-Hajj Mizanur Rahman', 'Al-Hajj Mizanur Rahman', '01718135079', 'Sonakhali Gate', '', '', '', 899350, 0, 'client', '', 0, '2015-07-12'),
(70, 'Md Nur Islam', 'Md Nur Islam', '01718135069', 'Dharmongol Fakirhat', '', '', '', 290273, 0, 'client', '', 0, '2015-07-12'),
(71, 'Md Monirul Islam', 'Md Monirul Islam', '01714633494', 'Sotal Bagerhat', '', '', '', 710638, 0, 'client', '', 0, '2015-07-12'),
(72, 'Md Monir Hossain Mukto', 'Md Monir Hossain Mukto', '01711482997', 'Fakirhat Bazar', '', '', '', 63300, 0, 'client', '', 0, '2015-07-12'),
(73, 'Md Abul Hossain', 'Md Abul Hossain', '01710064201', 'Moroldanga Fakirhat', '', '', '', 192575, 0, 'client', '', 0, '2015-07-12'),
(74, 'Md Khan Ismail Hossain', 'Md Khan Ismail Hossain', '01711180987', 'MolGhar Fakirhat', '', '', '', 60450, 0, 'client', '', 0, '2015-07-12'),
(75, 'Ms Molla Bhandar', 'Md Rubel Molla', '01912217900', 'Chachuria Bazar Kalia', '', '', '', 126000, 0, 'client', '', 0, '2015-07-12'),
(76, 'Ms Krishibiz Bhandar', 'Md daud Ali Sheikh', '01712699380', 'Notun Bazar Gopalgonj', '', '', '', 179527, 0, 'client', '', 0, '2015-07-12'),
(77, 'Md Rafiqul Islam(c)', '', '01719263367', 'Depara Bagerhat', '', '', '', 171330, 0, 'client', '', 0, '2015-07-12'),
(78, 'Master Abdul Khalek', '', '01718181656', 'Chaksri Bazar Bagerhat', '', '', '', 110800, 0, 'client', '', 0, '2015-07-12'),
(79, 'Ms Maliha Traders', 'Md Nazrul islam Sardar', '01716405685', 'Jatrapur Bazar', '', '', '', 277805, 0, 'client', '', 0, '2015-07-12'),
(80, 'Ms Mamoni traders', 'Md Mfizul Islam', '01716869315', 'Klaroa Satkhira', '', '', '', 420408, 0, 'client', '', 0, '2015-07-12'),
(81, 'Ms Musa Traders', 'Md Musa Loshkor', '01918055122', 'Alaikul Bazar rupsha', '', '', '', 895875, 0, 'client', '', 0, '2015-07-12'),
(82, 'Md Nazrul Islam', '', '01724132510', 'Badokhali Bagerhat', '', '', '', 30650, 0, 'client', '', 0, '2015-07-12'),
(83, 'Sheikh Mijanur Rahman', '', '01711280088', 'Upojela vice chairman', '', '', '', 78655, 0, 'client', '', 0, '2015-07-12'),
(84, 'Ms Bisshash Traders', 'Samsul Haque Bisshash', '01713744576', 'Chachuria Bazar', '', '', '', 103750, 0, 'client', '', 0, '2015-07-12'),
(85, 'Md Kabir Hossain', '', '01733259148', 'Barakpur Bagerhat', '', '', '', 56319, 0, 'client', '', 0, '2015-07-12'),
(86, 'Ms Ali Fishfeed', 'Kazi Ashraf Ali', '01711373105', 'Kolaroa Satkhira', '', '', '', 92634, 0, 'client', '', 0, '2015-07-12'),
(87, 'Ms Tara Entrprise', 'Md Tushar', '01711964695', 'Fultola Bazar', '', '', '', 176827, 0, 'client', '', 0, '2015-07-12'),
(88, 'Ms Joint Enterprise', 'Hiro Sardar', '01713144639', 'Jatrapur Bazar', '', '', '', 61875, 0, 'client', '', 0, '2015-07-12'),
(89, 'Hazi Abdussalam Sardar', '', '01724451566', 'Hospatia Rampal', '', '', '', 351050, 0, 'client', '', 0, '2015-07-12'),
(90, 'Md Faruk Hossain', '', '01712008760', 'Saralkhola Bagerhat', '', '', '', 119902, 0, 'client', '', 0, '2015-07-12'),
(91, 'Ms Al Mamun Bhandar', 'Nur Mohammad', '01711328278', 'Jatrapur Bazar', '', '', '', 71400, 0, 'client', '', 0, '2015-07-12'),
(92, 'Ms Five Star Fish Feed', 'Dipongkor Dash', '01713906610', 'Rogunathpur Bagerhat', '', '', '', 262576, 0, 'client', '', 0, '2015-07-12'),
(93, 'Sri prokash Ray', '', '01863425435', 'Fakirhat Bazar', '', '', '', 26850, 0, 'client', '', 0, '2015-07-12'),
(94, 'Ms Jhilik Enterprise', 'Md Masum Miah', '01823698540', 'Fakirhat Bazar', '', '', '', 464586, 0, 'client', '', 0, '2015-07-12'),
(95, 'Haji Abu Daut Hossain', '', '01715350995', 'Rayermohol Khulna', '', '', '', 675360, 0, 'client', '', 0, '2015-07-12'),
(96, 'Ms Organix Fish Feed', 'Shimphson Babu', '', 'Moth Botiaghata', '', '', '', 58000, 0, 'client', '', 0, '2015-07-12'),
(97, 'Ms Arafat Enterprise', 'Sheikh mijanur Rahman', '01711280088', 'Vice Chairman Fakirhat', '', '', '', 129500, 0, 'client', '', 0, '2015-07-12'),
(98, 'Shuvash Shomaddar', '', '01622844083', 'Dodanga Arua Masua', '', '', '', 99204, 0, 'client', '', 0, '2015-07-12'),
(99, 'Md Zulfikar Ali', '', '01718849864', 'Sahakari Thana Motsho Kormokorta', '', '', '', 88900, 0, 'client', '', 0, '2015-07-12'),
(100, 'Ms Shefa Enterprise', 'Md Shahjahan Sheikh', '01725714717', 'Fakirhat Bazar', '', '', '', 258850, 0, 'client', '', 0, '2015-07-12'),
(101, 'Ma lokkhi Vandar', 'Boloram Saha', '01712400326', 'Fakirhat Bazar', '', '', '', 440450, 0, 'client', '', 0, '2015-07-12'),
(102, 'Babu Poromando Mondol', '', '01740551031', 'Dhormo mouvo', '', '', '', 133500, 0, 'client', '', 0, '2015-07-12'),
(103, 'Ms Ziaur Krishi ghor', 'Sheikh Ziaur Rahman', '01740546052', 'Mansha Bazar', '', '', '', 640200, 0, 'client', '', 0, '2015-07-12'),
(104, 'Haldar Motsho fish', 'Golok Haldar', '01830676775', 'Dumuria Chitolmari', '', '', '', 520500, 0, 'client', '', 0, '2015-07-12'),
(105, 'Ms Sardar Traders', 'Rahmat Sardar', '01766290364', 'Khiligati Chitolmari', '', '', '', 374800, 0, 'client', '', 0, '2015-07-12'),
(106, 'Md Kabir Hossain(2)', '', '01719306450', 'Kartikdia Bagerhat', '', '', '', 462550, 0, 'client', '', 0, '2015-07-12'),
(107, 'Al-Hajj Mizanur Rahman(khokon)', '', '', 'Baroikhora Bagerhat', '', '', '', 323100, 0, 'client', '', 0, '2015-07-12'),
(108, 'Ms Shahjalal Traders', 'Md joshimuddin Morol', '01721759462', 'Fakirhat Bazar', '', '', '', 132650, 0, 'client', '', 0, '2015-07-12'),
(109, 'Bhai Bhai Enterprise', 'Md Jahirul Islam', '01835132769', '', '', '', '', 600200, 0, 'client', '', 0, '2015-07-12'),
(110, 'Ms Haldar Traders', 'Soroj Haldar', '01766388138', 'Dumuria Chitolmari', '', '', '', 286300, 0, 'client', '', 0, '2015-07-12'),
(111, 'Ms Asif Enterprise', 'Fm Mahfujul Alom', '01720570914', 'Fakirbari Nashuakhali', '', '', '', 335250, 0, 'client', '', 0, '2015-07-12'),
(112, 'Md Nazrul Islam(L)', 'Md Nazrul islam ', '', 'Birashia FakirHat Bagerhat', '', '', '', 0, 0, 'loan_source', '', 0, '2015-07-12'),
(113, 'Md Auib ali(FAFU)', 'Md Auib ali', '', 'Khanpur', '', '', '', 0, 0, 'loan_source', '', 0, '2015-07-12'),
(114, 'Md Aslam khan ', 'Aslam khan', '', 'Arepara', '', '', '', 0, 0, 'loan_source', '', 0, '2015-07-12'),
(115, 'Miss Kakoli Bagum', 'Miss kakoli Bagum', '', '', '', '', '', 0, 0, 'loan_source', '', 0, '2015-07-12'),
(116, 'Md Nur Mohammad', 'Nur Mohammad', '', '', '', '', '', 0, 0, 'loan_source', '', 0, '2015-07-12'),
(117, 'Ms Dyuti 3', '', '', '', '', '', '', 0, 0, 'client', 'This Client was created from the sales page while processing a new sales order', 1, '2015-10-23');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `unit` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `stock` float NOT NULL,
  `min_stock` float NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=48 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `unit`, `price`, `stock`, `min_stock`, `type`, `notes`) VALUES
(1, 'Dry Fish', 'Kg', 38, 304785, 100, 'raw_material', ''),
(2, 'Shurma Gold', 'Bag', 1210, 355, 50, 'finished_good', ''),
(3, 'Meat & Bone', 'Kg', 39, 21809, 500, 'raw_material', ''),
(4, 'soyabean cake', 'kg', 37, 19015, 1000, 'raw_material', ''),
(5, 'wheat', 'kg', 24, 0, 0, 'raw_material', ''),
(6, 'flour', 'kg', 20, 44415, 100000, 'raw_material', ''),
(7, 'vutta', 'kg', 15, 1839, 1000, 'raw_material', ''),
(8, 'Masterd seed oil cake', 'kg', 35, 3935, 2000, 'raw_material', ''),
(9, 'rice polish', 'kg', 18, 2100, 500, 'raw_material', ''),
(10, 'Boil polish', 'kg', 12, 28700, 2000, 'raw_material', ''),
(11, 'Hangor oil', 'kg', 80, 1810, 500, 'raw_material', ''),
(13, 'Bainder', 'kg', 70, 3164, 100, 'raw_material', ''),
(14, 'Ammonex', 'kg', 100, 729, 500, 'raw_material', ''),
(15, 'Borak Supar', 'Bag', 1050, 480, 50, 'finished_good', ''),
(16, 'Shapla Grower', 'Bag', 900, 740, 200, 'finished_good', ''),
(18, 'Stater 1', 'Bag', 1300, 0, 100, 'finished_good', ''),
(20, '100W Balv', 'Pcs', 0, 1, 0, 'utility_item', ''),
(31, 'Biskut', 'kg', 18, 0, 100, 'raw_material', ''),
(32, 'Salt', 'kg', 10, 0, 100, 'raw_material', ''),
(33, 'Calsiem', 'kg', 3, 2500, 100, 'raw_material', ''),
(34, 'Shurma bag', 'Pcs', 20, 8936, 100, 'raw_material', ''),
(35, 'Borak bag', 'Pcs', 20, 8884, 100, 'raw_material', ''),
(36, 'Shapla bag', 'Pcs', 20, 3571, 100, 'raw_material', ''),
(37, 'Tannin bag', 'Pcs', 20, 5000, 100, 'raw_material', ''),
(38, 'Boisakhi bag ', 'Pcs', 20, 3922, 100, 'raw_material', ''),
(39, 'Toshiba', 'Pcs', 20, 80, 100, 'raw_material', ''),
(40, 'Doiel bag', 'Pcs', 20, 254, 100, 'raw_material', ''),
(41, 'Dimond bag', 'Pcs', 20, 1938, 100, 'raw_material', ''),
(42, 'Golden bag', 'Pcs', 20, 387, 100, 'raw_material', ''),
(43, 'Ranue bag', 'Pcs', 20, 640, 100, 'raw_material', ''),
(44, 'Poli bag', 'Pcs', 20, 20741, 100, 'raw_material', ''),
(45, 'Stater 2', 'Bag', 1300, 0, 0, 'finished_good', ''),
(46, 'Narssary', 'Bag', 1300, 0, 0, 'finished_good', ''),
(47, 'Vitamin', 'kg', 125, 729, 100, 'raw_material', '');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `receipt_no` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `total_price` float NOT NULL,
  `discount_percentage` float NOT NULL,
  `discount` float NOT NULL,
  `vat_percentage` float NOT NULL,
  `vat` float NOT NULL,
  `payment_amount` float NOT NULL,
  `due_amount` float NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `receipt_no`, `date`, `time`, `company_id`, `total_price`, `discount_percentage`, `discount`, `vat_percentage`, `vat`, `payment_amount`, `due_amount`, `type`, `notes`) VALUES
(1, '12', '2015-07-11', '23:35:36', 4, 390000, 0, 0, 0, 0, 0, 390000, 'purchase', ''),
(2, '221', '2015-07-11', '12:17:19', 6, 253700, 0, 0, 0, 0, 0, 253700, 'purchase', ''),
(3, '222', '2015-07-11', '12:37:09', 6, 167000, 0, 0, 0, 0, 0, 167000, 'purchase', ''),
(4, '418', '2015-07-11', '12:39:32', 2, 509170, 0, 50917, 0, 22912.7, 50000, 431166, 'purchase', ''),
(5, '4', '2015-07-11', '13:08:39', 20, 144, 0, 0, 0, 0, 0, 144, 'purchase', ''),
(7, '709', '2015-07-11', '21:31:19', 19, 550440, 0, 0, 0, 0, 0, 550440, 'purchase', ''),
(12, 'FFML-S1436645098', '2015-07-11', '22:03:58', 15, 390000, 0, 0, 0, 0, 0, 390000, 'sales', ''),
(13, 'FFML-S1445619641', '2015-10-23', '23:00:02', 117, 11500, 0, 0, 0, 0, 11500, 0, 'sales', ''),
(14, '343224', '2015-12-14', '13:14:55', 2, 60734, 0, 0, 0, 0, 0, 60734, 'purchase', ''),
(15, 'FFML-S1457258196', '2016-03-06', '15:56:15', 52, 18200, 0, 0, 0, 0, 18200, 0, 'sales', ''),
(16, 'FFML-S1457445606', '2016-03-08', '19:59:07', 56, 23450, 0, 2345, 0, 0, 21105, 0, 'sales', ''),
(17, 'FFML-S1457690934', '2016-03-11', '16:08:28', 15, 53750, 0, 0, 0, 0, 0, 53750, 'sales', '');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `rate` float NOT NULL,
  `qty` float NOT NULL,
  `price` float NOT NULL,
  `type` int(11) NOT NULL COMMENT '1 for purchase 0 for sales',
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=38 ;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `item_id`, `rate`, `qty`, `price`, `type`, `date`) VALUES
(2, 1, 3, 26, 15000, 390000, 1, '2015-07-11'),
(5, 3, 10, 10, 16700, 167000, 1, '2015-07-11'),
(10, 2, 9, 17.2, 14750, 253700, 1, '2015-07-11'),
(12, 7, 8, 33, 16680, 550440, 1, '2015-07-11'),
(19, 12, 15, 1050, 200, 210000, 0, '2015-07-11'),
(20, 12, 16, 900, 200, 180000, 0, '2015-07-11'),
(21, 5, 20, 12, 12, 144, 1, '2015-07-11'),
(22, 13, 15, 1050, 6, 6300, 0, '2015-10-23'),
(23, 13, 46, 1300, 4, 5200, 0, '2015-10-23'),
(24, 4, 3, 25, 20350, 508750, 1, '2015-07-11'),
(25, 4, 31, 21, 20, 420, 1, '2015-07-11'),
(26, 14, 14, 100, 600, 60000, 1, '2015-12-14'),
(27, 14, 13, 70, 3, 210, 1, '2015-12-14'),
(28, 14, 10, 12, 37, 444, 1, '2015-12-14'),
(29, 14, 35, 20, 4, 80, 1, '2015-12-14'),
(30, 15, 15, 1050, 6, 6300, 0, '2016-03-06'),
(31, 15, 46, 1300, 5, 6500, 0, '2016-03-06'),
(32, 15, 16, 900, 6, 5400, 0, '2016-03-06'),
(33, 16, 15, 1050, 9, 9450, 0, '2016-03-08'),
(34, 16, 46, 1300, 8, 10400, 0, '2016-03-08'),
(35, 16, 16, 900, 4, 3600, 0, '2016-03-08'),
(36, 17, 15, 1050, 45, 47250, 0, '2016-03-11'),
(37, 17, 46, 1300, 5, 6500, 0, '2016-03-11');

-- --------------------------------------------------------

--
-- Table structure for table `production`
--

CREATE TABLE IF NOT EXISTS `production` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `receipt_no` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` float NOT NULL,
  `date` date NOT NULL,
  `time` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `production`
--

INSERT INTO `production` (`id`, `receipt_no`, `item_id`, `qty`, `date`, `time`, `notes`) VALUES
(1, 'FFML-PF1439370479', 46, 500, '2015-08-12', '15:07:54', '');

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE IF NOT EXISTS `salary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `month` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `year` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `basic_salary` float NOT NULL,
  `daily_salary` float NOT NULL,
  `presence` float NOT NULL,
  `earned_salary` float NOT NULL,
  `hourly_salary` float NOT NULL,
  `overtime_hours` float NOT NULL,
  `overtime_amount` float NOT NULL,
  `hourly_salary_deduction` float NOT NULL,
  `deduction_hours` float NOT NULL,
  `deduction_amount` float NOT NULL,
  `bonus_amount` float NOT NULL,
  `net_payable` float NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `receipt_no` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'salary',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`id`, `staff_id`, `month`, `year`, `basic_salary`, `daily_salary`, `presence`, `earned_salary`, `hourly_salary`, `overtime_hours`, `overtime_amount`, `hourly_salary_deduction`, `deduction_hours`, `deduction_amount`, `bonus_amount`, `net_payable`, `notes`, `date`, `time`, `receipt_no`, `type`) VALUES
(1, 1, 'May', '2015', 30000, 1000, 25, 25000, 125, 30, 3750, 125, 5, 625, 0, 25000, '', '2015-07-13', '12:33:37', 'FFML-MS1436769271', 'salary'),
(2, 1, 'June', '2015', 30000, 1000, 26, 26000, 125, 40, 5000, 125, 0, 0, 0, 25000, '', '2015-07-13', '12:34:42', 'FFML-MS1436769300', 'salary'),
(3, 1, 'January', '2015', 30000, 1000, 26, 26000, 125, 35, 4375, 125, 6, 750, 500, 25000, '', '2015-07-13', '14:02:12', 'FFML-MS1436774573', 'salary'),
(4, 1, 'November', '2015', 30000, 1000, 24, 24000, 125, 20, 2500, 125, 6, 750, 5000, 30750, '', '2015-12-14', '13:12:07', 'FFML-MS1450077186', 'salary');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_no` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `basic_salary` float NOT NULL,
  `daily_salary` float NOT NULL,
  `hourly_salary` float NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `joining_date` date NOT NULL,
  `dob` date NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `present_address` text COLLATE utf8_unicode_ci NOT NULL,
  `permanent_address` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `card_no`, `type`, `name`, `designation`, `basic_salary`, `daily_salary`, `hourly_salary`, `notes`, `joining_date`, `dob`, `phone`, `present_address`, `permanent_address`, `status`) VALUES
(1, '213', 'Staff', 'Sanat Sarker', 'Manager Sales', 30000, 1000, 125, '', '2015-07-01', '2014-07-15', '+8801711181662', 'Flat# 11NB-2, Laboni Complex\r\nLakecity Concord', 'Flat# 11NB-2, Laboni Complex\r\nLakecity Concord', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `usage`
--

CREATE TABLE IF NOT EXISTS `usage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `receipt_no` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `purpose` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `usage`
--

INSERT INTO `usage` (`id`, `receipt_no`, `date`, `time`, `purpose`, `description`, `type`) VALUES
(1, 'FFML-U1436680046', '2015-07-12', '07:35:43', 'To make feed', '', 'raw_material'),
(2, 'FFML-U1436680404', '2015-07-12', '07:51:42', 'To make feed', '', 'raw_material'),
(3, 'FFML-U1436682606', '2015-07-12', '08:29:52', 'use', '', 'utility_item');

-- --------------------------------------------------------

--
-- Table structure for table `usage_items`
--

CREATE TABLE IF NOT EXISTS `usage_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usage_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` float NOT NULL,
  `date` date NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `usage_items`
--

INSERT INTO `usage_items` (`id`, `usage_id`, `item_id`, `qty`, `date`, `type`) VALUES
(1, 1, 1, 5887, '2015-07-12', ''),
(2, 1, 3, 4800, '2015-07-12', ''),
(3, 1, 4, 1175, '2015-07-12', ''),
(4, 1, 6, 5913, '2015-07-12', ''),
(5, 1, 7, 863, '2015-07-12', ''),
(6, 1, 8, 3305, '2015-07-12', ''),
(7, 1, 9, 1425, '2015-07-12', ''),
(8, 1, 10, 1975, '2015-07-12', ''),
(9, 1, 13, 174, '2015-07-12', ''),
(10, 1, 14, 34.75, '2015-07-12', ''),
(11, 1, 33, 987, '2015-07-12', ''),
(12, 2, 47, 43, '2015-07-12', ''),
(13, 3, 20, 5, '2015-07-12', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `token_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL COMMENT '1=super_admin 2 = admin 3 = user',
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `full_name`, `email`, `token`, `token_type`, `role`, `status`) VALUES
(1, 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', '', 'saidurown@gmail.com', '', '', 1, 1),
(2, 'user', '5f4dcc3b5aa765d61d8327deb882cf99', 'Md Kamruzzaman', 'ahmed@ffml.com', '2', 'forgot_password', 3, 1),
(3, 'superadmin', '5f4dcc3b5aa765d61d8327deb882cf99', 'Mr. Super Admin', 'superadmin@ffmlbd.com', '', '', 1, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `FKOrderitem_order` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
