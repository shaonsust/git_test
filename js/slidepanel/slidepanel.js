(function($) {
 "use strict";

$(document).ready(function() {
	$(".topMenuAction").click( function() {
        var closeImageSrc = $(this).attr('closeImageSrc');
        var openImageSrc = $(this).attr('openImageSrc');
		if ($("#openCloseIdentifier").is(":hidden")) {
			$("#slider").animate({ 
				marginTop: "-1000px"
				}, 900 );
			$("#topMenuImage").html('<img src="'+ openImageSrc +'" alt="open" title="open" />');
			$("#openCloseIdentifier").show();
		} else {
			$("#slider").animate({ 
				marginTop: "0px"
				}, 300 );
			$("#topMenuImage").html('<img src="'+ closeImageSrc +'" alt="close" title="close" />');
			$("#openCloseIdentifier").hide();
		}
	});  
});

})(jQuery);
