/**
 * Created by sayed on 1/23/2015.
 */

$( ".datepicker" ).datepicker({
    dateFormat: "yy-mm-dd"
});

function CheckDelete()
{
    var check=confirm('Are you confirm to Delete this Item !');
    if(check)
    {
        return true;
    }
    else
    {
        return false;
    }
}

$('#check_password').click(function(){
    var password=$('.new_pass').val();
    var cpassword=$('.con_pass').val();
    if(password != '' && password == cpassword)
    {
        return true;
    }
    else
    {
        $('.error').html('Password Mismatch');
    }
    return false;
});

