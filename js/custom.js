$(document).ready(function(){
    $( ".datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
    $('.alphaOnly').keypress(function(key) {
        if((key.charCode < 97 || key.charCode > 122) && (key.charCode < 65 || key.charCode > 90) && (key.charCode != 45) && (key.charCode != 32) && (key.charCode != 46)) return false;
    });
    $('.numOnly').keypress(function(key) {
        if((key.charCode < 48 || key.charCode > 57) && (key.charCode != 46)) return false;
    });
    $('.digitOnly').keypress(function(key) {
        if(key.charCode < 48 || key.charCode > 57) return false;
    });
});

